# ClusterAnalysis

## Description
Program developed with the initial idea of distinguishing between molecular clusters of different conformations purely based on the geometry.
This was set up to compare the geometry of structures obtained from different methods of geometry optimization, but can compare any number of geometries obtained in arbitrary ways.

Additional features include: Determination of solvation shells build on a convex hull algorithm.

## Building the program

Building the program should be as easy as:

    mkdir build_dir
    cd build_dir
    cmake ..
    cmake --build . -j<N>

Which will build the program in the current directory using N threads. And now the executable should be located as `build_dir/ClusterAnalysis`.

### Requirements

The program requires at least:
- c++ 20
- CMake 3.16.3

The c++ should be installed by default by most Linux distributions. CMake should simply be a standard `sudo apt install cmake`.
Eigen and LBFGS++ are two headerfile only dependencies which will automatically be pulled from their respective git repositories.


## Installing the program

Installation is as simple as exporting the executable to your PATH, creating an alias or whatever else you might like:

    export PATH=$PATH:/path/to/git-repo-directory


## Running the program
Above cmake command will create an executable in current directory which can be run with a given input file.
One can also run the test suite to make sure everything is in order:

    ClusterAnalysis runtests

"runtests" is case-insensitive.

### Debugging the program
If serious debugging (seg-faults etc.) is needed, consider compiling with debug flag enabled for cmake this will enable AddressSanitizer which will automatically give an output with information about possible memory corruption. This can be done as follows:

    cmake -DCMAKE_BUILD_TYPE=Debug .
    make -j<N>
    
    ClusterAnalysis runtests

After your debugging session is over re-set the compiler flag to Release by:
    
    cmake -DCMAKE_BUILD_TYPE=Release
    make -j<N>


## Usage
Easiest way to learn to setup an input file right now would be to run the tests and look at the input files there. For additional options and keywords the best manual so far is the source code.
