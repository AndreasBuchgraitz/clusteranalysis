/********************************************************************
 * This file contains the definition of a ClusterSet.
 * 
 * This should contain a set of all clusters aswell as "global
 * information" which might be needed in construction of clusters or
 * other stuff. This might fx. be a list of all clusters and 
 * monomers. 
 * If I ever implement a "CalcDef" class this ClusterSet, should
 * propably just be constructed from it.
 ********************************************************************/

#include<tuple>
#include<ranges>
#include<algorithm>
#include<execution>

#include "CommonAliases.hpp"

#include "Input/ReadFiles.hpp"
#include "Structure/StructureIdentification.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "MathAndUtil/Util.hpp"
#include "Structure/ClusterSet.hpp"

namespace Structure
{
   /********************************************************************
    * Read in options under #3 KabschOptions
    ********************************************************************/
   bool ClusterSet::ReadKabschOptions(std::fstream& arInp, std::string& s)
   {
      // Define keyword input level
      int input_level = 4;

      // Enum for checking keywords
      enum INPUT
      {  KABSCHHYDROGENS
      ,  KABSCHRMSD
      };

      // Map for getting keywords from string
      const std::map<std::string, INPUT> input_word = 
         {  {"#4KABSCHHYDROGENS", KABSCHHYDROGENS}
         ,  {"#4KABSCHRMSD", KABSCHRMSD}
         };

      bool already_read = false;

      // input enum
      INPUT input;

      // Loop over input file.
      while (  ( already_read || Util::GetLine(arInp, s) )
            && !Input::IsLowerLevelKeyword(s, input_level)
            )
      {
         std::string s_parsed = Input::ParseInputKeyword(s);
         try
         {
            input = input_word.at(s_parsed);
         }
         catch(const std::out_of_range&)
         {
            std::cout << "In ReadKabschOptions: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
            exit(1);
         }

         switch(input)
         {
            case KABSCHHYDROGENS:      
            {
               Util::GetLine(arInp, s);
               std::string s_parsed = Input::ParseInputKeyword(s); // Remove white-space and convert to upper
               mInputOptions.at("KabschHydrogen") = GetEnumFromString<InputOptions>(s_parsed);
               already_read = false;
               break;
            }
            case KABSCHRMSD:
            {
               Util::GetLine(arInp, s);
               std::string s_parsed = Input::ParseInputKeyword(s); // Remove white-space and convert to upper
               mKabschInputOptions = GetEnumFromString<KabschInputOptions>(s_parsed);
               already_read = false;
               break;
            }
         }
      }

      return Input::CheckIfKeyword(s);
   }

   /********************************************************************
    * Read input for Compareing clusters
    ********************************************************************/
   bool ClusterSet::ReadClusterCompare(std::fstream& arInp, std::string& s)
   {
      // Define keyword input level
      int input_level = 3;

      // Enum for checking keywords
      enum INPUT
      {  COMPUTATIONALDATA
      ,  KABSCHOPTIONS
      };

      // Map for getting keywords from string
      const std::map<std::string, INPUT> input_word = 
         {  {"#3COMPUTATIONALDATA", COMPUTATIONALDATA}
         ,  {"#3KABSCHOPTIONS", KABSCHOPTIONS}
         };

      bool already_read = false;

      // input enum
      INPUT input;

      // Loop over input file.
      while (  ( already_read || Util::GetLine(arInp, s) )
            && !Input::IsLowerLevelKeyword(s, input_level)
            )
      {
         std::string s_parsed = Input::ParseInputKeyword(s);
         try
         {
            input = input_word.at(s_parsed);
         }
         catch(const std::out_of_range&)
         {
            std::cout << "In ReadComputationalMethods: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
            exit(1);
         }

         switch(input)
         {
            case COMPUTATIONALDATA:      
            {
               auto comp_data = ReadComputationalData(arInp, s);
               already_read = comp_data.first;
               mCompareClusterInput.emplace_back(comp_data.second);
               break;
            }
            case KABSCHOPTIONS:
            {
               already_read = ReadKabschOptions(arInp, s);
               break;
            }
         }
      }

      return Input::CheckIfKeyword(s);
   }

   /********************************************************************
    * Read input for filtering unique clusters.
    ********************************************************************/
   bool ClusterSet::ReadFilterUniqueClusters(std::fstream& arInp, std::string& s)
   {
      // Define keyword input level
      int input_level = 3;

      // Enum for checking keywords
      enum INPUT
      {  COMPUTATIONALDATA
      };

      // Map for getting keywords from string
      const std::map<std::string, INPUT> input_word = 
         {  {"#3COMPUTATIONALDATA", COMPUTATIONALDATA}
         };

      bool already_read = false;

      // input enum
      INPUT input;

      // Loop over input file.
      while (  ( already_read || Util::GetLine(arInp, s) )
            && !Input::IsLowerLevelKeyword(s, input_level)
            )
      {
         std::string s_parsed = Input::ParseInputKeyword(s);
         try
         {
            input = input_word.at(s_parsed);
         }
         catch(const std::out_of_range&)
         {
            std::cout << "In ReadComputationalMethods: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
            exit(1);
         }

         switch(input)
         {
            case COMPUTATIONALDATA:      
            {
               auto comp_data = ReadComputationalData(arInp, s);
               already_read = comp_data.first;
               mCompareClusterInput.emplace_back(comp_data.second);
               break;
            }
         }
      }

      return Input::CheckIfKeyword(s);
   }

   /********************************************************************
    * Read input for checking if a cluster has solvated monomers.
    ********************************************************************/
   bool ClusterSet::ReadCheckSolvation(std::fstream& arInp, std::string& s)
   {
      // Define keyword input level
      int input_level = 3;

      // Enum for checking keywords
      enum INPUT
      {  COMPUTATIONALDATA
      };

      // Map for getting keywords from string
      const std::map<std::string, INPUT> input_word = 
         {  {"#3COMPUTATIONALDATA", COMPUTATIONALDATA}
         };

      bool already_read = false;

      // input enum
      INPUT input;

      // Loop over input file.
      while (  ( already_read || Util::GetLine(arInp, s) )
            && !Input::IsLowerLevelKeyword(s, input_level)
            )
      {
         std::string s_parsed = Input::ParseInputKeyword(s);
         try
         {
            input = input_word.at(s_parsed);
         }
         catch(const std::out_of_range&)
         {
            std::cout << "In ReadCheckSolvation: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
            exit(1);
         }

         switch(input)
         {
            case COMPUTATIONALDATA:      
            {
               auto comp_data = ReadComputationalData(arInp, s);
               already_read = comp_data.first;
               mCompareClusterInput.emplace_back(comp_data.second);
               break;
            }
         }
      }

      return Input::CheckIfKeyword(s);
   }

   /********************************************************************
    * Read ComputationalData
    ********************************************************************/
   std::pair<bool, std::tuple<std::filesystem::path, std::string, std::string, bool>>
   ClusterSet::ReadComputationalData(std::fstream& arInp, std::string& s)
   {
      // Define keyword input level
      int input_level = 4;

      // Enum for checking keywords
      enum INPUT
      {  PATHTODATA
      ,  COMPUTATIONALMETHODS
      ,  CLUSTERNAMEINFILENAME
      ,  CLUSTERNAMEINCOMMENT
      ,  STRINGTOIGNOREINFILENAME
      };

      // Map for getting keywords from string
      const std::map<std::string, INPUT> input_word = 
         {  {"#4PATHTODATA", PATHTODATA}
         ,  {"#4COMPUTATIONALMETHODS", COMPUTATIONALMETHODS}
         ,  {"#4CLUSTERNAMEINFILENAME", CLUSTERNAMEINFILENAME}
         ,  {"#4CLUSTERNAMEINCOMMENT", CLUSTERNAMEINCOMMENT}
         ,  {"#4STRINGTOIGNOREINFILENAME", STRINGTOIGNOREINFILENAME}
         };

      bool already_read = false;

      // input enum
      INPUT input;

      std::tuple<std::filesystem::path, std::string, std::string, bool> input_tup = std::make_tuple(std::filesystem::path(), "", "", false);

      // Loop over input file.
      while (  ( already_read || Util::GetLine(arInp, s) )
            && !Input::IsLowerLevelKeyword(s, input_level)
            )
      {
         std::string s_parsed = Input::ParseInputKeyword(s);
         try
         {
            input = input_word.at(s_parsed);
         }
         catch(const std::out_of_range&)
         {
            std::cout << "In ReadComputationalMethods: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
            exit(1);
         }

         switch(input)
         {
            // Can be both directory of many files or single xyz file
            case PATHTODATA:      
            {
               // Get the /path/to/data
               Util::GetLine(arInp, s);
               std::get<0>(input_tup) = std::filesystem::path(s);
               already_read = false;
               break;
            }
            case COMPUTATIONALMETHODS:
            {
               already_read = ReadComputationalMethod(arInp, s, input_tup);
               break;
            }
            case CLUSTERNAMEINFILENAME:
            {
               mClusterNameInFilename = true;
               already_read = false;
               break;
            }
            case CLUSTERNAMEINCOMMENT:
            {
               mClusterNameInComment = true;
               already_read = false;
               break;
            }
            case STRINGTOIGNOREINFILENAME:
            {
               Util::GetLine(arInp, s);
               mStringToIgnoreInFilename = s;
               already_read = false;
               break;
            }
         }
      }

      if (mClusterNameInFilename && mClusterNameInComment)
      {
         std::cerr << "Please only have me read the ClusterName from either the filename OR the XYZ-file comment" << std::endl;
         exit(1);
      }
      else if (!mClusterNameInFilename && !mClusterNameInComment)
      {
         std::cerr << "You have not told me where to read the ClusterName from !\n"
            << "Please use either #4ClusterNameInFilename or #4 ClusterNameInComment"
            << std::endl;
         exit(1);
      }

      return std::make_pair(Input::CheckIfKeyword(s), input_tup);
   }

   /********************************************************************
    * Read ComputationalData
    ********************************************************************/
   bool ClusterSet::ReadComputationalMethod
      (  std::fstream& arInp
      ,  std::string& s
      ,  std::tuple<std::filesystem::path, std::string, std::string, bool>& arTup
      )
   {
      // Define keyword input level
      int input_level = 5;

      // Enum for checking keywords
      enum INPUT
      {  COMPMETH
      ,  BASISSET
      ,  REFERENCE
      };

      // Map for getting keywords from string
      const std::map<std::string, INPUT> input_word = 
         {  {"#5COMPMETH", COMPMETH}
         ,  {"#5BASISSET", BASISSET}
         ,  {"#5REFERENCE", REFERENCE}
         };

      bool already_read = false;

      // input enum
      INPUT input;

      // Loop over input file.
      while (  ( already_read || Util::GetLine(arInp, s) )
            && !Input::IsLowerLevelKeyword(s, input_level)
            )
      {
         std::string s_parsed = Input::ParseInputKeyword(s);
         try
         {
            input = input_word.at(s_parsed);
         }
         catch(const std::out_of_range&)
         {
            std::cout << "In ReadComputationalMethods: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
            exit(1);
         }

         switch(input)
         {
            case COMPMETH:      
            {
               // Get the /path/to/data
               Util::GetLine(arInp, s);
               std::get<1>(arTup) = s;
               break;
            }
            case BASISSET:
            {
               // Get the /path/to/data
               Util::GetLine(arInp, s);
               std::get<2>(arTup) = s;
               break;
            }
            case REFERENCE:
            {
               std::get<3>(arTup) = true;
               break;
            }
         }
      }

      return Input::CheckIfKeyword(s);
   }

   /********************************************************************
    * Initialize all the structures.
    ********************************************************************/
   void ClusterSet::InitializeClusterCompareStructures(const bool aUseClusterIdx)
   {
      for (const auto& tup : mCompareClusterInput)
      {
         // Get path in tuple
         std::filesystem::path xyz_path = std::get<0>(tup);

         // Check if file or directory
         if (std::filesystem::is_regular_file(xyz_path))
         {
            SaveCluster
               (  xyz_path
               ,  std::get<1>(tup)
               ,  std::get<2>(tup)
               ,  std::get<3>(tup)
               ,  aUseClusterIdx
               );
         }
         else if (std::filesystem::is_directory(xyz_path))
         {
            for (const auto& dir_entry : std::filesystem::directory_iterator(xyz_path))
            {
               SaveCluster
                  (  dir_entry.path()
                  ,  std::get<1>(tup)
                  ,  std::get<2>(tup)
                  ,  std::get<3>(tup)
                  ,  aUseClusterIdx
                  );
            }
         }
         else
         {
            std::cerr << "Bad path given: " << xyz_path.string() << "\n"
               << "Not a regular file or directory !"
               << std::endl;
            exit(1);
         }
      }
   }

   /********************************************************************
    * Saves 1 cluster to mReferenceClusters or mCompareClusters, by
    * reading and constructing Cluster from arPath.
    * 
    * aUseClusterIdx is used as a check of wether the clusters should
    * be saved all in the same container or for each conformer. So it is
    * only used for a check at the end where we emplace_back into the
    * Cluster vector.
    ********************************************************************/
   void ClusterSet::SaveCluster
      (  const std::filesystem::path& arPath
      ,  const std::string& arCompMethod
      ,  const std::string& arBasisSet
      ,  const bool aIsReference  
      ,  const bool aUseClusterIdx
      )
   {
      if (!std::filesystem::is_regular_file(arPath))
      {
         std::cerr << "SaveCluster has recieved a path which is not a regular file!!\n"
            << arPath.string()
            << std::endl;
      }
      // Get the filename. Fx. /path/to/file.ext => file
      std::string filename = arPath.stem();

      std::vector<std::string> split_filename = Util::VectorFromString<std::string>(filename, "_");

      std::string cluster_name_idx = ReadClusterName(arPath, true);
      std::string cluster_name_no_idx = ReadClusterName(arPath, false);

      // Create id
      StructureIdentification id
         (  cluster_name_idx
         ,  arCompMethod
         ,  arBasisSet
         ,  aIsReference
         );

      //
      Cluster cluster
         (  arPath
         ,  id
         );

      // try_emplace will only emplace stuff if the key is not already there.
      if (aIsReference)
      {
         cluster.SetConformerEnum(ConformerEnum::REFERENCE);
         auto [iter, inserted] = mReferenceClusters.emplace(cluster_name_idx, cluster);
         if (!inserted)
         {
            std::cerr << "I have already put in a reference cluster of this name !!\n"
               << "cluster_name: " << cluster_name_idx
               << std::endl;
            exit(1);
         }
      }
      else
      {
         std::string cluster_name = aUseClusterIdx ? cluster_name_idx : cluster_name_no_idx;
         mCompareClusters.try_emplace(cluster_name, std::vector<Cluster>());
         mCompareClusters.at(cluster_name).emplace_back(cluster);
      }
   }

   /********************************************************************
    * Loops over all files in mFilePaths.GetDataDir() and constructs and
    * saves a Cluster for them.
    ********************************************************************/
   void ClusterSet::InitializeCoordinateScan()
   {
      if (mCompareClusterInput.size() != 1)
      {
         std::cerr << "For CoordinateScan1D I only expect one directory or file" << std::endl;
         exit(1);
      }

      // To create all the Cluster objects we do as for ClusterCompareStructures()
      InitializeClusterCompareStructures(true);
   }

   /********************************************************************
    * Will read the cluster name from xyz file (either comment or filename)
    ********************************************************************/
   std::string ClusterSet::ReadClusterName
      (  const std::filesystem::path& arFilePath
      ,  const bool aUseClusterIdx
      )  const
   {
      if (!std::filesystem::is_regular_file(arFilePath))
      {
         std::cerr << "arFilePath in ReadClusterName() is not a regular file !!\n"
            << "arFilePath: " << arFilePath.string()
            << std::endl;
         exit(1);
      }

      if (mClusterNameInFilename)
      {
         // .stem() removes extension (fx .xyz)
         return InterpretStringAsClusterName(arFilePath.stem(), aUseClusterIdx);
      }
      else if (mClusterNameInComment)
      {
         // Initialize objects for reading inputfile
         std::fstream inputfile(arFilePath);
         std::string s;

         // First line in xyz file is number of atoms
         Util::GetLine(inputfile, s);

         // Second line in xyz file is random comment. We throw it away.
         Util::GetLine(inputfile, s);

         return InterpretStringAsClusterName(s, aUseClusterIdx);
      }
      else
      {
         //ERROR
         std::cerr << "Somethings rotten in ReadClusterName()" << std::endl;
         exit(1);
         return std::string();
      }
   }

   /********************************************************************
    * Checks that the given string has the expected syntax for a 
    * ClusterName.
    * 
    * Expected syntax is as follows: N<monomer_name>M<monomer_name>..._X
    * i.e. batches of a number followed by a name for the monomer
    * stringed together, with a "_X" at the end where X is a number, to 
    * distinguish between clusters of different conformers.
    * If there are non-alphabetical characters (fx. "_" anywhere before
    * the "_X" these are ignored).
    ********************************************************************/
   std::string ClusterSet::InterpretStringAsClusterName
      (  std::string arString
      ,  const bool aUseClusterIdx
      )  const
   {
      // Start by removing the substring we want to ignore.
      auto start_idx = arString.find(mStringToIgnoreInFilename);
      if (start_idx != std::string::npos)
      {
         arString.erase(start_idx, mStringToIgnoreInFilename.size());
      }

      // String to store temporary substrings.
      std::string current_monomer = "";

      // Index of last underscore so we can ignore every other underscore without losing the cluster index information.
      Uin last_underscore = arString.find_last_of('_');

      std::string cluster_index = "";

      // If there is possibility of cluster idx search for it and save it if it exists
      if (last_underscore < arString.size())
      {
         current_monomer = "";
         for (Uin i = last_underscore + 1; i < arString.size(); i++)
         {
            if (!std::isdigit(arString[i]))
            {
               current_monomer = "";
               break;
            }
            else
            {
               current_monomer.push_back(arString[i]);
            }
         }

         // Check if the last string can be interpreted as a number i.e. is it acutally the cluster number.
         if (current_monomer != "")
         {
            try
            {
               [[maybe_unused]] Uin cluster_idx = std::stoul(current_monomer);
               cluster_index = "_" + current_monomer;
            }
            catch(const std::invalid_argument& e)
            {
               std::cerr << "Caught std::invalid_argument in std::stoul (InterpretStringAsClusterName) with input: " << current_monomer << std::endl;
               exit(1);
            }
            catch(const std::out_of_range& e)
            {
            }
         }
      }

      // Empty this before going ahead
      current_monomer = "";

      // Will keep all "N<monomer_name>" and sorts them such that clusters of same monomers will be recognized as the same cluster.
      std::set<std::string> monomers;

      Uin end_of_monomers  =  cluster_index.empty() 
                           ?  arString.size()
                           :  last_underscore;

      for (Uin i = 0; i < end_of_monomers; i++)
      {
         if (std::isalpha(arString[i]) || Util::IsNumber(current_monomer))
         {
            current_monomer.push_back(arString[i]);
         }
         else if (std::isdigit(arString[i]))
         {
            // New monomer so save old and prepare new
            if (!current_monomer.empty())
            {
               monomers.emplace(current_monomer);
            }
            current_monomer = arString[i];
         }
         else
         {
            // We ignore this non-alphanumerical character.
         }
      }

      // We need to also save the last monomer found.
      monomers.emplace(current_monomer);

      // Now construct the cluster name
      std::string cluster_name = "";
      for (const std::string& monomer : monomers)
      {
         cluster_name += monomer;
      }

      if (!cluster_index.empty() && aUseClusterIdx)
      {
         cluster_name += cluster_index;
      }

      return cluster_name;
   }

   /********************************************************************
    * Will compute the RMSD for all the clusters and return as map with
    * key -> value as ClusterName -> RmsdData object.
    ********************************************************************/
   std::map<std::string, Kabsch::RmsdData> ClusterSet::CalculateConformationRMSD
      (
      )  const
   {
      // Initialize outout map
      std::map<std::string, Kabsch::RmsdData> res_map;

      // Loop over clusters
      for (const auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         // Small sanity check
         if (cluster_vec.empty())
         {
            std::cerr << "There is no cluster here to compare to the reference structure !\n"
               << "Error occured in CalculateConformationRMSD, for " << cluster_name
               << std::endl;
            exit(1);
         }

         res_map.emplace
            (  cluster_name
            ,  Kabsch::SimpleClusterRMSD
                  (  mReferenceClusters.at(cluster_name)
                  ,  cluster_vec
                  )
            );
      }

      // Return the result map
      return res_map;
   }

   /********************************************************************
    * Will compute the maximum angle deviation.
    * key -> value as ClusterName -> RmsdData object.
    ********************************************************************/
   std::map<std::string, Kabsch::RmsdData> ClusterSet::CalculateConformationAngleDeviation
      (
      )  const
   {
      // Initialize outout map
      std::map<std::string, Kabsch::RmsdData> res_map;

      // Loop over clusters
      for (const auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         // Small sanity check
         if (cluster_vec.empty())
         {
            std::cerr << "There is no cluster here to compare to the reference structure !\n"
               << "Error occured in CalculateConformationRMSD, for " << cluster_name
               << std::endl;
            exit(1);
         }

         // Initialize RmsdData object
         const Cluster& ref_cluster = mReferenceClusters.at(cluster_name);

         Kabsch::RmsdData angle_deviation(ref_cluster.GetId(), ref_cluster.GetClusterMolecule());

         for (const Structure::Cluster& cluster : cluster_vec)
         {
            const auto angle_vec = Kabsch::ComputeDipoleMomentRMSD(ref_cluster, cluster);
            if (angle_vec.empty())
            {
               std::cerr << "Vector of angles is for some reason empty !!!" << std::endl;
               exit(1);
            }

            angle_deviation.AddComparedMethodAndRMSD
               (  cluster.GetId()
               ,  cluster.GetConformerEnum()
               ,  std::vector<double>{*std::min_element(angle_vec.begin(), angle_vec.end())}
               );
         }

         res_map.emplace
            (  cluster_name
            ,  angle_deviation
            );
      }

      // Return the result map
      return res_map;
   }

   /********************************************************************
    * Will compute the maximum single coordiante deviation.
    * key -> value as ClusterName -> RmsdData object.
    ********************************************************************/
   std::map<std::string, Kabsch::RmsdData> ClusterSet::CalculateConformationCoordinateDeviation
      (
      )  const
   {
      // Initialize outout map
      std::map<std::string, Kabsch::RmsdData> res_map;

      // Loop over clusters
      for (const auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         // Small sanity check
         if (cluster_vec.empty())
         {
            std::cerr << "There is no cluster here to compare to the reference structure !\n"
               << "Error occured in CalculateConformationRMSD, for " << cluster_name
               << std::endl;
            exit(1);
         }

         // Initialize RmsdData object
         const Cluster& ref_cluster = mReferenceClusters.at(cluster_name);

         Kabsch::RmsdData coordinate_deviation(ref_cluster.GetId(), ref_cluster.GetClusterMolecule());

         for (const Structure::Cluster& cluster : cluster_vec)
         {
            const auto coordinate_rmsd_vec = Kabsch::ComputeRotMatRmsdAcross(ref_cluster, cluster);
            if (coordinate_rmsd_vec.empty())
            {
               std::cerr << "Vector of coordinate rmsd is for some reason empty !!!" << std::endl;
               exit(1);
            }

            coordinate_deviation.AddComparedMethodAndRMSD
               (  cluster.GetId()
               ,  cluster.GetConformerEnum()
               ,  std::vector<double>{*std::max_element(coordinate_rmsd_vec.begin(), coordinate_rmsd_vec.end())}
               );
         }

         res_map.emplace
            (  cluster_name
            ,  coordinate_deviation
            );
      }

      // Return the result map
      return res_map;
   }

   /********************************************************************
    * Loops through all Clusters and computes the monomer rotation
    * matrix compared to the reference monomer and saves it.
    * If any cluster have monomers with a different number of atoms
    * compared to the reference cluster, that is there is a proton
    * transfer, we set the rotation matrix as the rotation matrix of
    * the entire clusters.
    * 
    * This will also compute the cluster rotation matrix for all
    * clusters, to use in SimpleClusterRMSD.
    ********************************************************************/
   void ClusterSet::ScreenForProtonTransfer
      (
      )
   {
      for (auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         const auto& ref_cluster = mReferenceClusters.at(cluster_name);

         for (Cluster& cluster : cluster_vec)
         {
            // Set the rotation matrix for the entire cluster
            const auto& ref_cluster_mat = ref_cluster.GetClusterMoleculeMatrix();
            const auto& com_cluster_mat = cluster.GetClusterMoleculeMatrix();
            cluster.SetClusterMoleculeRotationMatrix(Kabsch::Kabsch(com_cluster_mat, ref_cluster_mat));

            auto it_ref = ref_cluster.GetMonomersIterConstBegin();
            auto it_compare = cluster.GetMonomersIterBegin();
            for(
               ;  it_ref != ref_cluster.GetMonomersIterConstEnd() && it_compare != cluster.GetMonomersIterEnd()
               ;  it_ref++, it_compare++
               )
            {
               const auto matrix_pair = Kabsch::GetOverlappingMoleculePositions(*it_compare, *it_ref);
               it_compare->SetMoleculeRotationMatrix(Kabsch::Kabsch(matrix_pair.first, matrix_pair.second));
            }
         }
      }
   }

   /********************************************************************
    * Will compute the RMSD for all monomers (rot mat probably) and
    * determine if a given cluster is the same conformer as the
    * referencecluster. 
    * 
    * In the future, maybe it will be nice to "re-evaluate" the clusters
    * screened by the "ScreenForProtonTransfer", that is, if the only
    * difference is that a proton has jumped slightly closer to anoter
    * monomer compared to the reference cluster, then the total RMSD
    * might be super small, but this is a question of how to define
    * whether two clusters are the same conformer.
    * To allow for this ScreenForProtonTransfer should propaly return
    * some information to use here. Something like which monomers are
    * involved in the proton transfer, so this method can fuse the two
    * monomers and treat them as one monomer, to overcome the problem
    * with different number of atoms in monomers in the Kabsch algorithm.
    ********************************************************************/
   void ClusterSet::ScreenForDifferingMonomers
      (
      )
   {
      // Loop over clusters.
      for (auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         const auto& ref_cluster = mReferenceClusters.at(cluster_name);

         // Compute RMSD based on atomic positions
         Kabsch::RmsdData rmsd_data = Kabsch::ClusterRMSD(ref_cluster, cluster_vec, Structure::InputOptions::ALL_ALL);
         std::map<StructureIdentification, ConformerEnum> conformer_info = rmsd_data.CheckConformersByAtomicPositions
            (  ref_cluster.GetNumMonomers()
            ,  ref_cluster.GetSizeSmallestMonomer()
            ,  ref_cluster.GetSmallestBondLength()
            );

         // Now also make an RmsdData based on DipoleMoment
         Kabsch::RmsdData rmsd_data_dipolemoment = Kabsch::ClusterDipoleMomentRMSD(ref_cluster, cluster_vec);

         std::map<StructureIdentification, ConformerEnum> dipole_conformer_info = rmsd_data_dipolemoment.CheckConformersByDipoleMoment(ref_cluster.GetNumMonomers());

         for (const auto& [id, conf_enum] : conformer_info)
         {
            auto it_cluster = std::find_if
                              (  cluster_vec.begin()
                              ,  cluster_vec.end()
                              ,  [&id] (const Cluster& arCluster) { return arCluster.GetId() == id; }
                              );

            // Get the conf_enum for dipole_moment
            ConformerEnum dip_mom_conf_enum = dipole_conformer_info.at(id);

            if (dip_mom_conf_enum == ConformerEnum::DefinitelyTheSameAsReference)
            {
               it_cluster->SetConformerEnum(ConformerEnum::SameAsReference);
            }
            else if  (  conf_enum == ConformerEnum::SameAsReference
                     && dip_mom_conf_enum == ConformerEnum::SameAsReference
                     )
            {
               it_cluster->SetConformerEnum(ConformerEnum::SameAsReference);
            }
            else
            {
               it_cluster->SetConformerEnum(ConformerEnum::DifferentFromReference);
            }
         }
      }
   }

   /********************************************************************
    * Will check if conformations which should be the same are actually
    * the same. That is we check if fx 1a1dma_02 optimized by 
    * CCSD/aug-cc-pVTZ and RI-MP2/aug-cc-pVTZ, end up in the same
    * minima i.e. if they are the same conformer.
    ********************************************************************/
   void ClusterSet::CheckConformersAcrossMethods(const std::filesystem::path& arWorkingDir)
   {
      // Screening nr. 1 (Check if any proton transfer occured)
      this->ScreenForProtonTransfer();

      // Screening nr. 2 (Check for each monomer whether conformer is different or not)
      this->ScreenForDifferingMonomers();

      // Compute RMSD
      std::map<std::string, Kabsch::RmsdData> cluster_rmsd = CalculateConformationRMSD();

      // Compute the maximum deviation angle
      std::map<std::string, Kabsch::RmsdData> angle_deviation = CalculateConformationAngleDeviation();

      // Compute the maximum deviation coordinate/atomic position distance.
      std::map<std::string, Kabsch::RmsdData> coordinate_deviation = CalculateConformationCoordinateDeviation();

      // Small sanity check
      if (cluster_rmsd.empty())
      {
         std::cerr << "For some reason cluster_rmsd is empty !" << std::endl;
         exit(1);
      }

      // Print RMSD values (one file for conformers different from the reference and one for the conformers
      // which are the same as the reference).
      ConvertSimpleRmsdDataToTableAndPrint(cluster_rmsd, arWorkingDir, "RmsdData");
      
      // Print out the angle deviations
      ConvertSimpleRmsdDataToTableAndPrint(angle_deviation, arWorkingDir, "AngleDeviation");

      // Print out coordinate deviation
      ConvertSimpleRmsdDataToTableAndPrint(coordinate_deviation, arWorkingDir, "CoordinateDeviation");
   }

   /********************************************************************
    * Loops over all pairs of clusters to compare...
    ********************************************************************/
   void ClusterSet::FilterUniqueClusters(const std::filesystem::path& arWorkingDir)
   {
      for (auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         // We have two vectors (sorted in same manner!!!) instead of map, as only vectors work with parallel std::transform for some weird reason in this case.
         std::vector<std::pair<const Cluster&, const Cluster&>> cluster_pairs;
         std::vector<ConformerEnum> unique_filter_map;
         // Fill the map with keys
         for (auto it_ref = cluster_vec.begin(); it_ref != cluster_vec.cend(); it_ref++)
         {
            for (auto it_compare = std::next(it_ref); it_compare != cluster_vec.cend(); it_compare++)
            {
               unique_filter_map.emplace_back(ConformerEnum::UNINITIALIZED);

               cluster_pairs.emplace_back(*it_ref, *it_compare);
            }
         }

         std::transform
            (  std::execution::par
            ,  cluster_pairs.begin()
            ,  cluster_pairs.end()
            ,  unique_filter_map.begin()
            ,  [](const std::pair<const Cluster&, const Cluster&>& arClusterPair)
               {
                  const Cluster& ref_cluster = arClusterPair.first;
                  const Cluster& com_cluster = arClusterPair.second;
                  auto it_ref_mol = ref_cluster.GetMonomersIterConstBegin();
                  const auto it_ref_mol_end = ref_cluster.GetMonomersIterConstEnd();
                  auto it_com_mol = com_cluster.GetMonomersIterConstBegin();
                  const auto it_com_mol_end = com_cluster.GetMonomersIterConstEnd();

                  // Set the molecule/monomer rotation matrices
                  std::vector<Eigen::Matrix3d> rotation_matrices;
                  rotation_matrices.reserve(std::distance(it_com_mol, it_com_mol_end));
                  for(
                     ;  it_ref_mol != it_ref_mol_end && it_com_mol != it_com_mol_end
                     ;  it_ref_mol++, it_com_mol++
                     )
                  {
                     const auto overlap_matrix = Kabsch::GetOverlappingMoleculePositions(*it_com_mol, *it_ref_mol);
                     rotation_matrices.emplace_back(Kabsch::Kabsch(overlap_matrix.first, overlap_matrix.second));
                  }

                  // Actually compare the monomers

                  // Compute RMSD based on atomic positions
                  Kabsch::RmsdData rmsd_data = Kabsch::ClusterRMSD(ref_cluster, std::vector<Cluster>{com_cluster}, Structure::InputOptions::ALL_ALL, rotation_matrices);
                  std::map<StructureIdentification, ConformerEnum> conformer_info = rmsd_data.CheckConformersByAtomicPositions
                     (  ref_cluster.GetNumMonomers()
                     ,  ref_cluster.GetSizeSmallestMonomer()
                     ,  ref_cluster.GetSmallestBondLength()
                     );

                  // Now also make an RmsdData based on DipoleMoment
                  Kabsch::RmsdData rmsd_data_dipolemoment = Kabsch::ClusterDipoleMomentRMSD(ref_cluster, std::vector<Cluster>{com_cluster}, rotation_matrices);

                  std::map<StructureIdentification, ConformerEnum> dipole_conformer_info = rmsd_data_dipolemoment.CheckConformersByDipoleMoment(ref_cluster.GetNumMonomers());

                  if (dipole_conformer_info.begin()->second == ConformerEnum::DefinitelyTheSameAsReference)
                  {
                     return ConformerEnum::SameAsReference;
                  }
                  else if  (  conformer_info.begin()->second == ConformerEnum::SameAsReference
                           && dipole_conformer_info.begin()->second == ConformerEnum::SameAsReference
                           )
                  {
                     return ConformerEnum::SameAsReference;
                  }
                  else
                  {
                     return ConformerEnum::DifferentFromReference;
                  }
               }
            );

         // Write the data to an file
         WriteUniqueClustersToFile(cluster_name, cluster_pairs, unique_filter_map, arWorkingDir);
      }
   }

   /********************************************************************
    * The main computation of the program.
    ********************************************************************/
   void ClusterSet::WriteUniqueClustersToFile
      (  const std::string& arClusterType
      ,  const std::vector<std::pair<const Cluster&, const Cluster&>>& arClusterPairs
      ,  const std::vector<ConformerEnum>& arConformerEnums
      ,  const std::filesystem::path& arWorkingDir
      )  const
   {
      if (arClusterPairs.size() != arConformerEnums.size())
      {
         std::cerr << "Mismatch in sizes of vectors in WriteUniqueClustersToFile\n"
            << "arClusterPairs.size()   = " << arClusterPairs.size() << "\n"
            << "arConformerEnums.size() = " << arConformerEnums.size() << "\n"
            << std::endl;
         exit(1);
      }
      std::vector<std::string> header{"ClusterPairs", "ConformationEnum"};

      // Two columns
      std::vector<std::vector<std::string>> table_data(arClusterPairs.size());
      for (Uin i = 0; i < arClusterPairs.size(); i++)
      {
         table_data[i].emplace_back(arClusterPairs[i].first.GetName() + "__" + arClusterPairs[i].second.GetName());
         table_data[i].emplace_back(GetStringFromEnum(arConformerEnums[i]));
      }

      std::filesystem::path data_dir = arWorkingDir / "UniqueClusterSearch";
      std::filesystem::create_directories(data_dir);

      std::filesystem::path out_file = data_dir / std::string(arClusterType + ".dat");

      Util::PrintTable(header, table_data, out_file);
   }

   /********************************************************************
    * Main func for computing solvation for clusters
    * 
    * Updates all the Clusters in-place.
    ********************************************************************/
   void ClusterSet::CheckSolvation(const std::filesystem::path& arWorkingDir)
   {
      std::for_each
         (  mCompareClusters.begin()
         ,  mCompareClusters.end()
         ,  [&arWorkingDir](std::pair<const std::string, std::vector<Cluster>>& arClusters)
            {
               std::cout << "Solvation shells for " << arClusters.first << std::endl;
               for (Cluster& cluster : arClusters.second)
               {
                  cluster.ComputeSolvationShells();

                  std::cout << "Cluster - Name: " << cluster.GetName() << std::endl;
                  std::cout << "Solvation Shells:" << std::endl;
                  std::cout << cluster.GetSolvationShells() << std::endl;

                  cluster.PrintSolvationShellToFile(arWorkingDir);
               }
            }
         );

      
      // Setup file for plotting step function for solvation shells.
      PrintClusterSolvationShellsForPlot(mCompareClusters, arWorkingDir);
   }

   /********************************************************************
    * Prints the number of solvation shells for a series of clusters
    * Assumes the arClusterVec only contains the proper series fx. all
    * lowest energy clusters of XsaYam_N (2sa2am_0, 3sa3am_0, ...)
    ********************************************************************/
   void ClusterSet::PrintClusterSolvationShellsForPlot
      (  const std::map<std::string, std::vector<Cluster>>& arClusters
      ,  const std::filesystem::path& arWorkingDir
      )  const
   {
      // Find all clusters of same type, i.e. all XsaYam_N.
      std::map<std::string, std::vector<std::string>> cluster_types;
      for(  auto it_names = std::views::keys(arClusters).begin()
         ;  it_names != std::views::keys(arClusters).end()
         ;  it_names++
         )
      {
         std::string cluster_type = Util::RemoveNonAlpha(*it_names);
         // If we cannot emplace it try_emplace.second returns false, as the key is already present, and then we simply append to the already existing vector.
         if (!cluster_types.try_emplace(cluster_type, std::vector<std::string>{*it_names}).second)
         {
            cluster_types.at(cluster_type).emplace_back(*it_names);
         }
      }

      std::vector<std::string> headers{"NumberMonomers", "NumSolvationShells"};

      for (const auto& [cluster_type, cluster_names_vec] : cluster_types)
      {
         const std::filesystem::path out_file = arWorkingDir / std::string("NumSolvationShells" + cluster_type + ".dat");

         std::vector<std::vector<std::size_t>> table;

         for (const std::string& cluster_name : cluster_names_vec)
         {
            for (const Cluster& cluster : arClusters.at(cluster_name))
            {
               table.emplace_back
                  (  std::vector<std::size_t>
                        {  cluster.GetNumMonomers()
                        ,  cluster.GetNumSolvationShells()
                        }
                  );
            }
         }

         // Sort table such that the file has ascending "x-values", that is ascending number of monomers.
         std::sort
            (  table.begin()
            ,  table.end()
            );

         Util::PrintTable(headers, table, out_file);
      }
   }

   /********************************************************************
    * The main computation of the program.
    ********************************************************************/
   void ClusterSet::RunCoordinateScan1D() const
   {
      std::cerr << "ABJ: RunCoordianteScan1D might be a bit broken, only looping over non-reference" << std::endl;
      // Setup the paths for data.
      std::filesystem::path cwd = std::filesystem::current_path();
      std::filesystem::path comp_dir = cwd / "DisplacedGeometries";
      if (!std::filesystem::exists(comp_dir))
      {
         std::filesystem::create_directories(comp_dir);
      }

      // Loop over clusters
      for (const auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         for (const Cluster& cluster : cluster_vec)
         {
            if (cluster.GetNumMonomers() > 2)
            {
               std::cerr << "CoordinateScan1D is only implemented for dimers so far" << std::endl;
               continue;
            }

            // Get the displacement coordinate
            Eigen::RowVector3d coord = cluster.ComputeDisplacementCoordinate1D();

            // Compute vector of displaced Molecules. Outer vector iterates over amount of scaling, inner vector iterates over monomers in cluster (so far only dimers are allowed).
            std::vector<std::vector<Molecule>> displaced_molecules = cluster.ApplyDisplacement(coord);

            //for (const auto& molecules : displaced_molecules)
            for (Uin i = 0; i < displaced_molecules.size(); i++)
            {
               std::string filename = cluster.GetName() + "_" + std::to_string(i) + ".xyz";
               PrintMolecules
                  (  displaced_molecules[i].begin()
                  ,  displaced_molecules[i].end()
                  ,  comp_dir / filename
                  );
            }
         }
      }
   }

   /********************************************************************
    * The main computation of the equilibrium distances module.
    ********************************************************************/
   void ClusterSet::RunEqDistances() const
   {
      std::cerr << "ABJ: RunEqDistances might be a bit broken, only looping over non-reference" << std::endl;
      // Setup the paths for data.
      std::filesystem::path cwd = std::filesystem::current_path();
      std::filesystem::path comp_dir = cwd / "EqDistances";
      if (!std::filesystem::exists(comp_dir))
      {
         std::filesystem::create_directories(comp_dir);
      }

      std::filesystem::path out_file = comp_dir / "EqDistances.dat";
      
      // De-reference begin iterator into variables
      const auto& [first_cluster, first_cluster_vec] = *mCompareClusters.begin();

      std::vector<std::string> header{"Cluster"};
      for (const Cluster& cluster : first_cluster_vec)
      {
         header.emplace_back(cluster.GetMethod() + "/" + cluster.GetBasisSet());
      }

      std::vector<std::vector<std::string>> table_data;

      // Loop over clusters
      for (const auto& [cluster_name, cluster_vec] : mCompareClusters)
      {
         table_data.emplace_back(std::vector<std::string>{cluster_name});
         for (const Cluster& cluster : cluster_vec)
         {
            // Get the center of masses coordinate
            std::vector<double> eq_distances = cluster.ComputeEqDistances();

            if (eq_distances.size() != 1)
            {
               std::stringstream ss;
               ss << "RunEqDistances() is only implemented for dimers, meaning there should only be one distance !\n"
                  << "I found " << eq_distances.size() << " distances !"
                  << std::endl;
               std::cerr << ss.str();
               exit(1);
            }

            std::stringstream ss;
            ss << std::setprecision(16) << eq_distances.back();
            table_data.back().emplace_back(ss.str());
         }
      }

      Util::PrintTable(header, table_data, out_file);
   }

   /********************************************************************
    * Sanity check for #2 CompareClusterAcrossMethodss
    ********************************************************************/
   void ClusterSet::SanityCheckCompareClustersAcrossMethods() const
   {
   }
}