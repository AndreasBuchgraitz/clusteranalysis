#include<iostream>
#include<tuple>
#include<regex>

#include "CommonAliases.hpp"
#include "Structure/StructureIdentification.hpp"
#include "MathAndUtil/Util.hpp"

namespace Structure
{
   /********************************************************************
    * Operator<< overload for StructureIdentification
    ********************************************************************/
   std::ostream& operator<<(std::ostream& arOut, const StructureIdentification& arId)
   {
      arOut << std::boolalpha;
      arOut << arId.GetName() << "\n"
            << arId.GetCompMethod() << "\n";
      if (!arId.GetBasisSet().empty())
      {
      arOut << arId.GetBasisSet() << "\n";
      }
      arOut << "Reference method ? " << arId.GetReferenceMethod()
            << std::endl;

      return arOut;
   }

   /********************************************************************
    * Operator< overload for StructureIdentification
    ********************************************************************/
   bool StructureIdentification::operator<(const StructureIdentification& arRhsId) const
   {
      return   std::tie
                  (  this->GetName()
                  ,  this->GetCompMethod()
                  ,  this->GetBasisSet()
                  ,  this->GetReferenceMethod()
                  )
            <  std::tie
                  (  arRhsId.GetName()
                  ,  arRhsId.GetCompMethod()
                  ,  arRhsId.GetBasisSet()
                  ,  arRhsId.GetReferenceMethod()
                  );
   }

   bool StructureIdentification::operator==(const StructureIdentification& arRhsId) const
   {
      return   std::tie
                  (  this->GetName()
                  ,  this->GetCompMethod()
                  ,  this->GetBasisSet()
                  ,  this->GetReferenceMethod()
                  )
            == std::tie
                  (  arRhsId.GetName()
                  ,  arRhsId.GetCompMethod()
                  ,  arRhsId.GetBasisSet()
                  ,  arRhsId.GetReferenceMethod()
                  );
   }
}