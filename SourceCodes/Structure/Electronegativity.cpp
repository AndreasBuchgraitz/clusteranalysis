/********************************************************************
 * File to contain the Electronegativity map
 ********************************************************************/

#include<type_traits>
#include<vector>

#include "Structure/StructureEnums.hpp"
#include "Structure/Electronegativity.hpp"

namespace Structure::Electronegativity
{
   namespace Detail
   {
      const static std::vector<double> ElectronegativityVec = 
         {  2.20,                                              -1
         ,  0.98, 1.57,          2.04, 2.55, 3.04, 3.44, 3.98, -1
         ,  0.93, 1.31,          1.61, 1.90, 2.19, 2.58, 3.16, -1
         };
      const static std::vector<std::size_t> NumElectronsOuterShellVec = 
         {  1,                               2
         ,  1, 2,             3, 4, 5, 6, 7, 8
         ,  1, 2,             3, 4, 5, 6, 7, 8
         };
   }

   /********************************************************************
    * We do minus one such that aAtomIdx aligns with common knowledge
    * that fx H is number 1, He is number 2 ...
    ********************************************************************/
   double GetElectronegativity(const int aAtomIdx)
   {
      return Detail::ElectronegativityVec[aAtomIdx - 1];
   }
   //
   double GetElectronegativity(const Structure::AtomName aAtomName)
   {
      return GetElectronegativity(std::underlying_type_t<Structure::AtomName>(aAtomName));
   }

   /********************************************************************
    * We do minus one such that aAtomIdx aligns with common knowledge
    * that fx H is number 1, He is number 2 ...
    ********************************************************************/
   std::size_t GetNumElectronsInOuterShell(const int aAtomIdx)
   {
      return Detail::NumElectronsOuterShellVec[aAtomIdx - 1];
   }
   //
   std::size_t GetNumElectronsInOuterShell(const Structure::AtomName aAtomName)
   {
      return GetNumElectronsInOuterShell(std::underlying_type_t<Structure::AtomName>(aAtomName));
   }
}