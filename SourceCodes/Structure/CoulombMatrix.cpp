/********************************************************************
 *
 ********************************************************************/

#include<numeric>

#include "CommonAliases.hpp"
#include "MathAndUtil/Math.hpp"
#include "Structure/CoulombMatrix.hpp"
#include "Structure/StructureEnums.hpp"

namespace Structure
{
   /********************************************************************
    *
    ********************************************************************/
   CoulombMatrix::CoulombMatrix
      (  const std::vector<AtomName>& arAtomNames
      ,  const Eigen::MatrixX3d& arAtomPositions
      )
      :  mAtomNames(arAtomNames)
      ,  mAtomPositions(arAtomPositions)
   {
      // Compute member matrix as having all atoms included.
      std::vector<Uin> atom_idx(mAtomNames.size());
      std::iota(atom_idx.begin(), atom_idx.end(), 0);
      mMatrix = ComputeCoulombMatrix(atom_idx);
   }

   /********************************************************************
    *
    ********************************************************************/
   Eigen::MatrixXd CoulombMatrix::ComputeCoulombMatrix
      (  const std::vector<Uin>& arAtomIdx
      )  const
   {
      // Result Matrix
      Eigen::MatrixXd res_mat(arAtomIdx.size(), arAtomIdx.size());

      for (Uin i = 0; i < arAtomIdx.size(); i++)
      {
         res_mat(i, i) = 0.5 * std::pow(GetNuclearCharge(mAtomNames[arAtomIdx[i]]), 2.4);
         for (Uin j = i + 1; j < arAtomIdx.size(); j++)
         {
            int idx_i = arAtomIdx[i];
            int idx_j = arAtomIdx[j];
            double distance = Math::RootSquaredDistance(mAtomPositions.row(idx_i), mAtomPositions.row(idx_j));
            double charge_product = GetNuclearCharge(mAtomNames[idx_i]) * GetNuclearCharge(mAtomNames[idx_j]);

            res_mat(i, j) = charge_product / distance;
            res_mat(j, i) = charge_product / distance;
         }
      }
   
      return res_mat;
   }
}