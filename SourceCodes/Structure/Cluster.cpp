/********************************************************************
 * This file contains the definition of a Cluster
 * which contains path to xyz file, method for optimization, objects
 * of structure classes (Molecule)
 ********************************************************************/

#include<iostream>
#include<vector>
#include<regex>
#include<algorithm>
#include<ranges>

#include "CommonAliases.hpp"

#include "MathAndUtil/Util.hpp"
#include "MathAndUtil/Math.hpp"
#include "Structure/StructureIdentification.hpp"
#include "Structure/Molecule.hpp"
#include "Structure/Cluster.hpp"
#include "Structure/ClusterSet.hpp"
#include "Input/ReadFiles.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "Exceptions.hpp"

namespace Structure
{
   /********************************************************************
    * Constructor for Cluster class.
    ********************************************************************/
   Cluster::Cluster
      (  const std::filesystem::path& arPathName
      ,  const StructureIdentification& arId
      )
      :  mPathToXYZ(arPathName)
      ,  mId(arId)
   {
      bool debug = this->mId.GetName() == "PUT-NAME-HERE-FOR-SPECIFIC-DEBUG";
      if (debug) 
      {
         std::cout << "\nEntering c-tor for " << this->mId << std::endl;
      }
      
      try
      {
         // Save the Cluster as a Molecule.
         mCluster = Input::ReadXYZAsMolecule(GetPathToXYZ(), arId.GetName());
      }
      catch(NeedClusterInformation& err)
      {
         std::cerr << err.what() << std::endl;;
         std::cerr << mId << std::endl;
         exit(1);
      }

      if (!mCluster.Empty())
      {
         mMonomers = ConstructMonomersNewImpl(this->GetClusterMolecule(), debug);
      
         // Set a "meta data" vector which simply contain the sizes of the found monomers. Careful with order!
         for (const auto& monomer : mMonomers)
         {
            mMonomerSizes.emplace(monomer.GetNumAtoms());
         }
      }
   }
   
   /********************************************************************
    * 
    ********************************************************************/
   std::vector<Molecule> Cluster::ConstructMonomersNewImpl
      (  const Molecule& arClusterMolecule
      ,  const bool aDebug
      )  const
   {
      std::vector<std::set<Uin>> monomer_sets = MergeAtomsToMonomers(arClusterMolecule.GetAtomConnectivityMap(), aDebug);

      AddHydrogensToMonomerSets(monomer_sets, arClusterMolecule.GetAtomNameIndices(AtomName::H), aDebug);

      return ConstructMoleculesFromIndexSet(monomer_sets, aDebug);
   }
   
   /********************************************************************
    * 
    ********************************************************************/
   std::vector<std::set<Uin>> Cluster::MergeAtomsToMonomers
      (  const std::map<Uin, std::vector<Uin>>& arAtomConnectivityMap
      ,  const bool aDebug
      )  const
   {
      if (aDebug)
      {
         std::cout << "Entering MergeAtomsToMonomers with input:\n";
         for (const auto& [atom_idx, connected_atoms] : arAtomConnectivityMap)
         {
            std::cout << "atom_idx: " << atom_idx << " -> ";
            for (const Uin con_atom : connected_atoms)
            {
               std::cout << con_atom << ", ";
            }
            std::cout << std::endl;
         }
      }

      std::vector<std::set<Uin>> monomer_sets;

      bool call_recursive = true;

      // Loop over all (non-hydrogen) atoms
      for (const auto& [atom_idx, connected_atom_idx_vec] : arAtomConnectivityMap)
      {
         //
         call_recursive = true;

         // We might have placed some of the indices, find idx in monomer_sets if so.
         if (!monomer_sets.empty())
         {
            for (Uin monomer_vec_idx = 0; monomer_vec_idx < monomer_sets.size(); monomer_vec_idx++)
            {
               if (monomer_sets[monomer_vec_idx].contains(atom_idx))
               {
                  call_recursive = false;
                  break;
               }
               for (const Uin connected_atom_idx : connected_atom_idx_vec)
               {
                  if (monomer_sets[monomer_vec_idx].contains(connected_atom_idx))
                  {
                     call_recursive = false;
                     break;
                  }
               }
               if (!call_recursive)
               {
                  break;
               }
            }
         }

         if (call_recursive)
         {
            // Call Emplace empty container before recursive call for new group
            monomer_sets.emplace_back(std::set<Uin>());

            // Recusively insert all connected atom idx.
            RecursiveConnectAtoms
               (  monomer_sets
               ,  atom_idx
               ,  arAtomConnectivityMap
               );
         }
      }

      if (aDebug)
      {
         std::cout << "Found the following monomers:\n";
         for (const std::set<Uin>& connected_atoms : monomer_sets)
         {
            for (const Uin con_atom : connected_atoms)
            {
               std::cout << con_atom << ", ";
            }
            std::cout << std::endl;
         }
      }

      return monomer_sets;
   }

   /********************************************************************
    * 
    ********************************************************************/
   void Cluster::RecursiveConnectAtoms
      (  std::vector<std::set<Uin>>& arMonomerSets
      ,  const Uin aAtomIdx
      ,  const std::map<Uin, std::vector<Uin>>& arAtomConnectivityMap
      )  const
   {
      // Base case. Check if aAtomIdx is already included from other recursive call.
      for (const std::set<Uin>& con_set : arMonomerSets)
      {
         if (con_set.contains(aAtomIdx))
         {
            return;
         }
      }

      // Insert new set with aAtomIdx
      arMonomerSets.back().emplace(aAtomIdx);

      // Loop over connected groups
      for (const Uin connected_atom_idx : arAtomConnectivityMap.at(aAtomIdx))
      {
         RecursiveConnectAtoms
            (  arMonomerSets
            ,  connected_atom_idx
            ,  arAtomConnectivityMap
            );
      }
   }

   /********************************************************************
    * 
    ********************************************************************/
   void Cluster::AddHydrogensToMonomerSets
      (  std::vector<std::set<Uin>>& arMonomerSets
      ,  const std::set<Uin>& arHydrogenIdxSet
      ,  const bool aDebug
      )  const
   {
      // Reference to the cluster molecule
      const Molecule& cluster_molecule = this->GetClusterMolecule();

      // Calculate the minimum distances between each pair of non-hydrogen and hydrogen and save the it with key as non-hydrogen and value as hydrogen idx
      std::map<Uin, Uin> min_distance_pairs;
      double min_distance = std::numeric_limits<double>::max();
      std::pair<Uin, Uin> min_distance_pair;

      for (const Uin hydrogen_idx : arHydrogenIdxSet)
      {
         // Reset temp containers for new non-hydrogen atom
         min_distance = std::numeric_limits<double>::max();
         min_distance_pair = std::pair<Uin, Uin>();

         for (const std::set<Uin>& monomer : arMonomerSets)
         {
            for (const Uin non_hydrogen_idx : monomer)
            {
               double distance = cluster_molecule.CalcDistanceOfAtoms(hydrogen_idx, non_hydrogen_idx);
               
               if (distance < min_distance)
               {
                  min_distance = distance;
                  min_distance_pair = std::make_pair(hydrogen_idx, non_hydrogen_idx);
               }
            }
         }

         min_distance_pairs.emplace(min_distance_pair);
      }

      // Loop over each minimum distance pair and monomer set and emplace.
      for (const auto& [hydrogen_idx, non_hydrogen_idx] : min_distance_pairs)
      {
         for (std::set<Uin>& monomer_set : arMonomerSets)
         {
            if (monomer_set.contains(non_hydrogen_idx))
            {
               monomer_set.emplace(hydrogen_idx);
            }
         }
      }

      if (aDebug)
      {
         std::cout << "Monomers after adding Hydrogens:\n";
         for (const std::set<Uin>& connected_atoms : arMonomerSets)
         {
            for (const Uin con_atom : connected_atoms)
            {
               std::cout << con_atom << ", ";
            }
            std::cout << std::endl;
         }
      }
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::string Cluster::CreateMonomernameFromIndices(const std::set<Uin>& arSet) const
   {
      const auto atom_names = this->GetClusterMolecule().GetAtomNames(arSet.cbegin(), arSet.cend());

      std::map<std::string, int> atom_counter;

      for (const std::string& atom_name_str : atom_names.second)
      {
         if (atom_counter.contains(atom_name_str))
         {
            atom_counter[atom_name_str] += 1;
         }
         else
         {
            atom_counter.emplace(atom_name_str, 1);
         }
      }

      std::string name;
      for (const auto& [atom, num] : atom_counter)
      {
         name += atom + std::to_string(num);
      }

      return name;
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::vector<Molecule> Cluster::ConstructMoleculesFromIndexSet
      (  const std::vector<std::set<Uin>>& arIndexSets
      ,  const bool aDebug
      )  const
   {
      if (aDebug)
      {
         std::cout << "Entering ConstructMoleculesFromIndexSet()" << std::endl;
      }
      std::vector<Molecule> result_vec;

      std::map<std::string, int> monomer_names;
      for (const std::set<Uin>& monomer_set : arIndexSets)
      {
         // Create numbered monomer names 
         std::string monomer_name = this->CreateMonomernameFromIndices(monomer_set);
         if (monomer_names.contains(monomer_name))
         {
            monomer_names[monomer_name] += 1;
         }
         else
         {
            monomer_names.emplace(monomer_name, 1);
         }
         try
         {
            monomer_name += "_" + std::to_string(monomer_names.at(monomer_name));
         }
         catch (const std::out_of_range&)
         {
            std::cout << "Could not find \"" << monomer_name << "\" in monomer_names map, error in ConstructMoleculesFromIndexSet()" << std::endl;
            exit(1);
         }

         result_vec.emplace_back
            (  monomer_name
            ,  this->GetClusterMolecule().GetAtomNames(monomer_set.cbegin(), monomer_set.cend())
            ,  this->GetClusterMolecule().GetEigenMat(monomer_set)
            ,  std::vector<Uin>(monomer_set.begin(), monomer_set.end())
            );
      }

      if (aDebug)
      {
         std::cout << "Actual Monomers constructed based on index sets:\n";
         for (const Molecule& monomer : result_vec)
         {
            std::cout << monomer << std::endl;
         }
      }

      return result_vec;
   }

   /********************************************************************
    * Compute the norm of the monomers and return the smallest.
    ********************************************************************/
   double Cluster::SmallestMonomerCoulombMatrixNorm() const
   {
      std::vector<double> norms;
      norms.reserve(mMonomers.size());
      for (const Molecule& monomer : mMonomers)
      {
         norms.emplace_back(monomer.CoulombMatrixNorm());
      }
      std::sort(norms.begin(), norms.end());

      return norms.front();
   }
   /********************************************************************
    * Compute the norm of the monomers and return the largest.
    ********************************************************************/
   double Cluster::LargestMonomerCoulombMatrixNorm() const
   {
      std::vector<double> norms;
      norms.reserve(mMonomers.size());
      for (const Molecule& monomer : mMonomers)
      {
         norms.emplace_back(monomer.CoulombMatrixNorm());
      }
      std::sort(norms.begin(), norms.end());

      return norms.back();
   }

   /********************************************************************
    * 
    ********************************************************************/
   void Cluster::ComputeSolvationShells()
   {
      mSolvationShells = SolvationShells(this->mMonomers);
   }

   /********************************************************************
    * Compute 1D coordinate for displacing two monomers
    ********************************************************************/
   Eigen::RowVector3d Cluster::ComputeDisplacementCoordinate1D() const
   {
      if (this->GetNumMonomers() < 2)
      {
         std::cerr << "We have less than two monomers in ComputeDisplacementCoordiate1D()\n"
            << "This->GetName() = " << this->GetName() << "\n"
            << std::endl;
         exit(1);
      }

      bool using_center_of_mass = true;
      if (using_center_of_mass)
      {
         return ComputeDisplacementCoordinate1DCenterOfMass();
      }
      else
      {
         return ComputeDisplacementCoordinate1DActualHydrogenBond();
      }
   }

   /********************************************************************
    * Compute the center of masses for all the monomers in this cluster.
    ********************************************************************/
   Eigen::MatrixX3d Cluster::ComputeCenterOfMassesForMonomers() const
   {
      Eigen::MatrixX3d center_of_masses(this->GetNumMonomers(), 3);
      Uin i = 0;
      auto monomer_it = this->GetMonomersIterConstBegin();
      for(
         ;  i < this->GetNumMonomers() && monomer_it != this->GetMonomersIterConstEnd()
         ;  i++, monomer_it++
         )
      {
         center_of_masses.row(i) = monomer_it->CalcCenterOfMass();
      }

      return center_of_masses;
   }

   /********************************************************************
    * Compute the equilibrium distances between all monomers.
    ********************************************************************/
   std::vector<double> Cluster::ComputeEqDistances() const
   {
      Eigen::MatrixX3d center_of_masses = ComputeCenterOfMassesForMonomers();

      std::vector<double> eq_distances;
      for (Uin i = 0; i < center_of_masses.rows(); i++)
      {
         for (Uin j = i + 1; j < center_of_masses.rows(); j++)
         {
            eq_distances.emplace_back((center_of_masses.row(i) - center_of_masses.row(j)).norm());
         }
      }

      return eq_distances;
   }

   /********************************************************************
    * Compute 1D coordinate for displacing two monomers using center of
    * mass approach
    ********************************************************************/
   Eigen::RowVector3d Cluster::ComputeDisplacementCoordinate1DCenterOfMass() const
   {
      Eigen::MatrixX3d center_of_masses = ComputeCenterOfMassesForMonomers();

      // Get Coordinate as the by subtracting the two COMs
      return Eigen::RowVector3d(center_of_masses.row(0) - center_of_masses.row(1));
   }

   /********************************************************************
    * Compute 1D coordinate for displacing two monomers by finding the
    * three (or more) atom indices involved in the actual hydrogen bond.
    ********************************************************************/
   Eigen::RowVector3d Cluster::ComputeDisplacementCoordinate1DActualHydrogenBond() const
   {
      std::cerr << "Entering ComputeDisplacementCoordiate1DActualHydrogenBond(), which have NOT been implemented yet !! I stop !" << std::endl;
      exit(1);
      /*
      // Loop over all the monomers and find all pairs of monomers which are closest to each other
      std::vector<std::pair<int, int>> monomer_pair_vec;
      for (Uin i = 0; i < this->GetNumMonomers(); i++)
      {
         double distance = std::numeric_limits<double>::max();

         int optimal_partner = -1;

         Eigen::RowVector3d com_1 = this->GetMonomer(i).CalcCenterOfMass();

         for (Uin j = i + 1; j < this->GetNumMonomers(); j++)
         {
            Eigen::RowVector3d com_2 = this->GetMonomer(j).CalcCenterOfMass();

            double distance_1_2 = Eigen::RowVector3d(com_1 - com_2).norm();
            if (  distance_1_2 < distance
               )
            {
               distance = distance_1_2;
               optimal_partner = j;
            }
         }

         monomer_pair_vec.emplace_back(i, optimal_partner);
      }

      // Now we know which molecules are next to each other. Figure out which ChemicalGroups are next to each other for each pair.
      //std::vector<std::pair<int, int>> binding_atom_idx;
      for (const auto& [monomer_i_1, monomer_i_2] : monomer_pair_vec)
      {
         const Molecule& monomer_1 = this->GetMonomer(monomer_i_1);
         const Molecule& monomer_2 = this->GetMonomer(monomer_i_2);
         const auto& chem_groups_1 = monomer_1.GetChemicalGroups();
         const auto& chem_groups_2 = monomer_2.GetChemicalGroups();

         double distance = std::numeric_limits<double>::max();

         int bind_idx_1 = -1;
         int bind_idx_2 = -1;

         std::vector<std::pair<int, int>> binding_atom_pairs;

         for (Uin group_idx_1 = 0; group_idx_1 < chem_groups_1.size(); group_idx_1++)
         {
            for (Uin group_idx_2 = 0; group_idx_2 < chem_groups_2.size(); group_idx_2++)
            {
               double distance_groups = Eigen::RowVector3d
                  (  chem_groups_1[group_idx_1].GetBindingAtomCoord() 
                  -  chem_groups_2[group_idx_2].GetBindingAtomCoord()
                  ).norm();
               
               if (  distance_groups < distance
                  && (distance - distance_groups) / distance < 0.05 // if within 5% of each other
                  )
               {
                  distance = distance_groups;
                  bind_idx_1 = chem_groups_1[group_idx_1].GetBindingAtomIdx();
                  bind_idx_2 = chem_groups_2[group_idx_2].GetBindingAtomIdx();

                  // Emplace this next additional pair.
                  binding_atom_pairs.emplace_back(bind_idx_1, bind_idx_2);
               }
               else if (distance_groups < distance) // if simply much lower.
               {
                  distance = distance_groups;
                  bind_idx_1 = chem_groups_1[group_idx_1].GetBindingAtomIdx();
                  bind_idx_2 = chem_groups_2[group_idx_2].GetBindingAtomIdx();

                  // Override current vector 
                  binding_atom_pairs = {std::make_pair(bind_idx_1, bind_idx_2)};
               }
               // THIS WILL ONLY FIND THE LOWEST BINDIND DISTANCE, BUT WHAT IF TWO ARE VERY CLOSE TO EACH OTHER...
            }
         }
         //binding_atom_idx.emplace_back(bind_idx_1, bind_idx_2);
         // DONT SAVE IT IN VECTOR COMPUTE THE COORDINATES FOR ALL PAIRS IN THIS LOOP DIRECTLY

         // Compute the coordinate for the current 

         // THIS IS ACTUALLY A LITTLE COMPLICATED
         // FX 1AA1EDA HAVE TWO HYDROGEN BONDS, BUT THE DISTANCE BETWEEN TWO OF THE BINDINGATOMS ARE 11% BIGGER FOR ONE PAIR COMPARED TO THE OTHER.....
         // SO MAYBE WE WOULD HAVE TO FIND ALL HYDROGENS BETWEEN THE TWO BINDINGATOMS AND THEN TAKE THE CLOSEST ONE. FOR ALL CHEMICALGROUPS, BUT THEN SOMEHOW CHOOSE ONLY THE RELEVANT CHEMICALGROUPS ....
      }
      */

      // Now Go through all 
      return Eigen::RowVector3d();
   }


   /********************************************************************
    * Applies the coordinate arCoord to the two monomers i.e. moves them
    * closer/farther apart
    ********************************************************************/
   std::vector<std::vector<Molecule>> Cluster::ApplyDisplacement
      (  const Eigen::RowVector3d& arCoord
      )  const
   {
      if (this->GetNumMonomers() != 2)
      {
         std::cerr << "ApplyDisplacement() has only been implemented for dimers" << std::endl;
         exit(1);
      }
      std::vector<std::vector<Molecule>> result_vec;

      // Scale the applied displacement in opposite directions for the two monomers.
      int monomer_fac_0 = -1;
      int monomer_fac_1 = 1;

      // Grid used to scale the vector for displacement.
      // Close grid around minimum, not too many points in "close direction" so it does not blow up, and a few points far away.
      std::vector<double> scaling = 
         {  -0.08
         ,  -0.06
         ,  -0.04
         ,  -0.02
         ,  0
         ,  0.02
         ,  0.04
         ,  0.06
         ,  0.08
         ,  0.1
         ,  0.2
         ,  0.3
         ,  0.4
         ,  0.6
         };

      // Ref to the coordinates of the monomers
      auto monomer_0_it = this->GetMonomersIterConstBegin();
      auto monomer_1_it = monomer_0_it++;
      const Eigen::MatrixX3d& coords_monomer_0 = monomer_0_it->GetEigenMat();
      const Eigen::MatrixX3d& coords_monomer_1 = monomer_1_it->GetEigenMat();

      // 
      for (const double scale : scaling)
      {
         // Construct Matrix for monomer 0
         Eigen::MatrixX3d new_coords_monomer_0(coords_monomer_0.rows(), 3);
         for (int i_row = 0; i_row < coords_monomer_0.rows(); i_row++)
         {
            new_coords_monomer_0.row(i_row) = coords_monomer_0.row(i_row) + monomer_fac_0 * scale * arCoord;
         }
         // Construct Matrix for monomer 1
         Eigen::MatrixX3d new_coords_monomer_1(coords_monomer_1.rows(), 3);
         for (int i_row = 0; i_row < coords_monomer_1.rows(); i_row++)
         {
            new_coords_monomer_1.row(i_row) = coords_monomer_1.row(i_row) + monomer_fac_1 * scale * arCoord;
         }

         // Emplace back the two new molecules.
         auto cluster_idx_range_monomer_0 = std::views::values(monomer_0_it->GetClusterIdxVec());
         auto cluster_idx_range_monomer_1 = std::views::values(monomer_1_it->GetClusterIdxVec());
         std::vector<Molecule> mol_sub_vec = 
            {  Molecule
                  (  monomer_0_it->GetName()
                  ,  monomer_0_it->GetAtomNames()
                  ,  new_coords_monomer_0
                  ,  monomer_0_it->GetAtomConnectivityMap()
                  ,  std::vector<Uin>(cluster_idx_range_monomer_0.begin(), cluster_idx_range_monomer_0.end())
                  )
            ,  Molecule
                  (  monomer_1_it->GetName()
                  ,  monomer_1_it->GetAtomNames()
                  ,  new_coords_monomer_1
                  ,  monomer_1_it->GetAtomConnectivityMap()
                  ,  std::vector<Uin>(cluster_idx_range_monomer_1.begin(), cluster_idx_range_monomer_1.end())
                  )
            };

         result_vec.emplace_back(std::move(mol_sub_vec));
      }

      return result_vec;
   }
   
   /********************************************************************
    * Overload of operator "less than" for two clusters.
    * 
    * Simply compare the ID
    ********************************************************************/
   bool Cluster::operator<(const Cluster& arClusterRhs) const
   {
      return this->GetId() < arClusterRhs.GetId();
   }

   /********************************************************************
    * Overload of operator<< for Clusters.
    * 
    * Might print too much right now, but hopefully good for debugging.
    ********************************************************************/
   std::ostream& operator<<(std::ostream& arOut, const Cluster& arCluster)
   {
      arOut << "Cluster: " << arCluster.GetName() << "\n";
      arOut << "Contains " << arCluster.GetNumMonomers() << " monomers in total.\n";
      arOut << "Method:  " << arCluster.GetMethod() << std::endl;
      arOut << "Basis set: " << arCluster.GetBasisSet() << std::endl;

      for (auto it = arCluster.GetMonomersIterConstBegin(); it != arCluster.GetMonomersIterConstEnd(); it++)
      {
         arOut << "Printing (cluster) monomers: " << it->GetName() << std::endl;
         arOut << *it << std::endl;
      }
      return arOut;
   }
}