/********************************************************************
 *
 ********************************************************************/

#include<numeric>
#include<map>
#include<set>

#include "CommonAliases.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "Structure/StructureEnums.hpp"
#include "Structure/DipoleMoment.hpp"
#include "Structure/Electronegativity.hpp"

namespace Structure
{
   /********************************************************************
    * DipoleMoment constructor
    ********************************************************************/
   DipoleMoment::DipoleMoment
      (  const std::vector<AtomName>& arAtomNames
      ,  const Eigen::MatrixX3d& arAtomPositions
      ,  const std::map<Uin, std::vector<Uin>>& arConnectivityMap
      )
      :  mDipoleMoment(ComputeDipoleMoment(arAtomNames, arAtomPositions, arConnectivityMap))
      ,  mHaveDipole(mDipoleMoment.norm() > 1.0E-2)
   {
   }

   /********************************************************************
    * Compute the Dipolemoment.
    * 
    * 1) Compute partial charges
    * 2) Compute atomic posistions from Center-Of-Mass 
    * 3) Sum up the position vectors scaled by the partial charges
    ********************************************************************/
   Eigen::RowVector3d DipoleMoment::ComputeDipoleMoment
      (  const std::vector<AtomName>& arAtomNames
      ,  const Eigen::MatrixX3d& arAtomPositions
      ,  const std::map<Uin, std::vector<Uin>>& arConnectivityMap
      )
   {
      // Get the indices of the actually used atoms
      std::set<Uin> used_atom_idx_set;
      for (const auto& [atom_idx, connected_indices] : arConnectivityMap)
      {
         used_atom_idx_set.emplace(atom_idx);
         for (const Uin con_idx : connected_indices)
         {
            used_atom_idx_set.emplace(con_idx);
         }
      }
      // Convert to vector for easier use.
      std::vector<Uin> used_atom_idx(used_atom_idx_set.begin(), used_atom_idx_set.end());

      // Create vector of "number of electrons in outer shell" for each atom (ordered as arAtomNames)
      std::vector<double> num_electrons(arAtomNames.size(), 0.0); // Needs size of all atoms for indexing in loop below, only relevant indices will be different from 0.
      for (const Uin idx : used_atom_idx)
      {
         num_electrons[idx] = Electronegativity::GetNumElectronsInOuterShell(arAtomNames[idx]);
      }

      // Compute partial charges
      for (const auto& [atom_idx, connected_indices] : arConnectivityMap)
      {
         AtomName atom_name = arAtomNames[atom_idx];
         double elec_neg_atom = Electronegativity::GetElectronegativity(atom_name);

         for (const Uin con_idx : connected_indices)
         {
            AtomName con_atom = arAtomNames[con_idx];
            double elec_neg_con_atom = Electronegativity::GetElectronegativity(con_atom);

            double diff = (elec_neg_atom - elec_neg_con_atom) / 2.0;

            num_electrons[atom_idx] += diff;
            num_electrons[con_idx] -= diff;
         }
      }

      // Compute atomic positions with origin in center of mass
      Eigen::MatrixX3d atom_positions_com(arAtomNames.size(), 3);

      //!@{ COMPUTE THE CENTER OF MASS
      Eigen::MatrixX3d atom_positions = arAtomPositions(used_atom_idx, Eigen::all);
      std::vector<AtomName> atom_names;
      for (const Uin idx : used_atom_idx)
      {
         atom_names.emplace_back(arAtomNames[idx]);
      }
      Eigen::RowVector3d com = Kabsch::CenterOfMass(atom_names, atom_positions);
      //!@}

      // Positions in the COM frame.
      for (Uin i = 0; i < arAtomNames.size(); i++)
      {
         if (used_atom_idx_set.contains(i))
         {
            atom_positions_com.row(i) = arAtomPositions.row(i) - com;
         }
         else
         {
            atom_positions_com.row(i) = Eigen::RowVector3d::Zero();
         }
      }

      // Sum up all scaled position vectors
      Eigen::RowVector3d zero = Eigen::RowVector3d::Zero();
      Eigen::RowVector3d dipole_moment = std::transform_reduce
               (  atom_positions_com.rowwise().begin(), atom_positions_com.rowwise().end()
               ,  num_electrons.begin()
               ,  zero
               ,  std::plus<>()
               ,  std::multiplies<>()
               );

      // If the dipole is small it probably belongs to a point group where it should be zero.
      if (dipole_moment.norm() < 5.0E-2)
      {
         dipole_moment = Eigen::RowVector3d::Zero();
         return dipole_moment;
      }
      else
      {
         return dipole_moment;
      }
   }
}