/********************************************************************
 * This file contains the Atom and Molecule class.
 ********************************************************************/

#include<sstream>
#include<tuple>
#include<optional>

#include "CommonAliases.hpp"
#include "MathAndUtil/Util.hpp"
#include "MathAndUtil/Math.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "Structure/StructureIdentification.hpp"
#include "Structure/Molecule.hpp"
#include "Structure/StructureEnums.hpp"
#include "Exceptions.hpp"

namespace Structure
{
   /********************************************************************
    * Molcule constructor
    * 
    * @param[in] arName
    *    Name of Molecule
    * @param[in] arAtoms
    *    Description of the atoms present
    ********************************************************************/
   Molecule::Molecule
      (  const std::string& arName
      ,  const std::pair<std::vector<AtomName>, std::vector<std::string>>& arAtomNames
      ,  const Eigen::MatrixX3d& arAtomPositions
      ,  const std::vector<Uin>& arClusterIdxVec
      )
      :  mName(arName)
      ,  mAtomNames(arAtomNames)
      ,  mAtomPositions(arAtomPositions)
      ,  mNonHydrogenIndex(ConstructNonHydrogenIndexVector())
      ,  mAtomIndexMap(ConstructAtomIndicesMap())
      ,  mAtomConnectivityMap(ConstructNonHydrogenConnectivityMap(mAtomIndexMap))
      ,  mDipoleMoment(arAtomNames.first, mAtomPositions, AddHydrogensToConnectivityMap(mAtomConnectivityMap))
      ,  mCoulombMatrix(mAtomNames.first, mAtomPositions)
      ,  mSmallestBondLength(ComputeSmallestBondLength())
   {
      // Sanity check
      if (mAtomNames.first.size() != Uin(mAtomPositions.rows()))
      {
         std::cerr << "You have not provided an equal number of AtomNames and coordinates for the Molecule c-tor, shame on you !" << std::endl;
         std::cerr << "Molecule Name = " << mName << std::endl;
         std::cerr << "mAtomNames.first.size() = " << mAtomNames.first.size() << std::endl;
         std::cerr << "mAtomPositions.rows() = " << mAtomPositions.rows() << std::endl;
         exit(1);
      }

      // Initialize the mClusterIdx vector (the pair may be a bit redundant, but nice and explicit)
      for (Uin i = 0; i < arClusterIdxVec.size(); i++)
      {
         mClusterIdxVec.emplace_back(i, arClusterIdxVec[i]);
      }
   }

   /********************************************************************
    * Molcule constructor
    * 
    * @param[in] arName
    *    Name of Molecule
    * @param[in] arAtoms
    *    Description of the atoms present
    * @param[in] arAtomConnectivityMap
    ********************************************************************/
   Molecule::Molecule
      (  const std::string& arName
      ,  const std::pair<std::vector<AtomName>, std::vector<std::string>>& arAtomNames
      ,  const Eigen::MatrixX3d& arAtomPositions
      ,  const std::map<Uin, std::vector<Uin>>& arAtomConnectivityMap
      ,  const std::vector<Uin>& arClusterIdxVec
      )
      :  mName(arName)
      ,  mAtomNames(arAtomNames)
      ,  mAtomPositions(arAtomPositions)
      ,  mNonHydrogenIndex(ConstructNonHydrogenIndexVector())
      ,  mAtomIndexMap(ConstructAtomIndicesMap())
      ,  mAtomConnectivityMap(arAtomConnectivityMap)
      ,  mDipoleMoment(arAtomNames.first, mAtomPositions, AddHydrogensToConnectivityMap(mAtomConnectivityMap))
      ,  mCoulombMatrix(mAtomNames.first, mAtomPositions)
      ,  mSmallestBondLength(ComputeSmallestBondLength())
   {
      // Sanity check
      if (mAtomNames.first.size() != Uin(mAtomPositions.rows()))
      {
         std::cerr << "You have not provided an equal number of AtomNames and coordinates for the Molecule c-tor, shame on you !" << std::endl;
         std::cerr << "Molecule Name = " << mName << std::endl;
         std::cerr << "mAtomNames.first.size() = " << mAtomNames.first.size() << std::endl;
         std::cerr << "mAtomPositions.rows() = " << mAtomPositions.rows() << std::endl;
         exit(1);
      }

      // Initialize the mClusterIdx vector (the pair may be a bit redundant, but nice and explicit)
      for (Uin i = 0; i < arClusterIdxVec.size(); i++)
      {
         mClusterIdxVec.emplace_back(i, arClusterIdxVec[i]);
      }
   }

   /********************************************************************
    * Get the mass of the atom, by converting the AtomName to an int.
    ********************************************************************/
   int Molecule::GetMass(const int aAtomIdx) const
   {
      return Structure::GetMass(this->GetAtomName(aAtomIdx));
   }

   /********************************************************************
    * Get all the indicies for atoms from a molecule. Return as map 
    * with keys as the atom name and values as std::set<int> 
    * containing the actual indices.
    ********************************************************************/
   std::map<AtomName, std::set<Uin>> Molecule::ConstructAtomIndicesMap
      (
      )  const
   {
      // Initialize result map
      std::map<AtomName, std::set<Uin>> res_map;

      // Fill in the map
      for (Uin atom_idx = 0; atom_idx < this->GetNumAtoms(); atom_idx++)
      {
         res_map.try_emplace(this->GetAtomName(atom_idx), std::set<Uin>());

         res_map.at(this->GetAtomName(atom_idx)).emplace(atom_idx);
      }

      //
      return res_map;
   }
      
   /********************************************************************
    * Returns vector containing atom indices for Hydrogen
    ********************************************************************/
   std::vector<Uin> Molecule::ConstructNonHydrogenIndexVector() const
   {
      std::vector<Uin> res_vec;
      res_vec.reserve(this->GetNumAtoms());

      // Fill in the map
      for (Uin atom_idx = 0; atom_idx < this->GetNumAtoms(); atom_idx++)
      {
         // Also Initialize mNonHydrogenIndex
         if (this->GetAtomName(atom_idx) != AtomName::H)
         {
            res_vec.emplace_back(atom_idx);
         }
      }

      return res_vec;
   }

   /********************************************************************
    * Returns set containing atom indices for AtomName
    ********************************************************************/
   const std::set<Uin>& Molecule::GetAtomNameIndices
      (  const AtomName arAtomName
      )  const
   {
      try
      {
         return mAtomIndexMap.at(arAtomName);
      }
      catch (const std::out_of_range& e)
      {
         std::cerr << "There is no " << GetStringFromEnum(arAtomName) << " atoms in this molecule:\n" << this->GetName() << std::endl;
         exit(1);
      }
   }

   /********************************************************************
    * Returns vector containing atom indices for Hydrogen
    ********************************************************************/
   std::vector<Uin> Molecule::GetHydrogenIndex() const
   {  
      try
      {
         return std::vector<Uin>(mAtomIndexMap.at(AtomName::H).begin(), mAtomIndexMap.at(AtomName::H).end());
      }
      catch (const std::out_of_range& e)
      {
         std::cerr << "There are no Hydrogen atoms in mAtomIndexMap.\n"
            << "Caught in GetHydrogenIndex() for Molecule: " << this->GetName()
            << std::endl;
         exit(1);
      }
   }

   /********************************************************************
    * Returns vector with indices for non hydrogens. Will construct it
    * first if it is not initialized.
    ********************************************************************/
   const std::vector<Uin>& Molecule::GetNonHydrogenIndex() const
   {
      return mNonHydrogenIndex;
   }

   /********************************************************************
    * Simply returns matrix saved in the atom_cont mAtoms
    ********************************************************************/
   const Eigen::MatrixX3d& Molecule::GetEigenMat() const
   {
      return mAtomPositions;
   }

   /********************************************************************
    * Returns the coordinate matrix sliced based on the index vector.
    * That is returns a matrix of size (arIndexVec.size(), 3)
    ********************************************************************/
   Eigen::MatrixX3d Molecule::GetEigenMat(const std::vector<Uin>& arIndexVec) const
   {
      return mAtomPositions(arIndexVec, Eigen::all);
   }

   /********************************************************************
    * Returns the coordinate matrix sliced based on the index vector.
    * That is returns a matrix of size (arIndexVec.size(), 3)
    ********************************************************************/
   Eigen::MatrixX3d Molecule::GetEigenMat(const std::set<Uin>& arIndexSet) const
   {
      return GetEigenMat(std::vector<Uin>(arIndexSet.begin(), arIndexSet.end()));
   }

   /********************************************************************
    * Get number of atoms for specific atom
    ********************************************************************/
   int Molecule::GetNumAtoms(const AtomName aAtomName) const
   {
      try
      {
         // Return size of vector in index map at atomname
         return mAtomIndexMap.at(aAtomName).size();
      }
      catch (const std::out_of_range& e)
      {
         // The atom name is not present in the molecule so return -1.
         return -1;
      }
   }

   /********************************************************************
    * Calculates the distance between two atom from their indices.
    ********************************************************************/
   double Molecule::CalcDistanceOfAtoms
      (  const int atom_idx_1
      ,  const int atom_idx_2
      )  const
   {
      return CalcDistanceOfAtoms(GetCoordinate(atom_idx_1), GetCoordinate(atom_idx_2));
   }

   /********************************************************************
    * Calculates the distance between two atom from their position
    * vectors.
    ********************************************************************/
   double Molecule::CalcDistanceOfAtoms
      (  const Eigen::RowVector3d& arVec1
      ,  const Eigen::RowVector3d& arVec2
      )  const
   {
      return (arVec1 - arVec2).norm();
   }

   /********************************************************************
    * Get all the nearest neighbours for all non-hydrogen atoms
    ********************************************************************/
   std::map<Uin, std::vector<Uin>> Molecule::ConstructNonHydrogenConnectivityMap
      (  const std::map<AtomName, std::set<Uin>>& arAtomIndexMap
      )  const
   {
      // Map for all indices with their nearest neighbours
      std::map<Uin, std::vector<Uin>> nearest_atoms;

      // Loop over all atoms
      for (Uin atom_idx = 0; atom_idx < this->GetNumAtoms(); atom_idx++)
      {
         // Skip Hydrogen atoms
         if (this->GetAtomName(atom_idx) == AtomName::H)
         {
            continue;
         }

         // Initialize the empty vector for neighbours
         nearest_atoms.emplace(atom_idx, std::vector<Uin>());

         // Map of all distances for atom_idx
         std::map<double, Uin> distances;

         // Loop over all other atoms
         for (const auto& [atomname, idx_set] : arAtomIndexMap)
         {
            // Skip Hydrogen atoms
            if (atomname == AtomName::H)
            {
               continue;
            }

            // Build up the distances map
            for (const Uin idx : idx_set)
            {
               if (idx != atom_idx)
               {
                  distances.emplace(CalcDistanceOfAtoms(atom_idx, idx), idx);
               }
            }
         }

         // Now save the relevant idx to nearest_atoms
         const auto dis_start = distances.begin();

         // The closest other atom is more than 2.1 Å away, maybe a problem maybe not (fx. if ammonia is present there are H close by and so not a problem).
         if (dis_start->first > 2.1)
         {
            std::pair<Uin, double> closest_hydrogen = this->GetClosestAtomOfType(atom_idx, AtomName::H);
            if (closest_hydrogen.second > 2.1)
            {
               throw(NeedClusterInformation("ERROR ! The smallest bond length found is larger than 2.1 Å, do you have an atom floating alone somewhere ??"));
            }
            else
            {
               // If there indeed are H-atoms close by we simply continue, this is propbably ammonia or other.
               // We need to continue otherwise we connect atoms in different monomers.
               continue;
            }
         }

         // Either dynamically determine the maximum distance we tolerate or take the maximum allowed distance to be 2.0 Angstrom (Assuming no covalent bonds are longer than 2.0 Angstrom...)
         double allowed_max_distance = std::min(dis_start->first + dis_start->first * 0.30, 2.0);
         for(  auto it = dis_start
            ;  it != distances.end() && it->first < allowed_max_distance
            ;  it++
            )
         {
            nearest_atoms.at(atom_idx).emplace_back(it->second);
         }
      }

      return nearest_atoms;
   }

   /********************************************************************
    * Adds hydrogen to a connectivity map as generated by
    * ConstructNonHydrogenConnectivityMap(...)
    * 
    * Takes input map by value, to not overwrite member variable as this
    * might cause trouble in the Cluster construction.
    * 
    * Right now it assumed that aMap keys are never an index for a
    * hydrogen atom... Fine I guess, but could check with range view.
    * SHOULD BE FIXED NOW!
    ********************************************************************/
   std::map<Uin, std::vector<Uin>> Molecule::AddHydrogensToConnectivityMap
      (  std::map<Uin, std::vector<Uin>> aMap
      )  const
   {
      // If no hydrogens in the molecule, we are done already
      if (!this->mAtomIndexMap.contains(AtomName::H))
      {
         return aMap;
      }

      // Loop over hydrogen indices
      for (const Uin hydrogen_idx : this->mAtomIndexMap.at(AtomName::H))
      {
         std::map<double, Uin> distance_map;
         // Loop over non-hydrogen atoms
         for (const auto& [atom_idx, connected_idx] : aMap)
         {
            // Only try to assign hydrogen atoms to non-hydrogen atoms
            if (hydrogen_idx != atom_idx)
            {
               distance_map.emplace(CalcDistanceOfAtoms(atom_idx, hydrogen_idx), atom_idx);
            }
         }

         // Lowest distance is at begin and second is the atom_idx for which hydrogen_idx is closest to.
         aMap.at(distance_map.begin()->second).emplace_back(hydrogen_idx);
      }
      return aMap;
   }

   /********************************************************************
    * Will find the atom of type aAtomsToCheck which is closest to
    * aAtomIdx.
    ********************************************************************/
   std::pair<Uin, double> Molecule::GetClosestAtomOfType
      (  const Uin aAtomIdx
      ,  const AtomName aAtomsToCheck
      )  const
   {
      const std::set<Uin>& atoms_to_check = this->GetAtomNameIndices(aAtomsToCheck);

      std::pair<Uin, double> res_pair = {aAtomIdx, std::numeric_limits<double>::max()};

      for (const Uin check_atom_idx : atoms_to_check)
      {
         double distance = this->CalcDistanceOfAtoms(aAtomIdx, check_atom_idx);
         if (distance < res_pair.second)
         {
            res_pair.first = check_atom_idx;
            res_pair.second = distance;
         }
      }

      if (res_pair.first == aAtomIdx)
      {
         std::cerr << "In GetClosestAtomOfType we found the closest atom to be it self ..." << std::endl;
         exit(1);
      }

      return res_pair;
   }

   /********************************************************************
    * Computes the center-of-mass for the current Molecule
    ********************************************************************/
   Eigen::RowVector3d Molecule::CalcCenterOfMass() const
   {
      return Kabsch::CenterOfMass(this->GetAtomNameEnums(), this->GetEigenMat());
   }

   /********************************************************************
    * Computes the centroid for the current Molecule
    ********************************************************************/
   Eigen::RowVector3d Molecule::CalcMolecularCentroid() const
   {
      return Math::Centroid(this->GetEigenMat().rowwise().cbegin(), this->GetEigenMat().rowwise().cend());
   }
   
   /********************************************************************
    * Computes an estimate for the molecular radius, as the average
    * of the 4 largest distances from the centroid found in the molecule.
    ********************************************************************/
   double Molecule::CalcMolecularRadius() const
   {
      const auto centroid = this->CalcMolecularCentroid();

      double sum = 0;
      std::vector<double> distances;
      distances.reserve(this->GetEigenMat().rows());

      for (const auto& atom_position : this->GetEigenMat().rowwise())
      {
         distances.emplace_back(Math::RootSquaredDistance(centroid, atom_position));
      }

      // Sort the elements in non-ascending order
      std::sort(distances.begin(), distances.end(), std::greater<double>());

      // Average over the 4 largest elements
      for (std::size_t i = 0; i < std::min(std::size_t(4), distances.size()); i++)
      {
         sum += distances[i];
      }
      sum /= std::min(std::size_t(4), distances.size());

      return sum;
   }

   /********************************************************************
    * Computes the smallest bond length in this Molecule
    ********************************************************************/
   double Molecule::ComputeSmallestBondLength() const
   {
      double smallest_bond_length = std::numeric_limits<double>::max();
      for (int i = 0; i < this->mAtomPositions.rows(); i++)
      {
         for (int j = i + 1; j < this->mAtomPositions.rows(); j++)
         {
            double bond_length = Math::RootSquaredDistance(mAtomPositions.row(i), mAtomPositions.row(j));
            if (bond_length < smallest_bond_length)
            {
               smallest_bond_length = bond_length;
            }
         }
      }

      return smallest_bond_length;
   }

   /********************************************************************
    * operator< overload for Molecule class.
    ********************************************************************/
   bool Molecule::operator<(const Molecule& arMoleculeRhs) const
   {
      return this->GetName() < arMoleculeRhs.GetName();
   }

   /********************************************************************
    * operator<< overload for Molecule class
    ********************************************************************/
   std::ostream& operator<<(std::ostream& arOut, const Molecule& arMolecule)
   {
      arOut << std::left;
      arOut << std::scientific;

      arOut << arMolecule.GetName() << "\n";
      for (Uin idx = 0; idx < arMolecule.GetNumAtoms(); idx++)
      {
         arOut << arMolecule.GetAtomStringName(idx) << ": " << arMolecule.GetCoordinate(idx) << std::endl;
      }
      
      // Endline and flush after the molecule has been printed.
      arOut << std::endl;

      return arOut;
   }
}
