#include "Structure/SolvationShells.hpp"
#include "Structure/Molecule.hpp"
#include "MathAndUtil/ConvexHull.hpp"
#include "Structure/StructureEnums.hpp"

namespace Structure
{
   /********************************************************************
    * 
    ********************************************************************/
   SolvationShells::SolvationShells
      (  const std::vector<Molecule>& arMonomers
      )
   {
      std::map<std::string, Eigen::RowVector3d> coms;
      std::map<std::string, double> molecular_radii;
      for (const Molecule& monomer : arMonomers)
      {
         coms.emplace(monomer.GetName(), monomer.CalcCenterOfMass());
         molecular_radii.emplace(monomer.GetName(), monomer.CalcMolecularRadius());
      }

      // Save the COMs for later use
      mCenterOfMasses = coms;

      int solvation_shell(0);
      while (!coms.empty())
      {
         // Compute current shell
         std::vector<std::string> curr_shell = this->ComputeCurrentShell(coms, molecular_radii);

         // Update data member "mShells"
         mShells.emplace(solvation_shell, curr_shell);

         // Remove current shell from center of mass map
         for (const auto& mono_name : curr_shell)
         {
            coms.erase(mono_name);
         }

         // Increment solvation shell number
         solvation_shell++;
      }
   }

   /********************************************************************
    * Computes the current outer shell and returns vector of monomer
    * names which are present in this outer shell.
    ********************************************************************/
   std::vector<std::string> SolvationShells::ComputeCurrentShell
      (  const std::map<std::string, Eigen::RowVector3d>& arComs
      ,  const std::map<std::string, double>& arMolecularRadii
      )  const
   {
      const auto coms_range = std::views::values(arComs);
      std::vector<Eigen::RowVector3d> coms(coms_range.begin(), coms_range.end());

      // Compute Convex hull...
      Math::ConvexHull<3, double> convex_hull(coms);

      // Some points might be very close to the faces of the ConvexHull, which we as chemists want to include as part of the current SolvationShell.
      std::vector<Eigen::RowVector3d> solvation_shell(convex_hull.GetHullVertices());
      for (const auto& [monomer_name, vertex] : arComs)
      {
         for (const auto& face : convex_hull.GetPolytope().GetFaces())
         {
            auto dist = face.DistanceFromPlaneToPoint(vertex);
            auto thr = arMolecularRadii.at(monomer_name);

            auto it_find = std::find(solvation_shell.begin(), solvation_shell.end(), vertex);
            if ( dist < thr
               && it_find == solvation_shell.end()
               )
            {
               solvation_shell.emplace_back(vertex);
               break;
            }
            else if (it_find != solvation_shell.end())
            {
               // Already included so skip to next point.
               break;
            }
         }
      }

      // Construct vector of monomers which is part of the shell
      std::vector<std::string> res_vec;
      for (const auto& vertex : solvation_shell)
      {
         auto it_find = std::find_if
            (  arComs.cbegin()
            ,  arComs.cend()
            ,  [&vertex](const auto& aInp)
               {
                  return aInp.second == vertex;
               }
            );

         if (  it_find != arComs.end()
            )
         {
            res_vec.emplace_back(it_find->first);
         }
      }

      if (res_vec.empty())
      {
         std::cerr << "Result vector in ComputeCurrentShell() is empty, I quit !!" << std::endl;
         exit(1);
      }

      return res_vec;
   }

   /********************************************************************
    * Prints _this_ SolvationShells object as a xyz file for easy
    * visualization.
    ********************************************************************/
   void SolvationShells::PrintSolvationToFile
      (  const std::filesystem::path& arWorkingDir
      ,  const std::string& arStructureName
      )  const
   {
      const std::filesystem::path out_file = arWorkingDir / std::string("SolvationShells" + arStructureName + ".xyz");

      std::fstream xyz_file_stream;
      xyz_file_stream.open(out_file, std::ios_base::out);
      xyz_file_stream << this->mCenterOfMasses.size() << "\n";
      xyz_file_stream << "\n";
      for (const auto& [shell_number, name_vec] : this->mShells)
      {
         AtomName atom_name = static_cast<AtomName>((shell_number + 1)* 2);
         std::string atom_name_str = GetStringFromEnum(atom_name);

         for (const auto& name : name_vec)
         {
            xyz_file_stream << std::setw(4) << atom_name_str;
            for (const auto& coord : mCenterOfMasses.at(name))
            {
               xyz_file_stream << std::setw(30) << coord;
            }
            xyz_file_stream << "\n";
         }
      }
      xyz_file_stream.close();
   }

   /********************************************************************
    * operator<< overload for SolvationShells class
    ********************************************************************/
   std::ostream& operator<<(std::ostream& arOut, const SolvationShells& arSolvationShells)
   {
      arOut << std::left;
      arOut << std::scientific;

      arOut << "Found " << arSolvationShells.GetNumSolvationShells() << " solvation shells" << std::endl;
      for (const auto& [shell_number, monomer_names] : arSolvationShells.GetShells())
      {
         arOut << "Shell number: " << shell_number << std::endl;
         for (const auto& monomer_name : monomer_names)
         {
            arOut << monomer_name << ", ";
         }
         arOut << std::endl;
      }
      // Endline and flush after the solvation shell has been printed.
      arOut << std::endl;

      return arOut;
   }
}