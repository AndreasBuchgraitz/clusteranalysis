/********************************************************************
 * Implementations of non-templated util functions.
 ********************************************************************/

#include "MathAndUtil/Util.hpp"

namespace Util
{
   /**
    * Trim a string aka remove leading and trailing whitespace
    **/
   std::string Trim
      (  const std::string& str
      ,  const std::string& whitespace
      )
   {
      const auto strBegin = str.find_first_not_of(whitespace);

      if (strBegin == std::string::npos)
      {
         return ""; // no content
      }

      const auto strEnd = str.find_last_not_of(whitespace);
      const auto strRange = strEnd - strBegin + 1;

      return str.substr(strBegin, strRange);
   }

   /**
    * Removes anything not an alphabetical letter from the input
    * string. This is so that string atomnames can be ordered fx
    * H1, H2, H3 ...
    * But the Enum AtomName should simply be H.
    **/
   std::string TrimAtomName(const std::string& arStr)
   {
      std::string res;
      res.reserve(arStr.size());

      // Explicitly loop as unsigned char so that std::isalpha is well defined.
      for (const unsigned char c : arStr)
      {
         if (std::isalpha(c))
         {
            res.push_back(c);
         }
      }

      return res;
   }

   /**
    * Delete all whitespace in a string.
    **/
   std::string RemoveWhiteSpace(const std::string& arStr)
   {
      std::string s(arStr);
      while(s.find(" ") != s.npos) 
      {
         s.erase(s.find(" "), 1);
      }
      return s;
   }

   /**
    * Convert all characters in string to uppercase
    **/
   std::string ConvertToUpperCase(const std::string& arStr)
   {
      std::string s(arStr);
      std::transform (s.begin(), s.end(), s.begin(), 
                     [](unsigned char c){ return std::toupper(c); }
                     );
      return s;
   }

   /**
    * Get line from file and return in str.  Can take different delimeters (e.g.
    * read only until `\t` or smt.).
    * Follows interface of std::getline.
    * 
    * If we read empty line we recursively read the next one. This allows for empty lines
    * in the input file. And then we only have to check it here and not every time I define a 
    * "read" function.
    *
    * @param is    Input stream to read from.
    * @param str   On output the read newly read line.
    * @return      Returns the input stream after line has been read, such that GetLine can be looped over.
    **/
   std::istream& GetLine
      (  std::fstream& is
      ,  std::string& str
      ,  const bool aNoRecursive
      )
   {
      auto&& return_val = std::getline(is,str);
      str = Util::Trim(str);

      // If a line contains '//' interpret the rest of the line as a comment
      // If the two first characters is '//' interpret entire line as a comment and read the next line.
      std::size_t it = str.find("//");
      std::string sub_str = str.substr(0, it);

      if (!sub_str.empty() || is.eof() || aNoRecursive)
      {
         return return_val;
      }
      else
      {
         return GetLine(is, str, false);
      }
   }

   /**
    * Checks if the string is a number by checking
    * if all characters are digits.
    **/
   bool IsNumber(const std::string& arString)
   {
      return      !arString.empty() 
               && std::find_if_not
                     (  arString.begin()
                     ,  arString.end()
                     ,  [](const auto aChar)
                        {
                           return std::isdigit(aChar);
                        }
                     )  == arString.end();
   }

   /**
    * Removes all non-alphabetical characters from a string
    **/
   std::string RemoveNonAlpha(const std::string& arString)
   {
      std::string res = "";

      for (const char c : arString)
      {
         if (std::isalpha(c))
         {
            res += c;
         }
      }

      return res;
   }
}