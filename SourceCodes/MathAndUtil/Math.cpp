#include<iostream>
#include<numeric>

#include "MathAndUtil/Math.hpp"

namespace Math
{
   /********************************************************************
    * Computes the elementwise dot product of input matrices
    * 
    * Eigen Does not have a .dot() function for matrices only vectors...
    * 
    * dot = \sum_i \sum_j a_{ij} * b_{ij}
    ********************************************************************/
   double Dot(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in Dot!" << std::endl;
         exit(1);
      }

      // Compute dot product
      double sum = 0;
      for (int i = 0; i < arMat1.rows(); i++)
      {
         for (int j = 0; j < arMat1.cols(); j++)
         {
            sum += arMat1(i,j) * arMat2(i,j);
         }
      }

      return sum;
   }

   /********************************************************************
    * Computes Root mean squared deviation of two matrices
    * 
    * RMSD = \sqrt(\sum_i \sum_j (a_{ij} - b_{ij})^2 / N)
    ********************************************************************/
   double MatrixRMSD(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in MatrixRMSD!" << std::endl;
         exit(1);
      }

      // Compute sum of square of differences
      double sum = 0;
      for (int i_row = 0; i_row < arMat1.rows(); i_row++)
      {
         for (int i_col = 0; i_col < arMat1.cols(); i_col++)
         {
            sum += std::pow(arMat1(i_row, i_col) - arMat2(i_row, i_col), 2);
         }
      }

      return std::sqrt(sum / arMat1.size());
   }

   /********************************************************************
    * Computes the difference between all elements of two matricies. 
    * 
    * Difference_ij = a_{ij} - b_{ij}
    ********************************************************************/
   std::vector<double> MatrixElementDiff(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in MatrixElementDiff!" << std::endl;
         exit(1);
      }

      std::vector<double> res_vec;
      res_vec.reserve(arMat1.size());
      for (int i_row = 0; i_row < arMat1.rows(); i_row++)
      {
         for (int i_col = 0; i_col < arMat1.cols(); i_col++)
         {
            res_vec.emplace_back(arMat1(i_row, i_col) - arMat2(i_row, i_col));
         }
      }

      return res_vec;
   }

   /********************************************************************
    * Computes the euclidian distance between each row-vector of the
    * inputmatrices.
    * 
    * Distance_i = \sqrt(\sum_j (a_{ij} - b_{ij})^2)
    ********************************************************************/
   std::vector<double> MatrixDistanceRowByRow(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in MatrixDistanceRowByRow!" << std::endl;
         exit(1);
      }

      // Compute sum of square of differences
      std::vector<double> res_vec;
      res_vec.reserve(arMat1.rows());

      for (int i_row = 0; i_row < arMat1.rows(); i_row++)
      {
         res_vec.emplace_back(Math::RootSquaredDistance(arMat1.row(i_row), arMat2.row(i_row)));
      }

      return res_vec;
   }

   /********************************************************************
    * Computes the AbsoluteDistance between to matrices
    * 
    * AbsDis = \sum_i \sum_j | a_{ij} - b_{ij} |
    ********************************************************************/
   double AbsoluteDistance(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in AbsoluteDistance!" << std::endl;
         exit(1);
      }

      return std::transform_reduce
               (  arMat1.reshaped().begin(), arMat1.reshaped().end()
               ,  arMat2.reshaped().begin()
               ,  double(0.0)
               ,  std::plus<>()
               ,  [](const auto elem1, const auto elem2) { return std::abs(elem1 - elem2); }
               );   
   }

   /********************************************************************
    * Computes the "Root Squared Distance" i.e. RMSD but without the
    * normalization factor.
    * 
    * If A and B are vectors this is also simply the euclidian distance
    * between the two vectors.
    * 
    * RSD = \sqrt( \sum_i \sum_j ( a_{ij} - b_{ij} )^2 )
    ********************************************************************/
   double RootSquaredDistance(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in RootSquaredDistance!" << std::endl;
         exit(1);
      }

      // Compute sum of square of differences
      double sum = std::transform_reduce
                     (  arMat1.reshaped().begin(), arMat1.reshaped().end()
                     ,  arMat2.reshaped().begin()
                     ,  double(0.0)
                     ,  std::plus<>()
                     ,  [](const auto elem1, const auto elem2) { return std::pow(elem1 - elem2, 2); }
                     );

      return std::sqrt(sum);
   }

   /********************************************************************
    * Computes the Frobenius Distance between two matrices.
    * 
    * THIS ONLY WORKS FOR ORTHOGONAL MATRICES. USE WITH CARE.
    * https://math.stackexchange.com/a/5299
    * 
    * FroDis = \sqrt( trace( (A-B)^T * (A-B) ) )
    ********************************************************************/
   double FrobeniusDistance(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      // Sanity check
      if (arMat1.rows() != arMat2.rows() || arMat1.cols() != arMat2.cols())
      {
         std::cout << "Mismatch in sizes of input matrices in FrobeniusDistance!" << std::endl;
         exit(1);
      }
      if (arMat1.rows() != arMat2.cols())
      {
         std::cout << "Frobenius Distance only makes sense for orthogonal matrices. These are not even square !" << std::endl;
         exit(1);
      }

      double trace_test = (arMat1.transpose() * arMat2).trace();
      if ((trace_test - arMat1.rows()) > 1.0E-14)
      {
         std::cout << "Frobenius Distance only makes sense for orthogonal matrices, but (A^T*A).trace() is not close to number of rows() (or cols())" << std::endl;
         exit(1);
      }


      double tr = ((arMat1 - arMat2).transpose() * (arMat1 - arMat2)).trace();

      return std::sqrt(tr);
   }

   /********************************************************************
    * Computes the Angle between rotation matrices computed through
    * the trace of their product.
    * 
    * R_{AB} = R_A^T * R_B
    * Angle = arccos( ( Tr(R_{AB}) - 1 ) / 2)
    ********************************************************************/
   double AngleOfRotationMatrices(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      double tr = (arMat1.transpose() * arMat2).trace();

      return ArcCos((tr - 1) / 2);
   }

   /********************************************************************
    * Computes the Angle between to vectorized matrices.
    * 
    * Angle = arccos( Dot(vec(A), vec(B)) / ( \sqrt(Dot(A, A)) * \sqrt(Dot(B, B)) ) )
    ********************************************************************/
   double VectorAngle(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2)
   {
      return ArcCos(Dot(arMat1, arMat2) / (arMat1.norm() * arMat2.norm()));
   }

   /********************************************************************
    * Returns the normalized vector of input vector
    ********************************************************************/
   Eigen::VectorXd NormalizeVector(const Eigen::VectorXd& arVec)
   {
      return Eigen::VectorXd(arVec) / arVec.norm();
   }

   /********************************************************************
    * Checks if two vectors are parallel.
    * Returns boolean and integer. Integer is +1 or -1 for parallel and
    * anti-parallel respectively. Anti-parallel is counted as parallel
    * i.e. bool will be true.
    * 
    * This will be checked using 
    *    Dot(A, B) = ||A|| * ||B|| * cos(theta),
    * if A and B are normalized this reduces to
    *    Dot(A, B) = cos(theta).
    * If the vectors are parallel or anti-parallel cos(theta) will be
    * either +1 or -1. So this is what we will check.
    ********************************************************************/
   std::pair<bool, int> IsVectorsParallel(const Eigen::VectorXd& arVec1, const Eigen::VectorXd& arVec2, const double aThreshold)
   {
      // Normalize vectors
      Eigen::VectorXd vec_1 = NormalizeVector(arVec1);
      Eigen::VectorXd vec_2 = NormalizeVector(arVec2);

      // Calculate dot product
      double dot = Dot(vec_1, vec_2);

      if (std::abs(dot - 1) < aThreshold)
      {
         // Parallel
         return std::make_pair(true, +1);
      }
      else if (std::abs(dot + 1) < aThreshold)
      {
         // Anti-parallel
         return std::make_pair(true, -1);
      }
      else
      {
         // Not parallel
         return std::make_pair(false, 0);
      }
   }
}