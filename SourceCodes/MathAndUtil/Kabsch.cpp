#include<iostream>
#include<numbers>
#include<ranges>

#include "CommonAliases.hpp"
#include "MathAndUtil/Math.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "Structure/Molecule.hpp"
#include "Structure/Cluster.hpp"
#include "Structure/StructureEnums.hpp"
#include "MathAndUtil/NormalDistribution.hpp"
#include "MathAndUtil/CauchyDistribution.hpp"
#include "MathAndUtil/MaximumLogLikelihoodOptimiser.hpp"
#include "MathAndUtil/Statistics.hpp"
#include "Exceptions.hpp"

namespace Kabsch
{
   /********************************************************************
    * Kabsch algorithm to find rotation matrix between two matrices.
    * Definition found on Wikipedia, where we try to rotate P into Q.
    * 
    * Returns the rotation matrix found between the two structures.
    ********************************************************************/
   Eigen::MatrixXd Kabsch
      (  const Eigen::MatrixXd& arP
      ,  const Eigen::MatrixXd& arQ
      )
   {
      int num_atoms = arP.rows();
      int space_dim = arP.cols();

      if (  arP.rows() != arQ.rows()
         || arQ.cols() != arQ.cols()
         )
      {
         std::cout << "ERROR in Kabsch. Mismatch of input matrices/structures !" << std::endl;
      }

      // Compute the centroids for the matrices 
      const Eigen::RowVectorXd centroid_p = Kabsch::Centroid(arP);
      const Eigen::RowVectorXd centroid_q = Kabsch::Centroid(arQ);

      // Create new matrices which have been translated based on the centroids
      Eigen::MatrixXd P = Eigen::MatrixXd::Zero(arP.rows(), arP.cols());
      Eigen::MatrixXd Q = Eigen::MatrixXd::Zero(arQ.rows(), arQ.cols());
      for (int i_row = 0; i_row < num_atoms; i_row++)
      {
         P.row(i_row) = arP.row(i_row) - centroid_p;
         Q.row(i_row) = arQ.row(i_row) - centroid_q;
      }

      // Compute the covariance matrix
      Eigen::MatrixXd H = P.transpose() * Q;

      // Compute SVD of covariance matrix
      Eigen::JacobiSVD<Eigen::MatrixXd> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);

      // Get the sign of the determinant of V*U^T
      double det = Eigen::MatrixXd(svd.matrixV() * svd.matrixU().transpose()).determinant();
      int sign = Math::GetSign(det);

      if (sign == 0)
      {
         std::cout << "FOUND A DETERMINANT OF 0 IN KABSCH !" << std::endl;
         exit(1);
      }

      // Compute optimal rotation matrix and return it
      Eigen::MatrixXd almost_identity = Eigen::MatrixXd::Identity(space_dim, space_dim);
      almost_identity(space_dim - 1, space_dim - 1) = sign;

      Eigen::MatrixXd R = svd.matrixV() * almost_identity * svd.matrixU().transpose();

      return R;
   }

   /********************************************************************
    * Calculates the centroid for a matrix.
    * Here defined as the "average" of all the coordiates. So size of 
    * return vector is most often 3 (x,y,z).
    ********************************************************************/
   Eigen::RowVectorXd Centroid
      (  const Eigen::MatrixXd& arMat
      )
   {
      return Math::Centroid(arMat.rowwise().begin(), arMat.rowwise().end());
   }

   /********************************************************************
    * Compute the centroid and subtract this from the structure to
    * obtain the centered structure.
    ********************************************************************/
   Eigen::MatrixXd MoveStructureToCenter
      (  const Eigen::MatrixXd& arStructure
      )
   {
      Eigen::RowVector3d centroid = Centroid(arStructure);
      Eigen::MatrixXd translated_structure = Eigen::MatrixXd::Zero(arStructure.rows(), arStructure.cols());

      for (int i_atom = 0; i_atom < arStructure.rows(); i_atom++)
      {
         translated_structure.row(i_atom) = arStructure.row(i_atom) - centroid;
      }
   
      return translated_structure;
   }

   /********************************************************************
    * Compute Center-Of-Mass for provided structure.
    ********************************************************************/
   Eigen::RowVector3d CenterOfMass
      (  const std::vector<Structure::AtomName>& arAtomNames
      ,  const Eigen::MatrixXd& arAtomPositions
      )
   {
      // Small sanity check
      if (arAtomNames.size() != Uin(arAtomPositions.rows()))
      {
         std::cerr << "Provided atom names and position indicate different number of atoms ! (Kabsch::CenterOfMass())" << std::endl;
         exit(1);
      }
      Eigen::RowVector3d com(0, 0, 0);
      double tot_mass = 0;
      for (Uin i = 0; i < arAtomNames.size(); i++)
      {
         com += Structure::GetMass(arAtomNames[i]) * arAtomPositions.row(i);
         tot_mass += Structure::GetMass(arAtomNames[i]);
      }

      com /= tot_mass;

      return com;
   }

   /********************************************************************
    * Calculates the structure for a matrix after rotating it based on
    * the given rotation matrix.
    * 
    * @param[in] arStructureMat
    *    Eigen::MatrixXd object for the xyz coordinates for given
    *    structure
    * @param[in] arRotMat
    *    Eigen::MatrixXd object for the rotation matrix we want to apply
    * 
    * @return
    *    Returns the Eigen::MatrixXd object corresponding to the rotated
    *    structure.
    *    IF we have NON_HYDROGEN_NON_HYDROGEN then the matrix will
    *    have number of rows equal to arNonHydrogenAtoms.size().
    ********************************************************************/
   Eigen::MatrixXd GetRotatedStructure
      (  const Eigen::MatrixXd& arStructureMat
      ,  const Eigen::MatrixXd& arRotMat
      )
   {
      // Get the centroid for the structure matrix
      Eigen::RowVectorXd centroid = Kabsch::Centroid(arStructureMat);

      // Construct new empty matrix to contain result.
      Eigen::MatrixXd rotated_structure;

      rotated_structure = Eigen::MatrixXd::Zero(arStructureMat.rows(), arStructureMat.cols());
      // Subtract the centroid from all atoms
      for (int i_atom = 0; i_atom < arStructureMat.rows(); i_atom++)
      {
         rotated_structure.row(i_atom) = arStructureMat.row(i_atom) - centroid;
      }
 
      // Perform rotation
      for (int i_atom = 0; i_atom < arStructureMat.rows(); i_atom++)
      {
         rotated_structure.row(i_atom) = arRotMat * Eigen::VectorXd(rotated_structure.row(i_atom));
      }

      return rotated_structure;
   }

   /********************************************************************
    * Function to construct the matrices used for the Kabsch algorithm
    * based on the inputoption.
    * 
    * Will return the pair indexed as: 
    * <cluster molecule, isolated molecule>
    ********************************************************************/
   std::pair<Eigen::MatrixXd, Eigen::MatrixXd> 
   SetupMoleculeMatsForKabsch
      (  const Structure::Molecule& arMolecule1
      ,  const Structure::Molecule& arMolecule2
      ,  const Structure::InputOptions& arInputOption
      )
   {
      if (arMolecule1.GetNumAtoms() != arMolecule2.GetNumAtoms())
      {
         std::cerr << "ERROR ! Mismatch in number of atoms in input molecules in \"SetupMoleculeMatsForKabsch\"" << std::endl;
         std::cerr << "arMolecule1:\n" << arMolecule1 << std::endl;
         std::cerr << "arMolecule2:\n" << arMolecule2 << std::endl;
         exit(1);
      }

      // We want to compute the rotation matrices based on all the atoms
      if (arInputOption == Structure::InputOptions::ALL_ALL)
      {
         return std::make_pair(arMolecule1.GetEigenMat(), arMolecule2.GetEigenMat());
      }
      // We only want to compute the rotation matrices based on the non hydrogen atoms
      else if  (  arInputOption == Structure::InputOptions::NON_HYDROGEN_ALL
               || arInputOption == Structure::InputOptions::NON_HYDROGEN_NON_HYDROGEN
               )
      {
         // Small sanity check
         for (Uin i_atom = 0; i_atom < arMolecule1.GetNumAtoms(); i_atom++)
         {
            if (arMolecule1.GetNonHydrogenIndex()[i_atom] != arMolecule2.GetNonHydrogenIndex()[i_atom])
            {
               std::cerr << "Unexpected disagreement between the two compared Molecules" << std::endl;
            }
         }
      
         return std::make_pair
            (  arMolecule1.GetEigenMat(arMolecule1.GetNonHydrogenIndex())
            ,  arMolecule2.GetEigenMat(arMolecule2.GetNonHydrogenIndex())
            );
      }
      else
      {
         std::cerr << "ERROR: Unknown option for Kabsch !" << std::endl;
         exit(1);
      }
   }

   /********************************************************************
    * Function to compute RMSD value between the structure of two
    * Molecule's. Before computing RMSD value it will make sure to
    * rotate the structures to the same coordinate system.
    ********************************************************************/
   double MoleculeRMSD
      (  const Structure::Molecule& arMolecule1
      ,  const Structure::Molecule& arMolecule2
      ,  const Structure::InputOptions& arInputOption
      )
   {
      // Get the eigen matrices to use for computing rotation matrices.
      const auto& [molecule1_eigen_mat, molecule2_eigen_mat] = SetupMoleculeMatsForKabsch
         (  arMolecule1
         ,  arMolecule2
         ,  arInputOption
         );
      
      // Compute rotation matrix
      Eigen::MatrixXd rot_mat = Kabsch::Kabsch(molecule1_eigen_mat, molecule2_eigen_mat);

      // Get the rotated structure
      Eigen::MatrixXd rotated_molecule = Kabsch::GetRotatedStructure(molecule1_eigen_mat, rot_mat);

      // Compute RMSD (remembering to move the second structure to center)
      return Math::MatrixRMSD(rotated_molecule, MoveStructureToCenter(molecule2_eigen_mat));
   }

   /********************************************************************
    * Function to compute RMSD value between two Cluster's.
    * 
    * The RMSD is simply computed on atom basis
    ********************************************************************/
   RmsdData SimpleClusterRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const std::vector<Structure::Cluster>& arClusterCompare
      )
   {
      // Initialize output object
      RmsdData res_rmsd(arClusterRef.GetId(), arClusterRef.GetClusterMolecule());

      for (const Structure::Cluster& cluster : arClusterCompare)
      {
         // Sanity check the two clusters.
         if (  arClusterRef.GetName() != cluster.GetName()
            )
         {
            std::cerr << "Unequal number of monomers in two clusters (from SimpleClusterRMSD) !\n"
               << "arClusterRef.GetName() = " << arClusterRef.GetName() << "\n"
               << "arClusterRef.GetNumMonomers() = " << arClusterRef.GetNumMonomers() << "\n"
               << "arClusterRef.GetMethod() = " << arClusterRef.GetMethod() << "\n"
               << "arClusterRef.GetBasisSet() = " << arClusterRef.GetBasisSet() << "\n"
               << "cluster.GetName() = " << cluster.GetName() << "\n"
               << "cluster.GetNumMonomers() = " << cluster.GetNumMonomers() << "\n"
               << "cluster.GetMethod() = " << cluster.GetMethod() << "\n"
               << "cluster.GetBasisSet() = " << cluster.GetBasisSet() << "\n"
               << std::endl;
            exit(1);
         }

         res_rmsd.AddComparedMethodAndRMSD
            (  cluster.GetId()
            ,  cluster.GetConformerEnum()
            ,  std::vector<double>
                  {  Kabsch::MoleculeRMSD
                     (  arClusterRef.GetClusterMolecule()
                     ,  cluster.GetClusterMolecule()
                     ,  Structure::InputOptions::ALL_ALL
                     )
                  }
            );

         // DEBUG
         const auto& [id, conf_enum, rmsd_vec] = res_rmsd.back();
         if (rmsd_vec[0] < 0.1 && conf_enum == Structure::ConformerEnum::DifferentFromReference)
         {
            std::cout << "Found cluster marked as DifferentFromReference, with very low rmsd\n"
               << id
               << "RMSD = " << rmsd_vec[0]
               << std::endl;
         }
      }
   
      return res_rmsd;
   }

   /********************************************************************
    * Function to compute RMSD value between two Cluster's.
    * 
    * Before computing RMSD value it will make sure to
    * rotate the structures to the same coordinate system.
    ********************************************************************/
   RmsdData ClusterRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const std::vector<Structure::Cluster>& arClusterCompare
      ,  const Structure::InputOptions& arKabschHydrogen
      ,  const std::vector<Eigen::Matrix3d>& arRotMats
      )
   {
      // Initialize output object
      RmsdData res_rmsd(arClusterRef.GetId(), arClusterRef.GetClusterMolecule());

      for (const Structure::Cluster& cluster : arClusterCompare)
      {
         // FIX-ME: No other option will work right now, as the rotation matrix stuff has changed to be a member (and the order regarding ref vs compared).
         Structure::KabschInputOptions input_enum = Structure::KabschInputOptions::COMPARE_MONOMER_ROT_MATS_ACROSS;

         if (input_enum == Structure::KabschInputOptions::COMPARE_MONOMER_STRUCTURES)
         {
            res_rmsd.AddComparedMethodAndRMSD
               (  cluster.GetId()
               ,  cluster.GetConformerEnum()
               ,  Kabsch::RMSD
                     (  arClusterRef.GetMonomersIterConstBegin()
                     ,  arClusterRef.GetMonomersIterConstEnd()
                     ,  cluster.GetMonomersIterConstBegin()
                     ,  cluster.GetMonomersIterConstEnd()
                     ,  arKabschHydrogen
                     )
               );
         }
         else if (input_enum == Structure::KabschInputOptions::COMPARE_MONOMER_ROT_MATS_DIRECTLY)
         {
            // Compute and save all rotation matrices.
            std::vector<std::pair<std::string, Eigen::MatrixXd>> rot_mats = ComputeAllRotMats(arClusterRef, cluster, arKabschHydrogen);

            // Compute RMSD between all pairs of rotation matrices.
            res_rmsd.AddComparedMethodAndRMSD
               (  cluster.GetId()
               ,  cluster.GetConformerEnum()
               ,  ComputeAllRotMatRmsd(rot_mats)
               );
         }
         else if (input_enum == Structure::KabschInputOptions::COMPARE_CLUSTER_STRUCTURES)
         {
            std::cerr << "Not implemented yet !" << std::endl;
         }
         else if (input_enum == Structure::KabschInputOptions::COMPARE_INDIVIDUAL_ATOMS)
         {
            res_rmsd.AddComparedMethodAndRMSD
               (  cluster.GetId()
               ,  cluster.GetConformerEnum()
               ,  ComputeRmsdForEachAtom(arClusterRef.GetClusterMolecule(), cluster.GetClusterMolecule())
               );   
         }
         else if (input_enum == Structure::KabschInputOptions::COMPARE_MONOMER_ROT_MATS_ACROSS)
         {
            res_rmsd.AddComparedMethodAndRMSD
               (  cluster.GetId()
               ,  cluster.GetConformerEnum()
               ,  ComputeRotMatRmsdAcross(arClusterRef, cluster, arRotMats)
               );
         }
         else
         {
            std::cerr << "Unknown option ??" << std::endl;
         }
      }
   
      return res_rmsd;
   }

   /********************************************************************
    * Compute the difference in dipolemoments for reference cluster
    * and all the other clusters.
    ********************************************************************/
   RmsdData ClusterDipoleMomentRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const std::vector<Structure::Cluster>& arClusterCompare
      ,  const std::vector<Eigen::Matrix3d>& arRotMats
      )
   {
      // Initialize output object
      RmsdData res_rmsd(arClusterRef.GetId(), arClusterRef.GetClusterMolecule());

      for (const Structure::Cluster& cluster : arClusterCompare)
      {
         res_rmsd.AddComparedMethodAndRMSD
            (  cluster.GetId()
            ,  cluster.GetConformerEnum()
            ,  ComputeDipoleMomentRMSD(arClusterRef, cluster, arRotMats)
            );
      }

      return res_rmsd;
   }

   /********************************************************************
    * Function to compute all rotation matrices between every single
    * monomer.
    ********************************************************************/
   std::vector<std::pair<std::string, Eigen::MatrixXd>> ComputeAllRotMats
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  const Structure::InputOptions& arInputOption
      )
   {
      std::vector<std::pair<std::string, Eigen::MatrixXd>> res_vec;
      res_vec.reserve(arClusterRef.GetNumMonomers());

      // Loop over monomers
      auto it_ref = arClusterRef.GetMonomersIterConstBegin();
      auto it_com = arClusterCompare.GetMonomersIterConstBegin();
      auto it_ref_end = arClusterRef.GetMonomersIterConstEnd();
      auto it_com_end = arClusterCompare.GetMonomersIterConstEnd();
      for(
         ;  (it_ref != it_ref_end) && (it_com != it_com_end)
         ;  it_ref++, it_com++
         )
      {
         // Sanity check
         if (*it_ref != *it_com)
         {
            std::cerr << "Molecules are not ordered in the same way, about to compare:\n"
               << "it_ref->GetName() = " << it_ref->GetName() << "\n"
               << "it_com->GetName() = " << it_com->GetName() << "\n"
               << "it_ref->GetNumAtoms() = " << it_ref->GetNumAtoms() << "\n"
               << "it_com->GetNumAtoms() = " << it_com->GetNumAtoms() << "\n"
               << "In ComputeAllRotMats()"
               << std::endl;
            exit(1);
         }

         // Get the eigen matrices to use for computing rotation matrices.
         const auto& [molecule1_eigen_mat, molecule2_eigen_mat] = SetupMoleculeMatsForKabsch
            (  *it_ref
            ,  *it_com
            ,  arInputOption
            );
      
         // Compute rotation matrix
         res_vec.emplace_back
            (  it_ref->GetName()
            ,  Kabsch::Kabsch(molecule1_eigen_mat, molecule2_eigen_mat)
            );
      }

      return res_vec;
   }

   /********************************************************************
    * Compute RMSD values between all pairs of rotation matrices.
    * 
    * For now we "throw away" all the monomer names, these might be
    * used later, if we want additional information about which monomers
    * are different...
    ********************************************************************/
   std::vector<double> ComputeAllRotMatRmsd
      (  const std::vector<std::pair<std::string, Eigen::MatrixXd>>& arRotMats
      )
   {
      // Initialize output vector
      std::vector<double> res_vec;
      // Number of pairs goes as binomial coef (n choose 2).
      int num_pairs = (arRotMats.size() - 1) * arRotMats.size() / int(2);
      res_vec.reserve(num_pairs);

      // Loop over all unique pairs of rotation matrices.
      for (Uin i = 0; i < arRotMats.size(); i++)
      {
         for (Uin j = i + 1; j < arRotMats.size(); j++)
         {
            const Eigen::MatrixXd& mat1 = arRotMats[i].second;
            const Eigen::MatrixXd& mat2 = arRotMats[j].second;

            res_vec.emplace_back(Math::AngleOfRotationMatrices(mat1, mat2));
         }
      }

      return res_vec;
   }

   /********************************************************************
    * Compute the RMSD for each atom in two molecules
    ********************************************************************/
   std::vector<double> ComputeRmsdForEachAtom
      (  const Structure::Molecule& arMolecule1
      ,  const Structure::Molecule& arMolecule2
      )
   {
      // Get the eigen matrices to use for computing rotation matrices.
      const auto& [molecule1_eigen_mat, molecule2_eigen_mat] = SetupMoleculeMatsForKabsch
         (  arMolecule1
         ,  arMolecule2
         ,  Structure::InputOptions::ALL_ALL
         );
      
      // Compute rotation matrix
      Eigen::MatrixXd rot_mat = Kabsch::Kabsch(molecule1_eigen_mat, molecule2_eigen_mat);

      // Get the rotated structure
      Eigen::MatrixXd rotated_molecule = Kabsch::GetRotatedStructure(molecule1_eigen_mat, rot_mat);

      // Compute rmsd for each atom.
      std::vector<double> atom_rmsd;
      atom_rmsd.reserve(arMolecule1.GetNumAtoms());

      for (Uin i_atom = 0; i_atom < arMolecule1.GetNumAtoms(); i_atom++)
      {
         atom_rmsd.emplace_back(Math::MatrixRMSD(rotated_molecule.row(i_atom), molecule2_eigen_mat.row(i_atom)));
      }

      return atom_rmsd;
   }

   /********************************************************************
    * Checks the Cluster idx for two molecules and returns the vector
    * of pairs for indices which corresponds to the same atom (in the
    * cluster) for the two molecules.
    ********************************************************************/
   std::vector<std::pair<Uin, Uin>> GetOverlappingMoleculeIdx
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      )
   {
      // Result vector
      std::vector<std::pair<Uin, Uin>> mol_idx_vec;

      for (const auto& [mol_1_idx, cluster_1_idx] : arMol1.GetClusterIdxVec())
      {
         for (const auto& [mol_2_idx, cluster_2_idx] : arMol2.GetClusterIdxVec())
         {
            if (cluster_1_idx == cluster_2_idx)
            {
               mol_idx_vec.emplace_back(mol_1_idx, mol_2_idx);
               break;
            }
         }
      }

      return mol_idx_vec;
   }

   /********************************************************************
    * Construct the atomic positions matrices for the overlapping
    * Molecule indices for two Molecules.
    ********************************************************************/
   std::pair<Eigen::MatrixX3d, Eigen::MatrixX3d> GetOverlappingMoleculePositions
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      )
   {
      const auto overlap_idx = GetOverlappingMoleculeIdx(arMol1, arMol2);

      std::pair<Eigen::MatrixX3d, Eigen::MatrixX3d> res_pair;
      Eigen::MatrixX3d& mat_1 = res_pair.first;
      Eigen::MatrixX3d& mat_2 = res_pair.second;

      mat_1 = Eigen::MatrixX3d::Zero(overlap_idx.size(), 3);
      mat_2 = Eigen::MatrixX3d::Zero(overlap_idx.size(), 3);

      for (Uin i = 0; i < overlap_idx.size(); i++)
      {
         mat_1.row(i) = arMol1.GetCoordinate(overlap_idx[i].first);
         mat_2.row(i) = arMol2.GetCoordinate(overlap_idx[i].second);
      }

      return res_pair;
   }


   /********************************************************************
    * Computes the RMSD bewteen atoms after the given monomer has been
    * rotated according to a rotation matrix obtained from another
    * monomer-pair.
    * 
    * For each monomer-pair (M1):
    *    compute rotation matrix A
    *    For each other monomer-pair (M2):
    *       apply A to M2 and compute RMSD
    * 
    * Either we compute RMSD for individual atoms or at the monomer
    * level. (I would like to do it at the monomer level, but we also
    * need enough data to properly create NormalDistribution).
    ********************************************************************/
   std::vector<double> ComputeRotMatRmsdAcross
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  const std::vector<Eigen::Matrix3d>& arRotMats
      )
   {
      try
      {
         return ComputeRmsdAcrossMonomers
                  (  arClusterRef
                  ,  arClusterCompare
                  ,  [](const Structure::Molecule& arMol1, const Structure::Molecule& arMol2) -> std::vector<double>
                     {
                        return ComputeAtomicPositionRMSD(arMol1, arMol2);
                     }
                  ,  arRotMats
                  );
      }
      catch(NeedClusterInformation& err)
      {
         std::cerr << "Found NeedClusterInformation when ComputeRmsdAcrossMonomers" << std::endl;
         std::cerr << err.what() << std::endl;
         std::cerr << "arClusterRef.GetId():\n" << arClusterRef.GetId() << std::endl;
         for(  auto it_monomer = arClusterRef.GetMonomersIterConstBegin()
            ;  it_monomer != arClusterRef.GetMonomersIterConstEnd()
            ;  it_monomer++
            )
         {
            std::cerr << "it_monomer->GetName() = " << it_monomer->GetName() << std::endl;
         }
         std::cerr << "arClusterCompare.GetId():\n" << arClusterCompare.GetId() << std::endl;
         for(  auto it_monomer = arClusterCompare.GetMonomersIterConstBegin()
            ;  it_monomer != arClusterCompare.GetMonomersIterConstEnd()
            ;  it_monomer++
            )
         {
            std::cerr << "it_monomer->GetName() = " << it_monomer->GetName() << std::endl;
         }
         exit(1);
      }
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::vector<double> ComputeAtomicPositionRMSD
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      )
   {
      std::vector<std::pair<Uin, Uin>> mol_idx_vec = GetOverlappingMoleculeIdx(arMol1, arMol2);

      if (mol_idx_vec.empty())
      {
         std::stringstream ss;
         ss << "Apparently no overlap in molecule idx for molecules:\n"
            << arMol1 << "\n"
            << arMol2 << "\n"
            << std::endl;
         throw(NeedClusterInformation(ss.str()));
      }

      std::vector<double> res_vec;
      for (const auto& [mol1_idx, mol2_idx] : mol_idx_vec)
      {
         Eigen::RowVector3d diff_vec = arMol1.GetCoordinate(mol1_idx) - arMol2.GetCoordinate(mol2_idx);
         for (const double elem : diff_vec)
         {
            res_vec.emplace_back(elem);
         }
      }

      return res_vec;
   }

   /********************************************************************
    * Computes the RMSD bewteen dipole-moment across mononers
    * 
    * For each monomer-pair (M1):
    *    compute rotation matrix A
    *    For each other monomer-pair (M2):
    *       apply A to M2 and compute RMSD
    ********************************************************************/
   std::vector<double> ComputeDipoleMomentRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  const std::vector<Eigen::Matrix3d>& arRotMats
      )
   {
      return ComputeRmsdAcrossMonomers
               (  arClusterRef
               ,  arClusterCompare
               ,  [](const Structure::Molecule& arMol1, const Structure::Molecule& arMol2) -> std::vector<double> 
                  {
                     return ComputeDipoleMomentRMSD(arMol1, arMol2);
                  }
               ,  arRotMats
               );
   }
   
   /********************************************************************
    * Computes the dot product of the normalized vectors, which means
    * we save the angle between them as cos(theta), instead of theta
    * itself.
    * 
    * Returns std::vector<double> for the interface to work, with the
    * other RMSD methods here, even though it is a single number for
    * this function.
    * 
    * Only computes something if the Molecules actually have a dipole
    * otherwise it simply returns cos() = 1 to not create chaos in the
    * following analysis.
    ********************************************************************/
   std::vector<double> ComputeDipoleMomentRMSD
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      )
   {
      if (arMol1.HaveDipole() && arMol2.HaveDipole())
      {
         // As there might be proton transfer between methods, we will only consider atoms which are present in both molecules.
         std::vector<std::pair<Uin, Uin>> common_idx = GetOverlappingMoleculeIdx(arMol1, arMol2);
         auto idx_1_range = std::views::keys(common_idx);
         auto idx_2_range = std::views::values(common_idx);

         std::vector<Uin> idx_1_vec(idx_1_range.begin(), idx_1_range.end());
         std::vector<Uin> idx_2_vec(idx_2_range.begin(), idx_2_range.end());

         std::map<Uin, std::vector<Uin>> con_map_1 = arMol1.AddHydrogensToConnectivityMap(arMol1.GetAtomConnectivityMap());
         std::map<Uin, std::vector<Uin>> con_map_2 = arMol2.AddHydrogensToConnectivityMap(arMol2.GetAtomConnectivityMap());

         // Clean the connectivity map of any atom idx not present in both molecules.
         std::map<Uin, std::vector<Uin>> con_map_1_clean;
         for (const Uin atom_idx : idx_1_vec)
         {
            if (con_map_1.contains(atom_idx))
            {
               std::vector<Uin> con_atoms;
               for (const Uin con_atom : con_map_1.at(atom_idx))
               {
                  if (std::find(idx_1_vec.begin(), idx_1_vec.end(), con_atom) != idx_1_vec.end())
                  {
                     con_atoms.emplace_back(con_atom);
                  }
               }
               con_map_1_clean.emplace(atom_idx, con_atoms);
            }
         }

         std::map<Uin, std::vector<Uin>> con_map_2_clean;
         for (const Uin atom_idx : idx_2_vec)
         {
            if (con_map_2.contains(atom_idx))
            {
               std::vector<Uin> con_atoms;
               for (const Uin con_atom : con_map_2.at(atom_idx))
               {
                  if (std::find(idx_2_vec.begin(), idx_2_vec.end(), con_atom) != idx_2_vec.end())
                  {
                     con_atoms.emplace_back(con_atom);
                  }
               }
               con_map_2_clean.emplace(atom_idx, con_atoms);
            }
         }

         // Compute new dipoles
         Structure::DipoleMoment dip_1(arMol1.GetAtomNames().first, arMol1.GetAtomPositions(), con_map_1_clean);
         Structure::DipoleMoment dip_2(arMol2.GetAtomNames().first, arMol2.GetAtomPositions(), con_map_2_clean);

         // Normalize the dipoles
         auto norm_dip_1 = Math::NormalizeVector(dip_1.GetDipoleMoment());
         auto norm_dip_2 = Math::NormalizeVector(dip_2.GetDipoleMoment());

         // Compute cos(theta)
         return std::vector<double>{ Math::Dot(norm_dip_1, norm_dip_2) };
      }
      else
      {
         // Simply return cos(theta) = 1. This should not polute anything, i hope.
         return std::vector<double>{1.0};
      }
   }

   /********************************************************************
    * Wrapper to compute RMSD for all the monomers across. That is for
    * each monomer pair compute the rot-mat and apply this to all others
    * and then compute the RMSD, for all monomer pairs.
    * 
    * Takes function on input which should take to molecule matricies
    * and return a std::vector<double>.
    * 
    * For each monomer-pair (M1):
    *    compute rotation matrix A
    *    For each other monomer-pair (M2):
    *       apply A to M2 and compute RMSD
    ********************************************************************/
   template<typename FUNC>
   std::vector<double> ComputeRmsdAcrossMonomers
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  FUNC aFuncForRMSD
      ,  const std::vector<Eigen::Matrix3d>& arRotMats
      )
   {
      // Result vector
      std::vector<double> res_vec;
      res_vec.reserve(arClusterRef.GetNumMonomers() * arClusterRef.GetNumMonomers());

      // Loop over monomers
      const auto it_ref_begin = arClusterRef.GetMonomersIterConstBegin();
      const auto it_com_begin = arClusterCompare.GetMonomersIterConstBegin();
      const auto it_ref_end = arClusterRef.GetMonomersIterConstEnd();
      const auto it_com_end = arClusterCompare.GetMonomersIterConstEnd();
      for(  auto it_ref_1 = it_ref_begin, it_com_1 = it_com_begin
         ;  (it_ref_1 != it_ref_end) && (it_com_1 != it_com_end)
         ;  it_ref_1++, it_com_1++
         )
      {
         // Compute rotation matrix
         const Eigen::Matrix3d& rot_mat = arRotMats.empty()
                                        ? it_com_1->GetMoleculeRotationMatrix()
                                        : arRotMats[std::distance(it_com_begin, it_com_1)];

         for(  auto it_ref_2 = it_ref_begin, it_com_2 = it_com_begin
            ;  (it_ref_2 != it_ref_end) && (it_com_2 != it_com_end)
            ;  it_ref_2++, it_com_2++
            )
         {
            const Eigen::MatrixX3d& com_2_mat = it_com_2->GetEigenMat();
            const Eigen::MatrixX3d& ref_2_mat = it_ref_2->GetEigenMat();

            // Get the rotated structure
            Eigen::MatrixXd rotated_molecule = Kabsch::GetRotatedStructure(com_2_mat, rot_mat);

            auto it_com_2_cluster_idx = std::views::values(it_com_2->GetClusterIdxVec());
            auto it_ref_2_cluster_idx = std::views::values(it_ref_2->GetClusterIdxVec());
            std::vector<Uin> com_2_cluster_idx_vec(it_com_2_cluster_idx.begin(), it_com_2_cluster_idx.end());
            std::vector<Uin> ref_2_cluster_idx_vec(it_ref_2_cluster_idx.begin(), it_ref_2_cluster_idx.end());
            std::vector<double> rmsd_vec = aFuncForRMSD
               (  Structure::Molecule(it_com_2->GetName(), it_com_2->GetAtomNames(), rotated_molecule, com_2_cluster_idx_vec)
               ,  Structure::Molecule(it_ref_2->GetName(), it_ref_2->GetAtomNames(), MoveStructureToCenter(ref_2_mat), ref_2_cluster_idx_vec)
               );

            res_vec.insert
               (  res_vec.end()
               ,  rmsd_vec.begin()
               ,  rmsd_vec.end()
               );
         }
      }

      return res_vec;
   }

   /********************************************************************
    * RmsdData Constructor
    ********************************************************************/
   RmsdData::RmsdData
      (  const Structure::StructureIdentification& arRefId
      ,  const Structure::Molecule& arRefMolecule
      )
      :  mReferenceId(arRefId)
      ,  mReferenceMolecule(arRefMolecule)
   {
   }

   /********************************************************************
    * Add Id's and RMSD values to the mComparedMethods member.
    * 
    * Take copy of RMSD values so we can sort them here, for easier
    * starting guess later.
    ********************************************************************/
   void RmsdData::AddComparedMethodAndRMSD
      (  const Structure::StructureIdentification& arId
      ,  const Structure::ConformerEnum arEnum
      ,  std::vector<double> arRMSD
      )
   {
      // Only include data if there are any.
      if (!arRMSD.empty())
      {
         std::sort(arRMSD.begin(), arRMSD.end());
         mComparedMethods.emplace_back(arId, arEnum, arRMSD);
      }
   }

   /********************************************************************
    * Will analyze the contained RMSD and return the ConformerEnum for
    * the cluster for the given ID.
    * 
    * Will try to fit the given data to a Cauchy distribution if this
    * succeeds with a low Anderson-Darling statistic we assume it is
    * the same conformer.
    ********************************************************************/
   std::map<Structure::StructureIdentification, Structure::ConformerEnum> RmsdData::CheckConformersByAtomicPositions
      (  const std::size_t aNumMonomers
      ,  const std::size_t aSizeSmallestMonomer
      ,  const double aSmallestBondLength
      )  const
   {
      std::map<Structure::StructureIdentification, Structure::ConformerEnum> res_map;

      for (const auto& [id, conf_enum, rmsd] : mComparedMethods)
      {
         //std::cout << "Inside CheckConformersByAtomicPositions" << std::endl;
         //std::cout << id << std::flush;

         using data_cont = std::decay_t<decltype(rmsd)>;
         using distribution = Math::CauchyDistribution<data_cont>;
         using optimiser = Math::MaximumLogLikelihoodOptimiser<data_cont, distribution>;

         std::vector
            <  Math::MaximumLogLikelihood
                  <  data_cont
                  ,  distribution
                  >
            > max_likelihoods;

         // For Cauchy we only try to fit to 1 distribution.
         constexpr std::size_t loop_end = std::is_same_v<distribution, Math::CauchyDistribution<data_cont>>
                                        ? 2
                                        : aNumMonomers
                                        ;

         // New way
         for (Uin i = 1; i < loop_end; i++)
         {
            optimiser max_likelihood_opti(rmsd, i, 1.0E-5);

            // Check if the optmisation converged, and do nothing if it did not.
            if (!max_likelihood_opti.Converged())
            {
               break;
            }

            const auto& max_likelihood = max_likelihood_opti.GetMaxLogLikelihood();

            max_likelihoods.push_back(max_likelihood);
         }

         auto anderson_darling_vec = Math::ComputeAndersonDarlingBySummation(max_likelihoods);

         // 0.05 is 5 percentage significance level.
         std::vector<bool> one_normal_distribution = Math::CompareAndersonDarlingStatistics(anderson_darling_vec, 0.05);


         // LOOSEN THE ALL_OF < 0.32 TO LET MIN/MAX(3, NUMATOMS*0.05) BE LARGER THAN THIS
         // 3 IS A GOOD NUMBER, IT ALLOWS 1 SINGLE ATOM (OR TECHNICALLY 3 POSITION COMPONENTS)
         // BE DIFFERENT FROM THE REFERENCE
         Uin num_atom_coords_large_deviation = std::count_if
            (  rmsd.begin(), rmsd.end()
            ,  [&aSmallestBondLength](const auto& val) { return val > aSmallestBondLength / 3.0; }
            );

         /********************************************************************
          * If all Anderson-Darling statistics claim this is well described
          * by a Cauchy distribution OR if all rmsd values are smaller than
          * 1/3 of the smallest bond length in the cluster Molecule then we
          * claim this conformer is the same as the reference.
          * 
          * So if all components of the position vector differences are
          * smaller than this, it is deemed very unlikely to be to different
          * conformers.
          ********************************************************************/
         if (  (std::all_of
                  (  one_normal_distribution.begin()
                  ,  one_normal_distribution.end()
                  ,  [](const bool aInp) { return aInp; }
                  )
               && !anderson_darling_vec.empty() // Empty means optimiser did not converge for 1 distribution
               )
            || num_atom_coords_large_deviation < std::min(3 * aNumMonomers, 3 * (aSizeSmallestMonomer - 1))
            )
         {
            res_map.emplace(id, Structure::ConformerEnum::SameAsReference);
         }
         else
         {
            res_map.emplace(id, Structure::ConformerEnum::DifferentFromReference);
         }
      }

      return res_map;
   }

   /********************************************************************
    * Checks the conformers based on the dipole-moment-rmsd.
    * This is mostly used as a rough check, if any computed angle is
    * crazy large (> 40 degree) it is deemed as DifferentFromReference.
    * I.e. no distribution fit as there are very few data points, and
    * the distribution is weird. Only defined in interval [0, 1] it
    * seems. And highest density close to 1.
    ********************************************************************/
   std::map<Structure::StructureIdentification, Structure::ConformerEnum> RmsdData::CheckConformersByDipoleMoment
      (  [[maybe_unused]] const std::size_t aNumMonomers
      )  const
   {
      std::map<Structure::StructureIdentification, Structure::ConformerEnum> res_map;

      for (const auto& [id, conf_enum, rmsd] : mComparedMethods)
      {
         using data_cont = std::decay_t<decltype(rmsd)>;
         using distribution = Math::NormalDistribution<data_cont>;
         using max_log_likelihood = Math::MaximumLogLikelihood<data_cont, distribution>;

         std::vector<max_log_likelihood> max_likelihoods;

         //std::cout << "Inside CheckConformersByDipoleMoment" << std::endl;
         //std::cout << id << std::flush;
         
         // If all cos(theta) > 0.96 all angles are less than 16.26 degrees, it is definitely the same conformer
         if (  std::all_of
                  (  rmsd.begin()
                  ,  rmsd.end()
                  ,  [](const auto aRmsdVal)
                     {
                        return aRmsdVal > 0.96;
                     }
                  )
            )
         {
            res_map.emplace(id, Structure::ConformerEnum::DefinitelyTheSameAsReference);
         }
         else if (  std::all_of
                  (  rmsd.begin()
                  ,  rmsd.end()
                  ,  [](const auto aRmsdVal)
                     {
                        return aRmsdVal > 0.76;
                     }
                  )
            ) // If all angles lies in [0.76, 0.96] the angles are between 16.26 and 40.54 degrees. It is the same conformer is the atomic positions agree on this.
         {
            res_map.emplace(id, Structure::ConformerEnum::SameAsReference);
         }
         else // There are angles larger than 40.54 degree, it is probably not the same conformer.
         {
            res_map.emplace(id, Structure::ConformerEnum::DifferentFromReference);
         } 
      }

      return res_map;
   }

   /********************************************************************
    * Prints out the data in the RmsdData object for use in plotting a 
    * frequency histogram.
    ********************************************************************/
   void RmsdData::PrintHistogramData(const std::filesystem::path& arDataDir) const
   {
      // Make sure the data directory exists before we start creating files in it.
      if (!std::filesystem::exists(arDataDir))
      {
         std::filesystem::create_directories(arDataDir);
      }

      // Create output stream
      std::fstream out_stream;
      const Structure::StructureIdentification& id = this->GetRefId();
      std::string filename = "HistogramData_" + id.GetName() + "_" + id.GetCompMethod();
      if (!id.GetBasisSet().empty())
      {
         filename += "_" + id.GetBasisSet();
      }
      filename += ".dat";
      std::filesystem::path out_filename = arDataDir / filename;

      // Open output stream and start writing data to it.
      out_stream.open(out_filename, std::ios_base::out);

      for (const auto& [compared_id, conf_enum, data_vec] : *this)
      {
         out_stream << "#" + compared_id.GetName() + ", " + compared_id.GetCompMethod();
         if (!compared_id.GetBasisSet().empty())
         {
            out_stream << ", " + compared_id.GetBasisSet();
         }
         out_stream << std::endl;

         for (const double& rmsd : data_vec)
         {
            out_stream << rmsd << std::endl;
         }

         out_stream << "\n" << std::endl;
      }

      out_stream.close();
   }

   /********************************************************************
    * 
    ********************************************************************/
   void ConvertSimpleRmsdDataToTableAndPrint
      (  const std::map<std::string, RmsdData>& arRmsdDataMap
      ,  const std::filesystem::path& arPath
      ,  const std::string& arFileSuffix
      )
   {
      std::vector<std::string> header = {"Cluster"};
      auto map_it = arRmsdDataMap.begin();
      for (const auto& [id, conf_enum, rmsd_vec] : map_it->second)
      {
         std::string s = id.GetBasisSet().empty() 
                       ? id.GetCompMethod() 
                       : id.GetCompMethod() + "/" + id.GetBasisSet();

         header.emplace_back(s);
      }

      std::vector<std::vector<std::string>> same_as_reference_table_data;
      std::vector<std::vector<std::string>> different_from_reference_table_data;
      for (const auto& [cluster_name, rmsd_data] : arRmsdDataMap)
      {
         same_as_reference_table_data.emplace_back(std::vector<std::string>{cluster_name});
         different_from_reference_table_data.emplace_back(std::vector<std::string>{cluster_name});
         for (const auto& [id, conf_enum, rmsd_vec] : rmsd_data)
         {
            for (const double val : rmsd_vec)
            {
               std::stringstream ss;
               ss << std::setprecision(16) << val;

               // Always add something even empty string, such that PrintTable will correctly align stuff
               if (  conf_enum == Structure::ConformerEnum::SameAsReference
                  )
               {
                  same_as_reference_table_data.back().emplace_back(ss.str());
                  different_from_reference_table_data.back().emplace_back("NaN");
               }
               else if (conf_enum == Structure::ConformerEnum::DifferentFromReference)
               {
                  same_as_reference_table_data.back().emplace_back("NaN");
                  different_from_reference_table_data.back().emplace_back(ss.str());
               }
               else
               {
                  std::cerr << "Unknown ConformerEnum encountered !!\n"
                     << "GetStringFromEnum<ConformerEnum>(..) = " << GetStringFromEnum<Structure::ConformerEnum>(conf_enum) << "\n"
                     << "For id:\n" << id
                     << std::endl;
                  exit(1);
               }
            }
         }
      }

      // Setup the paths for data.
      //std::filesystem::path cwd = std::filesystem::current_path();
      std::filesystem::path comp_dir = arPath / "RmsdData";
      std::filesystem::create_directories(comp_dir);

      std::filesystem::path out_file_1 = comp_dir / std::string("SameAsReference" + arFileSuffix + ".dat");
      std::filesystem::path out_file_2 = comp_dir / std::string("DifferentFromReference" + arFileSuffix + ".dat");

      Util::PrintTable(header, same_as_reference_table_data, out_file_1);
      Util::PrintTable(header, different_from_reference_table_data, out_file_2);
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::ostream& operator<<(std::ostream& arOut, const RmsdData& arRmsdData)
   {
      arOut << "Printing RmsdData object with reference ID:\n" << arRmsdData.GetRefId() << std::endl;
      arOut << "Printing all RMSD values for the compared methods:" << std::endl;
      for (const auto& [ID, conf_enum, rmsd_vec] : arRmsdData)
      {
         arOut << ID << std::endl;
         for (const auto& monomer_rmsd : rmsd_vec)
         {
            arOut << "RMSD: " << monomer_rmsd << std::endl;
         }
      }
      return arOut;
   }
}
