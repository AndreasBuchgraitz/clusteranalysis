/********************************************************************
 * File to contain the Anderson-Darling critical value map.
 * Values are taken from table 4.2 in Michael A. Stephens "Goodness-
 * of-fit techniques" (1986).
 ********************************************************************/

#include<map>
#include<iostream>

#include "MathAndUtil/Statistics.hpp"

namespace Math
{
   namespace Detail
   {
      /**
       * Key: Significance level (decimal)
       * Value: Critical Value of AndersonDarling distribution.
       * 
       * If a given hypothesis gives A^2 value lower than LowerTail
       * or larger than UpperTail values it is rejected.
       **/
      const static std::map<double, double> AndersonDarlingUpperTailCriticalValues = 
         {  {0.250, 1.248}
         ,  {0.150, 1.610}
         ,  {0.100, 1.933}
         ,  {0.050, 2.492}
         ,  {0.025, 3.070}
         ,  {0.010, 3.880}
         ,  {0.005, 4.500}
         ,  {0.001, 6.000}
         };
      const static std::map<double, double> AndersonDarlingLowerTailCriticalValues = 
         {  {0.150, 0.399}
         ,  {0.100, 0.346}
         ,  {0.050, 0.283}
         ,  {0.025, 0.240}
         ,  {0.010, 0.201}
         };
   }

   /********************************************************************
    * We do minus one such that aAtomIdx aligns with common knowledge
    * that fx H is number 1, He is number 2 ...
    ********************************************************************/
   double GetAndersonDarlingUpperTailCriticalValue(const double aSignificanceLevel)
   {
      try
      {
         return Detail::AndersonDarlingUpperTailCriticalValues.at(aSignificanceLevel);
      }
      catch(const std::out_of_range& e)
      {
         std::cerr << "The provided significance level does not have a tabulated value !" << std::endl;
         std::cerr << "aSignificanceLevel: " << aSignificanceLevel << std::endl;
         std::cerr << "I exit now !" << std::endl;
         exit(0);
      }
   }
   //
   double GetAndersonDarlingLowerTailCriticalValue(const double aSignificanceLevel)
   {
      try
      {
         return Detail::AndersonDarlingLowerTailCriticalValues.at(aSignificanceLevel);
      }
      catch(const std::out_of_range& e)
      {
         std::cerr << "The provided significance level does not have a tabulated value !" << std::endl;
         std::cerr << "aSignificanceLevel: " << aSignificanceLevel << std::endl;
         std::cerr << "I exit now !" << std::endl;
         exit(0);
      }
   }
}