/********************************************************************
 * This file contains declarations for all Tests.
 ********************************************************************/

#include<iostream>
#include<iomanip>
#include "Eigen_wrapper.hpp"
#include<filesystem>
#include<fstream>
#include<numbers>
#include<random>
#include<LBFGSB.h>

#include "Tests.hpp"
#include "CommonAliases.hpp"
#include "Concepts.hpp"
#include "MathAndUtil/Math.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "Structure/StructureEnums.hpp"
#include "Structure/Molecule.hpp"
#include "Structure/Cluster.hpp"
#include "Structure/ClusterSet.hpp"
#include "Structure/DipoleMoment.hpp"
#include "Structure/CoulombMatrix.hpp"
#include "Input/ReadFiles.hpp"
#include "Input/InputInfo.hpp"
#include "MathAndUtil/NormalDistribution.hpp"
#include "MathAndUtil/CauchyDistribution.hpp"
#include "MathAndUtil/MaximumLogLikelihoodOptimiser.hpp"
#include "MathAndUtil/MonteCarloIntegration.hpp"
#include "MathAndUtil/Statistics.hpp"
#include "MathAndUtil/NumericalIntegration.hpp"
#include "MathAndUtil/ConvexHull.hpp"
#include "MathAndUtil/Random.hpp"

namespace Test
{
   /********************************************************************
    * Run ALL relevant tests
    ********************************************************************/
   void RunTests()
   {
      // Set this to true if we need some basic print to figure out which tests crash !
      bool debug = false;

      std::cout << std::setprecision(16);

      RunGetSignTest<int>();
      RunGetSignTest<float>();
      RunGetSignTest<double>();

      RunUtilTests(debug);

      RunRMSDTest(debug);

      RunPolytopeTest(debug);
      RunConvexHullTest(debug);

      RunSVDTest(debug);

      RunMathTests(debug);

      RunMoleculeTests(debug);

      RunCentroidTest(debug);

      RunGetRotatedStructureTest(debug);
      
      RunKabschUtilTest(debug);
      RunKabschTest(debug);
      RunMoleculeKabschTest(debug);

      RunDipoleMomentTest(debug);

      RunCoulombMatrixTest(debug);

      RunNormalDistributionTest<double>(debug);
      RunCauchyDistributionTest<double>(debug);

      RunLBFGSppTest(debug);

      RunMonteCarloIntegrationTest(debug);
      RunQuadratureIntegrationTest(debug);

      RunMaximumLogLikelihoodOptimiserNormalDistributionTest(debug);
      RunMaximumLogLikelihoodOptimiserCauchyDistributionTest(debug);

      RunAndersonDarlingNormalDistributionTest(debug);
      RunAndersonDarlingCauchyDistributionTest(debug);

      RunClusterTests(debug);

      RunFullClusterConformationsTest(debug);
      
      Run1am2nta115Test(debug);
      Run1eda1ma1nta252Test(debug);
      Run1fa1msa1tma324Test(debug);
      Run1am1eda1fa22Test(debug);
      Run2tma100Test(debug);
      Run2eda1nta1sa590Test(debug);
      Run1sa2tma412Test(debug);

      std::cout << "SUCCESS ! All test ran through without errors !" << std::endl;
   }

   /********************************************************************
    * Run relevant SVD tests
    * 
    * Mostly acts as a template for how to use the SVD routine given by
    * Eigen. So see here if in doubt which matrices to conjugate and so
    * on.
    ********************************************************************/
   void RunSVDTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering SVDTest() !" << std::endl;
      }

      // Get randon matrix
      Eigen::MatrixXd m = Eigen::MatrixXd::Random(4, 4);

      // Compute SVD of matrix
      Eigen::JacobiSVD<Eigen::MatrixXd> svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);

      // Check that M = U * S * V^H
      Eigen::MatrixXd new_m = svd.matrixU() * svd.singularValues().asDiagonal() * svd.matrixV().adjoint();

      double rmsd = Math::MatrixRMSD(m, new_m);

      if (rmsd > m.norm() * 1.0E-15)
      {
         std::cout << "ERROR in SVD TEST! M != U * S * V^H" << std::endl;
         std::cout << "M = \n" << m << std::endl;
         std::cout << "U * S * V^H = \n" << new_m << std::endl;
         std::cout << "RMSD = " << rmsd << std::endl;
         exit(1);
      }

      // 4-by-4 unit matrix
      Eigen::MatrixXd I = Eigen::MatrixXd::Identity(4,4);

      Eigen::MatrixXd u_uh = svd.matrixU() * svd.matrixU().adjoint();
      Eigen::MatrixXd v_vh = svd.matrixV() * svd.matrixV().adjoint();

      double rmsd_u = Math::MatrixRMSD(I, u_uh);
      double rmsd_v = Math::MatrixRMSD(I, v_vh);

      if (rmsd_u > I.norm() * 1.0E-15)
      {
         std::cout << "ERROR in SVD TEST! U * U^H != I" << std::endl;
         std::cout << "u_uh = \n" << u_uh << std::endl;
         std::cout << "RMSD = " << rmsd_u << std::endl;
         exit(1);
      }

      if (rmsd_v > I.norm() * 1.0E-15)
      {
         std::cout << "ERROR in SVD TEST! V * V^H != I" << std::endl;
         std::cout << "v_vh = \n" << v_vh << std::endl;
         std::cout << "RMSD = " << rmsd_v << std::endl;
         exit(1);
      }
   }

   /********************************************************************
    * Run relevant tests for the Math namespace.
    ********************************************************************/
   void RunMathTests(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering MathTests() !" << std::endl;
      }

      // Sizes of vectors/matrices 
      for (const int size : {3, 4, 5, 10, 24})
      {
         // Get vector of one's
         Eigen::VectorXd one_vec = Eigen::RowVectorXd::Ones(size);
         // Get random test vector
         Eigen::VectorXd random_vec = Eigen::VectorXd::Random(size);
         // Scale random test vector with some semi-random factor
         Eigen::VectorXd scaled_random_vec = random_vec * 2.7;
         // Constuct (very) simple orthogonal vector to random_vec
         Eigen::VectorXd orthog_random_vec = Eigen::VectorXd::Zero(size);
         orthog_random_vec[0] = 1;
         orthog_random_vec[1] = - random_vec[0] / random_vec[1];

         /********************************************************************
          * Test Dot
          ********************************************************************/
         // Test parallel vector
         double parallel_res = Math::Dot(random_vec, scaled_random_vec) / (random_vec.norm() * scaled_random_vec.norm());
         if (parallel_res - 1 > 1.0E-15)
         {
            std::cerr << "Parallel vectors are not parallel according to Math::Dot !!\nparallel_res = " << parallel_res << std::endl;
         }
         
         // Test orthogonal vector
         double orthogonal_res = Math::Dot(random_vec, orthog_random_vec);
         if (orthogonal_res > 1.0E-15)
         {
            std::cerr << "Orthogonal vectors are not orthogonal according to Math::Dot !!\northogonal_res = " << orthogonal_res << std::endl;
         }

         // Test anti-parallel vector
         double anti_para_res = Math::Dot(random_vec, -scaled_random_vec) / (random_vec.norm() * (-scaled_random_vec).norm());
         if (anti_para_res + 1 > 1.0E-15)
         {
            std::cerr << "Anti-parallel vectors are not anti-parallel according to Math::Dot !!\nanti_para_res = " << anti_para_res << std::endl;
         }

         /********************************************************************
          * Test VectorAngle()
          ********************************************************************/
         parallel_res = Math::VectorAngle(random_vec, scaled_random_vec);
         if (parallel_res > 5.0E-8)
         {
            std::cerr << "Parallel vectors are not parallel according to Math::VectorAngle !!\nparallel_res = " << parallel_res << std::endl;
         }
         
         // Test orthogonal vector
         orthogonal_res = Math::VectorAngle(random_vec, orthog_random_vec);
         if (orthogonal_res - (std::numbers::pi_v<double> / 2) > 1.0E-15)
         {
            std::cerr << "Orthogonal vectors are not orthogonal according to Math::VectorAngle !!\northogonal_res = " << orthogonal_res << std::endl;
         }

         // Test anti-parallel vector
         anti_para_res = Math::VectorAngle(random_vec, -scaled_random_vec);
         if (anti_para_res - std::numbers::pi_v<double> > 1.0E-15)
         {
            std::cerr << "Anti-parallel vectors are not anti-parallel according to Math::VectorAngle !!\nanti_para_res = " << anti_para_res << std::endl;
         }

         /********************************************************************
          * Test IsVectorParallel()
          ********************************************************************/
         auto [para_bool, para_int] = Math::IsVectorsParallel(random_vec, scaled_random_vec, 1.0E-15);
         if (!para_bool && para_int != 1)
         {
            std::cerr << "Parallel vectors are not parallel according to Math::IsVectorsParallel !!\npara_int = " << para_int << std::endl;
         }
         
         // Test orthogonal vector
         auto [ortho_bool, ortho_int] = Math::IsVectorsParallel(random_vec, orthog_random_vec, 1.0E-15);
         if (ortho_bool && ortho_int != 0)
         {
            std::cerr << "Orthogonal vectors are not orthogonal according to Math::IsVectorsParallel !!\northo_int = " << ortho_int << std::endl;
         }

         // Test anti-parallel vector
         auto [anti_para_bool, anti_para_int] = Math::IsVectorsParallel(random_vec, -scaled_random_vec, 1.0E-15);
         if (!anti_para_bool && anti_para_int != 1)
         {
            std::cerr << "Anti-parallel vectors are not anti-parallel according to Math::IsVectorsParallel !!\nanti_para_int = " << anti_para_int << std::endl;
         }

         /********************************************************************
          * Test NormalizeVector()
          ********************************************************************/
         Eigen::VectorXd random_vec_normalized = Math::NormalizeVector(random_vec);
         if (random_vec_normalized.norm() - 1 > 1.0E-15)
         {
            std::cerr << "Normalize vector function is broken. Norm is " << random_vec_normalized.norm() << std::endl;
         }

         Eigen::VectorXd scaled_random_vec_normalized = Math::NormalizeVector(scaled_random_vec);
         if (scaled_random_vec_normalized.norm() - 1 > 1.0E-15)
         {
            std::cerr << "Normalize vector function is broken (for 24D vec). Norm is " << scaled_random_vec_normalized.norm() << std::endl;
         }
      }

      {  // Test the GenerateNtuples() method for 2-tuples
         std::vector<int> data{1, 2, 3, 4, 5};

         std::vector<std::vector<int>> expected_tuples
            {  {1, 2}
            ,  {1, 3}
            ,  {1, 4}
            ,  {1, 5}
            ,  {2, 3}
            ,  {2, 4}
            ,  {2, 5}
            ,  {3, 4}
            ,  {3, 5}
            ,  {4, 5}
            };

         std::vector<std::vector<int>> computed_tuples;
         std::vector<int> temp_cont;
         Math::GenerateNTuples<2>(data, computed_tuples, temp_cont, 0);

         if (expected_tuples != computed_tuples)
         {
            std::cerr << "Found unexpected 2-tuples !" << std::endl;
            std::cerr << "Expected:\n";
            for (const auto& vec : expected_tuples)
            {
               for (const auto& i : vec)
               {
                  std::cerr << i << ", ";
               }
               std::cerr << std::endl;
            }
            std::cerr << "Found:\n";
            for (const auto& vec : computed_tuples)
            {
               for (const auto& i : vec)
               {
                  std::cerr << i << ", ";
               }
               std::cerr << std::endl;
            }

            exit(1);
         }
      }

      {  // Test the GenerateNtuples() method for 3-tuples
         std::vector<int> data{1, 2, 3, 4, 5};

         std::vector<std::vector<int>> expected_tuples
            {  {1, 2, 3}
            ,  {1, 2, 4}
            ,  {1, 2, 5}
            ,  {1, 3, 4}
            ,  {1, 3, 5}
            ,  {1, 4, 5}
            ,  {2, 3, 4}
            ,  {2, 3, 5}
            ,  {2, 4, 5}
            ,  {3, 4, 5}
            };

         std::vector<std::vector<int>> computed_tuples;
         std::vector<int> temp_cont;
         Math::GenerateNTuples<3>(data, computed_tuples, temp_cont, 0);
         if (expected_tuples != computed_tuples)
         {
            std::cerr << "Found unexpected 2-tuples !" << std::endl;
            std::cerr << "Expected:\n";
            for (const auto& vec : expected_tuples)
            {
               for (const auto& i : vec)
               {
                  std::cerr << i << ", ";
               }
               std::cerr << std::endl;
            }
            std::cerr << "Found:\n";
            for (const auto& vec : computed_tuples)
            {
               for (const auto& i : vec)
               {
                  std::cerr << i << ", ";
               }
               std::cerr << std::endl;
            }

            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Done UtilTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run small tests for the util functions
    ********************************************************************/
   void RunUtilTests(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering UtilTest() !" << std::endl;
      }

      // Tests TrimAtomName
      if (::Util::TrimAtomName("H") != "H")
      {
         std::cerr << "TrimAtomName(\"H\") unexpectedly returns " << ::Util::TrimAtomName("H") << std::endl;
         exit(1);
      }
      if (::Util::TrimAtomName("C") != "C")
      {
         std::cerr << "TrimAtomName(\"C\") unexpectedly returns " << ::Util::TrimAtomName("C") << std::endl;
         exit(1);
      }
      if (::Util::TrimAtomName("H123") != "H")
      {
         std::cerr << "TrimAtomName(\"H123\") unexpectedly returns " << ::Util::TrimAtomName("H123") << std::endl;
         exit(1);
      }
      if (::Util::TrimAtomName("3C5") != "C")
      {
         std::cerr << "TrimAtomName(\"3C5\") unexpectedly returns " << ::Util::TrimAtomName("3C5") << std::endl;
         exit(1);
      }

      if (aDebug)
      {
         std::cout << "Done UtilTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run relevant tests for the Centroid function.
    * 
    * Test Centroid for 2D and 3D hardcoded examples.
    ********************************************************************/
   void RunCentroidTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering CentroidTest() !" << std::endl;
      }

      // 3 points in 2d space
      Eigen::MatrixXd points_3p_2d = Eigen::MatrixXd::Zero(3, 2);
      points_3p_2d(0,0) = 1;
      points_3p_2d(0,1) = 1;
      points_3p_2d(1,0) = 2;
      points_3p_2d(1,1) = 2;
      points_3p_2d(2,0) = 1;
      points_3p_2d(2,1) = 3;
      
      Eigen::MatrixXd centroid_3p_2d = Kabsch::Centroid(points_3p_2d);

      if (  centroid_3p_2d.size() != 2
         || centroid_3p_2d(0,0) != 4.0 / 3
         || centroid_3p_2d(0,1) != 2.0
         )
      {
         std::stringstream ss;
         ss << "Error in Centroid test for 3 points in 2d space!\n"
            << "for the structure:\n" << points_3p_2d << "\n"
            << "We got the centroid:\n" << centroid_3p_2d << "\n"
            << "but expected:\n(1.3333, 2.0000)"
            << std::endl;
         std::cout << ss.str();
      }
      
      // 4 points in 2d space
      Eigen::MatrixXd points_4p_2d = Eigen::MatrixXd::Zero(4, 2);
      points_4p_2d(0,0) = 2;
      points_4p_2d(0,1) = 1;
      points_4p_2d(1,0) = 1;
      points_4p_2d(1,1) = -3;
      points_4p_2d(2,0) = -1;
      points_4p_2d(2,1) = -1;
      points_4p_2d(3,0) = -2;
      points_4p_2d(3,1) = 3;
      
      Eigen::MatrixXd centroid_4p_2d = Kabsch::Centroid(points_4p_2d);

      if (  centroid_4p_2d.size() != 2
         || centroid_4p_2d(0,0) != 0.0
         || centroid_4p_2d(0,1) != 0.0
         )
      {
         std::stringstream ss;
         ss << "Error in Centroid test for 4 points in 2d space!\n"
            << "for the structure:\n" << points_4p_2d << "\n"
            << "We got the centroid:\n" << centroid_4p_2d << "\n"
            << "but expected:\n(0.0000, 0.0000)"
            << std::endl;
         std::cout << ss.str();
      }

      if (aDebug)
      {
         std::cout << "Done CentroidTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Runs tests like Center-Of-Mass
    ********************************************************************/
   void RunKabschUtilTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering KabschUtilTest() !" << std::endl;
      }

      // Strupid sel-drawn NH3
      {
         Eigen::MatrixX3d am_pos(4,3);
         am_pos <<  0.0,              0.0,               0.0,
                    0.0,   1/std::sqrt(3),   -std::sqrt(2/3.0),
                    0.5,  -std::sqrt(3)/6,   -std::sqrt(2/3.0),
                   -0.5,  -std::sqrt(3)/6,   -std::sqrt(2/3.0);

         std::vector<Structure::AtomName> am_names{Structure::AtomName::N, Structure::AtomName::H, Structure::AtomName::H, Structure::AtomName::H};

         Eigen::RowVector3d expected_com;
         expected_com << 0, 0, -std::sqrt(6)/17;

         Eigen::RowVector3d computed_com = Kabsch::CenterOfMass(am_names, am_pos);

         if ((expected_com - computed_com).norm() > 1.0E-17)
         {
            std::cerr << "Unexpected CenterOfMass found !\n"
               << "Found COM   : " << computed_com << "\n"
               << "expected COM: " << expected_com << "\n"
               << "(found - expected).norm() = " << (expected_com - computed_com).norm() << "\n"
               << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Done KabschUtilTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run ALL relevant tests
    ********************************************************************/
   void RunKabschTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering KabschTest() !" << std::endl;
      }

      // Start structure
      Eigen::MatrixXd points_3p_2d = Eigen::MatrixXd::Zero(3, 2);
      points_3p_2d(0,0) = 1;
      points_3p_2d(0,1) = 1;
      points_3p_2d(1,0) = 2;
      points_3p_2d(1,1) = 2;
      points_3p_2d(2,0) = 1;
      points_3p_2d(2,1) = 3;

      // Expected output structure
      Eigen::MatrixXd rot_struct = Eigen::MatrixXd::Zero(3, 2);
      rot_struct(0,0) = -1;
      rot_struct(0,1) = 1.0 / 3;
      rot_struct(1,0) = 0;
      rot_struct(1,1) = -2.0 / 3;
      rot_struct(2,0) = 1;
      rot_struct(2,1) = 1.0 / 3;

      // Should find pi / 2 rotation matrix here
      Eigen::MatrixXd rot_mat_1 = Kabsch::Kabsch(points_3p_2d, rot_struct);

      // Should find [[0, 1], [-1, 0]] that is "-" is on other out-of-diagonal element as compared to 
      // what is written on wiki for "rotation matrix", this is because we here do <struct> * <rot_mat>
      // as compared to <rot_mat> * <struct> as is assumed on wiki
      Eigen::MatrixXd pi_2_rot_mat = Eigen::MatrixXd::Zero(2,2);
      pi_2_rot_mat(0,1) = 1;
      pi_2_rot_mat(1,0) = -1;

      double rmsd_1 = Math::MatrixRMSD(rot_mat_1, pi_2_rot_mat);

      if (rmsd_1 > rot_mat_1.norm() * 1.0E-18)
      {
         std::stringstream ss;
         ss << std::setprecision(16);
         ss << "ERROR in Kabsch Algorithm test (Hardcoded test)!\n"
            << "Found the rotation matrix:\n" << rot_mat_1 << "\n"
            << "Expected the rotation matrix:\n" << pi_2_rot_mat << "\n"
            << "RMSD = " << rmsd_1 << std::endl;
         
         std::cout << ss.str();
         exit(1);
      }

      // Test on random matrix and known rotation matrix (again pi / 2)
      Eigen::MatrixXd random_structure = Eigen::MatrixXd::Random(5,3);

      // pi / 2 rotation around z axis
      Eigen::MatrixXd rot_mat_pi_2_z = Eigen::MatrixXd::Zero(3,3);
      rot_mat_pi_2_z(0, 1) = 1;
      rot_mat_pi_2_z(1, 0) = -1;
      rot_mat_pi_2_z(2, 2) = 1;

      // Create rotated structure based on hardcoded rotation matrix
      //Eigen::MatrixXd rotated_struct = Kabsch::GetRotatedStructure(random_structure, rot_mat_pi_2_z, Structure::InputOptions::ALL_ALL);
      Eigen::MatrixXd rotated_struct = Kabsch::GetRotatedStructure(random_structure, rot_mat_pi_2_z);

      // Hopefully re-obtain above rotation matrix
      Eigen::MatrixXd kabsch_rot_mat_2 = Kabsch::Kabsch(random_structure, rotated_struct);

      double rmsd_2 = Math::MatrixRMSD(kabsch_rot_mat_2, rot_mat_pi_2_z);

      if (rmsd_2 > rot_mat_pi_2_z.norm() * 1.0E-15)
      {
         std::stringstream ss;
         ss << std::setprecision(16);
         ss << "ERROR in Kabsch Algorithm test (Random test)!\n"
            << "Found the rotation matrix:\n" << kabsch_rot_mat_2 << "\n"
            << "Expected the rotation matrix:\n" << rot_mat_pi_2_z << "\n"
            << "RMSD = " << rmsd_2 << std::endl;
         
         std::cout << ss.str();
         exit(1);
      }

      // Test Kabsch for two isolated monomers (from actual data). 1tma.
      Eigen::MatrixXd ccsd = Eigen::MatrixXd::Zero(13, 3); // CCSD/AUG-CC-PVTZ (MOLPRO)
      ccsd(0, 0)  =  0.0000000000; ccsd(0, 1)  =  0.0000002338; ccsd(0, 2)  =  0.4046057602;
      ccsd(1, 0)  =  0.0000000000; ccsd(1, 1)  =  1.3777102401; ccsd(1, 2)  = -0.0577381504;
      ccsd(2, 0)  = -0.8844814776; ccsd(2, 1)  =  1.8927765914; ccsd(2, 2)  =  0.3185794742;
      ccsd(3, 0)  =  0.8844814776; ccsd(3, 1)  =  1.8927765914; ccsd(3, 2)  =  0.3185794742;
      ccsd(4, 0)  =  0.0000000000; ccsd(4, 1)  =  1.4490474166; ccsd(4, 2)  = -1.1587109800;
      ccsd(5, 0)  =  1.1931319719; ccsd(5, 1)  = -0.6888551524; ccsd(5, 2)  = -0.0577376603;
      ccsd(6, 0)  =  2.0814331924; ccsd(6, 1)  = -0.1804041715; ccsd(6, 2)  =  0.3185791072;
      ccsd(7, 0)  =  1.1969520518; ccsd(7, 1)  = -1.7123712772; ccsd(7, 2)  =  0.3185811986;
      ccsd(8, 0)  =  1.2549113612; ccsd(8, 1)  = -0.7245249356; ccsd(8, 2)  = -1.1587104346;
      ccsd(9, 0)  = -1.1931319719; ccsd(9, 1)  = -0.6888551524; ccsd(9, 2)  = -0.0577376603;
      ccsd(10, 0) = -1.1969520518; ccsd(10, 1) = -1.7123712772; ccsd(10, 2) =  0.3185811986;
      ccsd(11, 0) = -2.0814331924; ccsd(11, 1) = -0.1804041715; ccsd(11, 2) =  0.3185791072;
      ccsd(12, 0) = -1.2549113612; ccsd(12, 1) = -0.7245249356; ccsd(12, 2) = -1.1587104346;

      Eigen::MatrixXd mp2 = Eigen::MatrixXd::Zero(13, 3); // RI-MP2/AUG-CC-PVQZ (TURBOMOLE)
      mp2(0, 0)  =  0.00000001953222; mp2(0, 1)  =  0.51501635782382; mp2(0, 2)  =  0.00000000000000;
      mp2(1, 0)  =  1.37110013669573; mp2(1, 1)  =  0.04635218428130; mp2(1, 2)  =  0.00000000000000;
      mp2(2, 0)  =  1.88658330952334; mp2(2, 1)  =  0.41776294993616; mp2(2, 2)  = -0.88312012469402;
      mp2(3, 0)  =  1.88658330952334; mp2(3, 1)  =  0.41776294993616; mp2(3, 2)  =  0.88312012469402;
      mp2(4, 0)  =  1.43239569964546; mp2(4, 1)  = -1.05355143565711; mp2(4, 2)  =  0.00000000000000;
      mp2(5, 0)  = -0.68554996443533; mp2(5, 1)  =  0.04635252984409; mp2(5, 2)  =  1.18740738337567;
      mp2(6, 0)  = -0.17848643373547; mp2(6, 1)  =  0.41776226714717; mp2(6, 2)  =  2.07538911063479;
      mp2(7, 0)  = -1.70809561439619; mp2(7, 1)  =  0.41776474727309; mp2(7, 2)  =  1.19226969009987;
      mp2(8, 0)  = -0.71619922489305; mp2(8, 1)  = -1.05355104742449; mp2(8, 2)  =  1.24049048937656;
      mp2(9, 0)  = -0.68554996443533; mp2(9, 1)  =  0.04635252984409; mp2(9, 2)  = -1.18740738337567;
      mp2(10, 0) = -1.70809561439619; mp2(10, 1) =  0.41776474727309; mp2(10, 2) = -1.19226969009987;
      mp2(11, 0) = -0.17848643373547; mp2(11, 1) =  0.41776226714717; mp2(11, 2) = -2.07538911063479;
      mp2(12, 0) = -0.71619922489305; mp2(12, 1) = -1.05355104742449; mp2(12, 2) = -1.24049048937656;

      Eigen::MatrixXd kabsch_rot_mat_3 = Kabsch::Kabsch(mp2, ccsd);
      //Eigen::MatrixXd rotated_mp2 = Kabsch::GetRotatedStructure(mp2, kabsch_rot_mat_3, Structure::InputOptions::ALL_ALL);
      Eigen::MatrixXd rotated_mp2 = Kabsch::GetRotatedStructure(mp2, kabsch_rot_mat_3);

      double rmsd_3 = Math::MatrixRMSD(rotated_mp2, ccsd);

      // This is real world example with two different methods so rmsd is not super close to 0.
      if (rmsd_3 > 0.0595062163647)
      {
         std::stringstream ss;
         ss << std::setprecision(16);
         ss << "ERROR in Kabsch Algorithm test (1tma test)!\n"
            << "Found the rotation matrix:\n" << kabsch_rot_mat_3 << "\n"
            << "RMSD     = " << rmsd_3 << "\n"
            << "Expected = " << 0.0595062163647 << std::endl;
         
         std::cout << ss.str();
         exit(1);
      }

      if (aDebug)
      {
         std::cout << "Done KabschTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Runs tests for running the Kabsch algorithm on the Molecule
    * objects.
    ********************************************************************/
   void RunMoleculeKabschTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering MoleculeKabschTest() !" << std::endl;
      }

      // Test Kabsch for two clusters. Will check the entire cluster and the two monomers present.
      // 1a1ma_01
      // CCSD/AUG-CC-PVTZ
      // RI-MP2/AUG-CC-PVQZ

      // CCSD 1a
      std::vector<Structure::AtomName> ammonia_names =
         {  Structure::AtomName::N
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         };
      std::vector<std::string> ammonia_atoms_strings =
         {  "N"
         ,  "H"
         ,  "H"
         ,  "H"
         };
      Eigen::Matrix<double, 4, 3> ammonia_coords 
         {  {2.2773566090, -0.1221790810,  0.0575254175}
         ,  {2.3079937203, -0.7801636262, -0.7117735511}
         ,  {2.8617355955, -0.5041434795,  0.7910395704}
         ,  {2.7323548364,  0.7220871255, -0.2684138402}
         };
      // CCSD 1ma
      std::vector<Structure::AtomName> methyl_amine_names = 
         {  Structure::AtomName::N
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         };
      std::vector<std::string> methyl_amine_string_names =
         {  "N"
         ,  "H"
         ,  "C"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         };
      Eigen::Matrix<double, 7, 3> methyl_amine_coords
         {  {-0.9147676079,  0.6971682245, -0.1236342252}
         ,  { 0.0535112169,  0.4925350288,  0.0978659926}
         ,  {-1.7219673276, -0.5151348540,  0.0160367129}
         ,  {-1.6933906846, -0.9734303140,  1.0120225761}
         ,  {-2.7624870074, -0.2910935132, -0.2225712575}
         ,  {-1.3766620885, -1.2582101309, -0.7037871054}
         ,  {-1.2247972621,  1.3947846202,  0.5401097099}
         };

      // RI-MP2 1a
      Eigen::Matrix<double, 4, 3> ammonia_mp2_coords
         {  {2.09380457190318, -0.37762323063996, -0.09993415046731}  
         ,  {1.29462033048540,  0.22243139899883, -0.28441711082612}
         ,  {2.21142514786604, -0.97785756916168, -0.90432554877068}
         ,  {2.91203720821793,  0.21400633500988, -0.06159988236353}
         };
      // RI-MP2 1ma
      Eigen::Matrix<double, 7, 3> methyl_amine_mp2_coords
         {  {-0.77754784920634,  0.91955687220511,  0.06815992372532}           
         ,  {-0.63429793184949,  1.30161456543762,  0.99305565344950}
         ,  {-1.32444305305613, -0.43477955647893,  0.16383906185711}
         ,  {-2.26084295655288, -0.50723344193893,  0.71988460452209}
         ,  {-1.49492690786536, -0.81791339221325, -0.83886879784260}
         ,  {-0.58754622973004, -1.07539697060558,  0.64055620483245}
         ,  {-1.43228233021235,  1.53319498938684, -0.39634995811628}
         };


      // CCSD 1a
      Structure::Molecule ccsd_1a("CCSD-1a", std::make_pair(ammonia_names, ammonia_atoms_strings), ammonia_coords, {});
      // CCSD 1ma
      Structure::Molecule ccsd_1ma("CCSD-1ma", std::make_pair(methyl_amine_names, methyl_amine_string_names), methyl_amine_coords, {});
      // CCSD 1a1ma_01
      std::vector<Structure::AtomName> names_1a1ma(ammonia_names);
      names_1a1ma.insert(names_1a1ma.end(), methyl_amine_names.begin(), methyl_amine_names.end());
      std::vector<std::string> string_names_1a1ma(ammonia_atoms_strings);
      string_names_1a1ma.insert(string_names_1a1ma.end(), methyl_amine_string_names.begin(), methyl_amine_string_names.end());
      Eigen::Matrix<double, 11, 3> ccsd_1a1ma_coords;
      ccsd_1a1ma_coords.topRows<4>() = ammonia_coords;
      ccsd_1a1ma_coords.bottomRows<7>() = methyl_amine_coords;
      Structure::Molecule ccsd_1a1ma("CCSD-1a1ma", std::make_pair(names_1a1ma, string_names_1a1ma), ccsd_1a1ma_coords, {});

      // RI-MP2 1a
      Structure::Molecule mp2_1a("RI-MP2-1a", std::make_pair(ammonia_names, ammonia_atoms_strings), ammonia_mp2_coords, {});
      // RI-MP2 1ma
      Structure::Molecule mp2_1ma("RI-MP2-1ma", std::make_pair(methyl_amine_names, methyl_amine_string_names), methyl_amine_mp2_coords, {});
      // RI-MP2 1a1ma_01
      Eigen::Matrix<double, 11, 3> mp2_1a1ma_coords;
      mp2_1a1ma_coords.topRows<4>() = ammonia_mp2_coords;
      mp2_1a1ma_coords.bottomRows<7>() = methyl_amine_mp2_coords;
      Structure::Molecule mp2_1a1ma("RI-MP2-1ma", std::make_pair(names_1a1ma, string_names_1a1ma), mp2_1a1ma_coords, {});

      // Compute the RMSD of all three structures.
      double rmsd_1a    = Kabsch::MoleculeRMSD(ccsd_1a, mp2_1a, Structure::InputOptions::ALL_ALL);
      double rmsd_1ma   = Kabsch::MoleculeRMSD(ccsd_1ma, mp2_1ma, Structure::InputOptions::ALL_ALL);
      double rmsd_1a1ma = Kabsch::MoleculeRMSD(ccsd_1a1ma, mp2_1a1ma, Structure::InputOptions::ALL_ALL);

      check_rmsd(rmsd_1a, 3.525750026851887E-3, "CCSD vs RI-MP2. 1a.\n");
      check_rmsd(rmsd_1ma, 6.21245967267423E-3, "CCSD vs RI-MP2. 1ma.\n");
      check_rmsd(rmsd_1a1ma, 6.59541343348644E-1, "CCSD vs RI-MP2. 1a1ma.\n");

      if (aDebug)
      {
         std::cout << "Done MoleculeKabschTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Tests the function GetRotatedStructure which will move structure
    * to origin and then apply rotation matrix and return this rotated
    * structure placed in the origin.
    ********************************************************************/
   void RunGetRotatedStructureTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering GetRotatedStructureTest() !" << std::endl;
      }

      // pi / 2 rotation matrix
      Eigen::MatrixXd rot_mat = Eigen::MatrixXd::Zero(2,2);
      rot_mat(0,1) = 1;
      rot_mat(1,0) = -1;

      // Start structure
      Eigen::MatrixXd points_3p_2d = Eigen::MatrixXd::Zero(3, 2);
      points_3p_2d(0,0) = 1;
      points_3p_2d(0,1) = 1;
      points_3p_2d(1,0) = 2;
      points_3p_2d(1,1) = 2;
      points_3p_2d(2,0) = 1;
      points_3p_2d(2,1) = 3;

      // Expected output structure
      Eigen::MatrixXd expected_rot_struct = Eigen::MatrixXd::Zero(3, 2);
      expected_rot_struct(0,0) = -1;
      expected_rot_struct(0,1) = 1.0 / 3;
      expected_rot_struct(1,0) = 0;
      expected_rot_struct(1,1) = -2.0 / 3;
      expected_rot_struct(2,0) = 1;
      expected_rot_struct(2,1) = 1.0 / 3;

      //Eigen::MatrixXd rotated_structure = Kabsch::GetRotatedStructure(points_3p_2d, rot_mat, Structure::InputOptions::ALL_ALL);
      Eigen::MatrixXd rotated_structure = Kabsch::GetRotatedStructure(points_3p_2d, rot_mat);

      double rmsd = Math::MatrixRMSD(rotated_structure, expected_rot_struct);

      if (rmsd > rotated_structure.norm() * 1.0E-16)
      {
         std::stringstream ss;
         ss << std::setprecision(16);
         ss << "ERROR in GetRotatedStructureTest!\n"
            << "Got rotated structure:\n" << rotated_structure << "\n"
            << "Expected rotated structure:\n" << expected_rot_struct << "\n"
            << "RMSD = " << rmsd << std::endl;
         std::cout << ss.str();
      }

      if (aDebug)
      {
         std::cout << "Done GetRotatedStructureTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run tests for dipolemoment class.
    ********************************************************************/
   void RunDipoleMomentTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering DipoleMomentTest() !" << std::endl;
      }

      // Stupid self-drawn NH3
      {
         Eigen::MatrixX3d am_pos(4,3);
         am_pos <<  0.0,              0.0,                   0.0,
                    0.0,   1/std::sqrt(3),   -std::sqrt(2.0/3.0),
                    0.5,  -std::sqrt(3)/6,   -std::sqrt(2.0/3.0),
                   -0.5,  -std::sqrt(3)/6,   -std::sqrt(2.0/3.0);
         
         std::vector<Structure::AtomName> am_names{Structure::AtomName::N, Structure::AtomName::H, Structure::AtomName::H, Structure::AtomName::H};
         std::map<Uin, std::vector<Uin>> connectivity_map {{0, std::vector<Uin>{1, 2, 3}}};
         Structure::DipoleMoment dip_mom(am_names, am_pos, connectivity_map);

         Eigen::RowVector3d com = Kabsch::CenterOfMass(am_names, am_pos);

         std::pair<bool, int> paral_vec = Math::IsVectorsParallel(com, dip_mom.GetDipoleMoment(), 1.0E-20);

         if (!paral_vec.first)
         {
            std::cerr << "Dipolemoment for self-drawn NH3 is not parallel with COM-vector !!\n"
               << "Center-Of-Mass:          " << com << "\n"
               << "dip_mom.GetDipoleMoment()" << dip_mom.GetDipoleMoment() << "\n"
               << std::endl;
         }
      }

      // Real NH3 structure
      {
         Eigen::MatrixX3d am_pos(4,3);
         am_pos << -0.014828,   0.006258,   0.010001,
                    0.768410,  -0.240629,   0.611601,
                    0.320896,   0.708683,  -0.645915,
                   -0.248942,  -0.822731,  -0.532497;

         std::vector<Structure::AtomName> am_names{Structure::AtomName::N, Structure::AtomName::H, Structure::AtomName::H, Structure::AtomName::H};

         std::map<Uin, std::vector<Uin>> connectivity_map {{0, std::vector<Uin>{1, 2, 3}}};

         Structure::DipoleMoment dip_mom(am_names, am_pos, connectivity_map);

         Eigen::RowVector3d com = Kabsch::CenterOfMass(am_names, am_pos);
         // Create vector pointing from N to COM (or other way) as this should be the direction of the dipolemoment for NH3.
         Eigen::RowVector3d com_N = am_pos.row(0) - com;

         std::pair<bool, int> paral_vec = Math::IsVectorsParallel(com_N, dip_mom.GetDipoleMoment(), 1.0E-20);

         if (!paral_vec.first)
         {
            std::cerr << "Dipolemoment for Avogadro NH3 is not parallel with COM-N vector !!\n"
               << "Center-Of-Mass:          " << com_N << "\n"
               << "dip_mom.GetDipoleMoment()" << dip_mom.GetDipoleMoment() << "\n"
               << std::endl;
         }
      }

      if (aDebug)
      {
         std::cout << "Done DipoleMomentTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run tests for CoulombMatrix construction.
    ********************************************************************/
   void RunCoulombMatrixTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering CoulombMatrixTest() !" << std::endl;
      }

      // Stupid self-drawn NH3
      {
         // Every atom should be a distance of exactly 1 unit apart from every other atom.
         Eigen::MatrixX3d am_pos(4,3);
         am_pos <<  0.0,              0.0,                   0.0,
                    0.0,   1/std::sqrt(3),   -std::sqrt(2.0/3.0),
                    0.5,  -std::sqrt(3)/6,   -std::sqrt(2.0/3.0),
                   -0.5,  -std::sqrt(3)/6,   -std::sqrt(2.0/3.0);
         
         std::vector<Structure::AtomName> am_names{Structure::AtomName::N, Structure::AtomName::H, Structure::AtomName::H, Structure::AtomName::H};
         std::map<Uin, std::vector<Uin>> connectivity_map {{0, std::vector<Uin>{1, 2, 3}}};
         
         Structure::CoulombMatrix cou_mat(am_names, am_pos);

         Eigen::MatrixXd expected_full_coulomb_matrix(4, 4);
         expected_full_coulomb_matrix 
            << 0.5*std::pow(7, 2.4),   7,   7,   7,
                                  7, 0.5,   1,   1,
                                  7,   1, 0.5,   1,
                                  7,   1,   1, 0.5;
         
         if ((cou_mat.GetCoulombMatrix() - expected_full_coulomb_matrix).norm() != 0)
         {
            std::cerr << "Unexpected coulomb matrix for full stupid NH3\n"
               << "I found the matrix: \n" << cou_mat.GetCoulombMatrix() << "\n"
               << "But I expected: \n" << expected_full_coulomb_matrix << "\n"
               << "(mat1 - mat2).norm() = " << (cou_mat.GetCoulombMatrix() - expected_full_coulomb_matrix).norm()
               << std::endl;
            exit(1);
         }

         // Computing CoulombMatrix for N (atom 0) and the second hydrogen (atom 2).
         Eigen::MatrixXd reduced_cou_mat = cou_mat.ComputeCoulombMatrix(std::vector<Uin>{0, 2});
         Eigen::MatrixXd expected_reduced_coulomb_matrix(2, 2);
         expected_reduced_coulomb_matrix 
            << 0.5*std::pow(7, 2.4),   7,
                                  7, 0.5;
         
         if ((reduced_cou_mat - expected_reduced_coulomb_matrix).norm() != 0)
         {
            std::cerr << "Unexpected coulomb matrix for reduced stupid NH3\n"
               << "I found the matrix: \n" << reduced_cou_mat << "\n"
               << "But I expected: \n" << expected_reduced_coulomb_matrix << "\n"
               << "(mat1 - mat2).norm() = " << (reduced_cou_mat - expected_reduced_coulomb_matrix).norm()
               << std::endl;
            exit(1);
         }        

      }

      if (aDebug)
      {
         std::cout << "Done CoulombMatrixTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run tests for RMSD.
    ********************************************************************/
   void RunRMSDTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RMSDTest() !" << std::endl;
      }

      Eigen::MatrixXd m1 = Eigen::MatrixXd::Zero(3,3);
      m1(0,0) = 1;
      m1(0,1) = 4;
      m1(0,2) = 2;
      m1(1,0) = 6;
      m1(1,1) = 5;
      m1(1,2) = 2;
      m1(2,0) = 1;
      m1(2,1) = 7;
      m1(2,2) = 3;

      Eigen::MatrixXd m2 = Eigen::MatrixXd::Zero(3,3);
      m2(0,0) = 7;
      m2(0,1) = 3;
      m2(0,2) = 6;
      m2(1,0) = 1;
      m2(1,1) = 3;
      m2(1,2) = 1;
      m2(2,0) = 9;
      m2(2,1) = 6;
      m2(2,2) = 3;

      double rmsd1 = Math::MatrixRMSD(m1, m1);

      if (rmsd1 != 0.0)
      {
         std::stringstream ss;
         ss << "Error in RMSD test! Did not find RMSD = 0 for same matrix !" << std::endl;
         std::cout << ss.str();
      }

      double rmsd2 = Math::MatrixRMSD(m1, m2);
      if (rmsd2 != 4.055175020198813)
      {
         std::stringstream ss;
         ss << std::setprecision(16);
         ss << "Error in RMSD test! Did not find correct RMSD for the matrices:\n"
            << "m1 = \n" << m1 << "\n"
            << "m2 = \n" << m2 << "\n"
            << "Found RMSD = " << rmsd2 << "\n"
            << "Expected RMSD = " << 4.055175020198813 << std::endl;
         std::cout << ss.str();
      }


      if (aDebug)
      {
         std::cout << "Done RMSDTests() !" << std::endl;
      }
   }

   /********************************************************************
    * Run some tests for the Polytope class
    ********************************************************************/
   void RunPolytopeTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunPolytopeTest() !" << std::endl;
      }

      {  // Test for non trivial 3 dimensional case.
         const std::vector<Eigen::RowVector3d> data
            {  {-1,-1,-1}
            ,  {-1,-1, 1}
            ,  {-1, 1,-1}
            ,  {-1, 1, 1}
            ,  { 1,-1,-1}
            ,  { 1,-1, 1}
            ,  { 1, 1,-1}
            ,  { 1, 1, 1}
            };
         
         const Math::Polytope<3, double> polytope(data);

         if (polytope.GetNumFaces() != 6)
         {
            std::cerr << "Found unexpected number of faces for perfect cube" << std::endl;
            std::cerr << "Expected:" << std::endl;
            std::cerr << 6 << std::endl;

            std::cerr << "Computed:" << std::endl;
            std::cerr << polytope.GetNumFaces() << std::endl;

            std::cerr << "Found faces:" << std::endl;
            for (const auto& face : polytope.GetFaces())
            {
               std::cerr << face << std::endl;
            }
            exit(1);
         }
      }

      {
         const std::vector<Eigen::RowVector3d> data
            {  { 4.10908, -0.01276, 1.2352}
            ,  {-0.15521,  0.44329, -4.362}
            ,  {-1.92943,  3.05064, 1.5997}
            ,  {-1.70814, -0.05085, 3.2981}
            };
         
         const Math::Polytope<3, double> polytope(data);

         if (polytope.GetNumFaces() != 4)
         {
            std::cerr << "Found unexpected number of faces for tetrahedron from some data" << std::endl;
            std::cerr << "Expected:" << std::endl;
            std::cerr << 4 << std::endl;

            std::cerr << "Computed:" << std::endl;
            std::cerr << polytope.GetNumFaces() << std::endl;

            std::cerr << "Found faces:" << std::endl;
            for (const auto& face : polytope.GetFaces())
            {
               std::cerr << face << std::endl;
            }
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Entering RunPolytopeTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Run some tests for the convexhull class
    ********************************************************************/
   void RunConvexHullTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunConvexHullTest() !" << std::endl;
      }
      
      {  // Test for trivial 3 dimensional case.
         const std::vector<Eigen::RowVector3d> data
            {  {1,2,2}
            ,  {3,4,2}
            ,  {0,2,2}
            };

         const Math::Polytope<3, double> expected(data);

         const Math::ConvexHull<3, double> hull(data);

         const auto& computed_convex_hull = hull.GetPolytope();

         if (expected != computed_convex_hull)
         {
            std::cerr << "Found unexpected convexhull on trivial data set !" << std::endl;
            std::cerr << "Expected:" << std::endl;
            for (const auto& p : expected.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            std::cerr << "Computed:" << std::endl;
            for (const auto& p : computed_convex_hull.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            exit(1);
         }
      }

      {  // Test for non trivial 3 dimensional case. Data points are only the resulting convexhull
         const std::vector<Eigen::RowVector3d> data
            {  {-1,-1,-1}
            ,  {-1,-1, 1}
            ,  {-1, 1,-1}
            ,  {-1, 1, 1}
            ,  { 1,-1,-1}
            ,  { 1,-1, 1}
            ,  { 1, 1,-1}
            ,  { 1, 1, 1}
            };

         const std::vector<Eigen::RowVector3d> expected_data
            {  {-1,-1,-1}
            ,  {-1,-1, 1}
            ,  {-1, 1,-1}
            ,  { 1,-1,-1}
            ,  { 1, 1, 1}
            ,  {-1, 1, 1}
            ,  { 1,-1, 1}
            ,  { 1, 1,-1}
            };
         
         const auto expected_vertices(expected_data);

         const Math::Polytope<3, double> expected(expected_vertices);

         const Math::ConvexHull<3, double> hull(data);

         const auto& computed_convex_hull = hull.GetPolytope();

         if (expected != computed_convex_hull)
         {
            std::cerr << "Found unexpected convexhull on non-trivial data set (containing only convex hull) !" << std::endl;
            std::cerr << "Expected:" << std::endl;
            for (const auto& p : expected.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            std::cerr << "Computed:" << std::endl;
            for (const auto& p : computed_convex_hull.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            exit(1);
         }
      }

      {  // Test for non trivial 3 dimensional case. Data are convex hull plus a lot of points inside.
         std::vector<Eigen::RowVector3d> data
            {  {-1,-1,-1}
            ,  {-1,-1, 1}
            ,  {-1, 1,-1}
            ,  {-1, 1, 1}
            ,  { 1,-1,-1}
            ,  { 1,-1, 1}
            ,  { 1, 1,-1}
            ,  { 1, 1, 1}
            };
         
         const std::vector<Eigen::RowVector3d> expected_data
            {  {-1, 1, 1}
            ,  {-1, 1,-1}
            ,  { 1, 1, 1}
            ,  {-1,-1, 1}
            ,  { 1,-1,-1}
            ,  { 1, 1,-1}
            ,  {-1,-1,-1}
            ,  { 1,-1, 1}
            };
         
         const auto expected_vertices(expected_data);

         for (int i = 0; i < 20; i++)
         {
            data.emplace_back(Math::GetRandomVector<Eigen::RowVector3d, 3>(-1.0, 1.0));
         }

         const Math::Polytope<3, double> expected(expected_vertices);

         const Math::ConvexHull<3, double> hull(data);

         const auto& computed_convex_hull = hull.GetPolytope();

         bool is_equal = true;
         for(  auto it_comp = computed_convex_hull.GetVertices().cbegin()
            ;  it_comp != computed_convex_hull.GetVertices().cend() 
            ;  it_comp++
            )
         {
            auto it_find = std::find(expected.GetVertices().cbegin(), expected.GetVertices().cend(), *it_comp);

            if (it_find == expected.GetVertices().cend())
            {
               is_equal = false;
               break;
            }
         }

         if (!is_equal)
         {
            std::cerr << "Found unexpected convexhull on non-trivial data set (with many data points)!" << std::endl;
            std::cerr << "Expected:" << std::endl;
            for (const auto& p : expected.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            std::cerr << "Computed:" << std::endl;
            for (const auto& p : computed_convex_hull.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            exit(1);
         }
      }

      {  // Test using the COMs from 6am6sa_0 structure from haide.
         const std::vector<Eigen::RowVector3d> data
            {  { 4.10908, -0.01276,  1.2352}
            ,  {-0.15521,  0.44329, -4.3620}
            ,  {-1.92943,  3.05064,  1.5997}
            ,  {-2.46032, -1.59865, -2.3860}
            ,  {-1.70814, -0.05085,  3.2981}
            ,  {-1.78358,  2.38941, -1.9003}
            ,  { 1.48983,  2.17264,  2.3328}
            ,  { 1.51706,  2.78992, -0.9971}
            ,  {-1.98296, -2.63568,  0.9305}
            ,  { 2.75122, -0.34697, -2.1542}
            ,  { 1.29091, -2.70037,  1.4941}
            ,  { 0.65748, -3.22331, -1.8383}
            };

         const auto expected_vertices(data);
         const Math::Polytope<3, double> expected(expected_vertices);

         const Math::ConvexHull<3, double> hull(data);
         const auto& computed_convex_hull = hull.GetPolytope();

         bool is_equal = true;
         for(  auto it_comp = computed_convex_hull.GetVertices().cbegin()
            ;  it_comp != computed_convex_hull.GetVertices().cend() 
            ;  it_comp++
            )
         {
            auto it_find = std::find(expected.GetVertices().cbegin(), expected.GetVertices().cend(), *it_comp);

            if (it_find == expected.GetVertices().cend())
            {
               is_equal = false;
               break;
            }
         }

         if (!is_equal || expected_vertices.size() != computed_convex_hull.GetVertices().size())
         {
            std::cerr << "Found unexpected convexhull on COMs from 6sa6am_0!" << std::endl;
            std::cerr << "Expected ("<<expected_vertices.size()<<"):" << std::endl;
            for (const auto& p : expected.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            std::cerr << "Computed ("<<computed_convex_hull.GetVertices().size()<<"):" << std::endl;
            for (const auto& p : computed_convex_hull.GetVertices())
            {
               std::cerr << p << "\n";
            }
            std::cerr << std::endl;
            exit(1);
         }
      }
      if (aDebug)
      {
         std::cout << "Exiting RunConvexHullTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Test to make sure we have not broken anything in the intended
    * path through the code for a couple small systems.
    * 
    * Tests a pair of clusters which should be found to be the same
    * conformer, and a pair of clusters which should be found different.
    * 
    * The two clusters of the same conformer:
    *    -  1...
    *    -  
    * 
    * The two clusters of different conformer:
    *    -  1..
    *    -  1...
    ********************************************************************/
   void RunFullClusterConformationsTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering FullClusterConformationsTest() !" << std::endl;
      }

      // Create scratch directory is it does not exists.
      std::filesystem::path cwd = std::filesystem::current_path();
      
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "FullClusterConformationTestData";
      std::filesystem::path ccsd_data_dir = test_dir / "CCSD_aug-cc-pVTZ";
      std::filesystem::path mp2_data_dir = test_dir / "RI-MP2_aug-cc-pVQZ";
      if (!std::filesystem::exists(test_dir))
      {
         std::filesystem::create_directories(test_dir);
      }
      if (!std::filesystem::exists(ccsd_data_dir))
      {
         std::filesystem::create_directories(ccsd_data_dir);
      }
      if (!std::filesystem::exists(mp2_data_dir))
      {
         std::filesystem::create_directories(mp2_data_dir);
      }

      Util::PrintCCSD1a1eda_01ToPath(ccsd_data_dir);
      Util::PrintCCSD1a1ma_01ToPath(ccsd_data_dir);

      Util::PrintMP21a1eda_01ToPath(mp2_data_dir);
      Util::PrintMP21a1ma_01ToPath(mp2_data_dir);

      // Structure identofication for compared methods
      Structure::StructureIdentification a_eda_compare("1a1eda_01", "RI-MP2", "aug-cc-pVQZ", false);
      Structure::StructureIdentification a_ma_compare("1a1ma_01", "RI-MP2", "aug-cc-pVQZ", false);

      // Setup input file
      std::filesystem::path input_file = test_dir / "FullClusterConformationTest.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_STRUCTURES\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << ccsd_data_dir.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           CCSD\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVTZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_data_dir.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           RI-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      const Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();

      std::map<std::string, Kabsch::RmsdData> rmsd_data_map = cluster_set.CalculateConformationRMSD();

      // Bool if tests should stop
      bool stop = false;

      for (const auto& [rmsd_data_name, rmsd_data] : rmsd_data_map)
      {
         for (const auto& [monomer_id, conf_enum, rmsd_vec] : rmsd_data)
         {
            for (const double& rmsd : rmsd_vec)
            {
               // When we compare rot mats.
               //if (  (monomer_id == a_eda_compare && std::abs(rmsd - 0.08778825418432348) > 1.0E-16)
               //   || (monomer_id == a_ma_compare && std::abs(rmsd - 3.116630642638029) > 4.5E-16)
               //   )
               // When simple RMSD is used i.e. atom to atom positional RMSD
               if (  (monomer_id == a_eda_compare && std::abs(rmsd - 0.03759011257067126) > 1.0E-16)
                  || (monomer_id == a_ma_compare && std::abs(rmsd - 0.6595413433486439) > 4.5E-16)
                  )
               {
                  std::cerr << "Unexpectedly large RMSD value for\n" << monomer_id << "With value: " << rmsd << std::endl;
                  stop = true;
               }
            }
         }
      }

      if (stop)
      {
         exit(1);
      }

      if (aDebug)
      {
         std::cout << "Done FullClusterConformationsTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Runs all the different cluster tests.
    ********************************************************************/
   void RunClusterTests(const bool aDebug)
   {
      RunClusterConstructionTest(aDebug);
   }

   /********************************************************************
    * Tests that the Cluster objects are constructed correctly for the
    * following structures:
    *    -  1fa1tma (No ProtonTransfer, also tested as single Molecule)
    *    -  1sa1dma (ProtonTransfer, also tested as single Molecule)
    *    -  1aa1dma (No ProtonTransfer, not tested as single Molecule)
    *    -  1msa1aa (No ProtonTransfer, not tested as single Molecule)
    *    -  1na1tma (ProtonTransfer, not tested as single Molecule)
    *    -  1na1eda (ProtonTransfer (long O-H-N bonds), not tested as
    *       single molecule)
    *    -  1am1dma1fa1msa_0 (PM7 optimized)
    ********************************************************************/
   void RunClusterConstructionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering ClusterConstructionTest() !" << std::endl;
      }

      std::filesystem::path cwd = std::filesystem::current_path();
      std::filesystem::path xyz_dir = cwd / "TestScratch" / "ClusterConstruction";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }
      
      // Print files to dir
      std::filesystem::path fa_tma_path = Util::PrintGunnar1fa1tmaToDir(xyz_dir);
      std::filesystem::path sa_dma_path = Util::PrintGunnar1sa1dmaToDir(xyz_dir);
      std::filesystem::path aa_dma_path = Util::PrintGunnar1aa1dmaToDir(xyz_dir);
      std::filesystem::path msa_aa_path = Util::PrintGunnar1msa1aaToDir(xyz_dir);
      std::filesystem::path na_tma_path = Util::PrintGunnar1na1tmaToDir(xyz_dir);
      std::filesystem::path na_eda_path = Util::PrintGunnar1na1edaToDir(xyz_dir);
      std::filesystem::path haide_15sa15dma_path = Util::PrintHaide15sa15dmaToDir(xyz_dir);
      std::filesystem::path PM71am1dma1fa1msa_0_path = Util::PrintPM71am1dma1fa1msa_0_ToDir(xyz_dir);

      {  // Read and construct 1fa1tma
         if (aDebug)
         {
            std::cout << "1fa1tma !" << std::endl;
         }
         Structure::StructureIdentification id("1fa1tma", "Unknown", "Unknown", true);
         //std::map<std::string, Structure::InputOptions> input_options;
         Structure::Cluster fa_tma = Structure::Cluster
            (  fa_tma_path
            ,  id
            //,  input_options
            );

         std::string cluster_name = "1fa1tma";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            };
         std::vector<std::string> cluster_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "C"
            ,  "N"
            ,  "O"
            ,  "O"
            ,  "C"
            ,  "H"
            };
         Eigen::Matrix<double, 18, 3> cluster_coords
            {  {  0.45650,    0.76640,  -0.00055 }
            ,  { -2.04316,    1.62723,   0.88326 }
            ,  { -2.04351,    1.62652,  -0.88421 }
            ,  { -3.09720,    0.50304,   0.00018 }
            ,  { -0.17327,   -1.45301,   1.19338 }
            ,  { -0.99098,   -0.16046,   2.08285 }
            ,  { -1.95456,   -1.40711,   1.26151 }
            ,  { -0.99169,   -0.16203,  -2.08279 }
            ,  { -0.17367,   -1.45390,  -1.19262 }
            ,  { -1.95498,   -1.40806,  -1.26018 }
            ,  { -2.11427,    0.99389,  -0.00021 }
            ,  { -1.04270,   -0.79792,   1.20077 }
            ,  { -1.04311,   -0.79883,  -1.20021 }
            ,  { -1.02614,    0.02822,  -0.00003 }
            ,  {  2.14218,   -0.90258,   0.00004 }
            ,  {  1.37566,    1.20888,  -0.00037 }
            ,  {  2.31534,    0.29072,  -0.00003 }
            ,  {  3.31523,    0.74631,   0.00011 }
            };
         
         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string fa_name = "C1H2O2_1";
         std::vector<Structure::AtomName> fa_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            };
         std::vector<std::string> fa_string_names =
            {  "H"
            ,  "O"
            ,  "O"
            ,  "C"
            ,  "H"
            };
         Eigen::Matrix<double, 5, 3> fa_coords
            {  { 0.45650,  0.76640,  -0.00055  }
            ,  { 2.14218, -0.90258,   0.00004  }
            ,  { 1.37566,  1.20888,  -0.00037  }
            ,  { 2.31534,  0.29072,  -0.00003  }
            ,  { 3.31523,  0.74631,   0.00011  }
            };
         
         Structure::Molecule fa_molecule(fa_name, std::make_pair(fa_names, fa_string_names), fa_coords, {0, 14, 15, 16, 17});

         std::string tma_name = "C3H9N1_1";
         std::vector<Structure::AtomName> tma_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            };
         std::vector<std::string> tma_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "C"
            ,  "N"
            };
         Eigen::Matrix<double, 13, 3> tma_coords
            {  { -2.04316,    1.62723,   0.88326 }
            ,  { -2.04351,    1.62652,  -0.88421 }
            ,  { -3.09720,    0.50304,   0.00018 }
            ,  { -0.17327,   -1.45301,   1.19338 }
            ,  { -0.99098,   -0.16046,   2.08285 }
            ,  { -1.95456,   -1.40711,   1.26151 }
            ,  { -0.99169,   -0.16203,  -2.08279 }
            ,  { -0.17367,   -1.45390,  -1.19262 }
            ,  { -1.95498,   -1.40806,  -1.26018 }
            ,  { -2.11427,    0.99389,  -0.00021 }
            ,  { -1.04270,   -0.79792,   1.20077 }
            ,  { -1.04311,   -0.79883,  -1.20021 }
            ,  { -1.02614,    0.02822,  -0.00003 }
            };
         
         Structure::Molecule tma_molecule(tma_name, std::make_pair(tma_names, tma_string_names), tma_coords, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});

         std::vector<Structure::Molecule> monomers = {tma_molecule, fa_molecule};

         Util::check_cluster_molecule_and_monomers(fa_tma, cluster_molecule, monomers);
      }

      {  // Read and construct 1sa1dma
         if (aDebug)
         {
            std::cout << "1sa1dma !" << std::endl;
         }
         Structure::StructureIdentification id("1sa1dma", "Unknown", "Unknown", true);
         //std::map<std::string, Structure::InputOptions> input_options;
         Structure::Cluster cluster = Structure::Cluster
            (  sa_dma_path
            ,  id
            //,  input_options
            );

         std::string cluster_name = "1sa1dma";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::S
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::H
            ,  Structure::AtomName::N
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> cluster_string_names =
            {  "H"
            ,  "S"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "H"
            ,  "N"
            ,  "H"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 17, 3> cluster_coords
            {  {  1.20478, -0.03492,  0.78833  }
            ,  { -1.27848, -0.12984,  0.00613  }
            ,  { -1.97089,  1.33367,  0.02603  }
            ,  { -0.43473, -0.13470, -1.20686  }
            ,  { -2.36060, -1.08161, -0.00684  }
            ,  { -0.44197, -0.11641,  1.21825  }
            ,  { -2.88047,  1.21309, -0.26117  }
            ,  {  1.89635,  0.00026,  0.00289  }
            ,  {  1.18551, -0.04512, -0.76901  }
            ,  {  2.75456, -1.19255,  0.00119  }
            ,  {  3.39012, -1.18687,  0.88372  }
            ,  {  3.37130, -1.19652, -0.89455  }
            ,  {  2.11878, -2.07347,  0.01236  }
            ,  {  2.60286,  1.28887, -0.01305  }
            ,  {  3.23380,  1.37076,  0.86900  }
            ,  {  1.86363,  2.08520, -0.01108  }
            ,  {  3.21484,  1.35793, -0.90940  }
            };
         
         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string sa_name = "H1O4S1_1";
         std::vector<Structure::AtomName> sa_names =
            {  Structure::AtomName::S
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::H
            };
         std::vector<std::string> sa_string_names =
            {  "S"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "H"
            };

         Eigen::Matrix<double, 6, 3> sa_coords
            {  { -1.27848, -0.12984,  0.00613  }
            ,  { -1.97089,  1.33367,  0.02603  }
            ,  { -0.43473, -0.13470, -1.20686  }
            ,  { -2.36060, -1.08161, -0.00684  }
            ,  { -0.44197, -0.11641,  1.21825  }
            ,  { -2.88047,  1.21309, -0.26117  }
            };
         
         // No division by Molecule class for single monomer as a proton transfer has happend
         Structure::Molecule sa_molecule(sa_name, std::make_pair(sa_names, sa_string_names), sa_coords, {1, 2, 3, 4, 5, 6});

         std::string dma_name = "C2H8N1_1";
         std::vector<Structure::AtomName> dma_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::N
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> dma_string_names =
            {  "H"
            ,  "N"
            ,  "H"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 11, 3> dma_coords
            {  {  1.20478, -0.03492,  0.78833 }
            ,  {  1.89635,  0.00026,  0.00289 }
            ,  {  1.18551, -0.04512, -0.76901 }
            ,  {  2.75456, -1.19255,  0.00119 }
            ,  {  3.39012, -1.18687,  0.88372 }
            ,  {  3.37130, -1.19652, -0.89455 }
            ,  {  2.11878, -2.07347,  0.01236 }
            ,  {  2.60286,  1.28887, -0.01305 }
            ,  {  3.23380,  1.37076,  0.86900 }
            ,  {  1.86363,  2.08520, -0.01108 }
            ,  {  3.21484,  1.35793, -0.90940 }
            };
         
         // No division by Molecule class for single monomer as a proton transfer has happend
         Structure::Molecule dma_molecule(dma_name, std::make_pair(dma_names, dma_string_names), dma_coords, {0, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});

         std::vector<Structure::Molecule> monomers = {sa_molecule, dma_molecule};

         Util::check_cluster_molecule_and_monomers(cluster, cluster_molecule, monomers);
      }

      {  // Read and construct 1aa1dma
         if (aDebug)
         {
            std::cout << "1aa1dma !" << std::endl;
         }
         Structure::StructureIdentification id("1aa1dma", "Unknown", "Unknown", true);
         //std::map<std::string, Structure::InputOptions> input_options;
         Structure::Cluster cluster = Structure::Cluster
            (  aa_dma_path
            ,  id
            //,  input_options
            );

         std::string cluster_name = "1aa1dma";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> cluster_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "N"
            ,  "O"
            ,  "O"
            ,  "C"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 18, 3> cluster_coords
            {  {  0.09764, -0.60699,  0.31385  }
            ,  {  1.29406,  0.30053, -1.09306  }
            ,  {  2.48200, -1.73997, -1.00672  }
            ,  {  3.64009, -0.42020, -0.73841  }
            ,  {  3.00542, -1.34467,  0.63055  }
            ,  {  2.11471,  0.80676,  1.66253  }
            ,  {  2.76680,  1.71329,  0.28750  }
            ,  {  1.03085,  1.78591,  0.66343  }
            ,  {  1.90503,  1.13753,  0.64470  }
            ,  {  2.74906, -0.92097, -0.34092  }
            ,  {  1.61583, -0.02190, -0.18904  }
            ,  { -0.85898, -0.82246,  0.54793  }
            ,  { -1.23619,  0.88204, -0.84644  }
            ,  { -1.64854,  0.01672, -0.10587  }
            ,  { -3.10626, -0.22334,  0.17450  }
            ,  { -3.71234,  0.47976, -0.38737  }
            ,  { -3.36821, -1.24530, -0.09692  }
            ,  { -3.29623, -0.11210,  1.24152  }
            };
         
         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string aa_name = "C2H4O2_1";
         std::vector<Structure::AtomName> aa_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> aa_string_names =
            {  "H"
            ,  "O"
            ,  "O"
            ,  "C"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 8, 3> aa_coords
            {  {  0.09764, -0.60699,  0.31385   }
            ,  { -0.85898, -0.82246,  0.54793   }
            ,  { -1.23619,  0.88204, -0.84644   }
            ,  { -1.64854,  0.01672, -0.10587   }
            ,  { -3.10626, -0.22334,  0.17450   }
            ,  { -3.71234,  0.47976, -0.38737   }
            ,  { -3.36821, -1.24530, -0.09692   }
            ,  { -3.29623, -0.11210,  1.24152   }
            };
         
         Structure::Molecule aa_molecule(aa_name, std::make_pair(aa_names, aa_string_names), aa_coords, {0, 11, 12, 13, 14, 15, 16, 17});

         std::string dma_name = "C2H6N1_1";
         std::vector<Structure::AtomName> dma_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            };
         std::vector<std::string> dma_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "N"
            };
         Eigen::Matrix<double, 10, 3> dma_coords
            {  {  1.29406,  0.30053, -1.09306  }
            ,  {  2.48200, -1.73997, -1.00672  }
            ,  {  3.64009, -0.42020, -0.73841  }
            ,  {  3.00542, -1.34467,  0.63055  }
            ,  {  2.11471,  0.80676,  1.66253  }
            ,  {  2.76680,  1.71329,  0.28750  }
            ,  {  1.03085,  1.78591,  0.66343  }
            ,  {  1.90503,  1.13753,  0.64470  }
            ,  {  2.74906, -0.92097, -0.34092  }
            ,  {  1.61583, -0.02190, -0.18904  }
            };
         
         Structure::Molecule dma_molecule(dma_name, std::make_pair(dma_names, dma_string_names), dma_coords, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});

         std::vector<Structure::Molecule> monomers = {dma_molecule, aa_molecule};

         Util::check_cluster_molecule_and_monomers(cluster, cluster_molecule, monomers);
      }

      {  // Read and construct 1msa1aa
         if (aDebug)
         {
            std::cout << "1msa1aa !" << std::endl;
         }
         Structure::StructureIdentification id("1msa1aa", "Unknown", "Unknown", true);
         //std::map<std::string, Structure::InputOptions> input_options;
         Structure::Cluster cluster = Structure::Cluster
            (  msa_aa_path
            ,  id
            //,  input_options
            );

         std::string cluster_name = "1msa1aa";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::S
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            };
         std::vector<std::string> cluster_string_names =
            {  "H"
            ,  "H"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "S"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            };
         Eigen::Matrix<double, 17, 3> cluster_coords
            {  {  0.12083,  1.04895,  0.49943 }
            ,  {  0.98001, -1.20806, -0.08188 }
            ,  { -0.70943, -1.20277, -0.13052 }
            ,  { -0.85237,  1.08414,  0.74949 }
            ,  { -2.80167, -0.35626,  0.91063 }
            ,  {  1.96707, -1.18072, -0.10469 }
            ,  {  1.66822,  1.02088,  0.13235 }
            ,  { -1.63978, -0.10727,  0.11401 }
            ,  { -2.14020,  0.53038, -1.46285 }
            ,  { -1.25057,  0.79577, -2.02737 }
            ,  { -2.69567, -0.25830, -1.96332 }
            ,  { -2.77078,  1.39725, -1.28879 }
            ,  {  3.89122,  0.16559, -0.06542 }
            ,  {  4.32753, -0.39642,  0.75958 }
            ,  {  4.25259, -0.28152, -0.99025 }
            ,  {  4.18877,  1.20670, -0.00632 }
            ,  {  2.40053,  0.05232, -0.00229 }
            };
         
         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string msa_name = "C1H4O3S1_1";
         std::vector<Structure::AtomName> msa_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::S
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> msa_string_names =
            {  "H"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "S"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 9, 3> msa_coords
            {  {  0.12083,  1.04895,  0.49943   }
            ,  { -0.70943, -1.20277, -0.13052   }
            ,  { -0.85237,  1.08414,  0.74949   }
            ,  { -2.80167, -0.35626,  0.91063   }
            ,  { -1.63978, -0.10727,  0.11401   }
            ,  { -2.14020,  0.53038, -1.46285   }
            ,  { -1.25057,  0.79577, -2.02737   }
            ,  { -2.69567, -0.25830, -1.96332   }
            ,  { -2.77078,  1.39725, -1.28879   }
            };
         
         Structure::Molecule msa_molecule(msa_name, std::make_pair(msa_names, msa_string_names), msa_coords, {0, 2, 3, 4, 7, 8, 9, 10, 11});

         std::string aa_name = "C2H4O2_1";
         std::vector<Structure::AtomName> aa_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            };
         std::vector<std::string> aa_string_names =
            {  "H"
            ,  "O"
            ,  "O"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            };
         Eigen::Matrix<double, 8, 3> aa_coords
            {  {  0.98001, -1.20806, -0.08188  }
            ,  {  1.96707, -1.18072, -0.10469  }
            ,  {  1.66822,  1.02088,  0.13235  }
            ,  {  3.89122,  0.16559, -0.06542  }
            ,  {  4.32753, -0.39642,  0.75958  }
            ,  {  4.25259, -0.28152, -0.99025  }
            ,  {  4.18877,  1.20670, -0.00632  }
            ,  {  2.40053,  0.05232, -0.00229  }
            };
         
         Structure::Molecule aa_molecule(aa_name, std::make_pair(aa_names, aa_string_names), aa_coords, {1, 5, 6, 12, 13, 14, 15, 16});

         std::vector<Structure::Molecule> monomers = {msa_molecule, aa_molecule};

         Util::check_cluster_molecule_and_monomers(cluster, cluster_molecule, monomers);
      }

      {  // Read and construct 1na1tma
         if (aDebug)
         {
            std::cout << "1na1tma !" << std::endl;
            std::cout << "Constructing Cluster !" << std::endl;
         }
         Structure::StructureIdentification id("1na1tma", "Unknown", "Unknown", true);
         //std::map<std::string, Structure::InputOptions> input_options;
         Structure::Cluster cluster = Structure::Cluster
            (  na_tma_path
            ,  id
            //,  input_options
            );

         std::string cluster_name = "1na1tma";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            };
         std::vector<std::string> cluster_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "C"
            ,  "N"
            ,  "O"
            ,  "O"
            ,  "N"
            ,  "O"  
            };
         Eigen::Matrix<double, 18, 3> cluster_coords
            {  { -0.26449,  0.44499,  0.00027 }
            ,  { -2.02481,  1.80591,  0.88668 }
            ,  { -2.02406,  1.80766, -0.88363 }
            ,  { -3.26324,  0.88230,  0.00010 }
            ,  { -0.72694, -1.55923,  1.19322 }
            ,  { -1.29815, -0.14122,  2.08577 }
            ,  { -2.47982, -1.19235,  1.26420 }
            ,  { -1.29817, -0.13804, -2.08596 }
            ,  { -0.72680, -1.55737, -1.19559 }
            ,  { -2.47973, -1.19054, -1.26598 }
            ,  { -2.22264,  1.20758,  0.00085 }
            ,  { -1.47461, -0.77211,  1.21802 }
            ,  { -1.47456, -0.77028, -1.21917 }
            ,  { -1.32482,  0.04271,  0.00005 }
            ,  {  1.42752, -1.09699, -0.00009 }
            ,  {  1.00505,  1.02952,  0.00026 }
            ,  {  1.86673,  0.06347,  0.00013 }
            ,  {  3.04540,  0.33042,  0.00001 }
            };
         
         if (aDebug)
         {
            std::cout << "Creating cluster_molecule" << std::endl;
         }
         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string na_name = "N1O3_1";
         std::vector<Structure::AtomName> na_names =
            {  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            };
         std::vector<std::string> na_string_names =
            {  "O"
            ,  "O"
            ,  "N"
            ,  "O"
            };
         Eigen::Matrix<double, 4, 3> na_coords
            {  {  1.42752, -1.09699, -0.00009   }
            ,  {  1.00505,  1.02952,  0.00026   }
            ,  {  1.86673,  0.06347,  0.00013   }
            ,  {  3.04540,  0.33042,  0.00001   }
            };
         
         if (aDebug)
         {
            std::cout << "Creating Molecule na" << std::endl;
         }
         // ProtonTransfer so do not divide single monomer
         Structure::Molecule na_molecule(na_name, std::make_pair(na_names, na_string_names), na_coords, {14, 15, 16, 17});

         std::string tma_name = "C3H9N1_1";
         std::vector<Structure::AtomName> tma_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            };
         std::vector<std::string> tma_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "C"
            ,  "N"
            };
         Eigen::Matrix<double, 14, 3> tma_coords
            {  { -0.26449,  0.44499,  0.00027  }
            ,  { -2.02481,  1.80591,  0.88668  }
            ,  { -2.02406,  1.80766, -0.88363  }
            ,  { -3.26324,  0.88230,  0.00010  }
            ,  { -0.72694, -1.55923,  1.19322  }
            ,  { -1.29815, -0.14122,  2.08577  }
            ,  { -2.47982, -1.19235,  1.26420  }
            ,  { -1.29817, -0.13804, -2.08596  }
            ,  { -0.72680, -1.55737, -1.19559  }
            ,  { -2.47973, -1.19054, -1.26598  }
            ,  { -2.22264,  1.20758,  0.00085  }
            ,  { -1.47461, -0.77211,  1.21802  }
            ,  { -1.47456, -0.77028, -1.21917  }
            ,  { -1.32482,  0.04271,  0.00005  }
            };
         
         if (aDebug)
         {
            std::cout << "Creating Molecule tma" << std::endl;
         }
         // ProtonTransfer so do not divide single monomer
         Structure::Molecule tma_molecule(tma_name, std::make_pair(tma_names, tma_string_names), tma_coords, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});

         std::vector<Structure::Molecule> monomers = {tma_molecule, na_molecule};

         Util::check_cluster_molecule_and_monomers(cluster, cluster_molecule, monomers);
      }

      {  // Read and construct 1na1eda
         if (aDebug)
         {
            std::cout << "1na1eda !" << std::endl;
         }
         Structure::StructureIdentification id("1na1eda", "Unknown", "Unknown", true);
         //std::map<std::string, Structure::InputOptions> input_options;
         Structure::Cluster cluster = Structure::Cluster
            (  na_eda_path
            ,  id
            //,  input_options
            );

         std::string cluster_name = "1na1eda";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            };
         std::vector<std::string> cluster_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "N"
            ,  "N"
            ,  "O"
            ,  "O"
            ,  "N"
            ,  "O"
            };
         Eigen::Matrix<double, 17, 3> cluster_coords
            {  { -0.08131,  1.15515, -0.32340}
            ,  {  2.96964,  1.06457, -0.01706}
            ,  {  1.76781,  1.61389,  1.17240}
            ,  {  2.55805, -0.76612,  1.52761}
            ,  {  0.81743, -0.61755,  1.33474}
            ,  {  2.47200, -2.17605, -0.22020}
            ,  {  0.90569, -1.79571, -0.53977}
            ,  {  1.17409,  2.06746, -1.19277}
            ,  {  1.07203,  0.42458, -1.40258}
            ,  {  1.95833,  0.91492,  0.36008}
            ,  {  1.77101, -0.53029,  0.81191}
            ,  {  0.99407,  1.18688, -0.72734}
            ,  {  1.82256, -1.41711, -0.34421}
            ,  { -1.11083, -0.88062, -0.62972}
            ,  { -1.33813,  1.09545,  0.23463}
            ,  { -1.82108, -0.08169,  0.00372}
            ,  { -2.92609, -0.35166,  0.40807}
            };

         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string na_name = "N1O3_1";
         std::vector<Structure::AtomName> na_names =
            {  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::N
            ,  Structure::AtomName::O
            };
         std::vector<std::string> na_string_names =
            {  "O"
            ,  "O"
            ,  "N"
            ,  "O"
            };
         Eigen::Matrix<double, 4, 3> na_coords
            {  { -1.11083, -0.88062, -0.62972 }
            ,  { -1.33813,  1.09545,  0.23463 }
            ,  { -1.82108, -0.08169,  0.00372 }
            ,  { -2.92609, -0.35166,  0.40807 }
            };
         
         // ProtonTransfer so do not divide single monomer
         Structure::Molecule na_molecule(na_name, std::make_pair(na_names, na_string_names), na_coords, {13, 14, 15, 16});

         std::string eda_name = "C2H9N2_1";
         std::vector<Structure::AtomName> eda_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::N
            ,  Structure::AtomName::N
            };
         std::vector<std::string> eda_string_names =
            {  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "C"
            ,  "N"
            ,  "N"
            };
         Eigen::Matrix<double, 13, 3> eda_coords
            {  { -0.08131,  1.15515, -0.32340 }
            ,  {  2.96964,  1.06457, -0.01706 }
            ,  {  1.76781,  1.61389,  1.17240 }
            ,  {  2.55805, -0.76612,  1.52761 }
            ,  {  0.81743, -0.61755,  1.33474 }
            ,  {  2.47200, -2.17605, -0.22020 }
            ,  {  0.90569, -1.79571, -0.53977 }
            ,  {  1.17409,  2.06746, -1.19277 }
            ,  {  1.07203,  0.42458, -1.40258 }
            ,  {  1.95833,  0.91492,  0.36008 }
            ,  {  1.77101, -0.53029,  0.81191 }
            ,  {  0.99407,  1.18688, -0.72734 }
            ,  {  1.82256, -1.41711, -0.34421 }
            };
         
         // ProtonTransfer so do not divide single monomer
         Structure::Molecule eda_molecule(eda_name, std::make_pair(eda_names, eda_string_names), eda_coords, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});

         std::vector<Structure::Molecule> monomers = {eda_molecule, na_molecule};

         Util::check_cluster_molecule_and_monomers(cluster, cluster_molecule, monomers);
      }

      {  // Read and construct 15sa15dma_0
         if (aDebug)
         {
            std::cout << "15sa15dma_0 !" << std::endl;
         }
         Structure::StructureIdentification id("15sa15dma", "Unknown", "Unknown", false);
         Structure::Cluster cluster = Structure::Cluster
            (  haide_15sa15dma_path
            ,  id
            );

         if (  cluster.GetNumMonomers() != 30
            )
         {
            std::cerr << "Unexpected number of monomers for\n" << id << std::endl;
            std::cerr << "Found " << cluster.GetNumMonomers() << std::endl;
            std::cerr << "Expected " << 30 << std::endl;
            exit(1);
         }
      }

      {  // Read and construct 1am1dma1fa1msa_0 optimized by pm7
         // This test has introduced a minimum allowed distance in the ConstructNonHydrogenConnectivityMap.
         if (aDebug)
         {
            std::cout << "1am1dma1fa1msa_0 !" << std::endl;
         }
         Structure::StructureIdentification id("1am1dma1fa1msa_0", "PM7", "", false);
         Structure::Cluster cluster = Structure::Cluster
            (  PM71am1dma1fa1msa_0_path
            ,  id
            );


         std::string cluster_name = "1am1dma1fa1msa_0";
         std::vector<Structure::AtomName> cluster_names =
            {  Structure::AtomName::S
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::C
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::N
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::N
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> cluster_string_names =
            {  "S"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "H"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "C"
            ,  "O"
            ,  "O"
            ,  "N"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "N"
            ,  "C"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 28, 3> cluster_coords
            {  {  1.97012,       0.04862,       0.06963}
            ,  {  1.58479,      -1.34042,      -0.33978}
            ,  {  1.60001,       0.26992,       1.47890}
            ,  {  1.37277,       1.08600,      -0.82184}
            ,  { -0.37110,       1.35133,      -0.88132}
            ,  {  3.73531,       0.16465,      -0.08034}
            ,  {  4.14457,      -0.68328,      -0.63818}
            ,  {  4.03051,       1.07936,      -0.60650}
            ,  {  4.21163,       0.17747,       0.90644}
            ,  { -4.16230,      -1.66524,       0.69835}
            ,  { -0.66006,      -1.97600,      -0.44798}
            ,  { -3.13491,      -1.36246,       0.46229}
            ,  { -2.26577,      -2.23544,       0.33332}
            ,  { -2.94273,      -0.14053,       0.27278}
            ,  {  0.05208,      -2.71068,      -0.62063}
            ,  { -0.29850,      -3.56451,      -0.20489}
            ,  {  0.95796,      -2.38284,      -0.18536}
            ,  {  0.18799,      -2.79178,      -1.62557}
            ,  { -1.22473,       1.56076,      -0.28283}
            ,  { -2.27384,       2.26557,      -1.08079}
            ,  { -0.79441,       2.41280,       0.87115}
            ,  { -1.68498,       0.59452,       0.11980}
            ,  { -1.93395,       3.25262,      -1.42509}
            ,  { -2.53886,       1.65240,      -1.95884}
            ,  { -3.19728,       2.37974,      -0.48292}
            ,  { -0.27342,       3.31965,       0.52671}
            ,  { -1.64976,       2.68353,       1.50425}
            ,  { -0.06118,       1.84495,       1.49045}
            };

         Structure::Molecule cluster_molecule(cluster_name, std::make_pair(cluster_names, cluster_string_names), cluster_coords, {});

         std::string msa_name = "C1H3O3S1_1";
         std::vector<Structure::AtomName> msa_names =
            {  Structure::AtomName::S
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> msa_string_names =
            {  "S"
            ,  "O"
            ,  "O"
            ,  "O"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 8, 3> msa_coords
            {  {  1.97012,       0.04862,       0.06963 }
            ,  {  1.58479,      -1.34042,      -0.33978 }
            ,  {  1.60001,       0.26992,       1.47890 }
            ,  {  1.37277,       1.08600,      -0.82184 }
            ,  {  3.73531,       0.16465,      -0.08034 }
            ,  {  4.14457,      -0.68328,      -0.63818 }
            ,  {  4.03051,       1.07936,      -0.60650 }
            ,  {  4.21163,       0.17747,       0.90644 }
            };
         
         Structure::Molecule msa_molecule(msa_name, std::make_pair(msa_names, msa_string_names), msa_coords, {0, 1, 2, 3, 5, 6, 7, 8});

         std::string fa_name = "C1H1O2_1";
         std::vector<Structure::AtomName> fa_names =
            {  Structure::AtomName::C
            ,  Structure::AtomName::O
            ,  Structure::AtomName::O
            ,  Structure::AtomName::H
            };
         std::vector<std::string> fa_string_names =
            {  "C"
            ,  "O"
            ,  "O"
            ,  "H"
            };
         Eigen::Matrix<double, 4, 3> fa_coords
            {  { -4.16230,       -1.66524,       0.69835}
            ,  { -3.13491,       -1.36246,       0.46229}
            ,  { -2.26577,       -2.23544,       0.33332}
            ,  { -2.94273,       -0.14053,       0.27278}
            };
         
         Structure::Molecule fa_molecule(fa_name, std::make_pair(fa_names, fa_string_names), fa_coords, {9, 11, 12, 13});

         std::string am_name = "H4N1_1";
         std::vector<Structure::AtomName> am_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::N
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> am_string_names =
            {  "H"
            ,  "N"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 5, 3> am_coords
            {  {-0.66006,      -1.97600,      -0.44798}
            ,  { 0.05208,      -2.71068,      -0.62063}
            ,  {-0.29850,      -3.56451,      -0.20489}
            ,  { 0.95796,      -2.38284,      -0.18536}
            ,  { 0.18799,      -2.79178,      -1.62557}
            };
         
         Structure::Molecule am_molecule(am_name, std::make_pair(am_names, am_string_names), am_coords, {10, 14, 15, 16, 17});

         std::string dma_name = "C2H8N1_1";
         std::vector<Structure::AtomName> dma_names =
            {  Structure::AtomName::H
            ,  Structure::AtomName::N
            ,  Structure::AtomName::C
            ,  Structure::AtomName::C
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            ,  Structure::AtomName::H
            };
         std::vector<std::string> dma_string_names =
            {  "H"
            ,  "N"
            ,  "C"
            ,  "C"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            ,  "H"
            };
         Eigen::Matrix<double, 11, 3> dma_coords
            {  {-0.37110,       1.35133,      -0.88132}
            ,  {-1.22473,       1.56076,      -0.28283}
            ,  {-2.27384,       2.26557,      -1.08079}
            ,  {-0.79441,       2.41280,       0.87115}
            ,  {-1.68498,       0.59452,       0.11980}
            ,  {-1.93395,       3.25262,      -1.42509}
            ,  {-2.53886,       1.65240,      -1.95884}
            ,  {-3.19728,       2.37974,      -0.48292}
            ,  {-0.27342,       3.31965,       0.52671}
            ,  {-1.64976,       2.68353,       1.50425}
            ,  {-0.06118,       1.84495,       1.49045}
            };
         
         Structure::Molecule dma_molecule(dma_name, std::make_pair(dma_names, dma_string_names), dma_coords, {4, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27});

         std::vector<Structure::Molecule> monomers = {msa_molecule, fa_molecule, am_molecule, dma_molecule};

         Util::check_cluster_molecule_and_monomers(cluster, cluster_molecule, monomers);
      }

      if (aDebug)
      {
         std::cout << "Done ClusterConstructionTest() !" << std::endl;
      }
   }

   /********************************************************************
    * 
    ********************************************************************/
   void RunMoleculeAtomConnectivityMapTests(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering MoleculeAtomConnectivityMapTest() !" << std::endl;
      }

      {
         Structure::Molecule sa_na_molecule = Util::ConstructGunnar1sa1naMolecule();

         std::map<Uin, std::vector<Uin>> expected_connected_atoms = 
            {  {3, {9}}
            ,  {4, {9}}
            ,  {5, {9}}
            ,  {6, {9}}
            ,  {7, {11}}
            ,  {8, {11}}
            ,  {9, {3, 4, 5, 6}}
            ,  {10, {11}}
            ,  {11, {10, 8, 7}}
            };

         Util::check_connectivity_map(sa_na_molecule.GetAtomConnectivityMap(), expected_connected_atoms, "1sa1na");
      }

      {
         Structure::Molecule fa_tma_molecule = Util::ConstructGunnar1fa1tmaMolecule();

         std::map<Uin, std::vector<Uin>> expected_connected_atoms = 
            {  {10, {13}}
            ,  {11, {13}}
            ,  {12, {13}}
            ,  {13, {10, 11, 12}}
            ,  {14, {16}}
            ,  {15, {16}}
            ,  {16, {14, 15}}
            };
         Util::check_connectivity_map(fa_tma_molecule.GetAtomConnectivityMap(), expected_connected_atoms, "1fa1tma");
      }
      
      {
         Structure::Molecule sa_dma_molecule = Util::ConstructGunnar1sa1dmaMolecule();

         std::map<Uin, std::vector<Uin>> expected_connected_atoms = 
            {  {1, {4, 5, 3, 2}}
            ,  {2, {1}}
            ,  {3, {1}}
            ,  {4, {1}}
            ,  {5, {1}}
            ,  {7, {9, 13}}
            ,  {9, {7}}
            ,  {13, {7}}
            };
         Util::check_connectivity_map(sa_dma_molecule.GetAtomConnectivityMap(), expected_connected_atoms, "1sa1dma");
      }

      {
         Structure::Molecule a_eda_molecule = Util::ConstructCCSD1a1edaMolecule();

         std::map<Uin, std::vector<Uin>> expected_connected_atoms = 
            {  {0, {}}
            ,  {12, {14, 13}}
            ,  {13, {15, 12}}
            ,  {14, {12}}
            ,  {15, {13}}
            };
         Util::check_connectivity_map(a_eda_molecule.GetAtomConnectivityMap(), expected_connected_atoms, "1a1eda");
      }

      {
         Structure::Molecule sa_tma_molecule = Util::ConstructGunnar1sa1tmaMolecule();

         std::map<Uin, std::vector<Uin>> expected_connected_atoms = 
            {  {11, {14}}
            ,  {12, {14}}
            ,  {13, {14}}
            ,  {14, {11, 12, 13}}
            ,  {15, {19}}
            ,  {16, {19}}
            ,  {17, {19}}
            ,  {18, {19}}
            ,  {19, {15, 16, 17, 18}}
            };
         Util::check_connectivity_map(sa_tma_molecule.GetAtomConnectivityMap(), expected_connected_atoms, "1sa1tma");
      }

      {
         Structure::Molecule aa_a_molecule = Util::ConstructGunnar1aa1aMolecule();

         std::map<Uin, std::vector<Uin>> expected_connected_atoms = 
            {  {4, {}}
            ,  {5, {7}}
            ,  {6, {7}}
            ,  {7, {6, 5, 8}}
            ,  {8, {7}}
            };
         Util::check_connectivity_map(aa_a_molecule.GetAtomConnectivityMap(), expected_connected_atoms, "1aa1a");
      }

      if (aDebug)
      {
         std::cout << "Done MoleculeAtomConnectivityMapTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Tests all Molecule tests
    ********************************************************************/
   void RunMoleculeTests(const bool aDebug)
   {
      // New non-hydrogen atom based approach
      RunMoleculeAtomConnectivityMapTests(aDebug);
   }

   /********************************************************************
    * Run some tests for the NormalDistribution class
    ********************************************************************/
   template<std::floating_point T>
   void RunNormalDistributionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering NormalDistributionTest() !" << std::endl;
      }

      // Small sanity tests
      {
         auto data = std::vector<T>{2.0};
         Math::NormalDistribution<std::vector<T>> norm_dist(data, 1);
         if (  norm_dist.ComputeMean(data) != 2.0
            || norm_dist.ComputeStandardDeviation(data) != 0.0
            )
         {
            std::cerr << "Unexpected NormalDistribution for 1 data point. Found:\n"
               << "Mean     = " << norm_dist.ComputeMean(data) << "\n"
               << "StdDevi  = " << norm_dist.ComputeStandardDeviation(data) << "\n"
               << std::endl;
            exit(1);
         }
      }

      // Slightly bigger test
      {
         auto data = std::vector<T>{0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
         Math::NormalDistribution norm_dist(data, 1);
         if (  norm_dist.ComputeMean(data) != 4.5
            || norm_dist.ComputeStandardDeviation(data) != std::sqrt(8.25)
            )
         {
            std::cerr << "Unexpected NormalDistribution for 10 data points\n"
               << "Mean     = " << norm_dist.ComputeMean(data) << "\n"
               << "StdDevi  = " << norm_dist.ComputeStandardDeviation(data) << "\n"
               << "Expected: \n"
               << "4.5\n"
               << std::sqrt(8.25) << "\n"
               << "8.25\n"
               << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Done NormalDistributionTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Run some tests for the CauchyDistribution class
    ********************************************************************/
   template<std::floating_point T>
   void RunCauchyDistributionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering CauchyDistributionTest() !" << std::endl;
      }

      // Small sanity tests
      {
         auto data = std::vector<T>{2.0};
         Math::CauchyDistribution<std::vector<T>> dist(data, 1);
         if (  dist.ComputeMedian(data) != 2.0
            )
         {
            std::cerr << "Unexpected CauchyDistribution for 1 data point. Found:\n"
               << "Mean     = " << dist.ComputeMedian(data) << "\n"
               << std::endl;
            exit(1);
         }
      }

      // Slightly bigger test
      {
         auto data = std::vector<T>{0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
         Math::CauchyDistribution dist(data, 1);
         if (  dist.ComputeMedian(data) != 4.5
            )
         {
            std::cerr << "Unexpected CauchyDistribution for 10 data points\n"
               << "Mean     = " << dist.ComputeMedian(data) << "\n"
               << "Expected: \n"
               << "4.5\n"
               << std::endl;
            exit(1);
         }
      }
      // Slightly bigger test
      {
         auto data = std::vector<T>{0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
         Math::CauchyDistribution dist(data, 1);
         if (  dist.ComputeMedian(data) != 5.0
            )
         {
            std::cerr << "Unexpected CauchyDistribution for 11 data points\n"
               << "Mean     = " << dist.ComputeMedian(data) << "\n"
               << "Expected: \n"
               << "5.0\n"
               << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Done CauchyDistributionTest() !" << std::endl;
      }
   }

   namespace detail
   {
      /********************************************************************
       * Will find minimum of the Rosenbrock function:
       *    f(x, y) = (1 - x)^2 + 100(y - x^2)^2
       ********************************************************************/
      template
         <  FloatingPointContainer CONT
         >
      class RosenbrockMinimiser
      {
         public:
         //! Aliases
         using value_type = CONT::value_type;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;
         using mat_type = Eigen::Matrix<value_type, Eigen::Dynamic, Eigen::Dynamic>;

         RosenbrockMinimiser() = default;

         //! Function to compute the value of the function currently being optimised.
         value_type ComputeFunctionValue(const vec_type& arParams) const {
            return std::pow(1 - arParams(0), 2) + 100 * std::pow(arParams(1) - std::pow(arParams(0), 2), 2);
         }

         //! Function to compute the gradient vector for all the current parameters.
         void ComputeGradientVector(const vec_type& arParams, vec_type& arGradient) const {
            value_type x = arParams(0);
            value_type y = arParams(1);

            // Derivative wrt. x
            arGradient(0) = -2*(1 - x) - 400*x*(y - std::pow(x, 2));

            // Derivative wrt. y
            arGradient(1) = 200*(y - std::pow(x, 2));
         }

         //! Operator() overload, used in LBFGS++.
         value_type operator()(const vec_type& arParams, vec_type& arGradient)
         {
            ComputeGradientVector(arParams, arGradient);
            return ComputeFunctionValue(arParams);
         }
      };
   }

   /********************************************************************
    * Run some tests for the LBFGS++ library.
    ********************************************************************/
   void RunLBFGSppTest(const bool aDebug)
   {
      using value_type = double;
      using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;

      if (aDebug)
      {
         std::cout << "Entering RunLBFGSppTest()" << std::endl;
      }

      // Unconstrained Rosenbrock minimisation
      {
         LBFGSpp::LBFGSBParam<value_type> params;
         params.epsilon = 1.0e-10;
         params.max_iterations = 100;

         LBFGSpp::LBFGSBSolver<value_type> solver(params);

         vec_type lower_bounds = vec_type::Constant(2, -std::numeric_limits<value_type>::infinity());
         vec_type upper_bounds = vec_type::Constant(2, +std::numeric_limits<value_type>::infinity());

         vec_type start_guess = vec_type::Constant(2, 3.0);
         detail::RosenbrockMinimiser<vec_type> rosenbrock;

         value_type func_val;

         int num_iter = solver.minimize(rosenbrock, start_guess, func_val, lower_bounds, upper_bounds);

         vec_type expected_vec(2);
         expected_vec << 1.0, 1.0;

         value_type expected_val(0.0);

         if (  num_iter != 27
            || (start_guess - expected_vec).norm() > 3.0E-8
            || std::abs(func_val - expected_val) > 1.0E-15
            )
         {
            std::cout << "Bad optimisation in Unconstrained RosenBrock minimisation" << std::endl;
            std::cout << "Used iterations:     " << num_iter << std::endl;
            std::cout << "Expected iterations: " << 27 << std::endl;
            std::cout << "Optimised parameters:\n" << start_guess << std::endl;
            std::cout << "Expected parameters: \n" << expected_vec << std::endl;
            std::cout << "(opt - expect).norm(): \n" << (start_guess - expected_vec).norm() << std::endl;
            std::cout << "Function value at optimised parameters: " << func_val << std::endl;
            std::cout << "Function value at expected parameters:  " << expected_val << std::endl;
            exit(1);
         }
      }

      // Constrained Rosenbrock minimisation
      {
         LBFGSpp::LBFGSBParam<value_type> params;
         params.epsilon = 1.0e-7;
         params.max_iterations = 100;

         LBFGSpp::LBFGSBSolver<value_type> solver(params);

         vec_type lower_bounds = vec_type::Constant(2, 2.0);
         vec_type upper_bounds = vec_type::Constant(2, 4.0);

         vec_type start_guess = vec_type::Constant(2, 3.0);
         detail::RosenbrockMinimiser<vec_type> rosenbrock;

         value_type func_val;

         int num_iter = solver.minimize(rosenbrock, start_guess, func_val, lower_bounds, upper_bounds);

         vec_type expected_vec(2);
         expected_vec << 2.0, 4.0;

         value_type expected_val(1.0);

         if (  num_iter != 5
            || start_guess != expected_vec
            || std::abs(func_val - expected_val) > 1.0E-15
            )
         {
            std::cout << "Bad optimisation in Constrained RosenBrock minimisation" << std::endl;
            std::cout << "Used iterations:     " << num_iter << std::endl;
            std::cout << "Expected iterations: " << 5 << std::endl;
            std::cout << "Optimised parameters:\n" << start_guess << std::endl;
            std::cout << "Expected parameters: \n" << expected_vec << std::endl;
            std::cout << "Function value at optimised parameters: " << func_val << std::endl;
            std::cout << "Function value at expected parameters:  " << expected_val << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Exiting RunLBFGSppTest()" << std::endl;
      }
   }

   /********************************************************************
    * Run some tests for the MaximumLogLikelihood with a
    * NormalDistribution class.
    ********************************************************************/
   void RunMaximumLogLikelihoodOptimiserNormalDistributionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunMaximumLogLikelihoodOptimiserNormalDistributionTest() !" << std::endl;
      }

      using cont = std::vector<double>;
      using dist = Math::NormalDistribution<cont>;
      using optimiser = Math::MaximumLogLikelihoodOptimiser<cont, dist>;

      // Maximise likelihood for data generated from a single distribution
      {
         double mean = 4;
         double std_devi = 0.02;
         std::vector<double> test_data = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean, std_devi, 300);
         std::sort(test_data.begin(), test_data.end());

         optimiser max_like_opti(test_data, 1, 1.0E-8);

         const auto& max_like = max_like_opti.GetMaxLogLikelihood();

         if (  std::abs(max_like.GetOptimisedParam<0>(0) - mean) / mean > 1.0E-3
            || std::abs(max_like.GetOptimisedParam<1>(0) - std_devi) / std_devi > 1.0E-1
            )
         {
            std::cerr << "Unexpected deviation from mean and std-devi for single normal distribution !\n"
               << "Expected mean: " << mean << "\n"
               << "Computed mean: " << max_like.GetOptimisedParam<0>(0) << "\n"
               << "Expected std-devi: " << std_devi << "\n"
               << "Computed std-devi: " << max_like.GetOptimisedParam<1>(0) << "\n"
               << "Deviation (mean) = " << std::abs(max_like.GetOptimisedParam<0>(0) - mean) / mean << "\n"
               << "Deviation (std-devi) = " << std::abs(max_like.GetOptimisedParam<1>(0) - std_devi) / std_devi << "\n"
               << std::endl;
            exit(1);
         }
      }

      // Maximise likelihood for data generated from two distribution far apart
      {
         double mean_1 = 1;
         double std_devi_1 = 0.4;
         double mean_2 = 20;
         double std_devi_2 = 0.1;
         std::vector<double> test_data_1 = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean_1, std_devi_1, 300);
         std::vector<double> test_data_2 = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean_2, std_devi_2, 300);
         std::vector<double> test_data(test_data_1);
         test_data.insert(test_data.end(), test_data_2.begin(), test_data_2.end());
         std::sort(test_data.begin(), test_data.end());
         
         optimiser max_like_opti(test_data, 2, 1.0E-8);

         const auto& max_like = max_like_opti.GetMaxLogLikelihood();

         if (  std::abs(max_like.GetOptimisedParam<0>(0) - mean_1) / mean_1 > 5.0E-2
            || std::abs(max_like.GetOptimisedParam<1>(0) - std_devi_1) / std_devi_1 > 8.0E-2
            || std::abs(max_like.GetOptimisedParam<0>(1) - mean_2) / mean_2 > 1.0E-3
            || std::abs(max_like.GetOptimisedParam<1>(1) - std_devi_2) / std_devi_2 > 8.0E-2
            )
         {
            std::cerr << "Unexpected deviation from mean and std-devi for two normal distributions far apart !\n"
               << "Expected mean: " << mean_1 << ", " << mean_2 << "\n"
               << "Computed mean: " << max_like.GetOptimisedParam<0>(0) << ", " << max_like.GetOptimisedParam<0>(1) << "\n"
               << "Expected std-devi: " << std_devi_1 << ", " << std_devi_2 << "\n"
               << "Computed std-devi: " << max_like.GetOptimisedParam<1>(0) << ", " << max_like.GetOptimisedParam<1>(1) << "\n"
               << "Deviation (mean_1) = " << std::abs(max_like.GetOptimisedParam<0>(0) - mean_1) / mean_1 << "\n"
               << "Deviation (std-devi_1) = " << std::abs(max_like.GetOptimisedParam<1>(0) - std_devi_1) / std_devi_1 << "\n"
               << "Deviation (mean_2) = " << std::abs(max_like.GetOptimisedParam<0>(1) - mean_2) / mean_2 << "\n"
               << "Deviation (std-devi_2) = " << std::abs(max_like.GetOptimisedParam<1>(1) - std_devi_2) / std_devi_2 << "\n"
               << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Exiting RunMaximumLogLikelihoodOptimiserNormalDistributionTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Run some tests for the MaximumLogLikelihood with a
    * CauchyDistribution class.
    ********************************************************************/
   void RunMaximumLogLikelihoodOptimiserCauchyDistributionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunMaximumLogLikelihoodOptimiserCauchyDistributionTest() !" << std::endl;
      }

      using cont = std::vector<double>;
      using dist = Math::CauchyDistribution<cont>;
      using optimiser = Math::MaximumLogLikelihoodOptimiser<cont, dist>;

      // Maximise likelihood for data generated from a single distribution
      {
         double median = 4;
         double gamma = 0.02;
         std::vector<double> test_data = Util::GetRandomDistributedData<double, std::cauchy_distribution<double>>(median, gamma, 300);
         std::sort(test_data.begin(), test_data.end());

         optimiser max_like_opti(test_data, 1, 1.0E-8);

         const auto& max_like = max_like_opti.GetMaxLogLikelihood();

         if (  std::abs(max_like.GetOptimisedParam<0>(0) - median) / median > 1.0E-3
            || std::abs(max_like.GetOptimisedParam<1>(0) - gamma) / gamma > 1.0E-1
            )
         {
            std::cerr << "Unexpected deviation from median and gamma for single cauchy distribution !\n"
               << "Expected median: " << median << "\n"
               << "Computed median: " << max_like.GetOptimisedParam<0>(0) << "\n"
               << "Expected gamma:  " << gamma << "\n"
               << "Computed gamma:  " << max_like.GetOptimisedParam<1>(0) << "\n"
               << "Deviation (median) = " << std::abs(max_like.GetOptimisedParam<0>(0) - median) / median << "\n"
               << "Deviation (gamma) =  " << std::abs(max_like.GetOptimisedParam<1>(0) - gamma) / gamma << "\n"
               << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Exiting RunMaximumLogLikelihoodOptimiserCauchyDistributionTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Run some tests for the MonteCarloIntegration class.
    ********************************************************************/
   void RunMonteCarloIntegrationTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunMonteCarloIntegrationTest() !" << std::endl;
      }
      using value_type = double;
      using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;

      {
         if (aDebug)
         {
            std::cout << "About to compute integral of 1/sqrt(x * y * z) from [1, 1, 1] to [2, 2, 2]" << std::endl;
         }
         
         // Lambda for 1/sqrt(x*y*z*...)
         auto f = [](const vec_type& aVec) {
            value_type prod = std::transform_reduce
                                 (  aVec.begin(), aVec.end()
                                 ,  value_type(1)
                                 ,  std::multiplies<>()
                                 ,  [](const value_type elem)
                                    {
                                       return elem;
                                    }
                                 );
            return 1.0/std::sqrt(prod);
         };

         value_type thr = 0.01;

         vec_type lower_limits = vec_type::Constant(3, 1);
         vec_type upper_limits = vec_type::Constant(3, 2);

         Math::MonteCarloIntegration func_integration
            (  f
            ,  thr
            ,  10000
            ,  lower_limits
            ,  upper_limits
            );

         value_type expected_integral = 8 * (5*std::sqrt(2) - 7);

         if (  std::abs(func_integration.GetIntegral() - expected_integral) > thr
            || aDebug
            )
         {
            std::cerr << "Integral (Monte-Carlo) of 1/sqrt(x * y * z) from [1, 1, 1] to [2, 2, 2] gave unexpected value" << std::endl;
            std::cerr << "Integral (computed): " << func_integration.GetIntegral() << std::endl;
            std::cerr << "Integral (expected): " << expected_integral << std::endl;
            std::cerr << "Deviation: " << std::abs(func_integration.GetIntegral() - expected_integral) << std::endl;
            std::cerr << "Threshold: " << thr << std::endl;
            std::cout << "Recursive calls: " << func_integration.GetNumRecursiveCalls() << std::endl;
         }
      }

      {
         if (aDebug)
         {
            std::cout << "About to compute integral of x/(exp(x) - 1) from [0] to [+inf]" << std::endl;
         }
         
         // Lambda for x/(exp(x) - 1)
         auto f = [](const vec_type& aVec) {
            return aVec(0)/(std::exp(aVec(0)) - 1);
         };

         value_type thr = 0.005;

         vec_type lower_limits = vec_type::Constant(1, value_type(0));
         vec_type upper_limits = vec_type::Constant(1, +std::numeric_limits<value_type>::infinity());

         Math::MonteCarloIntegration func_integration
            (  f
            ,  thr
            ,  20000
            ,  lower_limits
            ,  upper_limits
            );

         value_type expected_integral = std::pow(std::numbers::pi_v<value_type>, 2) / 6.0;

         if (  std::abs(func_integration.GetIntegral() - expected_integral) > 3 * thr
            || aDebug
            )
         {
            std::cerr << "Integral (Monte-Carlo) of x/(exp(x) - 1) from [0] to [+inf] gave unexpected value" << std::endl;
            std::cerr << "Integral (computed): " << func_integration.GetIntegral() << std::endl;
            std::cerr << "Integral (expected): " << expected_integral << std::endl;
            std::cerr << "Deviation: " << std::abs(func_integration.GetIntegral() - expected_integral) << std::endl;
            std::cerr << "Threshold: " << 3 * thr << std::endl;
            std::cout << "Recursive calls: " << func_integration.GetNumRecursiveCalls() << std::endl;
         }
      }

      {
         if (aDebug)
         {
            std::cout << "About to compute integral of exp(-x^2 - y^2) from [-inf, -inf] to [+inf, -inf]" << std::endl;
         }
         
         // Lambda for exp(-x^2 - y^2)
         auto f = [](const vec_type& aVec) {
            value_type exponent = std::accumulate
                                    (  aVec.begin(), aVec.end()
                                    ,  value_type(0)
                                    ,  [](const value_type acc, const value_type elem)
                                       {
                                          return acc - elem*elem;
                                       }
                                    );
            return std::exp(exponent);
         };

         value_type thr = 0.05;

         vec_type lower_limits = vec_type::Constant(2, -std::numeric_limits<value_type>::infinity());
         vec_type upper_limits = vec_type::Constant(2, +std::numeric_limits<value_type>::infinity());

         Math::MonteCarloIntegration func_integration
            (  f
            ,  thr
            ,  10000
            ,  lower_limits
            ,  upper_limits
            );

         value_type expected_integral = std::numbers::pi_v<value_type>;

         if (  std::abs(func_integration.GetIntegral() - expected_integral) > 2 * thr
            || aDebug
            )
         {
            std::cerr << "Integral (Monte-Carlo) of exp(-x^2 - y^2) from [-inf, -inf] to [+inf, +inf] gave unexpected value" << std::endl;
            std::cerr << "Integral (computed): " << func_integration.GetIntegral() << std::endl;
            std::cerr << "Integral (expected): " << expected_integral << std::endl;
            std::cerr << "Deviation: " << std::abs(func_integration.GetIntegral() - expected_integral) << std::endl;
            std::cerr << "Threshold: " << 2 * thr << std::endl;
            std::cout << "Recursive calls: " << func_integration.GetNumRecursiveCalls() << std::endl;
         }
      }


      if (aDebug)
      {
         std::cout << "Exiting RunMonteCarloIntegrationTest() !" << std::endl;
      }
   }

   /********************************************************************
    * Tests the quadrature integration scheme.
    ********************************************************************/
   void RunQuadratureIntegrationTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunQuadratureIntegrationTest() !" << std::endl;
      }
      using value_type = double;

      {
         if (aDebug)
         {
            std::cout << "About to compute integral of 1/sqrt(x) from [1] to [2]" << std::endl;
         }
         
         // Lambda for 1/sqrt(x)
         auto f = [](const value_type& aVal) {
            return 1.0/std::sqrt(aVal);
         };

         value_type thr = 1.0E-7;

         value_type lower_limit = 1;
         value_type upper_limit = 2;

         Math::QuadratureIntegration func_integration(f, thr, lower_limit, upper_limit);

         value_type expected_integral = 2 * (std::sqrt(2) - 1);
         value_type computed_integral = func_integration.GetIntegral();

         if (  std::abs(computed_integral - expected_integral) > thr
            || aDebug
            )
         {
            std::cerr << "Integral (Quadrature) of 1/sqrt(x) from [1] to [2] gave unexpected value" << std::endl;
            std::cerr << "Integral (computed): " << computed_integral << std::endl;
            std::cerr << "Integral (expected): " << expected_integral << std::endl;
            std::cerr << "Deviation: " << std::abs(computed_integral - expected_integral) << std::endl;
            std::cerr << "Threshold: " << thr << std::endl;
         }
      }

      {
         if (aDebug)
         {
            std::cout << "About to compute integral of x/(exp(x) - 1) from [0] to [+inf]" << std::endl;
         }

         // Lambda for x/(exp(x) - 1)
         auto f = [](const value_type& aVal) {
            return aVal/(std::exp(aVal) - 1);
         };

         value_type thr = 1.0E-7;

         value_type lower_limit = 0;
         value_type upper_limit = +std::numeric_limits<value_type>::infinity();

         Math::QuadratureIntegration func_integration(f, thr, lower_limit, upper_limit);

         value_type expected_integral = std::pow(std::numbers::pi_v<value_type>, 2) / 6.0;
         value_type computed_integral = func_integration.GetIntegral();

         if (  std::abs(computed_integral - expected_integral) > thr
            || aDebug
            )
         {
            std::cerr << "Integral (Quadrature) of x/(exp(x) - 1) from [0] to [+inf] gave unexpected value" << std::endl;
            std::cerr << "Integral (computed): " << computed_integral << std::endl;
            std::cerr << "Integral (expected): " << expected_integral << std::endl;
            std::cerr << "Deviation: " << std::abs(computed_integral - expected_integral) << std::endl;
            std::cerr << "Threshold: " << thr << std::endl;
         }
      }

      {
         if (aDebug)
         {
            std::cout << "About to compute integral of exp(-x^2) from [-inf] to [+inf]" << std::endl;
         }

         // Lambda for exp(-x^2)
         auto f = [](const value_type& aVal) {
            return std::exp(-aVal * aVal);
         };

         value_type thr = 1.0E-7;

         value_type lower_limit = -std::numeric_limits<value_type>::infinity();
         value_type upper_limit = +std::numeric_limits<value_type>::infinity();

         Math::QuadratureIntegration func_integration(f, thr, lower_limit, upper_limit);

         value_type expected_integral = std::sqrt(std::numbers::pi_v<value_type>);
         value_type computed_integral = func_integration.GetIntegral();

         if (  std::abs(computed_integral - expected_integral) > 1.5 * thr
            || aDebug
            )
         {
            std::cerr << "Integral (Quadrature) of exp(-x^2) from [-inf] to [+inf] gave unexpected value" << std::endl;
            std::cerr << "Integral (computed): " << computed_integral << std::endl;
            std::cerr << "Integral (expected): " << expected_integral << std::endl;
            std::cerr << "Deviation: " << std::abs(computed_integral - expected_integral) << std::endl;
            std::cerr << "Threshold: " << 1.5 * thr << std::endl;
         }
      }


      if (aDebug)
      {
         std::cout << "Exiting RunQuadratureIntegrationTest() !" << std::endl;
      }
   }

   /********************************************************************
    * 
    ********************************************************************/
   void RunAndersonDarlingNormalDistributionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunAndersonDarlingNormalDistributionTest()!" << std::endl;
      }

      using cont = std::vector<double>;
      using dist = Math::NormalDistribution<cont>;
      using max_log_likelihood = Math::MaximumLogLikelihood<cont, dist>;
      using optimiser = Math::MaximumLogLikelihoodOptimiser<cont, dist>;

      // Data generated from a single distribution
      {
         if (aDebug)
         {
            std::cout << "About to compute Anderson-Darling statistic for single normal distribution" << std::endl;
         }
         double mean = 4;
         double std_devi = 0.5;
         std::vector<double> test_data = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean, std_devi, 300);
         std::sort(test_data.begin(), test_data.end());

         optimiser max_like_opti_1(test_data, 1, 1.0E-8);
         optimiser max_like_opti_2(test_data, 2, 1.0E-8);

         const auto& max_like_1 = max_like_opti_1.GetMaxLogLikelihood();
         const auto& max_like_2 = max_like_opti_2.GetMaxLogLikelihood();
         std::vector<max_log_likelihood> max_likelihoods{max_like_1, max_like_2};

         auto and_dar_vec = Math::ComputeAndersonDarlingByIntegral(max_likelihoods);
         auto and_dar_vec_sum = Math::ComputeAndersonDarlingBySummation(max_likelihoods);
         if (  std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) > 5.0E-3
            || std::abs(and_dar_vec[1] - and_dar_vec_sum[1]) > 5.0E-3
            )
         {
            std::cerr << "Unexpected large difference between Anderson-Darling numbers for a single normal distribution!" << std::endl;
            std::cerr << "Fit (integral) to single distributions: " << and_dar_vec[0] << std::endl;
            std::cerr << "Fit (integral) to two distributions:    " << and_dar_vec[1] << std::endl;
            std::cerr << "Fit (summation) to single distributions: " << and_dar_vec_sum[0] << std::endl;
            std::cerr << "Fit (summation) to two distributions:    " << and_dar_vec_sum[1] << std::endl;
            std::cerr << "abs(difference) one distribution = " << std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) << std::endl;
            std::cerr << "abs(difference) two distribution = " << std::abs(and_dar_vec[1] - and_dar_vec_sum[1]) << std::endl;
            std::cerr << "Thr: " << 5.0E-3 << std::endl;
            exit(1);
         }
      }

      // Data generated from two distribution far apart
      {
         if (aDebug)
         {
            std::cout << "About to compute Anderson-Darling for two normal distribution far apart" << std::endl;
         }
         double mean_1 = 1;
         double std_devi_1 = 1.0;
         double mean_2 = 20;
         double std_devi_2 = 1.0;
         std::vector<double> test_data_1 = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean_1, std_devi_1, 300);
         std::vector<double> test_data_2 = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean_2, std_devi_2, 300);
         std::vector<double> test_data(test_data_1);
         test_data.insert(test_data.end(), test_data_2.begin(), test_data_2.end());
         std::sort(test_data.begin(), test_data.end());
         
         optimiser max_like_opti_1(test_data, 1, 1.0E-8);
         optimiser max_like_opti_2(test_data, 2, 1.0E-8);

         const auto& max_like_1 = max_like_opti_1.GetMaxLogLikelihood();
         const auto& max_like_2 = max_like_opti_2.GetMaxLogLikelihood();

         std::vector<max_log_likelihood> max_likelihoods{max_like_1, max_like_2};

         auto and_dar_vec = Math::ComputeAndersonDarlingByIntegral(max_likelihoods);
         auto and_dar_vec_sum = Math::ComputeAndersonDarlingBySummation(max_likelihoods);
         if (  std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) > 2.0E-2
            || std::abs(and_dar_vec[1] - and_dar_vec_sum[1]) > 3.0E-1 // This is apparently hard to converge with large mean
            )
         {
            std::cerr << "Unexpected large difference between Anderson-Darling numbers for two normal distributions far apart" << std::endl;
            std::cerr << "Fit (integral) to single distributions: " << and_dar_vec[0] << std::endl;
            std::cerr << "Fit (integral) to two distributions:    " << and_dar_vec[1] << std::endl;
            std::cerr << "Fit (summation) to single distributions: " << and_dar_vec_sum[0] << std::endl;
            std::cerr << "Fit (summation) to two distributions:    " << and_dar_vec_sum[1] << std::endl;
            std::cerr << "abs(difference) one distribution = " << std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) << std::endl;
            std::cerr << "abs(difference) two distribution = " << std::abs(and_dar_vec[1] - and_dar_vec_sum[1]) << std::endl;
            exit(1);
         }
      }

      // Data generated from two distribution with large overlap
      {
         if (aDebug)
         {
            std::cout << "About to compute Anderson-Darling for two normal distribution close to each other" << std::endl;
         }
         double mean_1 = 1;
         double std_devi_1 = 0.5;
         double mean_2 = 1.5;
         double std_devi_2 = 0.5;
         std::vector<double> test_data_1 = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean_1, std_devi_1, 300);
         std::vector<double> test_data_2 = Util::GetRandomDistributedData<double, std::normal_distribution<double>>(mean_2, std_devi_2, 300);
         std::vector<double> test_data(test_data_1);
         test_data.insert(test_data.end(), test_data_2.begin(), test_data_2.end());
         std::sort(test_data.begin(), test_data.end());

         optimiser max_like_opti_1(test_data, 1, 1.0E-10);
         optimiser max_like_opti_2(test_data, 2, 1.0E-10);

         const auto& max_like_1 = max_like_opti_1.GetMaxLogLikelihood();
         const auto& max_like_2 = max_like_opti_2.GetMaxLogLikelihood();

         std::vector<max_log_likelihood> max_likelihoods{max_like_1, max_like_2};

         auto and_dar_vec = Math::ComputeAndersonDarlingByIntegral(max_likelihoods);
         auto and_dar_vec_sum = Math::ComputeAndersonDarlingBySummation(max_likelihoods);
         if (  std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) > 2.0E-3
            || std::abs(and_dar_vec[1] - and_dar_vec_sum[1]) > 2.0E-3
            )
         {
            std::cerr << "Unexpected large difference between Anderson-Darling numbers for two normal distributions close to each other!" << std::endl;
            std::cerr << "Fit (integral) to single distributions: " << and_dar_vec[0] << std::endl;
            std::cerr << "Fit (integral) to two distributions:    " << and_dar_vec[1] << std::endl;
            std::cerr << "Fit (summation) to single distributions: " << and_dar_vec_sum[0] << std::endl;
            std::cerr << "Fit (summation) to two distributions:    " << and_dar_vec_sum[1] << std::endl;
            std::cerr << "abs(difference) one distribution = " << std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) << std::endl;
            std::cerr << "abs(difference) two distribution = " << std::abs(and_dar_vec[1] - and_dar_vec_sum[1]) << std::endl;
            std::cerr << "Thr: " << 1.0E-3 << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Exiting RunAndersonDarlingNormalDistributionTest!()" << std::endl;
      }
   }

   /********************************************************************
    * The Cauchy distribution is very unstable when trying to fit sum of
    * two distributions so we only check for fitting a single
    * distribution.
    * One test against the integral and one test on the absolute value
    * of the Anderson-Darling statistic.
    ********************************************************************/
   void RunAndersonDarlingCauchyDistributionTest(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering RunAndersonDarlingCauchyDistributionTest()!" << std::endl;
      }

      using cont = std::vector<double>;
      using dist = Math::CauchyDistribution<cont>;
      using max_log_likelihood = Math::MaximumLogLikelihood<cont, dist>;
      using optimiser = Math::MaximumLogLikelihoodOptimiser<cont, dist>;

      // Data generated from a single distribution
      {
         if (aDebug)
         {
            std::cout << "About to compute Anderson-Darling statistic for single Cauchy distribution" << std::endl;
         }
         double median = 4;
         double gamma = 0.5;
         std::vector<double> test_data = Util::GetRandomDistributedData<double, std::cauchy_distribution<double>>(median, gamma, 300);
         std::sort(test_data.begin(), test_data.end());

         optimiser max_like_opti_1(test_data, 1, 1.0E-8);

         const auto& max_like_1 = max_like_opti_1.GetMaxLogLikelihood();
         std::vector<max_log_likelihood> max_likelihoods{max_like_1};

         auto and_dar_vec = Math::ComputeAndersonDarlingByIntegral(max_likelihoods);
         auto and_dar_vec_sum = Math::ComputeAndersonDarlingBySummation(max_likelihoods);
         if (  std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) > 5.0E-3
            )
         {
            std::cerr << "Unexpected large difference between Anderson-Darling numbers for a single Cauchy distribution!" << std::endl;
            std::cerr << "Fit (integral) to single distributions: " << and_dar_vec[0] << std::endl;
            std::cerr << "Fit (summation) to single distributions: " << and_dar_vec_sum[0] << std::endl;
            std::cerr << "abs(difference) one distribution = " << std::abs(and_dar_vec[0] - and_dar_vec_sum[0]) << std::endl;
            std::cerr << "Thr: " << 5.0E-3 << std::endl;
            exit(1);
         }
      }

      // Testing that the absolute value of the (summed) Anderson-Darling statistic is very small.
      {
         if (aDebug)
         {
            std::cout << "About to compute Anderson-Darling for two cauchy distribution far apart" << std::endl;
         }
         double median = 1;
         double gamma = 0.1;
         std::vector<double> test_data = Util::GetRandomDistributedData<double, std::cauchy_distribution<double>>(median, gamma, 400);
         std::sort(test_data.begin(), test_data.end());
         
         optimiser max_like_opti_1(test_data, 1, 1.0E-8);

         const auto& max_like_1 = max_like_opti_1.GetMaxLogLikelihood();

         std::vector<max_log_likelihood> max_likelihoods{max_like_1};

         auto and_dar_vec_sum = Math::ComputeAndersonDarlingBySummation(max_likelihoods);
         if (  and_dar_vec_sum[0] > 9.0E-1
            )
         {
            std::cerr << "Unexpected large Anderson-Darling numbers for 1 Cauchy distribution !" << std::endl;
            std::cerr << "Anderson-Darling (summation): " << and_dar_vec_sum[0] << std::endl;
            exit(1);
         }
      }

      if (aDebug)
      {
         std::cout << "Exiting RunAndersonDarlingCauchyDistributionTest!()" << std::endl;
      }
   }

   /********************************************************************
    * Run a full computation for 1am2nta_115 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as the same conformer.
    ********************************************************************/
   void Run1am2nta115Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run1am2nta115Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test1am2nta_115";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_aug_pvqz_1am2nta_ToDir(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_1am2nta_ToDir(xyz_dir);
      
      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::SameAsReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 1am2nta_115 to be: "
                  << "SameAsReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Run a full computation for 1eda1ma1nta_252 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as the same conformer.
    ********************************************************************/
   void Run1eda1ma1nta252Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run1eda1ma1nta252Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test1eda1ma1nta_252";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_aug_pvqz_1eda1ma1nta_ToDir(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_1eda1ma1nta_ToDir(xyz_dir);
      
      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::SameAsReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 1eda1ma1nta_252 to be: "
                  << "SameAsReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Run a full computation for 1fa1msa1tma_324 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as different conformer.
    ********************************************************************/
   void Run1fa1msa1tma324Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run1fa1msa1tma324Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test1fa1msa1tma_324";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_1fa1msa1tma_324_ToPath(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_1fa1msa1tma_324_ToPath(xyz_dir);
      
      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::DifferentFromReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 1fa1msa1tma_324 to be: "
                  << "DifferentFromReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Run a full computation for 1am1eda1fa_22 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as the same conformer.
    * 
    * This test was included as LBFGS++ had trouble converging this
    * distribution. The default in LBFGS++ is to only look at the
    * gradient, I have now also included a check on how the function
    * value itself changes (or do not change i.e. converged).
    ********************************************************************/
   void Run1am1eda1fa22Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run1am1eda1fa22Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test1am1eda1fa_22";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_1am1eda1fa_22_ToPath(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_1am1eda1fa_22_ToPath(xyz_dir);
      
      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::SameAsReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 1am1eda1fa_22 to be: "
                  << "SameAsReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Run a full computation for 2tma_100 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as the same conformer.
    * 
    * This test was included as the two conformers are very close to
    * eachother giving a seemingly broad distribution (which is gives
    * a large Anderson-Darling statistic for a Cauchy distribution), but
    * the actual values are very close to zero.
    * 
    * This results in now having a check in CheckByAtomicPositions which
    * checks if all rmsd values are smaller than 0.32 and if they are
    * the Anderson-Darling statistic is ignored.
    ********************************************************************/
   void Run2tma100Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run2tma100Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test2tma_100";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_2tma_100_ToPath(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_2tma_100_ToPath(xyz_dir);
      
      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::SameAsReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 2tma_100 to be: "
                  << "SameAsReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Run a full computation for 2eda1nta1sa_590 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as the same conformer.
    * 
    * This test was included as the two conformers are very close to
    * eachother, but the dipolemoments seem to be waaay off. It claims
    * some cos(theta) values close to 0.2 which corresponds to an angle
    * of approximately 78 degrees.
    ********************************************************************/
   void Run2eda1nta1sa590Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run2eda1nta1sa590Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test2eda1nta1sa_590";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_2eda1nta1sa_590_ToPath(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_2eda1nta1sa_590_ToPath(xyz_dir);
      
      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::SameAsReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 2eda1nta1sa_590 to be: "
                  << "SameAsReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Run a full computation for 1sa2tma_412 comparing
    * RI-MP2/aug-cc-pVQZ to r2-SCAN-3c.
    * 
    * Successfull test: These should be labled as the same conformer.
    * 
    * This test was included as the two conformers are very close to
    * eachother, but the dipolemoments are for some reason antiparallel
    * instead of parallel. This was because of the proton transfer
    * between one of the tma and sa which occured in mp2 but not in
    * r2-scan. So now a proper mapping occurs in the dipole moment
    * computation.
    ********************************************************************/
   void Run1sa2tma412Test(const bool aDebug)
   {
      if (aDebug)
      {
         std::cout << "Entering Run1sa2tma412Test() !" << std::endl;
      }
      
      //
      std::filesystem::path cwd = std::filesystem::current_path();
      // Create directory for all datafiles
      std::filesystem::path test_dir = cwd / "TestScratch" / "Test1sa2tma_412";
      std::filesystem::path xyz_dir  = test_dir / "xyz_files";
      if (!std::filesystem::exists(xyz_dir))
      {
         std::filesystem::create_directories(xyz_dir);
      }

      std::filesystem::path mp2_xyz_file = Util::Print_dfmp2_1sa2tma_412_ToPath(xyz_dir);
      std::filesystem::path r2_scan_xyz_file = Util::Print_r2_scan3c_1sa2tma_412_ToPath(xyz_dir);

      // Setup input file
      std::filesystem::path input_file = test_dir / "analysis.inp";

      std::fstream input_file_stream;
      input_file_stream.open(input_file, std::ios_base::out);
      input_file_stream << "#1 ClusterConformations\n";
      input_file_stream << "#2 CompareClustersAcrossMethods\n";
      input_file_stream << "   #3 KabschOptions\n";
      input_file_stream << "     #4 KabschRMSD\n";
      input_file_stream << "        COMPARE_MONOMER_ROT_MATS_ACROSS\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << mp2_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           DF-MP2\n";
      input_file_stream << "        #5 BasisSet\n";
      input_file_stream << "           aug-cc-pVQZ\n";
      input_file_stream << "        #5 Reference\n";
      input_file_stream << "   #3 ComputationalData\n";
      input_file_stream << "     #4 ClusterNameInComment\n";
      input_file_stream << "     #4 PathToData\n";
      input_file_stream << "        " << r2_scan_xyz_file.string() << "\n";
      input_file_stream << "     #4 ComputationalMethods\n";
      input_file_stream << "        #5 CompMeth\n";
      input_file_stream << "           r2-SCAN-3c\n";

      input_file_stream.close();

      // Setup InputInfo (and ClusterSet) based on input file above
      Input::InputInfo cluster_test_input_info(input_file);

      // Setup ClusterSet based on input file above
      Structure::ClusterSet& cluster_set = cluster_test_input_info.GetClusterSetClusterConformation();
      cluster_set.CheckConformersAcrossMethods(test_dir);

      for (auto it = cluster_set.CompareClustersBegin(); it != cluster_set.CompareClustersEnd(); it++)
      {
         for (const auto& cluster : it->second)
         {
            if (cluster.GetConformerEnum() != Structure::ConformerEnum::SameAsReference)
            {
               std::cerr << "I did not find the ConformerEnum of r2-SCAN-3c for 1sa2tma_412 to be: "
                  << "SameAsReference as expected ! Instead I found it as:\n"
                  << Structure::GetStringFromEnum(cluster.GetConformerEnum())
                  << std::endl;
               
               exit(1);
            }
         }
      }
   }

   /********************************************************************
    * Helper to generate normal distributed data.
    * Templated over the distribution type
    *    -  Normal distribution
    *    -  Cauchy distribution
    ********************************************************************/
   template<std::floating_point value_type, typename DIST>
   std::vector<value_type> Util::GetRandomDistributedData
      (  const value_type aA
      ,  const value_type aB
      ,  const int aSize
      )
   {
      std::random_device rd{};
      std::mt19937 gen{rd()};
      // d{mean, std_devi} for normal-dist. d{median, gamma} for cauchy.
      DIST d{aA, aB};

      // lambda to generate random number from
      auto random_number = [&d, &gen]{ return d(gen); };

      std::vector<value_type> numbers;
      numbers.reserve(aSize);
      for (int i = 0; i < aSize; i++)
      {
         numbers.emplace_back(random_number());
      }

      return numbers;
   }

   /********************************************************************
    * Helper to check if reasonable rmsd value
    ********************************************************************/
   void Util::check_cluster_molecule_and_monomers
      (  const Structure::Cluster& arFoundCluster
      ,  const Structure::Molecule& arExpectedClusterMolecule
      ,  const std::vector<Structure::Molecule>& arExpectedMonomers
      )
   {
      bool stop = false;
      if (arFoundCluster.GetClusterMolecule().GetEigenMat() != arExpectedClusterMolecule.GetEigenMat())
      {
         std::cerr << "Coordinates for the Cluster Molecule differs" << std::endl;
         std::cerr << "arFoundCluster.GetClusterMolecule() == " << arFoundCluster.GetClusterMolecule() << std::endl;
         std::cerr << "arExpectedClusterMolecule == " << arExpectedClusterMolecule << std::endl;
         stop = true;
      }
      if (arExpectedMonomers.size() != arFoundCluster.GetNumMonomers())
      {
         std::cerr << "Wrong number of monomers found !" << std::endl;
         stop = true;
      }
      auto expected_it = arExpectedMonomers.begin();
      auto found_it = arFoundCluster.GetMonomersIterConstBegin();
      for(  
         ;  expected_it != arExpectedMonomers.end() && found_it != arFoundCluster.GetMonomersIterConstEnd()
         ;  expected_it++, found_it++
         )
      {
         if (found_it->GetEigenMat() != expected_it->GetEigenMat())
         {
            std::cerr << "Coordinates for the monomer Molecule differs" << std::endl;
            std::cerr << "*expected_it == " << *expected_it << std::endl;
            std::cerr << "*found_it == " << *found_it << std::endl;
            stop = true;
         }
      }

      if (stop)
      {
         std::cerr << "Exiting from check_cluster_molecule_and_monomer() with arFoundCluster.GetName() = " << arFoundCluster.GetName() << std::endl;
         exit(1);
      }
   }

   /********************************************************************
    * Helper to check if reasonable rmsd value
    ********************************************************************/
   void check_rmsd(const double arRmsd, const double arThr, const std::string& arErrMessage)
   {
      if (arRmsd > arThr)
      {
         std::cerr << "RMSD is larger than threshold:\n"
            << "RMSD = " << arRmsd << "\n"
            << "Thr  = " << arThr << "\n"
            << arErrMessage
            << std::endl;
      }
   }


   /********************************************************************
    * Prints 1a structure based on CCSD/aug-cc-pVTZ (MOLPRO) to path
    ********************************************************************/
   std::filesystem::path Util::PrintCCSD1aToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "CCSD_aug-cc-pVTZ_1a_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "4\n";
      out_stream << "CCSD/AUG-CC-PVTZ  ENERGY=-56.47221020\n";
      out_stream << "H          0.0000000000        0.9376022391       -0.2559992336\n";
      out_stream << "H          0.8119851373       -0.4687972769       -0.2560001648\n";
      out_stream << "H         -0.8119851373       -0.4687972769       -0.2560001648\n";
      out_stream << "N          0.0000000000        0.0000023148        0.1258595632\n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1eda structure based on CCSD/aug-cc-pVTZ (MOLPRO) to path
    ********************************************************************/
   std::filesystem::path Util::PrintCCSD1edaToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "CCSD_aug-cc-pVTZ_1eda_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "12\n";
      out_stream << " CCSD/AUG-CC-PVTZ  ENERGY=-190.19158388\n";
      out_stream << " H          0.6375581402        0.5802794493        1.3819327116\n";
      out_stream << " H          1.2354851343        1.5055042388        0.0030705575\n";
      out_stream << " H         -1.1790419576        1.5354279519       -0.0348729950\n";
      out_stream << " H         -0.5975622710        0.5606752330       -1.3926794080\n";
      out_stream << " H         -1.6995501553       -0.4936785650        1.0986480200\n";
      out_stream << " H         -2.2582042822       -0.7154476364       -0.4082329195\n";
      out_stream << " H          2.3517063206       -0.6575854882        0.2274749338\n";
      out_stream << " H          0.9296316618       -1.3970982150       -0.0712935540\n";
      out_stream << " C          0.7206139290        0.5845045644        0.2848040255\n";
      out_stream << " C         -0.6799143154        0.5975598293       -0.3053338882\n";
      out_stream << " N          1.4647128117       -0.5551696430       -0.2472816794\n";
      out_stream << " N         -1.4123750161       -0.5971617191        0.1329641957\n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1eda structure based on CCSD/aug-cc-pVTZ (MOLPRO) to path
    ********************************************************************/
   Structure::Molecule Util::ConstructCCSD1edaMolecule()
   {
      std::string name = "1eda CCSD/aug-cc-pVTZ";

      std::vector<Structure::AtomName> names = 
         {  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::N
         ,  Structure::AtomName::N
         };
      std::vector<std::string> eda_string_names =
         {  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "C"
         ,  "C"
         ,  "N"
         ,  "N"
         };
      Eigen::Matrix<double, 12, 3> coords 
         {  { 0.6375581402,    0.5802794493,  1.3819327116 }
         ,  { 1.2354851343,    1.5055042388,  0.0030705575 }
         ,  {-1.1790419576,    1.5354279519, -0.0348729950 }
         ,  {-0.5975622710,    0.5606752330, -1.3926794080 }
         ,  {-1.6995501553,   -0.4936785650,  1.0986480200 }
         ,  {-2.2582042822,   -0.7154476364, -0.4082329195 }
         ,  { 2.3517063206,   -0.6575854882,  0.2274749338 }
         ,  { 0.9296316618,   -1.3970982150, -0.0712935540 }
         ,  { 0.7206139290,    0.5845045644,  0.2848040255 }
         ,  {-0.6799143154,    0.5975598293, -0.3053338882 }
         ,  { 1.4647128117,   -0.5551696430, -0.2472816794 }
         ,  {-1.4123750161,   -0.5971617191,  0.1329641957 }
         };


      return Structure::Molecule(name, std::make_pair(names, eda_string_names), coords, {});
   }
   /********************************************************************
    * Creates 1a1eda structure based on CCSD/aug-cc-pVTZ (MOLPRO) to path
    ********************************************************************/
   Structure::Molecule Util::ConstructCCSD1a1edaMolecule()
   {
      std::string name = "1a1eda CCSD/aug-cc-pVTZ";

      std::vector<Structure::AtomName> names = 
         {  Structure::AtomName::N
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::N
         ,  Structure::AtomName::N
         };
      std::vector<std::string> a_eda_string_names =
         {  "N"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "C"
         ,  "C"
         ,  "N"
         ,  "N"
         };
      Eigen::Matrix<double, 16, 3> coords 
         {  {-3.4466802710, -0.2581829268, -0.1990013412 }
         ,  {-3.7389378410,  0.0382936661, -1.1222396452 }
         ,  {-4.1995453789, -0.0173667585,  0.4342405136 }
         ,  {-3.3985891989, -1.2697809523, -0.2201380716 }
         ,  { 2.2407945422,  0.9514316460,  0.7932319472 }
         ,  { 2.5178691399,  0.8920560156, -0.9377907087 }
         ,  { 0.2927810394,  1.7767683841, -0.5293873638 }
         ,  { 0.1348836922,  0.1833704238, -1.2699319448 }
         ,  {-0.2188384180,  0.7569148507,  1.5689540894 }
         ,  {-1.3212679349,  0.1626540193,  0.5243907391 }
         ,  { 1.6444854541, -1.4068024694,  0.6202687324 }
         ,  { 2.1783080553, -1.4209636450, -0.9063708265 }
         ,  { 1.9366817945,  0.4448427272, -0.1282471818 }
         ,  { 0.4499599089,  0.7025725543, -0.3603983755 }
         ,  { 2.3047956334, -0.9650868239, -0.0101622130 }
         ,  {-0.3333702171,  0.1662992887,  0.7540816504 }
         };


      return Structure::Molecule(name, std::make_pair(names, a_eda_string_names), coords, {});
   }
   /********************************************************************
    * Prints 1ma structure based on CCSD/aug-cc-pVTZ (MOLPRO) to path
    ********************************************************************/
   std::filesystem::path Util::PrintCCSD1maToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "CCSD_aug-cc-pVTZ_1ma_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "7\n";
      out_stream << " 1ma\n";
      out_stream << " H         -0.4337570254       -1.1118000180        0.8091691702\n";
      out_stream << " H          0.5866120648        1.0646659457       -0.8785330303\n";
      out_stream << " H         -0.9469179613        1.1563364211        0.0000000000\n";
      out_stream << " H          0.5866120648        1.0646659457        0.8785330303\n";
      out_stream << " H         -0.4337570254       -1.1118000180       -0.8091691702\n";
      out_stream << " C          0.0512549694        0.7048536233        0.0000000000\n";
      out_stream << " N          0.0611029132       -0.7601518998        0.0000000000\n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1a1eda_01 structure based on CCSD/aug-cc-pVTZ (MOLPRO) to
    * path
    ********************************************************************/
   std::filesystem::path Util::PrintCCSD1a1eda_01ToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "CCSD_aug-cc-pVTZ_1a1eda_01_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "   16\n";
      out_stream << " 1a1eda_01\n";
      out_stream << " N         -3.4466802710       -0.2581829268       -0.1990013412\n";
      out_stream << " H         -3.7389378410        0.0382936661       -1.1222396452\n";
      out_stream << " H         -4.1995453789       -0.0173667585        0.4342405136\n";
      out_stream << " H         -3.3985891989       -1.2697809523       -0.2201380716\n";
      out_stream << " H          2.2407945422        0.9514316460        0.7932319472\n";
      out_stream << " H          2.5178691399        0.8920560156       -0.9377907087\n";
      out_stream << " H          0.2927810394        1.7767683841       -0.5293873638\n";
      out_stream << " H          0.1348836922        0.1833704238       -1.2699319448\n";
      out_stream << " H         -0.2188384180        0.7569148507        1.5689540894\n";
      out_stream << " H         -1.3212679349        0.1626540193        0.5243907391\n";
      out_stream << " H          1.6444854541       -1.4068024694        0.6202687324\n";
      out_stream << " H          2.1783080553       -1.4209636450       -0.9063708265\n";
      out_stream << " C          1.9366817945        0.4448427272       -0.1282471818\n";
      out_stream << " C          0.4499599089        0.7025725543       -0.3603983755\n";
      out_stream << " N          2.3047956334       -0.9650868239       -0.0101622130\n";
      out_stream << " N         -0.3333702171        0.1662992887        0.7540816504\n";
      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1eda1a_01 structure based on CCSD/aug-cc-pVTZ (MOLPRO) to
    * path
    ********************************************************************/
   std::filesystem::path Util::PrintCCSD1eda1a_01ToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "CCSD_aug-cc-pVTZ_1eda1a_01_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "   16\n";
      out_stream << " 1eda1a_01\n";
      out_stream << " H          2.2407945422        0.9514316460        0.7932319472\n";
      out_stream << " H          2.5178691399        0.8920560156       -0.9377907087\n";
      out_stream << " H          0.2927810394        1.7767683841       -0.5293873638\n";
      out_stream << " H          0.1348836922        0.1833704238       -1.2699319448\n";
      out_stream << " H         -0.2188384180        0.7569148507        1.5689540894\n";
      out_stream << " H         -1.3212679349        0.1626540193        0.5243907391\n";
      out_stream << " H          1.6444854541       -1.4068024694        0.6202687324\n";
      out_stream << " H          2.1783080553       -1.4209636450       -0.9063708265\n";
      out_stream << " C          1.9366817945        0.4448427272       -0.1282471818\n";
      out_stream << " C          0.4499599089        0.7025725543       -0.3603983755\n";
      out_stream << " N          2.3047956334       -0.9650868239       -0.0101622130\n";
      out_stream << " N         -0.3333702171        0.1662992887        0.7540816504\n";
      out_stream << " N         -3.4466802710       -0.2581829268       -0.1990013412\n";
      out_stream << " H         -3.7389378410        0.0382936661       -1.1222396452\n";
      out_stream << " H         -4.1995453789       -0.0173667585        0.4342405136\n";
      out_stream << " H         -3.3985891989       -1.2697809523       -0.2201380716\n";
      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1a1ma_01 structure based on CCSD/aug-cc-pVTZ (MOLPRO) to
    * path
    ********************************************************************/
   std::filesystem::path Util::PrintCCSD1a1ma_01ToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "CCSD_aug-cc-pVTZ_1a1ma_01_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "   11\n";
      out_stream << " 1a1ma_01\n";
      out_stream << " N          2.2773566090       -0.1221790810        0.0575254175\n";
      out_stream << " H          2.3079937203       -0.7801636262       -0.7117735511\n";
      out_stream << " H          2.8617355955       -0.5041434795        0.7910395704\n";
      out_stream << " H          2.7323548364        0.7220871255       -0.2684138402\n";
      out_stream << " N         -0.9147676079        0.6971682245       -0.1236342252\n";
      out_stream << " H          0.0535112169        0.4925350288        0.0978659926\n";
      out_stream << " C         -1.7219673276       -0.5151348540        0.0160367129\n";
      out_stream << " H         -1.6933906846       -0.9734303140        1.0120225761\n";
      out_stream << " H         -2.7624870074       -0.2910935132       -0.2225712575\n";
      out_stream << " H         -1.3766620885       -1.2582101309       -0.7037871054\n";
      out_stream << " H         -1.2247972621        1.3947846202        0.5401097099\n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1a structure based on RI-MP2/aug-cc-pVQZ (TURBOMOLE) to
    * path.
    ********************************************************************/
   std::filesystem::path Util::PrintMP21aToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "RI-MP2_aug-cc-pVQZ_1a_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "4\n";
      out_stream << "1a\n";
      out_stream << "H  0.93699080363397 -0.09437637389291 0.00000000000000 \n";
      out_stream << "H  -0.46849531442176 -0.09437728669646 0.81145774710515 \n";
      out_stream << "H  -0.46849531442176 -0.09437728669646 -0.81145774710515 \n";
      out_stream << "N  -0.00000017479043 0.28313094728582 0.00000000000000 \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1eda structure based on RI-MP2/aug-cc-pVQZ (TURBOMOLE) to
    * path.
    ********************************************************************/
   std::filesystem::path Util::PrintMP21edaToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "RI-MP2_aug-cc-pVQZ_1eda_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "12\n";
      out_stream << "1eda\n";
      out_stream << "H  0.67377666680748 0.51398930675713 1.32403254374640 \n";
      out_stream << "H  1.27718167535976 1.42678355947788 -0.05655188042496 \n";
      out_stream << "H  -1.13815571992587 1.45608102789115 -0.08899979774240 \n";
      out_stream << "H  -0.55056892404663 0.48635649990963 -1.44445707691917 \n";
      out_stream << "H  -1.63814147466150 -0.58208538804385 1.04195719728238 \n";
      out_stream << "H  -2.19687157085490 -0.80958288005945 -0.46429374670719 \n";
      out_stream << "H  2.37419404021780 -0.75181406473899 0.17413854288562 \n";
      out_stream << "H  0.93853862075456 -1.46549023184190 -0.13197886517175 \n";
      out_stream << "C  0.75980510286786 0.51145935797008 0.22934996200955 \n";
      out_stream << "C  -0.63572232113426 0.52289111241433 -0.35975113288762 \n";
      out_stream << "N  1.48964348853105 -0.63266780818475 -0.29910510067714 \n";
      out_stream << "N  -1.35367958391541 -0.67592049155127 0.07565935460624 \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1ma structure based on RI-MP2/aug-cc-pVQZ (TURBOMOLE) to
    * path.
    ********************************************************************/
   std::filesystem::path Util::PrintMP21maToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "RI-MP2_aug-cc-pVQZ_1ma_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "7\n";
      out_stream << "1ma\n";
      out_stream << "H  -0.35549268548985 -1.25549731509903 0.80903897399186 \n";
      out_stream << "H  0.66031835717195 0.91791362475317 -0.87550004768571 \n";
      out_stream << "H  -0.86842278870599 1.01477528622452 0.00000000000000 \n";
      out_stream << "H  0.66031835717195 0.91791362475317 0.87550004768571 \n";
      out_stream << "H  -0.35549268548985 -1.25549731509903 -0.80903897399186 \n";
      out_stream << "C  0.12506785060398 0.56037907160811 0.00000000000000 \n";
      out_stream << "N  0.13370359473783 -0.89998697714095 0.00000000000000 \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1a1eda_01 structure based on RI-MP2/aug-cc-pVTZ (TURBOMOLE)
    * to path.
    ********************************************************************/
   std::filesystem::path Util::PrintMP21a1eda_01ToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "RI-MP2_aug-cc-pVQZ_1a1eda_01_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "16\n";
      out_stream << "1a1eda_01\n";
      out_stream << "N  -3.18802978057134 -0.31593605099689 -0.14250746563873 \n";
      out_stream << "H  -3.43095172904893 -0.02771719295437 -1.08028603594657 \n";
      out_stream << "H  -3.97914483650463 -0.09217249288356 0.44540920187525 \n";
      out_stream << "H  -3.11028980543141 -1.32381732689611 -0.15635981870475 \n";
      out_stream << "H  2.42141161025852 0.89334531837538 0.83225464195212 \n";
      out_stream << "H  2.66975360007717 0.81875410345563 -0.89873569949807 \n";
      out_stream << "H  0.47010245908703 1.75062634197263 -0.44665339434999 \n";
      out_stream << "H  0.27221955885698 0.16321012942929 -1.18452499878689 \n";
      out_stream << "H  -0.01214175748148 0.72909543819077 1.65854463707335 \n";
      out_stream << "H  -1.14315360519185 0.14203356117405 0.63545409480532 \n";
      out_stream << "H  1.75122229747081 -1.44576364649076 0.66917403716172 \n";
      out_stream << "H  2.29747958328888 -1.47900873415476 -0.85353064644155 \n";
      out_stream << "C  2.09316753492454 0.38953910334641 -0.07963820985558 \n";
      out_stream << "C  0.61188707082841 0.67558555479834 -0.28303660741138 \n";
      out_stream << "N  2.42609906192661 -1.02388263510710 0.04118348023310 \n";
      out_stream << "N  -0.14963126248927 0.14610852874102 0.84325278353274 \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * Prints 1a1ma_01 structure based on RI-MP2/aug-cc-pVTZ (TURBOMOLE)
    * to path.
    ********************************************************************/
   std::filesystem::path Util::PrintMP21a1ma_01ToPath(const std::filesystem::path& arPath)
   {
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "RI-MP2_aug-cc-pVQZ_1a1ma_01_TEST.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "11\n";
      out_stream << "1a1ma_01\n";
      out_stream << "N  2.09380457190318 -0.37762323063996 -0.09993415046731 \n";
      out_stream << "H  1.29462033048540 0.22243139899883 -0.28441711082612 \n";
      out_stream << "H  2.21142514786604 -0.97785756916168 -0.90432554877068 \n";
      out_stream << "H  2.91203720821793 0.21400633500988 -0.06159988236353 \n";
      out_stream << "N  -0.77754784920634 0.91955687220511 0.06815992372532 \n";
      out_stream << "H  -0.63429793184949 1.30161456543762 0.99305565344950 \n";
      out_stream << "C  -1.32444305305613 -0.43477955647893 0.16383906185711 \n";
      out_stream << "H  -2.26084295655288 -0.50723344193893 0.71988460452209 \n";
      out_stream << "H  -1.49492690786536 -0.81791339221325 -0.83886879784260 \n";
      out_stream << "H  -0.58754622973004 -1.07539697060558 0.64055620483245 \n";
      out_stream << "H  -1.43228233021235 1.53319498938684 -0.39634995811628 \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintGunnar1fa1tmaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Gunnar1fa1tma.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "18   \n";
      out_stream << "1fa_1tma\n";
      out_stream << "H          0.45650        0.76640       -0.00055   \n";
      out_stream << "H         -2.04316        1.62723        0.88326   \n";
      out_stream << "H         -2.04351        1.62652       -0.88421   \n";
      out_stream << "H         -3.09720        0.50304        0.00018   \n";
      out_stream << "H         -0.17327       -1.45301        1.19338   \n";
      out_stream << "H         -0.99098       -0.16046        2.08285   \n";
      out_stream << "H         -1.95456       -1.40711        1.26151   \n";
      out_stream << "H         -0.99169       -0.16203       -2.08279   \n";
      out_stream << "H         -0.17367       -1.45390       -1.19262   \n";
      out_stream << "H         -1.95498       -1.40806       -1.26018   \n";
      out_stream << "C         -2.11427        0.99389       -0.00021   \n";
      out_stream << "C         -1.04270       -0.79792        1.20077   \n";
      out_stream << "C         -1.04311       -0.79883       -1.20021   \n";
      out_stream << "N         -1.02614        0.02822       -0.00003   \n";
      out_stream << "O          2.14218       -0.90258        0.00004   \n";
      out_stream << "O          1.37566        1.20888       -0.00037   \n";
      out_stream << "C          2.31534        0.29072       -0.00003   \n";
      out_stream << "H          3.31523        0.74631        0.00011   \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintGunnar1sa1dmaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Gunnar1sa1dma.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "17   \n";
      out_stream << "1sa_1dma\n";
      out_stream << "H          1.20478       -0.03492        0.78833   \n";
      out_stream << "S         -1.27848       -0.12984        0.00613   \n";
      out_stream << "O         -1.97089        1.33367        0.02603   \n";
      out_stream << "O         -0.43473       -0.13470       -1.20686   \n";
      out_stream << "O         -2.36060       -1.08161       -0.00684   \n";
      out_stream << "O         -0.44197       -0.11641        1.21825   \n";
      out_stream << "H         -2.88047        1.21309       -0.26117   \n";
      out_stream << "N          1.89635        0.00026        0.00289   \n";
      out_stream << "H          1.18551       -0.04512       -0.76901   \n";
      out_stream << "C          2.75456       -1.19255        0.00119   \n";
      out_stream << "H          3.39012       -1.18687        0.88372   \n";
      out_stream << "H          3.37130       -1.19652       -0.89455   \n";
      out_stream << "H          2.11878       -2.07347        0.01236   \n";
      out_stream << "C          2.60286        1.28887       -0.01305   \n";
      out_stream << "H          3.23380        1.37076        0.86900   \n";
      out_stream << "H          1.86363        2.08520       -0.01108   \n";
      out_stream << "H          3.21484        1.35793       -0.90940   \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintGunnar1aa1dmaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Gunnar1aa1dma.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "18   \n";
      out_stream << "1aa_1dma\n";
      out_stream << "H          0.09764       -0.60699        0.31385   \n";
      out_stream << "H          1.29406        0.30053       -1.09306   \n";
      out_stream << "H          2.48200       -1.73997       -1.00672   \n";
      out_stream << "H          3.64009       -0.42020       -0.73841   \n";
      out_stream << "H          3.00542       -1.34467        0.63055   \n";
      out_stream << "H          2.11471        0.80676        1.66253   \n";
      out_stream << "H          2.76680        1.71329        0.28750   \n";
      out_stream << "H          1.03085        1.78591        0.66343   \n";
      out_stream << "C          1.90503        1.13753        0.64470   \n";
      out_stream << "C          2.74906       -0.92097       -0.34092   \n";
      out_stream << "N          1.61583       -0.02190       -0.18904   \n";
      out_stream << "O         -0.85898       -0.82246        0.54793   \n";
      out_stream << "O         -1.23619        0.88204       -0.84644   \n";
      out_stream << "C         -1.64854        0.01672       -0.10587   \n";
      out_stream << "C         -3.10626       -0.22334        0.17450   \n";
      out_stream << "H         -3.71234        0.47976       -0.38737   \n";
      out_stream << "H         -3.36821       -1.24530       -0.09692   \n";
      out_stream << "H         -3.29623       -0.11210        1.24152   \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintGunnar1msa1aaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Gunnar1msa1aa.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "17   \n";
      out_stream << "1msa_1aa\n";
      out_stream << "H          0.12083        1.04895        0.49943   \n";
      out_stream << "H          0.98001       -1.20806       -0.08188   \n";
      out_stream << "O         -0.70943       -1.20277       -0.13052   \n";
      out_stream << "O         -0.85237        1.08414        0.74949   \n";
      out_stream << "O         -2.80167       -0.35626        0.91063   \n";
      out_stream << "O          1.96707       -1.18072       -0.10469   \n";
      out_stream << "O          1.66822        1.02088        0.13235   \n";
      out_stream << "S         -1.63978       -0.10727        0.11401   \n";
      out_stream << "C         -2.14020        0.53038       -1.46285   \n";
      out_stream << "H         -1.25057        0.79577       -2.02737   \n";
      out_stream << "H         -2.69567       -0.25830       -1.96332   \n";
      out_stream << "H         -2.77078        1.39725       -1.28879   \n";
      out_stream << "C          3.89122        0.16559       -0.06542   \n";
      out_stream << "H          4.32753       -0.39642        0.75958   \n";
      out_stream << "H          4.25259       -0.28152       -0.99025   \n";
      out_stream << "H          4.18877        1.20670       -0.00632   \n";
      out_stream << "C          2.40053        0.05232       -0.00229   \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintGunnar1na1tmaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Gunnar1na1tma.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "18   \n";
      out_stream << "1na_1tma\n";
      out_stream << "H         -0.26449        0.44499        0.00027   \n";
      out_stream << "H         -2.02481        1.80591        0.88668   \n";
      out_stream << "H         -2.02406        1.80766       -0.88363   \n";
      out_stream << "H         -3.26324        0.88230        0.00010   \n";
      out_stream << "H         -0.72694       -1.55923        1.19322   \n";
      out_stream << "H         -1.29815       -0.14122        2.08577   \n";
      out_stream << "H         -2.47982       -1.19235        1.26420   \n";
      out_stream << "H         -1.29817       -0.13804       -2.08596   \n";
      out_stream << "H         -0.72680       -1.55737       -1.19559   \n";
      out_stream << "H         -2.47973       -1.19054       -1.26598   \n";
      out_stream << "C         -2.22264        1.20758        0.00085   \n";
      out_stream << "C         -1.47461       -0.77211        1.21802   \n";
      out_stream << "C         -1.47456       -0.77028       -1.21917   \n";
      out_stream << "N         -1.32482        0.04271        0.00005   \n";
      out_stream << "O          1.42752       -1.09699       -0.00009   \n";
      out_stream << "O          1.00505        1.02952        0.00026   \n";
      out_stream << "N          1.86673        0.06347        0.00013   \n";
      out_stream << "O          3.04540        0.33042        0.00001   \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintGunnar1na1edaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Gunnar1na1eda.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "17   \n";
      out_stream << "1na_1eda\n";
      out_stream << "H    -0.08131        1.15515       -0.32340     \n";
      out_stream << "H     2.96964        1.06457       -0.01706     \n";
      out_stream << "H     1.76781        1.61389        1.17240     \n";
      out_stream << "H     2.55805       -0.76612        1.52761     \n";
      out_stream << "H     0.81743       -0.61755        1.33474     \n";
      out_stream << "H     2.47200       -2.17605       -0.22020     \n";
      out_stream << "H     0.90569       -1.79571       -0.53977     \n";
      out_stream << "H     1.17409        2.06746       -1.19277     \n";
      out_stream << "H     1.07203        0.42458       -1.40258     \n";
      out_stream << "C     1.95833        0.91492        0.36008     \n";
      out_stream << "C     1.77101       -0.53029        0.81191     \n";
      out_stream << "N     0.99407        1.18688       -0.72734     \n";
      out_stream << "N     1.82256       -1.41711       -0.34421     \n";
      out_stream << "O    -1.11083       -0.88062       -0.62972     \n";
      out_stream << "O    -1.33813        1.09545        0.23463     \n";
      out_stream << "N    -1.82108       -0.08169        0.00372     \n";
      out_stream << "O    -2.92609       -0.35166        0.40807     \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintHaide15sa15dmaToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "Haide15sa15dma_0.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << " 255                                            \n";
      out_stream << "15dma15sa_0                                     \n";
      out_stream << "  S      -3.344613443254940     -6.258805085765600     -2.581719727877390   \n";
      out_stream << "  O      -3.676856619195440     -5.009029425305060     -3.381954533147500   \n";
      out_stream << "  O      -3.880494162059940     -7.505134303393560     -3.128490529292110   \n";
      out_stream << "  O      -1.882071234646010     -6.264096316069160     -2.237401942505100   \n";
      out_stream << "  O      -4.151740053223400     -6.058052235775490     -1.153666387868890   \n";
      out_stream << "  H      -3.996990452104700     -5.120413997727370     -0.853097709890300   \n";
      out_stream << "  S      -3.109043716144080     -2.551950543319020     -1.154949767474110   \n";
      out_stream << "  O      -3.959097168845110     -2.134493296327980     -2.321291551941750   \n";
      out_stream << "  O      -3.803792255229340     -3.568290270162030     -0.297060431929630   \n";
      out_stream << "  O      -1.728824917732900     -2.938776513188380     -1.507969841980010   \n";
      out_stream << "  O      -2.981261677284800     -1.243613938449350     -0.188891404183830   \n";
      out_stream << "  H      -3.447053820834270     -0.456635927889580     -0.598897805887740   \n";
      out_stream << "  S       2.557996874056230      1.151497622429910     -1.996683024701950   \n";
      out_stream << "  O       1.845065678782990      1.971375054576280     -0.979743886677920   \n";
      out_stream << "  O       1.617085417237560      0.407097429319080     -2.899646895684370   \n";
      out_stream << "  O       3.608767021425220      1.911527298087100     -2.743911962372790   \n";
      out_stream << "  O       3.349444371851840      0.028627639576980     -1.113415529150330   \n";
      out_stream << "  H       3.391824622208760     -0.822919648165390     -1.624081806155620   \n";
      out_stream << "  S      -4.935512003586110      1.001040774026060     -2.368593807633600   \n";
      out_stream << "  O      -4.050739599030430      0.986960202852310     -1.111345808626330   \n";
      out_stream << "  O      -6.181356526297970      0.210035025520440     -2.107655165253120   \n";
      out_stream << "  O      -5.131568633728590      2.347096279945350     -2.914732259793400   \n";
      out_stream << "  O      -4.067612060751240      0.204604851143680     -3.484422330713220   \n";
      out_stream << "  H      -3.936167279218290     -0.715958476483510     -3.114445102469070   \n";
      out_stream << "  S       4.500458304723940      5.114834538317780     -1.376376855348020   \n";
      out_stream << "  O       5.716543469561240      5.466558074212400     -2.128332592765010   \n";
      out_stream << "  O       4.695038358303380      3.971072304610310     -0.406880929979410   \n";
      out_stream << "  O       3.787022729188690      6.261657516923430     -0.729759793261970   \n";
      out_stream << "  O       3.379923663850150      4.578635876397740     -2.460358226405870   \n";
      out_stream << "  H       3.499705538426830      3.597255368524230     -2.577892632033310   \n";
      out_stream << "  S      -4.956927511766790      2.795008667259060      1.998746244653570   \n";
      out_stream << "  O      -5.893128900837000      2.238389719158990      2.991480120325120   \n";
      out_stream << "  O      -4.898185800641210      4.280611184195000      1.952732138816440   \n";
      out_stream << "  O      -3.579638094830260      2.156022643186890      2.036050662845890   \n";
      out_stream << "  O      -5.555423322453650      2.350827321739800      0.522817767851650   \n";
      out_stream << "  H      -4.869556142896280      1.823935528646940      0.007580982502960   \n";
      out_stream << "  S       4.589236678863490     -0.366692713856250      3.438601637600430   \n";
      out_stream << "  O       5.660562761862520      0.467527447678180      2.850307439754470   \n";
      out_stream << "  O       5.096120112967780     -1.714428294996470      3.898869872681060   \n";
      out_stream << "  O       3.732772920753570      0.304024891789860      4.454163340931880   \n";
      out_stream << "  O       3.516461993678530     -0.718228048935080      2.229920117249730   \n";
      out_stream << "  H       4.035185725199720     -1.257920015844540      1.563006100881710   \n";
      out_stream << "  S      -0.895440571962910      4.352642790665170      1.301074240799420   \n";
      out_stream << "  O      -0.408893448217260      3.574458871907150      2.468143866538580   \n";
      out_stream << "  O      -1.728287651751350      5.529090266882770      1.629429766226790   \n";
      out_stream << "  O       0.219249249459250      4.669093183734290      0.320700695518470   \n";
      out_stream << "  O      -1.823162309810740      3.337886197710500      0.392592407917890   \n";
      out_stream << "  H      -2.465514152086280      2.888311723523570      1.009559817130030   \n";
      out_stream << "  S       0.017650564133800      0.364530237667010      5.204783760250960   \n";
      out_stream << "  O      -1.211068200350800     -0.142689313527850      4.476003966400170   \n";
      out_stream << "  O      -0.188586114072690      1.780586873634210      5.642236571920340   \n";
      out_stream << "  O       0.553779342421560     -0.542045478118160      6.231493191814860   \n";
      out_stream << "  O       1.109140900574770      0.421100019103830      3.976497344001130   \n";
      out_stream << "  H       2.038115307659530      0.373267689192580      4.322007553408060   \n";
      out_stream << "  S      -1.007634039536900      6.853472975278980     -2.167132022259120   \n";
      out_stream << "  O      -1.882010459662230      8.031092726651760     -2.244366480939550   \n";
      out_stream << "  O      -1.744744501680990      5.547044686119200     -1.946474163277330   \n";
      out_stream << "  O      -0.014372327216650      6.715885878268270     -3.285095447591830   \n";
      out_stream << "  O      -0.102975458245380      7.069921844231820     -0.813344754090390   \n";
      out_stream << "  H       0.024294072302090      6.179140955345050     -0.378043316297100   \n";
      out_stream << "  S      -0.791341948859230     -3.385542054582480      3.266276686381390   \n";
      out_stream << "  O       0.493739665641540     -2.601818429904560      3.143518744582430   \n";
      out_stream << "  O      -1.616103620195110     -3.216353685198380      2.008829844701100   \n";
      out_stream << "  O      -0.598703268789210     -4.791323281180450      3.665937942669820   \n";
      out_stream << "  O      -1.639632167680550     -2.691921543729100      4.449806210261280   \n";
      out_stream << "  H      -1.407491597350320     -1.698277205749570      4.480504339653050   \n";
      out_stream << "  S       8.000717746382859     -1.344419754801790     -1.147800164356750   \n";
      out_stream << "  O       8.601741963878441     -0.448536903765350     -2.150182541574260   \n";
      out_stream << "  O       8.113221254422291     -2.807978923778590     -1.422502583667840   \n";
      out_stream << "  O       8.321191038599810     -0.989614282087580      0.271319682314630   \n";
      out_stream << "  O       6.357974529338400     -1.005866089008730     -1.321372200959470   \n";
      out_stream << "  H       5.866252437054700     -1.480619772274080     -0.591618251717790   \n";
      out_stream << "  S       2.435978347201740     -2.944115305426910     -3.345825215197830   \n";
      out_stream << "  O       3.472096937556220     -2.445850986674840     -2.321198725021210   \n";
      out_stream << "  O       2.965319671574440     -4.025750120417400     -4.192180034976500   \n";
      out_stream << "  O       1.143745369134630     -3.201628468591810     -2.657653655063180   \n";
      out_stream << "  O       2.231205716633140     -1.701968225103050     -4.374005738504520   \n";
      out_stream << "  H       2.015944598092100     -0.884421756820350     -3.839156388262500   \n";
      out_stream << "  S       4.679908698775220     -3.687733980258850      0.654868544385170   \n";
      out_stream << "  O       5.021479642345310     -2.192810357060450      0.637172315362090   \n";
      out_stream << "  O       5.562752210059040     -4.436523612224620     -0.262391851879730   \n";
      out_stream << "  O       4.544494702116390     -4.207127517262840      2.026790686281480   \n";
      out_stream << "  O       3.155926261642910     -3.758669173628260      0.055494587371690   \n";
      out_stream << "  H       3.194743708129550     -3.332997997829510     -0.840406433456860   \n";
      out_stream << "  S      -7.461763235317740     -2.100690476137580      0.431429170926500   \n";
      out_stream << "  O      -7.121899240071000     -3.469583596558260     -0.052212046029890   \n";
      out_stream << "  O      -8.474393992185750     -1.404798511093690     -0.432419621025490   \n";
      out_stream << "  O      -7.692357311093940     -2.006097566911700      1.886468640818570   \n";
      out_stream << "  O      -6.076239099323210     -1.181436991109860      0.238399569975360   \n";
      out_stream << "  H      -6.060487959750890     -0.823131781935230     -0.686252934121630   \n";
      out_stream << "  C       0.364084197087680     -5.322725376924830      0.194676957010930   \n";
      out_stream << "  N      -1.108014249215320     -5.455339301483040      0.279894828722410   \n";
      out_stream << "  C      -1.570221898144210     -6.618122330474220      1.071155481434100   \n";
      out_stream << "  H      -1.479233151395300     -4.593123934707770      0.702046797167120   \n";
      out_stream << "  H       0.598291927634910     -4.491785636139230     -0.456386681696190   \n";
      out_stream << "  H       0.766828548986570     -5.148103425641470      1.184532752704440   \n";
      out_stream << "  H       0.777809501874860     -6.234109227482780     -0.223029853119980   \n";
      out_stream << "  H      -2.651144892232880     -6.667975047754300      1.007960458280810   \n";
      out_stream << "  H      -1.149754149879380     -7.520337460942840      0.640281980931720   \n";
      out_stream << "  H      -1.252515028041200     -6.491208256576020      2.098787076229880   \n";
      out_stream << "  H      -1.479720326712050     -5.549897213966150     -0.683435108077420   \n";
      out_stream << "  C      -1.371022780390240      2.154271899777280     -2.676768586362090   \n";
      out_stream << "  N      -0.345322031357280      3.207140669445950     -2.494244753263060   \n";
      out_stream << "  C       0.389621994767090      3.534059133637590     -3.738410344227370   \n";
      out_stream << "  H      -0.805975825903410      4.070321580064800     -2.147238404224690   \n";
      out_stream << "  H      -0.883166490225170      1.242644475467110     -3.000137634673870   \n";
      out_stream << "  H      -1.882758539558530      1.995037491645600     -1.738569420985520   \n";
      out_stream << "  H      -2.087417866305190      2.482847849015710     -3.420826268071800   \n";
      out_stream << "  H       0.840195414223090      2.628130528030040     -4.126819862732650   \n";
      out_stream << "  H      -0.308940008233360      3.953769010089990     -4.452769714488020   \n";
      out_stream << "  H       1.149610272858070      4.267975518258450     -3.507940377066410   \n";
      out_stream << "  H       0.329742539798290      2.907421698207690     -1.780675055431720   \n";
      out_stream << "  C       7.944264671650420     -3.636157658809950      2.341274111441260   \n";
      out_stream << "  N       7.505794294210260     -2.238327044501830      2.540697102460520   \n";
      out_stream << "  C       8.307195196161080     -1.483828745881310      3.527979235121170   \n";
      out_stream << "  H       7.587475861631630     -1.747162780008010      1.627434063231620   \n";
      out_stream << "  H       7.353691658765560     -4.084950865650000      1.552476358734490   \n";
      out_stream << "  H       8.989013122003030     -3.633659186232600      2.049055912411730   \n";
      out_stream << "  H       7.811955012638530     -4.190557126610740      3.264579071381810   \n";
      out_stream << "  H       9.330510942477041     -1.416488327166180      3.174157072871070   \n";
      out_stream << "  H       7.874700893993960     -0.495082916214320      3.622844088671680   \n";
      out_stream << "  H       8.272114131387310     -1.993455308955980      4.485405731969450   \n";
      out_stream << "  H       6.517258767067640     -2.195565375441880      2.842891990026440   \n";
      out_stream << "  C      -6.950395604146090     -5.383681307475590     -2.764333706501460   \n";
      out_stream << "  N      -6.279799584679120     -4.074089015139630     -2.596658516420370   \n";
      out_stream << "  C      -6.863493465922960     -2.992629814046670     -3.420976750693520   \n";
      out_stream << "  H      -6.367646293740240     -3.794967743801150     -1.600281876845620   \n";
      out_stream << "  H      -6.469066821682370     -6.101564895022010     -2.112483801279590   \n";
      out_stream << "  H      -7.996566760117370     -5.276522583176030     -2.499125294656180   \n";
      out_stream << "  H      -6.848784562316040     -5.706947970209090     -3.794549086703340   \n";
      out_stream << "  H      -7.900819864538560     -2.858384938008540     -3.134691570421760   \n";
      out_stream << "  H      -6.322052131792930     -2.074783485258900     -3.233563333554100   \n";
      out_stream << "  H      -6.793019547427870     -3.265477727427670     -4.469102877574280   \n";
      out_stream << "  H      -5.277723097407240     -4.189076179140890     -2.816130550949010   \n";
      out_stream << "  C      -8.834883055571980      1.315695695974970      1.591087438611450   \n";
      out_stream << "  N      -8.152741448996149      1.223569462635350      0.279632428394650   \n";
      out_stream << "  C      -8.594178532394411      2.211328012666840     -0.726885125472420   \n";
      out_stream << "  H      -7.143069106873980      1.355711098096390      0.426822642185160   \n";
      out_stream << "  H      -8.502351835460759      0.484888160498380      2.202383060289760   \n";
      out_stream << "  H      -8.572506120336289      2.253013769645040      2.068050589017670   \n";
      out_stream << "  H      -9.906908655518400      1.249024321825850      1.436310692223130   \n";
      out_stream << "  H      -8.014875616911510      2.059269236616240     -1.630352891725570   \n";
      out_stream << "  H      -9.649865525948860      2.064386084139060     -0.930811075196180   \n";
      out_stream << "  H      -8.425409879258440      3.212189006492990     -0.343613432145700   \n";
      out_stream << "  H      -8.286025033420280      0.253338215000330     -0.089917132429390   \n";
      out_stream << "  C      -2.627748981792320      4.256424277089300      4.795456956133920   \n";
      out_stream << "  N      -2.512636727074830      2.787422091398700      4.664336961008970   \n";
      out_stream << "  C      -3.479385570336140      2.031660973227110      5.492307502268530   \n";
      out_stream << "  H      -2.656641071015240      2.530373784089910      3.680932488584430   \n";
      out_stream << "  H      -3.619907414180250      4.566203653885940      4.484936014317650   \n";
      out_stream << "  H      -2.453960589998630      4.528676765643720      5.831636994527560   \n";
      out_stream << "  H      -1.889993755497610      4.719191222181610      4.153441119861450   \n";
      out_stream << "  H      -3.350935341553520      2.320897699356710      6.530019483405320   \n";
      out_stream << "  H      -4.487477015050590      2.246037858788460      5.155593143369600   \n";
      out_stream << "  H      -3.257301949490240      0.977731755969320      5.382284481079410   \n";
      out_stream << "  H      -1.549642589022990      2.471229604669400      4.927452495897870   \n";
      out_stream << "  C       6.887313286907990      2.279055646150650     -2.543149058679000   \n";
      out_stream << "  N       6.384120649281240      1.856852089626080     -1.212601122109070   \n";
      out_stream << "  C       7.311385568413660      2.158333959086730     -0.094651901369400   \n";
      out_stream << "  H       5.506269477364080      2.356957191131250     -1.030770290337360   \n";
      out_stream << "  H       7.822938620906550      1.768629653876300     -2.731770950065270   \n";
      out_stream << "  H       6.150082456277950      1.996176186631980     -3.285213094884020   \n";
      out_stream << "  H       7.008059825474370      3.355890059917860     -2.540257284473490   \n";
      out_stream << "  H       8.248328465743210      1.645408608645390     -0.269182404505010   \n";
      out_stream << "  H       7.453141595700310      3.232175764740990     -0.052266391718560   \n";
      out_stream << "  H       6.866516377888300      1.801214939207090      0.827576476386950   \n";
      out_stream << "  H       6.193924019425730      0.843149704243880     -1.230703181368600   \n";
      out_stream << "  C       2.756930856730100      5.110805006944200      2.380505988590290   \n";
      out_stream << "  N       2.678812779854530      3.943657465368660      1.478436963599940   \n";
      out_stream << "  C       2.817833629626320      2.641203118692710      2.164717731274780   \n";
      out_stream << "  H       1.776753310393550      3.996704079032080      0.979260627148240   \n";
      out_stream << "  H       1.949446429015010      5.048136411163380      3.101524784820600   \n";
      out_stream << "  H       3.715244308480930      5.103081285612550      2.889320878536630   \n";
      out_stream << "  H       2.664481702682860      6.011602712458420      1.785706605261540   \n";
      out_stream << "  H       3.750612759370520      2.617939546341190      2.716233446595910   \n";
      out_stream << "  H       1.981672803946910      2.494850567478390      2.835035313760620   \n";
      out_stream << "  H       2.836106879306060      1.866527346619260      1.409197187186150   \n";
      out_stream << "  H       3.415536404646870      4.009402643872680      0.750819154940930   \n";
      out_stream << "  C      -4.042370112680480      7.158936775275580      0.206967515971800   \n";
      out_stream << "  N      -3.958175060085920      5.759877438365610     -0.269786094383610   \n";
      out_stream << "  C      -4.969295844638360      5.408497475585270     -1.290637309139420   \n";
      out_stream << "  H      -3.024166919123920      5.609263997720440     -0.687645195994840   \n";
      out_stream << "  H      -5.025798004777200      7.325689370529210      0.634794089402490   \n";
      out_stream << "  H      -3.276275074313260      7.302706718996590      0.957909816392720   \n";
      out_stream << "  H      -3.862960741846420      7.824479367543530     -0.629075160729700   \n";
      out_stream << "  H      -4.825665030348950      4.381076914190600     -1.603105621916340   \n";
      out_stream << "  H      -5.959299723164830      5.524831654851320     -0.862238773639130   \n";
      out_stream << "  H      -4.846293277699080      6.068704985519750     -2.142166088543620   \n";
      out_stream << "  H      -4.055993829555800      5.130311053723350      0.540829620755230   \n";
      out_stream << "  C       6.252318587636780     -2.729024090693850     -4.273080007387440   \n";
      out_stream << "  N       6.027703408144560     -3.566958985656100     -3.074867080338700   \n";
      out_stream << "  C       6.009911833034490     -5.020730354764920     -3.342886726198230   \n";
      out_stream << "  H       6.764780307393630     -3.347940883470450     -2.372411756100610   \n";
      out_stream << "  H       6.254160257933240     -1.690356628749200     -3.965197858126410   \n";
      out_stream << "  H       7.214511154333100     -2.980669187184560     -4.705942442616490   \n";
      out_stream << "  H       5.452775876163260     -2.908690122622560     -4.983016315661260   \n";
      out_stream << "  H       5.857379703836770     -5.533304894316590     -2.401547419290900   \n";
      out_stream << "  H       5.198044963272540     -5.240625844795170     -4.025540158659200   \n";
      out_stream << "  H       6.962969216543190     -5.309316269092410     -3.774912296532690   \n";
      out_stream << "  H       5.132928660951300     -3.279883941519860     -2.656546533794050   \n";
      out_stream << "  C       2.795093032489410     -2.777334327965200      5.897209404302310   \n";
      out_stream << "  N       2.793699089267580     -3.316563277281000      4.516280458335320   \n";
      out_stream << "  C       2.729408496685780     -4.794216163508360      4.450699050690100   \n";
      out_stream << "  H       1.969509809296650     -2.946130164837010      4.006283538464810   \n";
      out_stream << "  H       3.637582297963670     -3.200969912717390      6.434297509775950   \n";
      out_stream << "  H       2.886712442337980     -1.700775259633960      5.853732409500500   \n";
      out_stream << "  H       1.861518692207140     -3.038509839637080      6.380562550019330   \n";
      out_stream << "  H       2.785595034852800     -5.095079297398540      3.413358883871330   \n";
      out_stream << "  H       3.571821231807210     -5.209824138682170      4.994046003813150   \n";
      out_stream << "  H       1.792675344244310     -5.120927261205270      4.886710830283100   \n";
      out_stream << "  H       3.633778138512620     -2.965074394638580      4.032830883202780   \n";
      out_stream << "  C      -0.610463792694380     -4.973225801386130     -4.992790963570710   \n";
      out_stream << "  N      -1.177841830285960     -3.907771900811190     -4.136958763590030   \n";
      out_stream << "  C      -1.342093819623350     -2.603316617353960     -4.812750965988540   \n";
      out_stream << "  H      -2.091631778334690     -4.223442943380620     -3.759939540066300   \n";
      out_stream << "  H       0.397281205002650     -4.697418862634060     -5.284157688062510   \n";
      out_stream << "  H      -0.594388547707640     -5.890533949280400     -4.416842551404760   \n";
      out_stream << "  H      -1.240205905464800     -5.098665883930290     -5.867369033793860   \n";
      out_stream << "  H      -0.374740186839790     -2.261096975238120     -5.163499461864580   \n";
      out_stream << "  H      -2.029661980469960     -2.715911205413340     -5.644500570273670   \n";
      out_stream << "  H      -1.742258732014490     -1.899666656788730     -4.093882652865250   \n";
      out_stream << "  H      -0.550753165284630     -3.761628981457770     -3.338222884953710   \n";
      out_stream << "  C      -0.369830995685070      0.530214400713180      1.204945235673010   \n";
      out_stream << "  N       0.640057197604860     -0.547167012699070      1.284545720974540   \n";
      out_stream << "  C       0.648959774243390     -1.432812712644240      0.099294607693420   \n";
      out_stream << "  H       1.569143978315360     -0.156856878465450      1.432114950177180   \n";
      out_stream << "  H      -1.344392357535920      0.074623351944700      1.095553532949610   \n";
      out_stream << "  H      -0.326712300971860      1.122611663752330      2.105387788169390   \n";
      out_stream << "  H      -0.145965381888670      1.146651269786620      0.345626305680160   \n";
      out_stream << "  H       1.456971943053720     -2.142865410647940      0.205530639395810   \n";
      out_stream << "  H      -0.296513466658120     -1.950000388436210      0.047603864615720   \n";
      out_stream << "  H       0.784488406342320     -0.841344859828210     -0.795171750317270   \n";
      out_stream << "  H       0.479659164299420     -1.136038539527550      2.118150920126650   \n";
      out_stream << "  C      -4.902471056926270     -3.570200519439190      2.865320053022650   \n";
      out_stream << "  N      -4.274965243196190     -2.351593946549730      2.305127504823300   \n";
      out_stream << "  C      -4.449698283050360     -1.141211054807460      3.138747103437270   \n";
      out_stream << "  H      -3.269494425172160     -2.550104517923610      2.161694844410460   \n";
      out_stream << "  H      -4.758340710824490     -4.379378629480390      2.159515667292580   \n";
      out_stream << "  H      -4.418178171580080     -3.803437418773420      3.807295002764140   \n";
      out_stream << "  H      -5.961580163913740     -3.389287744464040      3.001378197521430   \n";
      out_stream << "  H      -4.019543446752540     -1.328049344397540      4.115236928522460   \n";
      out_stream << "  H      -3.942057491335130     -0.309891147564570      2.665266511111030   \n";
      out_stream << "  H      -5.507702899656850     -0.923163571181560      3.217595133575140   \n";
      out_stream << "  H      -4.659944978882130     -2.165698367642650      1.375859479266860   \n";
      out_stream << "  C       3.298474986134900      7.706300332213310     -3.785038762996010   \n";
      out_stream << "  N       2.418740697919470      7.738250118958890     -2.598321548281370   \n";
      out_stream << "  C       2.167826392985230      9.090509239359241     -2.059381826204290   \n";
      out_stream << "  H       1.499851343674660      7.300453828032780     -2.837154604855380   \n";
      out_stream << "  H       4.270150757052950      8.112911622384241     -3.525234783580410   \n";
      out_stream << "  H       3.419069115473920      6.675595164231310     -4.096456851324970   \n";
      out_stream << "  H       2.847577095116290      8.288545108049069     -4.582447392737960   \n";
      out_stream << "  H       3.109242112119470      9.537766836205570     -1.756178627150870   \n";
      out_stream << "  H       1.694999037649250      9.699456296664509     -2.823247682180480   \n";
      out_stream << "  H       1.502997537843990      8.997804179052901     -1.209307846199590   \n";
      out_stream << "  H       2.852590981389030      7.151025139137670     -1.855080778682900   \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 
    ********************************************************************/
   std::filesystem::path Util::PrintPM71am1dma1fa1msa_0_ToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "PM71am1dma1fa1msa_0.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << " 28                                             \n";
      out_stream << "1am1dma1fa1msa_0                                \n";
      out_stream << " S          1.97012        0.04862        0.06963   \n";
      out_stream << " O          1.58479       -1.34042       -0.33978   \n";
      out_stream << " O          1.60001        0.26992        1.47890   \n";
      out_stream << " O          1.37277        1.08600       -0.82184   \n";
      out_stream << " H         -0.37110        1.35133       -0.88132   \n";
      out_stream << " C          3.73531        0.16465       -0.08034   \n";
      out_stream << " H          4.14457       -0.68328       -0.63818   \n";
      out_stream << " H          4.03051        1.07936       -0.60650   \n";
      out_stream << " H          4.21163        0.17747        0.90644   \n";
      out_stream << " H         -4.16230       -1.66524        0.69835   \n";
      out_stream << " H         -0.66006       -1.97600       -0.44798   \n";
      out_stream << " C         -3.13491       -1.36246        0.46229   \n";
      out_stream << " O         -2.26577       -2.23544        0.33332   \n";
      out_stream << " O         -2.94273       -0.14053        0.27278   \n";
      out_stream << " N          0.05208       -2.71068       -0.62063   \n";
      out_stream << " H         -0.29850       -3.56451       -0.20489   \n";
      out_stream << " H          0.95796       -2.38284       -0.18536   \n";
      out_stream << " H          0.18799       -2.79178       -1.62557   \n";
      out_stream << " N         -1.22473        1.56076       -0.28283   \n";
      out_stream << " C         -2.27384        2.26557       -1.08079   \n";
      out_stream << " C         -0.79441        2.41280        0.87115   \n";
      out_stream << " H         -1.68498        0.59452        0.11980   \n";
      out_stream << " H         -1.93395        3.25262       -1.42509   \n";
      out_stream << " H         -2.53886        1.65240       -1.95884   \n";
      out_stream << " H         -3.19728        2.37974       -0.48292   \n";
      out_stream << " H         -0.27342        3.31965        0.52671   \n";
      out_stream << " H         -1.64976        2.68353        1.50425   \n";
      out_stream << " H         -0.06118        1.84495        1.49045   \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1am2nta_115
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_aug_pvqz_1am2nta_ToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "dfmp2_aug_pvqz_1am2nta.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "   14                                                             \n";
      out_stream << " 1am2nta              \n";
      out_stream << " N         -0.9500968160        2.2794321280        0.0963160667  \n";
      out_stream << " H         -0.9762013293        3.0681668376       -0.5374026993  \n";
      out_stream << " H         -0.0483395267        1.7914481973        0.0381129123  \n";
      out_stream << " H         -1.0810845667        2.5949150919        1.0498671941  \n";
      out_stream << " N          2.1592828469       -0.1220886005       -0.0895619510  \n";
      out_stream << " O          1.5726100481        0.9527098485       -0.2162051805  \n";
      out_stream << " O          3.3181215563       -0.3315551985       -0.3436304499  \n";
      out_stream << " O          1.4643640551       -1.1720942542        0.3728959922  \n";
      out_stream << " H          0.5084467600       -0.8579811075        0.5411564446  \n";
      out_stream << " N         -1.8018705866       -0.6347395400       -0.0050819960  \n";
      out_stream << " O         -0.8997715550       -0.3208504452        0.8442092664  \n";
      out_stream << " O         -1.9540311289       -1.7805860648       -0.3849794029  \n";
      out_stream << " O         -2.5305950376        0.3145686124       -0.4367018490  \n";
      out_stream << " H         -1.7087167196        1.5224454949       -0.1312833477  \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 1am2nta_115
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_1am2nta_ToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_1am2nta.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << " 14                                                                  \n";
      out_stream << " 1am2nta                 \n";
      out_stream << "   N   -0.97897914912599      2.30609424579275      0.08324198750860 \n";
      out_stream << "   H   -1.01166218977526      3.08464142383522     -0.56857082662606 \n";
      out_stream << "   H   -0.06817845530943      1.83210593334675      0.03042954279072 \n";
      out_stream << "   H   -1.10287821343526      2.64786501781949      1.03211207000496 \n";
      out_stream << "   N   2.24070941274995     -0.11657435253082     -0.07087997670516  \n";
      out_stream << "   O   1.66932360154368      0.97301564411116     -0.12358865716518  \n";
      out_stream << "   O   3.39607253942700     -0.32856254519327     -0.31307583477229  \n";
      out_stream << "   O   1.51028752437253     -1.20948788430122      0.29998365470491  \n";
      out_stream << "   H   0.55206229631338     -0.90489957005170      0.48104892182555  \n";
      out_stream << "   N   -1.84564829873866     -0.65270860822040      0.01418244789881 \n";
      out_stream << "   O   -0.88916089568888     -0.38487512195229      0.81919580828502 \n";
      out_stream << "   O   -2.10910353147480     -1.77860569768149     -0.33958427503590 \n";
      out_stream << "   O   -2.53441272352479      0.35450331646235     -0.40981342210359 \n";
      out_stream << "   H   -1.75631391733348      1.48127919856347     -0.13697044061039 \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 1eda1ma1nta_252
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_aug_pvqz_1eda1ma1nta_ToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "dfmp2_aug_pvqz_1eda1ma1nta.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "          24                                                       \n";
      out_stream << "  1eda1ma1nta                                                      \n";
      out_stream << "  N          0.6661576092        1.7364475354       -0.4424328883  \n";
      out_stream << "  H          0.9479784526        0.7998723320       -0.8282461970  \n";
      out_stream << "  C          1.7892109954        2.4627766023        0.1647756093  \n";
      out_stream << "  H          2.5367776837        2.6917772570       -0.5876656149  \n";
      out_stream << "  H          1.4312312945        3.3834375209        0.6126894426  \n";
      out_stream << "  H          2.2181849140        1.8199492931        0.9235814161  \n";
      out_stream << "  H          0.2173608227        2.2509527475       -1.1923737075  \n";
      out_stream << "  H          2.4996997819       -2.0434091737        0.3457055313  \n";
      out_stream << "  H          1.1572480105       -2.7615923831        1.2259700696  \n";
      out_stream << "  H          0.6142607916       -2.8867464823       -1.0598109053  \n";
      out_stream << "  H         -0.4433788311       -1.6896814894       -0.3434520681  \n";
      out_stream << "  H          1.8577723647       -1.1462330707       -2.0477169050  \n";
      out_stream << "  H          0.2931992828       -0.7505222334       -2.2782665204  \n";
      out_stream << "  H          0.3833679639       -0.4491574975        1.6025454659  \n";
      out_stream << "  H          1.8674256591       -0.6905447810        2.2190764172  \n";
      out_stream << "  C          1.4552822621       -1.8948497805        0.6280528354  \n";
      out_stream << "  C          0.5916625105       -1.8884864418       -0.6167613992  \n";
      out_stream << "  N          1.3529538914       -0.6277470851        1.3499724947  \n";
      out_stream << "  N          1.0134965432       -0.8516059860       -1.5718542475  \n";
      out_stream << "  N         -1.8963207493        0.1767544998        0.1494309284  \n";
      out_stream << "  O         -1.7060365725        0.5052663166       -1.0461670049  \n";
      out_stream << "  O         -2.5906966021       -0.7919747828        0.4604589936  \n";
      out_stream << "  O         -1.3105814923        0.8589821065        1.0667704994  \n";
      out_stream << "  H         -0.1137785865        1.5028939762        0.2463327546  \n";

      out_stream.close();

      return file_path;
   }
   /********************************************************************
    * 1eda1ma1nta_252
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_1eda1ma1nta_ToDir(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_1eda1ma1nta.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << " 24                                                                 \n";
      out_stream << " 1eda1ma1nta                                                        \n";
      out_stream << "   N   0.65977558567795      1.74868689458150     -0.45227532864152 \n";
      out_stream << "   H   0.96054963666563      0.81144886821223     -0.83677450490850 \n";
      out_stream << "   C   1.77233104418952      2.49594745747642      0.17339542910931 \n";
      out_stream << "   H   2.53984098127530      2.73311685452588     -0.56865022853230 \n";
      out_stream << "   H   1.39661249166214      3.42212651919827      0.61556092340413 \n";
      out_stream << "   H   2.20158633780380      1.86068963507777      0.94995552052418 \n";
      out_stream << "   H   0.21326565786764      2.25954804600517     -1.21073344116859 \n";
      out_stream << "   H   2.55609154649045     -2.08991213981854      0.32917875711739 \n";
      out_stream << "   H   1.21100962748883     -2.81457370445068      1.21929338965427 \n";
      out_stream << "   H   0.63356684898371     -2.92108280336648     -1.05689287713425 \n";
      out_stream << "   H   -0.41100052898342     -1.70680662544516     -0.32505659002013\n";
      out_stream << "   H   1.89320577577202     -1.16171780881173     -2.05315889860649 \n";
      out_stream << "   H   0.31944985751200     -0.79232593231418     -2.29678022262447 \n";
      out_stream << "   H   0.45358802446279     -0.49376683426043      1.63619544437467 \n";
      out_stream << "   H   1.94454061715661     -0.74955743715252      2.23827935509319 \n";
      out_stream << "   C   1.50839975217943     -1.93137057862541      0.62672661486995 \n";
      out_stream << "   C   0.62655925549940     -1.91230914540442     -0.61389289320165 \n";
      out_stream << "   N   1.42323792391523     -0.66749357099533      1.37181094695738 \n";
      out_stream << "   N   1.03947825427674     -0.87405585125140     -1.58407288831093 \n";
      out_stream << "   N   -2.01523436413906      0.23937908515080      0.13408874752572\n";
      out_stream << "   O   -1.77321212381134      0.48778385470971     -1.07382033607071\n";
      out_stream << "   O   -2.86012399882266     -0.57647415999761      0.48582677538763\n";
      out_stream << "   O   -1.31755064469629      0.86151953875231      1.03579249779333\n";
      out_stream << "   H   -0.14348955842644      1.48775883820382      0.23661880740841\n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1fa1msa1tma_324 optimised by DF-MP2/aug-cc-pVQZ
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_1fa1msa1tma_324_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "mp2_pvqz_1fa1msa1tma_324.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "   27\n";
      out_stream << "1fa1msa1tma_324\n";
      out_stream << " S         -0.9824940417       -0.9381007331       -0.3831980541\n";
      out_stream << " O         -0.1586960363       -1.4696755570       -1.4639250063\n";
      out_stream << " O         -1.7843949943        0.2329919222       -0.7834755780\n";
      out_stream << " O         -0.1994129960       -0.6596889766        0.8463687594\n";
      out_stream << " H          1.0909290697        0.2055612947        0.4071205800\n";
      out_stream << " C         -2.1393291789       -2.1922617494        0.0749159840\n";
      out_stream << " H         -2.7580088092       -2.4045018354       -0.7907871922\n";
      out_stream << " H         -1.5834463856       -3.0732888871        0.3781302207\n";
      out_stream << " H         -2.7403571374       -1.8119366416        0.8946024088\n";
      out_stream << " H         -1.5165550796        3.4550974528        2.2059349560\n";
      out_stream << " H         -2.0025637034        1.3875369347        0.3077275411\n";
      out_stream << " C         -1.2261774153        2.7459935519        1.4238990288\n";
      out_stream << " O         -2.2651276064        2.0716134789        0.9972396320\n";
      out_stream << " O         -0.0773595785        2.6390440154        1.0336618857\n";
      out_stream << " N          1.9874063533        0.5457443236       -0.0481256804\n";
      out_stream << " C          1.6066070128        1.2953722611       -1.2691260498\n";
      out_stream << " H          1.0389287273        0.6270655227       -1.9095910112\n";
      out_stream << " H          0.9957564889        2.1388500417       -0.9684880657\n";
      out_stream << " H          2.5178829861        1.6241965210       -1.7639546727\n";
      out_stream << " C          2.7334515805        1.3992814379        0.8977239853\n";
      out_stream << " H          2.1157373839        2.2539517762        1.1475215611\n";
      out_stream << " H          2.9527126256        0.8205458773        1.7896726031\n";
      out_stream << " H          3.6601164535        1.7127490283        0.4226716443\n";
      out_stream << " C          2.7474371546       -0.6726001205       -0.4013913103\n";
      out_stream << " H          3.0009354070       -1.2011793132        0.5112632045\n";
      out_stream << " H          2.1115583735       -1.2917535287       -1.0265311960\n";
      out_stream << " H          3.6512513455       -0.3771810979       -0.9282911781\n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1fa1msa1tma_324 optimised by r2-scan-3c
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_1fa1msa1tma_324_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_1fa1msa1tma_324.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "27\n";
      out_stream << "1fa1msa1tma_324\n";
      out_stream << "  S   -0.61611033377651     -1.52062409182405      0.02146432553232\n";
      out_stream << "  O   -0.69356462736534     -2.88903689265356     -0.50192281988030\n";
      out_stream << "  O   -0.63367869850605     -0.43730296750357     -1.03880940449319\n";
      out_stream << "  O   0.57601190444275     -1.28717681415122      0.94593204407122\n";
      out_stream << "  H   1.43193482050753     -0.21703963076025      0.44122950872244\n";
      out_stream << "  C   -2.06090096656796     -1.22650360263584      1.06517067196181\n";
      out_stream << "  H   -2.95324709412286     -1.34840469022910      0.44599469164164\n";
      out_stream << "  H   -2.04676554053917     -1.97072663730671      1.86447541074127\n";
      out_stream << "  H   -1.99270711091941     -0.21318399689063      1.46902998954677\n";
      out_stream << "  H   -3.02691078607996      3.15587904988566     -0.16264708503863\n";
      out_stream << "  H   -1.70605283778994      0.76350917022354     -1.02517042849653\n";
      out_stream << "  C   -2.28585324251093      2.34095913945247     -0.07484453802740\n";
      out_stream << "  O   -2.38100481071814      1.51459035000124     -1.10351863245094\n";
      out_stream << "  O   -1.50863657282429      2.25056464362439      0.85414220521046\n";
      out_stream << "  N   2.18637462213511      0.49199442506188      0.04626723572488\n";
      out_stream << "  C   1.56477859766983      1.83771838228476     -0.06907618501169\n";
      out_stream << "  H   0.73465647148758      1.77306480085299     -0.77301040686560\n";
      out_stream << "  H   1.16548945429322      2.13391471541445      0.90252525235426\n";
      out_stream << "  H   2.31387546984907      2.55826328597249     -0.41502657330957\n";
      out_stream << "  C   3.31111591522967      0.48308115448654      1.01489332859168\n";
      out_stream << "  H   2.94948686338294      0.84258026173059      1.98109786039019\n";
      out_stream << "  H   3.67380157314225     -0.54161050952014      1.12579997946114\n";
      out_stream << "  H   4.11920394860518      1.12936102068244      0.65685102274581\n";
      out_stream << "  C   2.59572797172797     -0.03878133379870     -1.28176755616795\n";
      out_stream << "  H   2.99428380097218     -1.04756056328534     -1.14943067223506\n";
      out_stream << "  H   1.70824370603792     -0.08774899925072     -1.91629936943691\n";
      out_stream << "  H   3.35723550223734      0.61364733013635     -1.72178085528214\n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1am1eda1fa_22 optimised by DF-MP2/aug-cc-pVQZ
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_1am1eda1fa_22_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "dfmp2_aug_pvqz_1am1eda1fa_22.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "21\n";
      out_stream << "1am1eda1fa_22\n";
      out_stream << "  C          2.6646959454        0.2111229886       -0.0230360077 \n";
      out_stream << "  O          1.7957535484        1.0582554468       -0.1256384244 \n";
      out_stream << "  O          2.4816279559       -1.0661559786        0.2095752362 \n";
      out_stream << "  H          3.7282021617        0.4509101461       -0.1228985626 \n";
      out_stream << "  H         -0.3073712903       -0.1002239983       -0.9925431260 \n";
      out_stream << "  H         -0.6497496245       -1.7230218655       -1.5825232287 \n";
      out_stream << "  H         -2.7177917313       -0.3796424095       -1.3770284068 \n";
      out_stream << "  H         -2.7590000421       -1.7538726981       -0.2736162481 \n";
      out_stream << "  H         -2.0568211469        0.9848244236        0.4907409589 \n";
      out_stream << "  H         -3.3444826795        0.1077575954        1.0093934726 \n";
      out_stream << "  H          1.4705072998       -1.2728783036        0.3128311448 \n";
      out_stream << "  H         -0.4701839129       -1.0990739123        1.2383871738 \n";
      out_stream << "  C         -0.7864929366       -1.0458557818       -0.7413351654 \n";
      out_stream << "  C         -2.2633519564       -0.8025952842       -0.4747119849 \n";
      out_stream << "  N         -0.0872785106       -1.5880822769        0.4311748223 \n";
      out_stream << "  N         -2.3833430646        0.0432492222        0.7064632592 \n";
      out_stream << "  H         -0.3256101512       -2.5649732571        0.5545724301 \n";
      out_stream << "  N         -0.8350057965        2.6792046878       -0.0261672337 \n";
      out_stream << "  H         -0.8308599452        3.3515533966        0.7289552595 \n";
      out_stream << "  H         -0.9168169962        3.2137361013       -0.8803154240 \n";
      out_stream << "  H          0.0842528738        2.2477127574       -0.0372179450 \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1am1eda1fa_22 optimised by r2-SCAN-3c
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_1am1eda1fa_22_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_1am1eda1fa_22.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "21\n";
      out_stream << "1am1eda1fa_22\n";
      out_stream << "  C   2.72105424367092      0.19134412764726     -0.03005464191979  \n";
      out_stream << "  O   1.88102222816088      1.07053194494649     -0.11520955958211  \n";
      out_stream << "  O   2.49865190771986     -1.08586875430498      0.18213302553339  \n";
      out_stream << "  H   3.80236341744692      0.40105145905986     -0.13045339504016  \n";
      out_stream << "  H   -0.33246012764728     -0.05913030768035     -0.98317986453155 \n";
      out_stream << "  H   -0.64495704251184     -1.68940777927325     -1.59213518493896 \n";
      out_stream << "  H   -2.74341957695085     -0.39174880689924     -1.38325593716897 \n";
      out_stream << "  H   -2.76663679086281     -1.78984408006491     -0.29402777672809 \n";
      out_stream << "  H   -2.12018220952714      0.96075157500719      0.53292785490833 \n";
      out_stream << "  H   -3.42383489007143      0.07296718873525      0.99226326628999 \n";
      out_stream << "  H   1.46585789914272     -1.27414721349653      0.29284803466818  \n";
      out_stream << "  H   -0.43845496767992     -1.07740700410612      1.25285342378489 \n";
      out_stream << "  C   -0.79580164203461     -1.02244770244033     -0.73507417304719 \n";
      out_stream << "  C   -2.28652619812846     -0.81566652055191     -0.47016919714705 \n";
      out_stream << "  N   -0.07206834683950     -1.56750476206720      0.43489540606358 \n";
      out_stream << "  N   -2.44779892574463      0.01197185569526      0.72881833673889 \n";
      out_stream << "  H   -0.31243179418748     -2.54661549410801      0.56552296950440 \n";
      out_stream << "  N   -0.83996243545433      2.69688820543161     -0.01718939565923 \n";
      out_stream << "  H   -0.84554161530117      3.43055977911287      0.68319850358956 \n";
      out_stream << "  H   -0.89123096758384      3.16620835905135     -0.91481211912422 \n";
      out_stream << "  H   0.08323783438401      2.26946493030569      0.02516242380612  \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 2tma_100 optimised by DF-MP2/aug-cc-pVQZ
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_2tma_100_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "dfmp2_aug_pvqz_2tma_100.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "26\n";
      out_stream << "2tma_100\n";
      out_stream << "  N          1.5619201089       -0.0021671712        0.0001474149  \n";
      out_stream << "  C          2.0198341456        0.6088947988       -1.2303908659  \n";
      out_stream << "  H          1.6433644236        0.0454270610       -2.0823816919  \n";
      out_stream << "  H          1.6424982881        1.6278769402       -1.2969573163  \n";
      out_stream << "  H          3.1187175527        0.6415981681       -1.2951198282  \n";
      out_stream << "  C          2.0234888315        0.7572639473        1.1436177279  \n";
      out_stream << "  H          1.6466596544        1.7768394405        1.0836183984  \n";
      out_stream << "  H          1.6492446005        0.3046885168        2.0602550394  \n";
      out_stream << "  H          3.1225627209        0.7970821984        1.2007223717  \n";
      out_stream << "  C          2.0211897212       -1.3729987134        0.0850983339  \n";
      out_stream << "  H          1.6464387836       -1.8296980600        0.9994827911  \n";
      out_stream << "  H          1.6430409348       -1.9401287441       -0.7637156134  \n";
      out_stream << "  H          3.1201298746       -1.4445745282        0.0874676515  \n";
      out_stream << "  N         -2.3818807011        0.0034661455        0.0003587452  \n";
      out_stream << "  C         -1.9114794521       -0.6073454643        1.2307053929  \n";
      out_stream << "  H         -2.2853329055       -0.0430729587        2.0833122352  \n";
      out_stream << "  H         -2.2916264135       -1.6250648214        1.3003269516  \n";
      out_stream << "  H         -0.8133813033       -0.6365572411        1.2809769978  \n";
      out_stream << "  C         -1.9113939872       -0.7588150374       -1.1422962129  \n";
      out_stream << "  H         -2.2905883840       -1.7774485268       -1.0813807555  \n";
      out_stream << "  H         -2.2860905873       -0.3082173352       -2.0597251316  \n";
      out_stream << "  H         -0.8132860950       -0.7932810215       -1.1889352177  \n";
      out_stream << "  C         -1.9050994139        1.3721246299       -0.0869996388  \n";
      out_stream << "  H         -2.2795033053        1.8303275884       -1.0007776917  \n";
      out_stream << "  H         -2.2799892740        1.9429453317        0.7607832480  \n";
      out_stream << "  H         -0.8067578182        1.4248848566       -0.0900733355  \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1am1eda1fa_22 optimised by r2-SCAN-3c
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_2tma_100_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_2tma_100.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "26\n";
      out_stream << "2tma_100\n";
      out_stream << " N   1.63495484126158     -0.00024763160882      0.00082045829965   \n";
      out_stream << " C   2.08481385811802      0.61508036506298     -1.24135921008926   \n";
      out_stream << " H   1.69713624194350      0.05076242852830     -2.09708324433325   \n";
      out_stream << " H   1.70111948847993      1.63942370979726     -1.30872767728687   \n";
      out_stream << " H   3.19009868176724      0.65549812482492     -1.32833758371067   \n";
      out_stream << " C   2.09038918998079      0.76502399634781      1.15446850463828   \n";
      out_stream << " H   1.70805515965058      1.79028506242207      1.09415202341974   \n";
      out_stream << " H   1.70510805256324      0.31306798413353      2.07549709380832   \n";
      out_stream << " H   3.19605374118358      0.81411740322617      1.23174448884693   \n";
      out_stream << " C   2.08107990866628     -1.38505173952190      0.08619090722549   \n";
      out_stream << " H   1.69811321301071     -1.84268127105058      1.00537561062309   \n";
      out_stream << " H   1.68996882621514     -1.95428745260870     -0.76470671086195   \n";
      out_stream << " H   3.18608819833186     -1.48425086175258      0.08756067393413   \n";
      out_stream << " N   -2.43008675933036      0.00185814746053     -0.00001513881596  \n";
      out_stream << " C   -1.97096267219272     -0.61339755774840      1.24114838030263  \n";
      out_stream << " H   -2.35418381558342     -0.04684567813231      2.09769703324421  \n";
      out_stream << " H   -2.35842983303945     -1.63634612619429      1.31170481995850  \n";
      out_stream << " H   -0.86532448576806     -0.65285197220850      1.31481160229724  \n";
      out_stream << " C   -1.97034039444092     -0.76596813136987     -1.15279385730375  \n";
      out_stream << " H   -2.35666457644027     -1.78996652148166     -1.09232294723313  \n";
      out_stream << " H   -2.35403933588097     -0.31370630610324     -2.07460243294440  \n";
      out_stream << " H   -0.86466074598011     -0.81332373355963     -1.22094972934719  \n";
      out_stream << " C   -1.96826367775417      1.38358225345739     -0.08791802597132  \n";
      out_stream << " H   -2.35225463722506      1.84382580182539     -1.00563239648080  \n";
      out_stream << " H   -2.35264891820456      1.95626289717061      0.76400530606441  \n";
      out_stream << " H   -0.86243954933237      1.46418680908349     -0.09260794828406  \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 2eda1nta1sa_590 optimised by dfmp2/aug-cc-pvqz
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_2eda1nta1sa_590_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "dfmp2_aug_pvqz_2eda1nta1sa_590.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "36\n";
      out_stream << "2eda1nta1sa_590\n";
      out_stream << "  S         -0.1266877204        1.5845793226        0.4443806550  \n";
      out_stream << "  O         -0.8610374012        0.7969174579        1.6383607178  \n";
      out_stream << "  O         -0.9224589255        2.7675268007        0.2648995085  \n";
      out_stream << "  O          1.2569561383        1.8038806517        0.8841534037  \n";
      out_stream << "  O         -0.1585356289        0.6267861271       -0.6869554046  \n";
      out_stream << "  H          2.1117384908        0.3856516598        0.7436971291  \n";
      out_stream << "  H         -1.5332079907       -0.2925167852       -0.7437148561  \n";
      out_stream << "  N          0.0479028827       -2.5961774058        0.3403067410  \n";
      out_stream << "  O          1.2784473059       -2.7753010372        0.1966107644  \n";
      out_stream << "  O         -0.7623410670       -3.1534313459       -0.4338983281  \n";
      out_stream << "  O         -0.3802634320       -1.8563087092        1.2627073581  \n";
      out_stream << "  H         -0.5303111794       -0.1257484838        1.6248673769  \n";
      out_stream << "  H         -4.1704127833       -1.2995318163        0.1914370903  \n";
      out_stream << "  H         -2.9006695120       -0.5912647959        1.2080091193  \n";
      out_stream << "  H         -4.6266888998        1.1188719071        0.6201966219  \n";
      out_stream << "  H         -3.0526698196        1.5310208471       -0.0428742929  \n";
      out_stream << "  H         -5.3443689085        0.5435817528       -1.5320685459  \n";
      out_stream << "  H         -4.2942295276        1.7238254323       -1.9115340728  \n";
      out_stream << "  H         -1.9759393084       -1.8980214905       -0.6412710309  \n";
      out_stream << "  H         -2.7881931481       -0.8351626122       -1.6974313587  \n";
      out_stream << "  C         -3.3690731475       -0.5638740134        0.2309257531  \n";
      out_stream << "  C         -3.8766953731        0.8219989756       -0.1141417845  \n";
      out_stream << "  N         -2.3578318441       -0.9430849508       -0.7793560068  \n";
      out_stream << "  N         -4.3695728776        0.8077324184       -1.4936745136  \n";
      out_stream << "  H          3.3503505587       -1.1553733344       -1.4357957933  \n";
      out_stream << "  H          1.9928855768       -0.0110368987       -1.5206382550  \n";
      out_stream << "  H          4.1224553315        1.1057599578       -2.2122233545  \n";
      out_stream << "  H          3.4624471560        1.8088000155       -0.7373919859  \n";
      out_stream << "  H          5.7421881330       -0.0028397665       -0.8905672212  \n";
      out_stream << "  H          5.6277200211        1.4269107907       -0.1275466345  \n";
      out_stream << "  H          2.1270930747       -1.3196019059        0.5480040618  \n";
      out_stream << "  H          3.5578903867       -0.4643507828        0.8895445326  \n";
      out_stream << "  C          2.9389776341       -0.2310584323       -1.0353543313  \n";
      out_stream << "  C          3.9218439248        0.9150616619       -1.1568262022  \n";
      out_stream << "  N          2.6614940168       -0.4392845025        0.4033772929  \n";
      out_stream << "  N          5.1127938626        0.5926902901       -0.3693521538  \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 2eda1nta1sa_590 optimised by r2-SCAN-3c
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_2eda1nta1sa_590_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_2eda1nta1sa_590.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "36\n";
      out_stream << "2eda1nta1sa_590\n";
      out_stream << "   S   -0.13542621599260      1.63933438074935      0.40255509560426  \n";
      out_stream << "   O   -0.90200318881452      0.85001048462340      1.64177017711942  \n";
      out_stream << "   O   -0.92730208239594      2.85351038712765      0.22154529351863  \n";
      out_stream << "   O   1.27772781466726      1.84048589793204      0.85252468628761   \n";
      out_stream << "   O   -0.18549360520267      0.65340295156663     -0.74175136205547  \n";
      out_stream << "   H   2.14254139221075      0.39977958411977      0.74842660156202   \n";
      out_stream << "   H   -1.57799315686143     -0.27537166569413     -0.77016621906739  \n";
      out_stream << "   N   0.06111804549512     -2.59840374449221      0.33185807300567   \n";
      out_stream << "   O   1.29534822425257     -2.77667195275035      0.17973410262979   \n";
      out_stream << "   O   -0.74908410728254     -3.14711711246686     -0.45370094425517  \n";
      out_stream << "   O   -0.36481575187457     -1.87515518912442      1.27066800437028  \n";
      out_stream << "   H   -0.55737381024045     -0.07002397149007      1.64822585892622  \n";
      out_stream << "   H   -4.21793398794769     -1.35339457117385      0.15906172458089  \n";
      out_stream << "   H   -2.95284491190783     -0.64064438819928      1.19279528270740  \n";
      out_stream << "   H   -4.70785296912023      1.05369332441005      0.68138379861183  \n";
      out_stream << "   H   -3.14785233719360      1.52128109024659      0.00184173815747  \n";
      out_stream << "   H   -5.46022755402651      0.54667718272278     -1.49101075117140  \n";
      out_stream << "   H   -4.44349651543899      1.76856370311570     -1.83882183979729  \n";
      out_stream << "   H   -1.99910786360219     -1.89781779340604     -0.68514163202130  \n";
      out_stream << "   H   -2.82786840133246     -0.83260415209289     -1.73807499253916  \n";
      out_stream << "   C   -3.42579980861280     -0.59668365642956      0.20988610706332  \n";
      out_stream << "   C   -3.96503668769809      0.79521373008468     -0.08824349804072  \n";
      out_stream << "   N   -2.39883344566071     -0.94247636239189     -0.81593170062370  \n";
      out_stream << "   N   -4.48653612141189      0.82807176687852     -1.46630406357102  \n";
      out_stream << "   H   3.41817590842359     -1.18678102992326     -1.40616554378838   \n";
      out_stream << "   H   2.07839209424711     -0.01427772146326     -1.53025661641105   \n";
      out_stream << "   H   4.22447748683796      1.05466110956070     -2.22446481397412   \n";
      out_stream << "   H   3.56557508646275      1.81049666542688     -0.76611208623221   \n";
      out_stream << "   H   5.83916963898148     -0.03586425894669     -0.86049046612785   \n";
      out_stream << "   H   5.74063054020010      1.42262260279338     -0.14690828959571   \n";
      out_stream << "   H   2.15596472247627     -1.31851372427390      0.56376303366945   \n";
      out_stream << "   H   3.58795703239200     -0.46302376169872      0.94197129622375   \n";
      out_stream << "   C   3.01754150955724     -0.24305257162838     -1.01892937069865   \n";
      out_stream << "   C   4.02164898925656      0.89182150490300     -1.15468303672432   \n";
      out_stream << "   N   2.70286145019019     -0.43508227967382      0.42865595602171   \n";
      out_stream << "   N   5.21574858696675      0.58096054105845     -0.35064760336478   \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1sa2tma_412 optimised by DF-MP2/aug-cc-pVQZ
    ********************************************************************/
   std::filesystem::path Util::Print_dfmp2_1sa2tma_412_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "dfmp2_aug_pvqz_1sa2tma_412.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "33\n";
      out_stream << "1sa2tma_412\n";
      out_stream << "  S         -0.0317091804        1.2554857018       -0.1032313205  \n";
      out_stream << "  O          0.1118701741        0.7097789622       -1.4493237182  \n";
      out_stream << "  O          0.2763144263        0.0293370425        0.8868214912  \n";
      out_stream << "  O          0.8494492089        2.3442273472        0.2357957936  \n";
      out_stream << "  O         -1.4616207233        1.5331404759        0.2304295397  \n";
      out_stream << "  H         -2.1662980379        0.3360446036        0.0592512920  \n";
      out_stream << "  N          2.6715338553       -0.7751750622        0.1227334507  \n";
      out_stream << "  C          3.3834735528        0.3938566705       -0.3918066950  \n";
      out_stream << "  H          2.7872627945        0.8599830643       -1.1709787827  \n";
      out_stream << "  H          3.5194499706        1.1157348944        0.4088767121  \n";
      out_stream << "  H          4.3616777417        0.1045593167       -0.7950816285  \n";
      out_stream << "  C          3.4417256259       -1.4142902308        1.1793211434  \n";
      out_stream << "  H          3.6086958220       -0.7034602435        1.9850724024  \n";
      out_stream << "  H          2.8897058591       -2.2662266266        1.5712719470  \n";
      out_stream << "  H          4.4137010323       -1.7647047634        0.8108878400  \n";
      out_stream << "  C          2.4032018790       -1.7114646564       -0.9619736171  \n";
      out_stream << "  H          1.8698086693       -2.5762495563       -0.5699913732  \n";
      out_stream << "  H          1.7834177933       -1.2152854906       -1.7052584778  \n";
      out_stream << "  H          3.3329803445       -2.0578686174       -1.4293853529  \n";
      out_stream << "  H          1.2268731851       -0.2890815939        0.6461103092  \n";
      out_stream << "  N         -2.7512764519       -0.6187276911       -0.0029434697  \n";
      out_stream << "  C         -2.7658277192       -1.1777474096        1.3592249535  \n";
      out_stream << "  H         -1.7366237874       -1.3174400130        1.6739325008  \n";
      out_stream << "  H         -3.2497248448       -0.4667723859        2.0217821099  \n";
      out_stream << "  H         -3.3078184509       -2.1219323641        1.3622156144  \n";
      out_stream << "  C         -4.1005762783       -0.2680530314       -0.4687211517  \n";
      out_stream << "  H         -4.5339204723        0.4489638903        0.2216771081  \n";
      out_stream << "  H         -4.0221776674        0.1821765049       -1.4532312472  \n";
      out_stream << "  H         -4.7193190868       -1.1623532200       -0.5153261936  \n";
      out_stream << "  C         -2.0686910087       -1.5220929201       -0.9472196972  \n";
      out_stream << "  H         -1.9850283575       -1.0245260112       -1.9074887216  \n";
      out_stream << "  H         -1.0699465710       -1.7145165448       -0.5708154428  \n";
      out_stream << "  H         -2.6391932968       -2.4458900418       -1.0344573184  \n";

      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * 1sa2tma_412 optimised by r2-SCAN-3c
    ********************************************************************/
   std::filesystem::path Util::Print_r2_scan3c_1sa2tma_412_ToPath(const std::filesystem::path& arPath)
   {
      if (!std::filesystem::is_directory(arPath))
      {
         std::cerr << "The given path is not a directory. I will not create any files here! " << std::endl;
         exit(1);
      }
      std::fstream out_stream;
      std::filesystem::path file_path = arPath / "r2_scan3c_1sa2tma_412.xyz";
      out_stream.open(file_path, std::ios_base::out);
      out_stream << "33\n";
      out_stream << "1sa2tma_412\n";
      out_stream << "   S   0.02675699096597      1.30033681988508     -0.08729107035190  \n";
      out_stream << "   O   0.16091225911698      0.77928381587461     -1.46289843539784  \n";
      out_stream << "   O   0.42791769683314      0.09952522098893      0.91526078255427  \n";
      out_stream << "   O   0.76398385491195      2.51163383342274      0.26106990632589  \n";
      out_stream << "   O   -1.48445085350692      1.49534817345485      0.28815855904716 \n";
      out_stream << "   H   -2.05457529693355      0.54704012452437      0.11991770812008 \n";
      out_stream << "   N   2.72227162889225     -0.78384350043113      0.09718548997540  \n";
      out_stream << "   C   3.45076029820358      0.38965811213816     -0.41881743737226  \n";
      out_stream << "   H   2.87660305056443      0.83927070704750     -1.23342491944639  \n";
      out_stream << "   H   3.55172468286799      1.13430683383832      0.37616196826890  \n";
      out_stream << "   H   4.45173983414247      0.10886580957666     -0.78561547408106  \n";
      out_stream << "   C   3.44796756806112     -1.40867133159684      1.20994677153069  \n";
      out_stream << "   H   3.59439264857781     -0.67393926697428      2.00775605877485  \n";
      out_stream << "   H   2.86202176207831     -2.24325707762231      1.60870529623497  \n";
      out_stream << "   H   4.43391133698397     -1.78892892469779      0.89436735420258  \n";
      out_stream << "   C   2.45548096421720     -1.74778283510676     -0.98041682124510  \n";
      out_stream << "   H   1.89570291158950     -2.59885248922159     -0.57782789829606  \n";
      out_stream << "   H   1.84839059265151     -1.25949309118427     -1.74874954722151  \n";
      out_stream << "   H   3.38883437277041     -2.12305891922095     -1.43202474860736  \n";
      out_stream << "   H   1.37767957850855     -0.27869441051237      0.59299899029653  \n";
      out_stream << "   N   -2.81153279770782     -0.63898670358364      0.00006737843295 \n";
      out_stream << "   C   -2.83683168840462     -1.23736474994387      1.34606259241584 \n";
      out_stream << "   H   -1.80827304058514     -1.41569048224871      1.67213440383742 \n";
      out_stream << "   H   -3.30456989478512     -0.53765745031774      2.04511123416086 \n";
      out_stream << "   H   -3.39831400489376     -2.18462045018534      1.35469757265448 \n";
      out_stream << "   C   -4.16281203911958     -0.27745886799264     -0.45642179914602 \n";
      out_stream << "   H   -4.60919028734361      0.42848241437967      0.25048256426666 \n";
      out_stream << "   H   -4.09620822183938      0.20552038114629     -1.43572455785377 \n";
      out_stream << "   H   -4.81241657566450     -1.16300096817953     -0.53666986740309 \n";
      out_stream << "   C   -2.14310006731279     -1.53234092559196     -0.96412301930063 \n";
      out_stream << "   H   -2.06795842070240     -1.03038963555467     -1.93210715934367 \n";
      out_stream << "   H   -1.12831547712124     -1.73819942384861     -0.61334450630104 \n";
      out_stream << "   H   -2.69711336601670     -2.47761074226216     -1.07645736973180 \n";
 
      out_stream.close();

      return file_path;
   }

   /********************************************************************
    * Constructs 1sa (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1saMolecule()
   {
      std::string name = "1sa";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::S
         };
      std::vector<std::string> string_names =
         { "H"
         , "H"
         , "O"
         , "O"
         , "O"
         , "O"
         , "S"
         };

      Eigen::Matrix<double, 7, 3> coords 
         {  {  1.69972,  0.00293,  1.09470 }
         ,  { -1.69986, -0.00261,  1.09449 }
         ,  {  0.63493,  1.08590, -0.82663 }
         ,  { -0.63482, -1.08615, -0.82639 }
         ,  {  1.04534, -0.65999,  0.84555 }
         ,  { -1.04545,  0.66024,  0.84523 }
         ,  {  0.00001, -0.00002, -0.15571 }
         };

      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Constructs 1na (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1naMolecule()
   {
      std::string name = "1na";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::N
         };
      std::vector<std::string> string_names =
         {  "H"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "N"
         };
      Eigen::Matrix<double, 5, 3> coords 
         {  { -1.71307,  0.20350, -0.00011 }
         ,  { -1.11224, -0.55409,  0.00003 }
         ,  {  0.15698,  1.23712, -0.00008 }
         ,  {  1.04397, -0.73876, -0.00024 }
         ,  {  0.14334,  0.03463,  0.00036 }
         };

      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Constructs 1sa1na (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1sa1naMolecule()
   {
      std::string name = "1sa1na";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::S
         ,  Structure::AtomName::O
         ,  Structure::AtomName::N
         };
      std::vector<std::string> string_names =
         {  "H"
         ,  "H"
         ,  "H"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "S"
         ,  "O"
         ,  "N"
         };
      Eigen::Matrix<double, 12, 3> coords 
         {  { -0.04626, -1.16264, -0.44250}
         ,  {  2.62064, -0.76111,  1.55375}
         ,  { -1.03778,  1.10612, -0.04917}
         ,  {  2.86615,  0.23588, -0.71562}
         ,  {  0.64386,  1.17758, -0.07522}
         ,  {  0.91536, -1.18122, -0.65587}
         ,  {  1.80933, -0.24933,  1.45066}
         ,  { -2.02661,  1.17197, -0.03894}
         ,  { -1.70936, -1.00495, -0.00924}
         ,  {  1.60041,  0.09911, -0.08251}
         ,  { -3.69292, -0.17476,  0.06877}
         ,  { -2.51278, -0.08135,  0.00884}
         };

      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Constructs 1fa1tma (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1fa1tmaMolecule()
   {
      std::string name = "1fa1tma";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::N
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::C
         ,  Structure::AtomName::H
         };
      std::vector<std::string> string_names =
         {  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "C"
         ,  "C"
         ,  "C"
         ,  "N"
         ,  "O"
         ,  "O"
         ,  "C"
         ,  "H"
         };
      Eigen::Matrix<double, 18, 3> coords 
         {  {  0.45650,  0.76640, -0.00055 }
         ,  { -2.04316,  1.62723,  0.88326 }
         ,  { -2.04351,  1.62652, -0.88421 }
         ,  { -3.09720,  0.50304,  0.00018 }
         ,  { -0.17327, -1.45301,  1.19338 }
         ,  { -0.99098, -0.16046,  2.08285 }
         ,  { -1.95456, -1.40711,  1.26151 }
         ,  { -0.99169, -0.16203, -2.08279 }
         ,  { -0.17367, -1.45390, -1.19262 }
         ,  { -1.95498, -1.40806, -1.26018 }
         ,  { -2.11427,  0.99389, -0.00021 }
         ,  { -1.04270, -0.79792,  1.20077 }
         ,  { -1.04311, -0.79883, -1.20021 }
         ,  { -1.02614,  0.02822, -0.00003 }
         ,  {  2.14218, -0.90258,  0.00004 }
         ,  {  1.37566,  1.20888, -0.00037 }
         ,  {  2.31534,  0.29072, -0.00003 }
         ,  {  3.31523,  0.74631,  0.00011 }
         };

      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Constructs 1sa1dma (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1sa1dmaMolecule()
   {
      std::string name = "1sa1dma";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::S
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::H
         ,  Structure::AtomName::N
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         };
      std::vector<std::string> string_names =
         {  "H"
         ,  "S"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "H"
         ,  "N"
         ,  "H"
         ,  "C"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "C"
         ,  "H"
         ,  "H"
         ,  "H"
         };
      Eigen::Matrix<double, 17, 3> coords 
         {  {  1.20478, -0.03492,  0.78833 }
         ,  { -1.27848, -0.12984,  0.00613 }
         ,  { -1.97089,  1.33367,  0.02603 }
         ,  { -0.43473, -0.13470, -1.20686 }
         ,  { -2.36060, -1.08161, -0.00684 }
         ,  { -0.44197, -0.11641,  1.21825 }
         ,  { -2.88047,  1.21309, -0.26117 }
         ,  {  1.89635,  0.00026,  0.00289 }
         ,  {  1.18551, -0.04512, -0.76901 }
         ,  {  2.75456, -1.19255,  0.00119 }
         ,  {  3.39012, -1.18687,  0.88372 }
         ,  {  3.37130, -1.19652, -0.89455 }
         ,  {  2.11878, -2.07347,  0.01236 }
         ,  {  2.60286,  1.28887, -0.01305 }
         ,  {  3.23380,  1.37076,  0.86900 }
         ,  {  1.86363,  2.08520, -0.01108 }
         ,  {  3.21484,  1.35793, -0.90940 }
         };

      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Constructs 1sa1tma (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1sa1tmaMolecule()
   {
      std::string name = "1sa1tma";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::N
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::S
         };
      std::vector<std::string> string_names =
         {  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "C"
         ,  "C"
         ,  "C"
         ,  "N"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "O"
         ,  "S"
         };
      Eigen::Matrix<double, 20, 3> coords 
         {  { -0.87443, -0.00339,  0.48677 }
         ,  {  2.98841,  1.30926,  0.46631 }
         ,  { -2.68575, -0.66009,  1.87200 }
         ,  { -2.58367,  1.10063,  1.70462 }
         ,  { -3.83537,  0.19723,  0.81373 }
         ,  { -1.28407, -1.36111, -1.39467 }
         ,  { -1.95840, -2.08200,  0.07708 }
         ,  { -3.05311, -1.29964, -1.09394 }
         ,  { -1.71099,  2.06714, -0.31670 }
         ,  { -1.14969,  1.00903, -1.62261 }
         ,  { -2.90870,  1.21496, -1.32758 }
         ,  { -2.81195,  0.17328,  1.18602 }
         ,  { -2.06207, -1.27355, -0.64198 }
         ,  { -1.92074,  1.15658, -0.87200 }
         ,  { -1.87403,  0.01207,  0.06129 }
         ,  {  2.58605, -1.02800,  0.36289 }
         ,  {  0.86791, -0.28135, -1.26607 }
         ,  {  0.48025, -0.02199,  1.10719 }
         ,  {  2.16820,  1.35074, -0.03347 }
         ,  {  1.50771, -0.12935,  0.03184 }
         };
         
      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Constructs 1aa1a (Gunnar test set)
    ********************************************************************/
   Structure::Molecule Util::ConstructGunnar1aa1aMolecule()
   {
      std::string name = "1aa1a";

      std::vector<Structure::AtomName> names =
         {  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::N
         ,  Structure::AtomName::O
         ,  Structure::AtomName::O
         ,  Structure::AtomName::C
         ,  Structure::AtomName::C
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         ,  Structure::AtomName::H
         };
      std::vector<std::string> string_names = 
         {  "H"
         ,  "H"
         ,  "H"
         ,  "H"
         ,  "N"
         ,  "O"
         ,  "O"
         ,  "C"
         ,  "C"
         ,  "H"
         ,  "H"
         ,  "H"
         };
      Eigen::Matrix<double, 12, 3> coords 
         {  {  0.946660, -0.787860,  0.000240 }
         ,  {  2.176070,  0.880570, -0.000280 }
         ,  {  3.146290, -0.161490, -0.817200 }
         ,  {  3.146080, -0.160760,  0.817830 }
         ,  {  2.560690, -0.058350,  0.000200 }
         ,  { -0.022200, -1.017130, -0.000050 }
         ,  { -0.229420,  1.210320,  0.000000 }
         ,  { -0.727700,  0.107580, -0.000580 }
         ,  { -2.209180, -0.147080,  0.000130 }
         ,  { -2.479270, -0.732270,  0.878460 }
         ,  { -2.746550,  0.795530, -0.000440 }
         ,  { -2.479890, -0.733830, -0.876950 }
         };
         
      return Structure::Molecule(name, std::make_pair(names, string_names), coords, {});
   }

   /********************************************************************
    * Print elements of expected and unexpected vectors of pairs.
    ********************************************************************/
   void Util::PrintVectorOfPairs
      (  const std::vector<std::pair<int, int>>& arUnexpectedVec
      ,  const std::vector<std::pair<int, int>>& arExpectedVec
      ,  const std::string& arErrorMessage
      )
   {
      std::cerr << arErrorMessage << std::endl;
      if (arUnexpectedVec.size() != arExpectedVec.size())
      {
         std::stringstream ss;
         ss << "Wrong sizes for input vectors in Test::Util::PrintVectorOfPairs\n"
            << "Unexpected size: " << arUnexpectedVec.size() << "\n"
            << "Expected size:   " << arExpectedVec.size() << "\n"
            << std::endl;
         std::cerr << ss.str() << std::endl;
      }

      for (Uin i = 0; i < arUnexpectedVec.size(); i++)
      {
         std::stringstream ss;
         ss << "Index: " << i << "\n"
            << "Expected: first = " << arExpectedVec[i].first << ", second = " << arExpectedVec[i].second << "\n"
            << "Unexpected: first = " << arUnexpectedVec[i].first << ", second = " << arUnexpectedVec[i].second << "\n"
            << std::endl;
         std::cerr << ss.str();
      }
   }
}