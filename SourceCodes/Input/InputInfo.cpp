/********************************************************************
 * Class definition for object containing information from inputfile.
 * Members are simply "CalcDef's" for the different modules.
 ********************************************************************/

#include "Input/InputInfo.hpp"
#include "MathAndUtil/Util.hpp"
#include "Input/ReadFiles.hpp"

/********************************************************************
 * Constructor
 ********************************************************************/
Input::InputInfo::InputInfo
   (  const std::filesystem::path& arPathToInputFile
   )
{
   enum INPUT
      {  CLUSTERCONFORMATIONS
      };

   const std::map<std::string, INPUT> input_word = 
      {  {"#1CLUSTERCONFORMATIONS", CLUSTERCONFORMATIONS}
      };
   
   std::string s;
   bool already_read = false;

   std::fstream inputfile(arPathToInputFile);

   // input enum
   INPUT input;

   // Loop over input file.
   while (  already_read
         || Util::GetLine(inputfile, s)
         )
   {
      std::string s_parsed = Input::ParseInputKeyword(s);
      try
      {
         input = input_word.at(s_parsed);
      }
      catch(const std::out_of_range&)
      {
         std::cout << "In ReadInputFile: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
         exit(1);
      }

      switch(input)
      {
         case CLUSTERCONFORMATIONS:
         {
            InitializeClusterSets(inputfile, s);
            already_read = Input::CheckIfKeyword(s);
            break;
         }
      }
   }
}

/********************************************************************
 * Read stuff for cluster set
 ********************************************************************/
void Input::InputInfo::InitializeClusterSets
   (  std::fstream& arInputFileStream
   ,  std::string& s
   )
{
   // Setup path to output dir
   std::filesystem::path path_to_data_output_dir = std::filesystem::current_path() / "OutputDataDir";

   enum INPUT
   {  COMPARECLUSTERSACROSSMETHODS
   ,  FILTERUNIQUECLUSTERS
   ,  CHECKSOLVATION
   ,  COORDINATESCAN1D
   ,  EQDISTANCES
   ,  OUTPUTDATADIR
   };

   const std::map<std::string, INPUT> input_word = 
      {  {"#2COMPARECLUSTERSACROSSMETHODS", COMPARECLUSTERSACROSSMETHODS}
      ,  {"#2FILTERUNIQUECLUSTERS", FILTERUNIQUECLUSTERS}
      ,  {"#2CHECKSOLVATION", CHECKSOLVATION}
      ,  {"#2COORDINATESCAN1D", COORDINATESCAN1D}
      ,  {"#2EQDISTANCES", EQDISTANCES}
      ,  {"#2OUTPUTDATADIR", OUTPUTDATADIR}
      };

   bool already_read = false;

   int input_level = 2;

   // input enum
   INPUT input;

   // Loop over input file.
   while (  ( already_read || Util::GetLine(arInputFileStream, s) )
         && !Input::IsLowerLevelKeyword(s, input_level)
         )
   {
      std::string s_parsed = Input::ParseInputKeyword(s);
      try
      {
         input = input_word.at(s_parsed);
      }
      catch(const std::out_of_range&)
      {
         std::cout << "In ReadInputFile: Caught out of range when reading input. Bad string = " << s_parsed << std::endl;
         exit(1);
      }

      switch(input)
      {
         case COMPARECLUSTERSACROSSMETHODS:
         {
            mDoCheckConformersAcrossMethods = true;
            already_read = mClusterCompareClusterSet.ReadClusterCompare(arInputFileStream, s);
            break;
         }
         case FILTERUNIQUECLUSTERS:
         {
            mDoFilterUniqueClusters = true;
            already_read = mFilterUniqueClusters.ReadFilterUniqueClusters(arInputFileStream, s);
            mFilterUniqueClusters.SetKabschInputOptions(Structure::KabschInputOptions::COMPARE_MONOMER_ROT_MATS_ACROSS);
            break;
         }
         case CHECKSOLVATION:
         {
            mDoCheckSolvation = true;
            already_read = mCheckSolvationClusters.ReadCheckSolvation(arInputFileStream, s);
            break;
         }
         case COORDINATESCAN1D:
         {
            already_read = mCoordinateScan1DClusterSet.ReadClusterCompare(arInputFileStream, s);
            mDoCoordinateScan1D = true;
            break;
         }
         case EQDISTANCES:
         {
            already_read = mEqDistancesClusterSet.ReadClusterCompare(arInputFileStream, s);
            mDoEqDistances = true;
            break;
         }
         case OUTPUTDATADIR:
         {
            std::cerr << "Implement ability to set the output dir where you want!" << std::endl;
            exit(1);
            break;
         }
         default:
         {
            std::cerr << "UNKOWN INPUT: " << s_parsed << std::endl;
            exit(1);
            break;
         }
      }
   }

   // Sanity checks !
   if (mDoCheckConformersAcrossMethods && mDoFilterUniqueClusters && mDoCoordinateScan1D && mDoEqDistances)
   {
      std::cerr << "I can only do one thing at a time. Choose either ClusterCompare OR CoordinateScan1D" << std::endl;
   }

   if (mDoCheckConformersAcrossMethods)
   {
      mClusterCompareClusterSet.SetOutputDataDir(path_to_data_output_dir);
      bool use_cluster_idx = true;
      mClusterCompareClusterSet.InitializeClusterCompareStructures(use_cluster_idx);
      
      //! Do sanity check
      mClusterCompareClusterSet.SanityCheckCompareClustersAcrossMethods();
   }
   else if (mDoFilterUniqueClusters)
   {
      mFilterUniqueClusters.SetOutputDataDir(path_to_data_output_dir);
      bool use_cluster_idx = false;
      mFilterUniqueClusters.InitializeClusterCompareStructures(use_cluster_idx);
   }
   else if (mDoCheckSolvation)
   {
      mCheckSolvationClusters.SetOutputDataDir(path_to_data_output_dir);
      bool use_cluster_idx = true;
      mCheckSolvationClusters.InitializeClusterCompareStructures(use_cluster_idx);
   }
   else if (mDoCoordinateScan1D)
   {
      mCoordinateScan1DClusterSet.SetOutputDataDir(path_to_data_output_dir);
      mCoordinateScan1DClusterSet.InitializeCoordinateScan();
   }
   else if (mDoEqDistances)
   {
      mEqDistancesClusterSet.SetOutputDataDir(path_to_data_output_dir);
      bool use_cluster_idx = true;
      mEqDistancesClusterSet.InitializeClusterCompareStructures(use_cluster_idx);
   }
   else
   {
      std::cerr << "You have not told me to do anything under #1 ClusterConformations... I stop !" << std::endl;
      exit(1);
   }

}