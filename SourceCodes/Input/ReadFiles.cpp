/********************************************************************
 * This file contains functionalities to read files.
 ********************************************************************/

#include<fstream>
#include<sstream>
#include<regex>

#include "CommonAliases.hpp"

#include "MathAndUtil/Util.hpp"
#include "Structure/StructureEnums.hpp"
#include "MathAndUtil/Kabsch.hpp"      // Needs this for RmsdData.
#include "Input/InputInfo.hpp"
#include "Input/ReadFiles.hpp"
#include "Structure/Molecule.hpp"    // Should come before Cluster.hpp as Cluster class contains Molecule class.
#include "Structure/Cluster.hpp"     // Include Cluster.hpp so that ClusterSet knows the definition.
#include "Structure/ClusterSet.hpp"

namespace Input
{
   /********************************************************************
    * Parse string from input file i.e. remove all white space and 
    * convert the string to uppercase.
    ********************************************************************/
   std::string ParseInputKeyword(const std::string& arString)
   {
      return Util::ConvertToUpperCase(Util::RemoveWhiteSpace(arString));
   }

   /********************************************************************
    * Simply checks if input string starts with #<integer>, if it does 
    * then we have found a keyword.
    ********************************************************************/
   bool CheckIfKeyword(const std::string& arString)
   {
      // Remove all whitespace so regex check works.
      std::string s = Util::RemoveWhiteSpace(arString);

      // Regex for "#<integer><string>" with at least 1 integer and at least 1 character.
      std::regex keyword_regex("#[0-9]+[a-zA-Z]+");

      // See if "s" starts with above regex.
      return std::regex_match(s, keyword_regex);
   }

   /********************************************************************
    * Checks if given string is a keyword of lower input level than the
    * current level.
    * 
    * returns for arString containing #<input level>
    *    -  true if "<input level>" < aCurrentInputLevel
    *    -  false if "<input level>" == || > aCurrentInputLevel
    ********************************************************************/
   bool IsLowerLevelKeyword(const std::string& arString, const int aCurrentInputLevel)
   {
      // If keyword we perform check
      if (CheckIfKeyword(arString))
      {
         // String should be trimmed but we parse it again just to be sure.
         std::string s_parsed = Input::ParseInputKeyword(arString);

         // Regex for "#<integer>" with at least 1 integer.
         std::regex keyword_regex("#[0-9]+");
         
         // 
         std::smatch keyword_match;

         //
         std::regex_search(s_parsed, keyword_match, keyword_regex);

         // Get input level as substring of entire match (.str(0)), coutned from first integer (i.e. after #)
         int input_level = Util::NumFromString<int>(keyword_match.str(0).substr(1, std::string::npos));

         return input_level < aCurrentInputLevel;
      }
      else // We just return false. It is not lower level keyword if it is not a keyword.
      {
         return false;
      }
   }

   /********************************************************************
    * Reads a xyz file and returns it as a Molecule object.
    * 
    * @param[in] arInputxyz
    *    Full path to input xyz file
    * @param[in] arClusterNames
    *    Set of string defining which clusters are present (should be
    *    taken from ClusterSet)
    ********************************************************************/
   Structure::Molecule ReadXYZAsMolecule
      (  const std::filesystem::path& arInputxyz
      ,  const std::string& arStructureName
      )
   {
      if (arStructureName.empty())
      {
         std::cout << "Empty structure name !" << std::endl;
         exit(1);
      }
      if (!std::filesystem::is_regular_file(arInputxyz))
      {
         std::cerr << "Input path for ReadXYZAsMolecule is not a regular file !!\n"
            << "arInputxyz: " << arInputxyz.string()
            << std::endl;
         exit(1);
      }
      if (std::filesystem::is_empty(arInputxyz))
      {
         std::cerr << "The following filename refers to an empty file. It exists, but contain no data !\n"
            << arInputxyz << "\n"
            << "I will return an empty Molecule"
            << std::endl;
         //exit(1);
         return Structure::Molecule();
      }

      // Initialize objects for reading inputfile
      std::fstream inputfile(arInputxyz);
      std::string s;

      // First line in xyz file is number of atoms
      Util::GetLine(inputfile, s);
      Uin num_atoms = Util::NumFromString<Uin>(s);

      // Second line in xyz file is random comment. We throw it away.
      bool no_recursive = true; // Make sure we only read ONE line even if it is empty.
      Util::GetLine(inputfile, s, no_recursive);

      // Initialize set of atoms for construction of Molecule object.
      std::pair<std::vector<Structure::AtomName>, std::vector<std::string>> atom_names_pair;
      std::vector<Structure::AtomName>& atom_names = atom_names_pair.first;
      std::vector<std::string>& atom_string_names = atom_names_pair.second;
      atom_names.reserve(num_atoms);
      atom_string_names.reserve(num_atoms);
      Eigen::MatrixX3d atom_positions = Eigen::MatrixX3d(num_atoms, 3);

      // GetLine trims the string, i.e. no whitespace at beginning or end.
      while (Util::GetLine(inputfile, s))
      {
         // Get Atom from string, will write directly to atoms.first and atoms.second
         Input::AtomFromString(s, atom_names, atom_string_names, atom_positions);
      }

      // Remember to close fstream
      inputfile.close();

      if (  atom_names.size() != num_atoms
         || atom_string_names.size() != num_atoms
         || atom_names.size() != Uin(atom_positions.rows())
         )
      {
         std::stringstream ss;
         ss << "Wrong number of atoms found in inputfile !!\n"
            << "inputfile: " << arInputxyz << "\n"
            << "num_atoms: " << num_atoms << "\n"
            << "atom_names.size(): " << atom_names.size() << "\n"
            << "atom_string_names.size(): " << atom_string_names.size() << "\n"
            << "atom_positions.rows(): " << atom_positions.rows() << "\n"
            << std::endl;

         std::cout << ss.str();

         exit(1);
      }

      // Construct Molecule and return it. Use empty cluster idx vector as this is most likely a cluster.
      return Structure::Molecule(arStructureName, atom_names_pair, atom_positions, {});
   }


   /********************************************************************
    * Assumes string is of the same form as in a standard xyz file
    * i.e. "<Atom name> <X-coord> <Y-coord> <Z-coord>"
    * 
    * Writes AtomName and coordinates directly to containers allocated
    * outside this function.
    * 
    * @param[in] arString
    *    String to read atom details from
    * @param[in, out] arNameVec
    *    Reference to vector of pairs as (AtomName, AtomNameString)
    * @param[in, out] arCoordinateMatrix
    *    Reference to coordinate matrix 
    ********************************************************************/
   void AtomFromString
      (  const std::string& arString
      ,  std::vector<Structure::AtomName>& arAtomNames
      ,  std::vector<std::string>& arAtomStringNames
      ,  Eigen::MatrixX3d& arCoordinateMatrix
      )
   {
      std::size_t atom_name_idx(0);

      // Figure out when the atom name ends.
      std::string::const_iterator it = arString.begin();
      while 
         (  *it != ' ' 
         && *it != '\t'
         && it != arString.end()
         )
      {
         atom_name_idx++;
         it++;
      }

      // Extract the atom name from the string.
      std::string atom_name = arString.substr(0, atom_name_idx);
      arAtomNames.emplace_back(Structure::GetEnumFromString<Structure::AtomName>(atom_name));
      arAtomStringNames.emplace_back(atom_name);

      // Get the substring containing the coords.
      const std::size_t strRange = arString.size() - atom_name_idx + 1;
      std::string atom_coords_string = arString.substr(atom_name_idx + 1, strRange);
      atom_coords_string = Util::Trim(atom_coords_string);

      // Create array from string containing the coords and assign directly to matrix.
      arCoordinateMatrix.row(arAtomNames.size() - 1) = Util::RowVectorFromString<double, 3>(atom_coords_string);
   }
}