/********************************************************************
 * This file contains declarations for all Tests.
 ********************************************************************/

#ifndef TESTS_HPP
#define TESTS_HPP

#include<filesystem>

#include "MathAndUtil/Math.hpp"
#include "Structure/Molecule.hpp"

// Forward declarations
namespace Structure
{
   class Cluster;
}

namespace Test
{
   // Useful Aliases

   //! Top level function, this is what is called when testing is done.
   void RunTests();

   //! Test SVD - making sure you understand Eigen SVD.
   void RunSVDTest(const bool aDebug);

   //! Run small math tests
   void RunMathTests(const bool aDebug);

   //! Run small util tests
   void RunUtilTests(const bool aDebug);

   //! Test Centroid functionallity.
   void RunCentroidTest(const bool aDebug);

   //! Kabsch utility functions
   void RunKabschUtilTest(const bool aDebug);

   //! Test Kabsch algorithm.
   void RunKabschTest(const bool aDebug);
   void RunMoleculeKabschTest(const bool aDebug);

   //! Test the function GetRotatedStructure.
   void RunGetRotatedStructureTest(const bool aDebug);

   //! Tests the Dipolemoment class
   void RunDipoleMomentTest(const bool aDebug);

   //! Tests the CoulombMatrix class
   void RunCoulombMatrixTest(const bool aDebug);

   //! Test all the different RMSD schemes.
   void RunRMSDTest(const bool aDebug);

   //! Test ConvexHull
   void RunPolytopeTest(const bool aDebug);

   //! Test ConvexHull
   void RunConvexHullTest(const bool aDebug);

   //!@{
   //! Tests for the Molecule class
   void RunMoleculeTests(const bool aDebug);
   void RunMoleculeAtomConnectivityMapTests(const bool aDebug);
   //!@}

   //!@{ 
   //! Test to run all tests for the Cluster class
   void RunClusterTests(const bool aDebug);
   //! Checks that the Cluster objects is constructed correctly i.e. the correct Molecules are found.
   void RunClusterConstructionTest(const bool aDebug);
   //!@}

   //!@{ Checks a "normal" run through of the code for specific clusters.
   void Run1am2nta115Test(const bool aDebug);
   void Run1eda1ma1nta252Test(const bool aDebug);
   void Run1fa1msa1tma324Test(const bool aDebug);
   void Run1am1eda1fa22Test(const bool aDebug);
   void Run2tma100Test(const bool aDebug);
   void Run2eda1nta1sa590Test(const bool aDebug);
   void Run1sa2tma412Test(const bool aDebug);
   //!@}
   
   //! Tests intended path through code.
   void RunFullClusterConformationsTest(const bool aDebug);

   //! Helper function to check if RMSD value is below threshold
   void check_rmsd(const double arRmsd, const double arThr, const std::string& arErrMessage = "");

   //!@{ Test distributions
   template<std::floating_point T>
   void RunNormalDistributionTest(const bool aDebug);
   template<std::floating_point T>
   void RunCauchyDistributionTest(const bool aDebug);
   //!@}
   
   //! Test the LBFGS++ library (/figure out how it works) 
   void RunLBFGSppTest(const bool aDebug);

   //! Test MaximumLoglikelihood class for fitting a sum of gaussians to some data.
   void RunMaximumLogLikelihoodOptimiserNormalDistributionTest(const bool aDebug);
   void RunMaximumLogLikelihoodOptimiserCauchyDistributionTest(const bool aDebug);

   //! Test the monte carlo integration
   void RunMonteCarloIntegrationTest(const bool aDebug);
   void RunQuadratureIntegrationTest(const bool aDebug);

   //! Test the computation of bayes factors for simple distributions.
   void RunAndersonDarlingNormalDistributionTest(const bool aDebug);
   void RunAndersonDarlingCauchyDistributionTest(const bool aDebug);

   //! Test GetSign function
   template<typename T>
   void RunGetSignTest()
   {
      T a = -4;
      T b = 0;
      T c = 4;

      if (Math::GetSign(a) != -1)
      {
         std::cout << "Expected sign of \"-4\" to be \"-1\" but it is: " << Math::GetSign(a) << std::endl;
         exit(1);
      }
      if (Math::GetSign(b) != 0)
      {
         std::cout << "Expected sign of \"0\" to be \"0\" but it is: " << Math::GetSign(b) << std::endl;
         exit(1);
      }
      if (Math::GetSign(c) != 1)
      {
         std::cout << "Expected sign of \"4\" to be \"1\" but it is: " << Math::GetSign(c) << std::endl;
         exit(1);
      }
   }

   namespace Util
   {
      //!@{ Print structures based on CCSD/aug-cc-pVTZ to given path
      std::filesystem::path PrintCCSD1aToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintCCSD1edaToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintCCSD1maToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintCCSD1a1eda_01ToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintCCSD1eda1a_01ToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintCCSD1a1ma_01ToPath(const std::filesystem::path& arPath);
      //!@}

      //!@{ Print structures based on RI-MP2/aug-cc-pVQZ to given path
      std::filesystem::path PrintMP21aToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintMP21edaToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintMP21maToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintMP21a1eda_01ToPath(const std::filesystem::path& arPath);
      std::filesystem::path PrintMP21a1ma_01ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_1fa1msa1tma_324_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_1am1eda1fa_22_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_2tma_100_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_2eda1nta1sa_590_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_1sa2tma_412_ToPath(const std::filesystem::path& arPath);
      //!@}

      //!@{ Print structures based on r2-scan-3c to given path
      std::filesystem::path Print_r2_scan3c_1fa1msa1tma_324_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_r2_scan3c_1am1eda1fa_22_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_r2_scan3c_2tma_100_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_r2_scan3c_2eda1nta1sa_590_ToPath(const std::filesystem::path& arPath);
      std::filesystem::path Print_r2_scan3c_1sa2tma_412_ToPath(const std::filesystem::path& arPath);
      //!@}

      //!@{ Create Molecule objects from CCSD/aug-cc-pVTZ
      Structure::Molecule ConstructCCSD1edaMolecule();
      Structure::Molecule ConstructCCSD1a1edaMolecule();
      Structure::Molecule ConstructGunnar1saMolecule();
      Structure::Molecule ConstructGunnar1naMolecule();
      Structure::Molecule ConstructGunnar1sa1naMolecule();
      Structure::Molecule ConstructGunnar1fa1tmaMolecule();
      Structure::Molecule ConstructGunnar1sa1dmaMolecule();
      Structure::Molecule ConstructGunnar1sa1tmaMolecule();
      Structure::Molecule ConstructGunnar1aa1aMolecule();

      std::filesystem::path PrintGunnar1fa1tmaToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintGunnar1sa1dmaToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintGunnar1aa1dmaToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintGunnar1msa1aaToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintGunnar1na1tmaToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintGunnar1na1edaToDir(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_aug_pvqz_1am2nta_ToDir(const std::filesystem::path& arPath);
      std::filesystem::path Print_r2_scan3c_1am2nta_ToDir(const std::filesystem::path& arPath);
      std::filesystem::path Print_dfmp2_aug_pvqz_1eda1ma1nta_ToDir(const std::filesystem::path& arPath);
      std::filesystem::path Print_r2_scan3c_1eda1ma1nta_ToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintHaide15sa15dmaToDir(const std::filesystem::path& arPath);
      std::filesystem::path PrintPM71am1dma1fa1msa_0_ToDir(const std::filesystem::path& arPath);
      //!@}

      //! Helper for checking connectivity map
      template<typename T>
      void check_connectivity_map
         (  const std::map<T, std::vector<T>>& arFoundMap
         ,  const std::map<T, std::vector<T>>& arExpMap
         ,  const std::string& arErrorMessage
         );

      //! Helper for checking the cluster molecule and monomers
      void check_cluster_molecule_and_monomers
         (  const Structure::Cluster& arFoundCluster
         ,  const Structure::Molecule& arExpectedClusterMolecule
         ,  const std::vector<Structure::Molecule>& arExpectedMonomers
         );

      //! Prints elements of two std::vector<std::pair<int, int>>
      void PrintVectorOfPairs
         (  const std::vector<std::pair<int, int>>& arUnexpectedVec
         ,  const std::vector<std::pair<int, int>>& arExpectedVec
         ,  const std::string& arErrorMessage = ""
         );
   
      //! Generate a set of normal distributed data around mean and std deviation
      template<std::floating_point value_type, typename DIST>
      std::vector<value_type> GetRandomDistributedData
         (  const value_type aA
         ,  const value_type aB
         ,  const int aSize
         );
   }
}

/********************************************************************
 * Helper to check if ChemicalGroups are correct
 ********************************************************************/
template<typename T>
void Test::Util::check_connectivity_map
   (  const std::map<T, std::vector<T>>& arFoundMap
   ,  const std::map<T, std::vector<T>>& arExpMap
   ,  const std::string& arErrorMessage
   )
{
   bool stop = false;
   if (arFoundMap.size() != arExpMap.size())
   {
      std::cerr << "Mismatch in connectivity map sizes" << std::endl;
      std::cerr << "arFoundMap.size() = " << arFoundMap.size() << std::endl;
      std::cerr << "arExpMap.size() = " << arExpMap.size() << std::endl;
      stop = true;
   }
   for   (  auto it_found = arFoundMap.cbegin(), it_exp = arExpMap.cbegin()
         ;  it_found != arFoundMap.end() && it_exp != arExpMap.end()
         ;  it_found++, it_exp++
         )
   {
      if (it_found->first != it_exp->first)
      {
         std::cerr << "Maps are not ordered in same way\n"
            << "it_found->first: " << it_found->first
            << "it_exp->first: " << it_exp->first
            << std::endl;
         stop = true;
      }
      if (it_found->second.size() != it_exp->second.size())
      {
         std::cerr << "I did not find the expected number of connections\n"
            << "it_found->second.size() = " << it_found->second.size() << "\n"
            << "it_exp->second.size() = " << it_exp->second.size() << "\n"
            << std::endl;
         stop = true;
      }
      for   (  auto it_found_vec = it_found->second.begin(), it_exp_vec = it_exp->second.begin()
            ;  it_found_vec != it_found->second.end() && it_exp_vec != it_exp->second.end()
            ;  it_found_vec++, it_exp_vec++
            )
      {
         if (*it_found_vec != *it_exp_vec)
         {
            std::cerr << "Value: " << it_found->first 
               << " Does not have the expected connections:\n\n"
               << "Found connection: " << *it_found_vec << "\n"
               << "expected connection: " << *it_exp_vec << "\n"
               << std::endl;

            stop = true;
         }
      }
   }

   if (stop)
   {
      std::cerr << "Called check_connectivity_map from " << arErrorMessage << std::endl;
      exit(1);
   }
}


#endif /* TESTS_HPP */