/********************************************************************
 * Header file to include Eigen but disable certain compiler warnings
 ********************************************************************/

_Pragma("GCC diagnostic push")

_Pragma("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")

#include<Eigen/Dense>

_Pragma("GCC diagnostic pop")