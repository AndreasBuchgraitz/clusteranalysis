#ifndef EXCEPTIONS_HPP
#define EXCEPTOINS_HPP

#include<string>
#include<exception>

class NeedClusterInformation : public std::exception
{
   private:
      std::string message;

   public:
      NeedClusterInformation
         (  std::string apMessage
         )
         :  message(apMessage)
      {
      }

      std::string what()
      {
         return message;
      }
};

#endif /* EXCEPTIONS_HPP */