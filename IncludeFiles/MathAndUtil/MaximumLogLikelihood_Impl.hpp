/********************************************************************
 *
 ********************************************************************/

#ifndef MAXIMUMLOGLIKELIHOOD_IMPL_HPP
#define MAXIMUMLOGLIKELIHOOD_IMPL_HPP

#include<stdexcept>
#include<cmath>
#include<numbers>
#include<numeric>
#include<algorithm>

#include "MathAndUtil/NormalDistribution.hpp"
#include "MathAndUtil/NumericalIntegration.hpp"

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   MaximumLogLikelihood<CONT, DIST>::MaximumLogLikelihood
      (  const CONT arData
      ,  const int aNumDistributions
      )
      :  mData(arData)
      ,  mDistribution(mData, aNumDistributions) // Only contains const ref to mData, so when mData is sorted below so is the const ref
   {
      // Sort the data just to be sure.
      std::sort(mData.begin(), mData.end());
   }

   /********************************************************************
    * Operator() overload used in LBFGSB++ interface. Besides computing
    * the nessecary stuff for the library also updates some member
    * variables to be used after optimisation.
    * 
    * Returns the negative gradient and function value, as we want to
    * maximise, but LBFGSB++ can only minimise.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   MaximumLogLikelihood<CONT, DIST>::value_type MaximumLogLikelihood<CONT, DIST>::operator()
      (  const vec_type& arParams
      ,  vec_type& arGradient
      )
   {
      // Check if any std deviation is zero
      for (int i = 0; i < arParams.size(); i += 2)
      {
         if (arParams(i + 1) == 0)
         {
            throw(std::range_error("operator() in MaximumLogLikelihood received std deviation of 0 !"));
         }
      }

      mDistribution.SetParams(arParams);

      mDistribution.ComputeLogLikelihoodGradient(arParams, arGradient);
      arGradient *= -1;

      return -1.0 * mDistribution.ComputeLogLikelihood(arParams);
   }

   /********************************************************************
    * Wraps the optimised parameters from one long vector into vector
    * of pairs, to indicate the paried nature of mean and variance.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   void MaximumLogLikelihood<CONT, DIST>::SetParams(const vec_type& arParams)
   {
      // Set parameter member of mDistributions
      mDistribution.SetParams(arParams);

      // Wrap mDistributions.mParams in a nicer way.
      this->SetOptimisedDistributions();
   }
   
   /********************************************************************
    * Wraps the optimised parameters from one long vector into vector
    * of pairs, to indicate the paried nature of mean and variance.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   void MaximumLogLikelihood<CONT, DIST>::SetOptimisedDistributions()
   {
      // Emplace all the means and std devis into the vector<pair<>> member.
      for (int i = 0; i < this->mDistribution.NumParams(); i += 2)
      {
         //this->mOptimisedDistributions.emplace_back(arParams(i), arParams(i + 1), arParams(i + 2));
         this->mOptimisedDistributions.emplace_back(this->mDistribution.GetParams()(i), this->mDistribution.GetParams()(i + 1));
      }

      // Sort the optimised distributions based on the mean values.
      std::sort
         (  this->mOptimisedDistributions.begin(), this->mOptimisedDistributions.end()
         ,  []( const std::tuple<value_type, value_type>& aTup1
              , const std::tuple<value_type, value_type>& aTup2
              )
            {
               return std::get<0>(aTup1) < std::get<0>(aTup2);
            }
         );
   }

   /********************************************************************
    * Checks if all the means are smaller than a given threshold.
    * Assumes sorted by means so simply checks the last (largest) one.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   bool MaximumLogLikelihood<CONT, DIST>::CheckMeansSmallerThanThreshold(const value_type aThr) const
   {
      return std::get<0>(this->mOptimisedDistributions.back()) < aThr;
   }

   /********************************************************************
    * Checks if all the means are larger than a given threshold.
    * Assumes sorted by means so simply checks the first (smallest) one.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   bool MaximumLogLikelihood<CONT, DIST>::CheckMeansLargerThanThreshold(const value_type aThr) const
   {
      return std::get<0>(this->mOptimisedDistributions.front()) > aThr;
   }

   /********************************************************************
    * Computes the empirical Cumultative Distribution Function.
    * Assumes mData is ordered (should be in c-tor).
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   MaximumLogLikelihood<CONT, DIST>::value_type MaximumLogLikelihood<CONT, DIST>::EmpiricalCDF(const value_type aVal) const
   {
      // Lambda to check if a given value is smaller than aVal
      auto smaller_than_val = [&aVal](const value_type val) { return val < aVal; };

      // Find the point at which values in mData stops being smaller than aVal
      auto it = std::partition_point(mData.begin(), mData.end(), smaller_than_val);

      // Use std::distance as a counter of element from begin to it.
      return std::distance(mData.begin(), it) / value_type(mData.size());
   }

   /********************************************************************
    * Computes the Kolmogorov-Smirnov statistic:
    *    D_n = max{|F_n(x) - F(x)|}
    * 
    * Assumes sum of normal distributions.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename MaximumLogLikelihood<CONT, DIST>::value_type> MaximumLogLikelihood<CONT, DIST>::ComputeKolmogorovSmirnov() const
   {
      std::vector<value_type> res_vec;

      for (const value_type elem : mData)
      {
         res_vec.emplace_back(std::abs(EmpiricalCDF(elem) - mDistribution.TheoreticalCDF(elem)));
      }

      std::sort(res_vec.begin(), res_vec.end());

      return res_vec;
   }

   /********************************************************************
    * Computes the Anderson-Darling statistic:
    *    A_n^2 = n*int[-inf, inf] (F_n(x) - F(x))^2 / (F(x)(1 - F(x))) dF(x)
    * 
    * Assumes sum of normal distributions.
    * 
    * THIS METHOD IS IN PRINCIPLE CORRECT, BUT NUMERICALLY TROUBLESOME 
    * IF THE DISTRIBUTION IS VERY NARROW, AS THE QUADRATURE MIGHT NOT
    * EVALUATE THE FUNCTION IN A POINT THAT IS NON-ZERO...
    * USE ComputeAndersonDarlingBySummation INSTEAD !
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   typename MaximumLogLikelihood<CONT, DIST>::value_type MaximumLogLikelihood<CONT, DIST>::ComputeAndersonDarlingByIntegral() const
   {
      // Lambda for integrand used in Monte-Carlo
      auto integrand = [this](const value_type& aVal)
      {
         value_type theo_cdf = mDistribution.TheoreticalCDF(aVal);
         value_type numerator = std::pow(EmpiricalCDF(aVal) - theo_cdf, 2);
         value_type denominator = theo_cdf * (1 - theo_cdf);

         // Computes enture probability density function i.e. it includes normalisation
         value_type exp_val = mDistribution.EvaluateProbDensFunc(aVal, mDistribution.GetParams());

         if (denominator == 0)
         {
            return 0.0;
         }
         else
         {
            return numerator * exp_val / denominator;
         }
      };

      value_type lower_limit = -std::numeric_limits<value_type>::infinity();
      value_type upper_limit = +std::numeric_limits<value_type>::infinity();

      Math::QuadratureIntegration anderson_darling_integral(integrand, 1.0E-8, lower_limit, upper_limit);
      value_type integral = anderson_darling_integral.GetIntegral();

      value_type result = integral * mData.size();

      return result;
   }

   /********************************************************************
    * Computes the Anderson-Darling statistic:
    *    A_n^2 = -n - 1/n \sum[i=1:n] (2i-1)(log(F(x_i)) + log(1 - F(x_{n+1-i})))
    * 
    * Assumes sum of normal distributions.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   typename MaximumLogLikelihood<CONT, DIST>::value_type MaximumLogLikelihood<CONT, DIST>::ComputeAndersonDarlingBySummation() const
   {
      value_type S(0);
      
      // Iterator to first and last element to be incremented and decremented in each loop iteration
      for (Uin i = 0; i < mData.size(); i++)
      {
         value_type theo_val_1 = mDistribution.TheoreticalCDF(mData[i]);
         value_type theo_val_2 = mDistribution.TheoreticalCDF(mData[mData.size() - 1 - i]);
         S += (2*(i+1) - 1) * (std::log(theo_val_1) + std::log(1 - theo_val_2));
      }

      S /= value_type(mData.size());
      
      return -value_type(mData.size()) - S;
   }
}

#endif /* MAXIMUMLOGLIKELIHOOD_IMPL_HPP */