/********************************************************************
 *
 ********************************************************************/

#ifndef QUADRATUREINTEGRATION_IMPL_HPP
#define QUADRATUREINTEGRATION_IMPL_HPP

#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Constructor
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   QuadratureIntegration<FUNC, VALUE_TYPE>::QuadratureIntegration
      (  FUNC aFunc
      ,  const value_type aThr
      ,  const value_type aLowerLimit
      ,  const value_type aUpperLimit
      )
      :  mFunc(aFunc)
      ,  mRelThr(aThr)
      ,  mLowerLimit(aLowerLimit)
      ,  mUpperLimit(aUpperLimit)
   {
      // Make sure we have properly transformed possible infinite integration limits.
      // Transformations are taken from: https://en.wikipedia.org/wiki/Numerical_integration#Integrals_over_infinite_intervals
      if (  mLowerLimit == -std::numeric_limits<value_type>::infinity()
         && mUpperLimit == +std::numeric_limits<value_type>::infinity()
         )
      {
         // If both limits are (+/-)infinity: [-inf, inf] -> [-1, +1]
         // And f(x)dx -> f(t / (1-t^2)) * (1 + t^2) / (1 - t^2)^2 dt
         mLowerLimit = -1;
         mUpperLimit = +1;

         mTransformedLimits = detail::IntegralLimitTransformation::DoubleInf;
      }
      else if  (  mLowerLimit == -std::numeric_limits<value_type>::infinity()
               && mUpperLimit != +std::numeric_limits<value_type>::infinity()
               )
      {
         // If only lower limit is -inf: [-inf, a] -> [0, 1]
         // And f(x)dx -> f(a - (1-t)/t) 1/t^2 dt
         mLowerLimit = 0;
         mUpperLimit = 1;

         mTransformedLimits = detail::IntegralLimitTransformation::LowerInf;
      }
      else if  (  mLowerLimit != -std::numeric_limits<value_type>::infinity()
               && mUpperLimit == +std::numeric_limits<value_type>::infinity()
               )
      {
         // If only upper limit is +inf: [a, inf] -> [0, 1]
         // And f(x)dx -> f(a + t/(1-t)) 1/(1 - t)^2 dt
         mLowerLimit = 0;
         mUpperLimit = 1;

         mTransformedLimits = detail::IntegralLimitTransformation::UpperInf;
      }

      // Perform actual integration.
      mIntegral = Integrate();
   }

   /********************************************************************
    * Compute the integrand at a given position.
    *  Transformations are taken from: https://en.wikipedia.org/wiki/Numerical_integration#Integrals_over_infinite_intervals
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   QuadratureIntegration<FUNC, VALUE_TYPE>::value_type QuadratureIntegration<FUNC, VALUE_TYPE>::ComputeFunctionValue
      (  const value_type aVal
      )  const 
   {
      value_type transformed_input(0);

      value_type factor(1.0);

      // Apply transformations
      if (mTransformedLimits == detail::IntegralLimitTransformation::DoubleInf)
      {
         transformed_input = aVal / (1 - aVal*aVal);

         factor = (1 + aVal*aVal) / std::pow(1 - aVal*aVal, 2);
      }
      else if (mTransformedLimits == detail::IntegralLimitTransformation::LowerInf)
      {
         transformed_input = mUpperLimit - (1 - aVal) / aVal;

         factor = 1 / aVal*aVal;
      }
      else if (mTransformedLimits == detail::IntegralLimitTransformation::UpperInf)
      {
         transformed_input = mLowerLimit + aVal / (1 - aVal);

         factor = 1 / std::pow(1 - aVal, 2);
      }
      else
      {
         transformed_input = aVal;
         factor = 1;
      }

      return mFunc(transformed_input) * factor;
   }

   /********************************************************************
    * Do the actual integration based on the settings setup in the c-tor
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   QuadratureIntegration<FUNC, VALUE_TYPE>::value_type QuadratureIntegration<FUNC, VALUE_TYPE>::Integrate() const
   {
      return Open8PointQuad
               (  mLowerLimit
               ,  mUpperLimit
               ,  mRelThr
               );
   }

   /********************************************************************
    * Integration using the adaptive open 8-point quadrature
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   QuadratureIntegration<FUNC, VALUE_TYPE>::value_type QuadratureIntegration<FUNC, VALUE_TYPE>::Open8PointQuad
      (  const value_type aLowerLimit
      ,  const value_type aUpperLimit
      ,  const value_type aAbsThr
      ,  value_type f2
      ,  value_type f3
      ,  value_type f6
      ,  value_type f7
      )  const
   {
      value_type limit_diff = aUpperLimit - aLowerLimit;
      value_type f1 = ComputeFunctionValue(aLowerLimit +  1*limit_diff/12.0);
      value_type f4 = ComputeFunctionValue(aLowerLimit +  5*limit_diff/12.0);
      value_type f5 = ComputeFunctionValue(aLowerLimit +  7*limit_diff/12.0);
      value_type f8 = ComputeFunctionValue(aLowerLimit + 11*limit_diff/12.0);
      
      // If first call to Open8PointQuad these have not been set in previous recursive call
      if (std::isnan(f2))
      {
         f2 = ComputeFunctionValue(aLowerLimit +  2*limit_diff/12.0);
         f3 = ComputeFunctionValue(aLowerLimit +  4*limit_diff/12.0);
         f6 = ComputeFunctionValue(aLowerLimit +  8*limit_diff/12.0);
         f7 = ComputeFunctionValue(aLowerLimit + 10*limit_diff/12.0);
      }

      // Compute the integral based on the high-order quadrature
      value_type Q = (w1*f1 + w2*f2 + w3*f3 + w4*f4 + w5*f5 + w6*f6 + w7*f7 + w8*f8) * limit_diff;

      // Compute the integral based on the low-order quadrature
      value_type q = (v1*f1 + v2*f2 + v3*f3 + v4*f4 + v5*f5 + v6*f6 + v7*f7 + v8*f8) * limit_diff;

      // Check convergence and return appropriate values
      value_type err = std::abs(Q - q) / 2.0;
      value_type tol = aAbsThr + mRelThr * std::abs(Q);

      if (err <= tol)
      {
         return Q;
      }
      else
      {
         value_type mid_point = (aLowerLimit + aUpperLimit) / 2.0;
         return     Open8PointQuad(aLowerLimit, mid_point, aAbsThr / std::sqrt(2.0), f1, f2, f3, f4)
                  + Open8PointQuad(mid_point, aUpperLimit, aAbsThr / std::sqrt(2.0), f5, f6, f7, f8);
      }
   }
}

#endif /* QUADRATUREINTEGRATION_IMPL_HPP */