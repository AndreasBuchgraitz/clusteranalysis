#ifndef RANDOM_IMPL_HPP
#define RANDOM_IMPL_HPP

#include<random>
#include<type_traits>

#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Get a random number between two values
    ********************************************************************/
   template<typename T>
   T GetRandomNumber(const T aMinVal, const T aMaxVal)
   {
      std::random_device rd{};
      std::mt19937 gen{rd()};

      // Distribution to draw from
      if constexpr (std::is_integral_v<T>)
      {
         std::uniform_int_distribution<T> dist{aMinVal, aMaxVal};
         return dist(gen);
      }
      else if constexpr (std::is_floating_point_v<T>)
      {
         std::uniform_real_distribution<T> dist{aMinVal, aMaxVal};
         return dist(gen);
      }
      else
      {
         std::cerr << "Unknown type. Implement me !!" << std::endl;
         exit(1);
      }
   }

   /********************************************************************
    * Get a random number between two values
    ********************************************************************/
   template<FloatingPointContainer CONT, int SIZE>
   CONT GetRandomVector(const typename CONT::value_type aMinVal, const typename CONT::value_type aMaxVal)
   {
      CONT res_vec;
      if (res_vec.size() != SIZE)
      {
         res_vec.resize(SIZE);
      }

      for (auto it = res_vec.begin(); it != res_vec.end(); it++)
      {
         *it = GetRandomNumber<typename CONT::value_type>(aMinVal, aMaxVal);
      }

      return res_vec;
   }
}

#endif /* RANDOM_IMPL_HPP */