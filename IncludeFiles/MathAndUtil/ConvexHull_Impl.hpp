/********************************************************************
 *
 ********************************************************************/

#ifndef CONVEXHULL_IMPL_HPP
#define CONVEXHULL_IMPL_HPP

#include<map>

#include "MathAndUtil/ConvexHull.hpp"
#include "MathAndUtil/Math.hpp"
#include "MathAndUtil/Polytope.hpp"

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   ConvexHull<DIM, T>::ConvexHull
      (  const std::vector<vector_t>& arData
      )
      :  mData(arData)
   {
      if (mData.empty())
      {
         return;
      }

      // The convex hull is trivial, there is not enough points.
      if (this->mDim + 1 >= mData.size()) // For 3d it is trivial for 1,2,3 and 4 data points.
      {
         this->mPolytope = polytope_t(mData);
         return;
      }

      // Copy input data so we can delete points which are no longer relevant to consider.
      std::vector<vector_t> reduced_data(mData);

      // Pick d + 1 points. I choose the ones with greatest distance from the centroid.
      // start_points is the initial convex_hull estimate.
      std::vector<int> start_points = GenerateStartingGuess(this->mDim + 1);
      std::vector<vector_t> polytope_points;
      for (const int idx : start_points)
      {
         polytope_points.emplace_back(reduced_data[idx]);
      }

      // Initialize the polytope object describing the convex hull.
      this->mPolytope = polytope_t(polytope_points);

      // Locate points which are outside and inside the polytope
      const auto [points_outside_1, points_inside_1] = this->mPolytope.FindPointsOutsidePolytope(reduced_data);

      // Collect points which are not needed anymore, so we can loop over reduced data and only copy over the ones which are still needed (cannot figure out how to safely delete the unwanted ones so we copy instead).
      std::vector<int> idx_to_skip(start_points.begin(), start_points.end());
      idx_to_skip.insert(idx_to_skip.end(), points_inside_1.begin(), points_inside_1.end());

      // Remove unwanted points
      std::vector<vector_t> data_copy;
      for (Uin i = 0; i < reduced_data.size(); i++)
      {
         if (std::find(idx_to_skip.begin(), idx_to_skip.end(), i) == idx_to_skip.end())
         {
            data_copy.emplace_back(reduced_data[i]);
         }
      }
      reduced_data = std::move(data_copy);
      idx_to_skip.clear();

      int iterations = 0;

      while (!reduced_data.empty())
      {
         iterations+= 1;

         // Find new points which should be part of the convexhull
         std::vector<int> new_polytope_points = FindNewPointsForPolytope(GetPolytope(), reduced_data);

         for (const int idx : new_polytope_points)
         {
            polytope_points.emplace_back(reduced_data[idx]);
         }

         // Update polytope with the new points. This is done a quite inefficient right now, it recomputes all faces, instead of simply updating the existing ones, this is a bit of a challenge though...
         this->mPolytope = polytope_t(polytope_points);
      
         // Append all points within the polytope defined by start_points.
         const auto [points_outside, points_inside] = this->mPolytope.FindPointsOutsidePolytope(reduced_data);

         idx_to_skip.insert(idx_to_skip.end(), points_inside.begin(), points_inside.end());

         // Remove unwanted points
         data_copy.clear();
         for (Uin i = 0; i < reduced_data.size(); i++)
         {
            if (std::find(idx_to_skip.begin(), idx_to_skip.end(), i) == idx_to_skip.end())
            {
               data_copy.emplace_back(reduced_data[i]);
            }
         }
         reduced_data = std::move(data_copy);
         idx_to_skip.clear();

         if (iterations > 100)
         {
            std::cerr << "ConvexHull interations are over 100, might be buggy ..." << std::endl;
            exit(1);
         }
      }
   }

   /********************************************************************
    * Generate starting guess by selecting aNumPoints which are furthest
    * away from the centroid of the total data set.
    * These data points needs to NOT lie in the same plane !!!
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   std::vector<int> ConvexHull<DIM, T>::GenerateStartingGuess
      (  const int aNumPoints
      )  const
   {
      // Compute the centroid of all data points.
      const vector_t centroid = Centroid(mData.begin(), mData.end());

      // Compute all distances, sorted with largest elements first.
      std::multimap<double, int, std::greater<double>> distances;
      for (Uin i = 0; i < mData.size(); i++)
      {
         distances.emplace(RootSquaredDistance(mData[i], centroid), i);
      }

      // Find a vector of indices which are furthest away from the centroid, but does NOT lie in the same plane.
      std::vector<int> idx_vec;
      auto it = distances.begin();
      while (  idx_vec.size() < Uin(aNumPoints)
            && it != distances.end()
            )
      {
         idx_vec.emplace_back(it->second);
         it++;

         // If the current idx_vec contains more than 3 points which lie in the same plane pop_back and continue so that we simply try the next point.
         if (  idx_vec.size() > 3 
            && CheckPointsInSamePlane(idx_vec, mData)
            )
         {
            idx_vec.pop_back();
         }
      }

      if (idx_vec.size() != Uin(aNumPoints))
      {
         std::cerr << "For some reason could not find enough (or found to many) points" << std::endl;
         exit(1);
      }

      return idx_vec;
   }

   /********************************************************************
    * Find additional points for the polytope, by looking for points
    * far away from faces.
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   std::vector<int> ConvexHull<DIM, T>::FindNewPointsForPolytope
      (  const polytope_t& arPolytope
      ,  const std::vector<vector_t>& arData
      )  const
   {
      // Result vector
      std::vector<int> res_vec;

      // Divide the Points outside the polytope into sets belonging to each face.
      auto face_sets = arPolytope.DividePointsIntoSetsForFaces(arData);
      auto it_face_sets = face_sets.cbegin();

      //const auto& face_planes = arPolytope.GetPolytopeFacesPlanes();
      const auto& faces = arPolytope.GetFaces();
      auto it_face = faces.cbegin();

      // For each set find the point furthest outside of it.
      for(
         ;  it_face != faces.cend() && it_face_sets != face_sets.cend()
         ;  it_face++, it_face_sets++
         )
      {
         double dist = 0;
         int max_dist_idx = -1;
         for (const int idx : *it_face_sets)
         {
            double dist_2 = it_face->DistanceFromPlaneToPoint(arData[idx]);
            if (dist_2 > dist)
            {
               dist = dist_2;
               max_dist_idx = idx;
            }
         }

         if (dist != 0 && max_dist_idx != -1)
         {
            // Save one max distance index per face.
            res_vec.emplace_back(max_dist_idx);
         }
      }

      return res_vec;
   }

   /********************************************************************
    * Checks if the points given by the idx vector lies in the same
    * plane.
    * 
    * Computes all faces for the data as a polytope. Then checks if ALL
    * points lies inside ALL the faces.
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   bool ConvexHull<DIM, T>::CheckPointsInSamePlane
      (  const std::vector<int>& arIdxVec
      ,  const std::vector<vector_t>& arData
      )  const
   {
      // Trivial case
      if (arIdxVec.size() <= 3)
      {
         return true;
      }

      std::vector<vector_t> vec;
      vec.reserve(arIdxVec.size());
      for (const int idx : arIdxVec)
      {
         vec.emplace_back(arData[idx]);
      }

      polytope_t poly(vec);

      for (const auto& face : poly.GetFaces())
      {
         for (const auto& vertex : poly.GetVertices())
         {
            // If vertex lies in plane this evaluates to zero. So if it does not evaluate to zero the point is outside of the plane and we return false !
            if (face.ComputeWhichSideOfPlanePointIs(vertex) != 0)
            {
               return false;
            }
         }
      }

      return true;
   }
}

#endif /* CONVEXHULL_IMPL_HPP */