/********************************************************************
 * This file contains different math functionalites.
 ********************************************************************/

#ifndef MATH_HPP
#define MATH_HPP

#include "Math_Decl.hpp"

#include "Math_Impl.hpp"

#endif /* MATH_HPP */