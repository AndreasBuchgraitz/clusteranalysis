/********************************************************************
 *
 ********************************************************************/

#ifndef CAUCHYDISTRIBUTION_IMPL_HPP
#define CAUCHYDISTRIBUTION_IMPL_HPP

#include<cmath>
#include<numeric>

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::CauchyDistribution
      (  const CONT& arData
      ,  const int aNumDistributions
      )
      :  DistributionBase<CONT>
            (  arData
            )
   {
      if (this->mData.size() >= 3)
      {
         this->mParams = GenerateStartingGuess(aNumDistributions);
      }
   }

   /********************************************************************
    * Compute the Mean of given input data.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::value_type CauchyDistribution<CONT>::ComputeMedian(const CONT& arData) const
   {
      if (arData.empty())
      {
         std::cerr << "arData is empty, this is not allowed as we try to index arData here in CauchyDistribution::ComputeMedian()" << std::endl;
         exit(1);
      }
      else if (!std::is_sorted(arData.begin(), arData.end()))
      {
         std::cerr << "arData need to be sorted for CauchyDistribution::ComputeMedian() !" << std::endl;
         exit(1);
      }
      else
      {
         // For even sized data sets we take average of the two values in "the middle"
         if (arData.size() % 2 == 0)
         {
            value_type lower = arData[(arData.size() / 2) - 1];
            value_type upper = arData[arData.size() / 2];
            return (lower + upper) / 2.0;
         }
         else // For odd sized data sets we simply take "the middle value"
         {
            // Use integer division to get middle value by rounding down.
            return arData[int(arData.size() / 2)];
         }
      }
   }

   /********************************************************************
    * Estimate the scale parameter (gamma) for a given distribution.
    * This is done by computing the median of the absolute value of the
    * data set.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::value_type CauchyDistribution<CONT>::EstimateGamma(const CONT& arData) const
   {
      if (arData.empty())
      {
         std::cerr << "arData is empty, this does not make any sense !" << std::endl;
         exit(1);
      }
      else
      {
         // If even (2n) or if odd (2n+1) get n.
         int n = arData.size() % 2 == 0 ? arData.size() / 2 : (arData.size() - 1) / 2;

         // Q1 is median of n smallest values
         auto it_q1 = std::next(arData.begin(), n);
         CONT q1_data(arData.begin(), it_q1);
         value_type Q1 = ComputeMedian(q1_data);

         // Q3 is median of n largest values
         auto it_q3 = std::prev(arData.end(), n);
         CONT q3_data(it_q3, arData.end());
         value_type Q3 = ComputeMedian(q3_data);

         // Estimate gamma ny computing median of absolute valued data
         return Q3 - Q1;
      }
   }

   /********************************************************************
    * Computes the probability density function at a given point.
    *
    * frac = (x - x_0_j) / gamma_j
    * P(x) = \sum_j=[1:M] 1 / (pi * gamma_j * (1 + frac^2))
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::value_type CauchyDistribution<CONT>::EvaluateProbDensFunc
      (  const value_type aVal
      ,  const vec_type& arParams
      )  const
   {
      value_type tmp(0);
      for (Uin i = 0; i < arParams.size(); i += 2)
      {
         value_type median = arParams(i);
         value_type gamma  = arParams(i + 1);
         value_type frac = (aVal - median) / gamma;
         tmp += 1.0 / (gamma * (1 + frac * frac));
      }
      tmp /= std::numbers::pi_v<value_type>;
      
      // Normalise by dividing with the number of distributions.
      tmp /= (arParams.size() / 2);
      return tmp;
   }

   /********************************************************************
    * Computes the theoretical Cumultative Distribution Function.
    *
    * CDF(x) = 1/pi * arctan((x-x_0)/gamma) + 0.5
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::value_type CauchyDistribution<CONT>::TheoreticalCDF(const value_type aVal) const
   {
      value_type res(0);

      for (Uin i = 0; i < this->mParams.size(); i += 2)
      {
         value_type median = this->mParams(i);
         value_type gamma  = this->mParams(i + 1);
         res += std::atan((aVal - median) / gamma);
      }
      // 
      res /= std::numbers::pi_v<value_type>;

      // Add 0.5 for each distribution used
      res +=  (this->mParams.size() / 2.0) / 2.0;

      return res;
   }

   /********************************************************************
    * Compute value of function begin optimised.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::value_type CauchyDistribution<CONT>::ComputeLogLikelihood
      (  const vec_type& arParams
      )  const
   {
      return std::transform_reduce
               (  this->mData.begin()
               ,  this->mData.end()
               ,  value_type(0)
               ,  std::plus<>()
               ,  [this, &arParams](const value_type aDataPoint)
                  {
                     return std::log(EvaluateProbDensFunc(aDataPoint, arParams));
                  }
               );
   }

   /********************************************************************
    * Compute gradient vector inplace on input vector
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   void CauchyDistribution<CONT>::ComputeLogLikelihoodGradient
      (  const vec_type& arParams
      ,  vec_type& arGradient
      )  const
   {
      // Compute g for all data points (intermediate)
      std::vector<value_type> g_vector;
      g_vector.reserve(this->mData.size());
      for (const auto& data : this->mData)
      {
         g_vector.emplace_back(EvaluateProbDensFunc(data, arParams));
      }

      // Loop over all even k idx
      for (Uin k = 0; k < arParams.size(); k += 2)
      {
         value_type median_k = arParams(k);
         value_type gamma_k  = arParams(k + 1);

         // Compute derivatives
         value_type median_deriv_tmp(0);
         value_type gamma_deriv_tmp(0);
         for (Uin i = 0; i < this->mData.size(); i++)
         {
            value_type x_i = this->mData[i];

            value_type diff = x_i - median_k;
            value_type frac = std::pow(diff / gamma_k, 2);

            // Value of the single probability distribution function for given pair of params
            value_type f = 1.0 / (std::numbers::pi_v<value_type> * gamma_k * (1 + frac));

            // This ratio is one if only one distribution, compute here to avoid numerical noise if included seperately in numerator and denominator below
            value_type f_g_ratio = f / g_vector[i];

            // Contribution to median derivative
            median_deriv_tmp += 2 * diff * f_g_ratio / (std::pow(gamma_k, 2) + std::pow(diff, 2));

            // Contribution to the derivative wrt. sigma
            gamma_deriv_tmp += f_g_ratio * (-1.0 / gamma_k + 2*f * std::numbers::pi_v<value_type> * frac);
         }

         // Derivative wrt. median
         arGradient(k) = median_deriv_tmp;

         // Derivative wrt. gamma
         arGradient(k + 1) = gamma_deriv_tmp;
      }
   }

   /********************************************************************
    * Compute gradient vector inplace on input vector
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   CauchyDistribution<CONT>::vec_type CauchyDistribution<CONT>::GenerateStartingGuess
      (  const int aNumDistributions
      )  const
   {
      // Create initial guess by simply dividing the this->mData into i normal distributions without any overlap.
      std::vector<CONT> starting_guess_chunks;

      int chunks = this->mData.size() / aNumDistributions;

      auto it_chunk_start = this->mData.begin();
      auto it_chunk_end = this->mData.begin();
      std::advance(it_chunk_end, chunks);
      for (int i = 0; i < aNumDistributions; i++)
      {
         starting_guess_chunks.emplace_back(CONT(it_chunk_start, it_chunk_end));
         std::advance(it_chunk_start, chunks);

         // For the last chunk simply take the rest.
         if (i == aNumDistributions - 1)
         {
            it_chunk_end = this->mData.end();
         }
         else
         {
            std::advance(it_chunk_end, chunks);
         }
      }

      // Two parameters per distribution.
      vec_type params(2 * aNumDistributions);

      // Insert starting guess in params CONT
      for (int i = 0; i < params.size(); i += 2)
      {
         int distance = i / 2;
         params(i) = ComputeMedian(starting_guess_chunks.at(distance));
         params(i + 1) = EstimateGamma(starting_guess_chunks.at(distance));
      }

      return params;
   }

   /********************************************************************
    * Construct the vector of bounds for the optimisation.
    * 
    * Lower bound for median is -inf and for gamma > 0.
    * Upper bound for median is +inf and for gamma it is +inf.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   std::pair
      <  typename CauchyDistribution<CONT>::vec_type
      ,  typename CauchyDistribution<CONT>::vec_type
      >
   CauchyDistribution<CONT>::GetOptimisationBounds
      (
      )  const
   {
      vec_type lower_bounds = vec_type::Constant(this->mParams.size(), 0.0);
      for (int i = 0; i < this->mParams.size(); i += 2)
      {
         // Median lower bound
         lower_bounds(i) = -std::numeric_limits<value_type>::infinity();
         // Gamma lower bound
         lower_bounds(i + 1) = std::numeric_limits<value_type>::min();
      }
      vec_type upper_bounds = vec_type::Constant(this->mParams.size(), +std::numeric_limits<value_type>::infinity());

      return std::make_pair(lower_bounds, upper_bounds);
   }

   /********************************************************************
    * Compute hessian matrix inplace on input matrix
    * 
    * Saved just in case i need it again, but maybe just delete LBFGS++
    * works very nicely.
    ********************************************************************/
   //template
   //   <  FloatingPointContainer CONT
   //   >
   //void MaximumLogLikelihood<CONT>::ComputeHessianMatrix
   //   (  mat_type& arHessian
   //   )  const
   //{
   //   // Compute g for all data points (intermediate)
   //   std::vector<value_type> g_vector;
   //   g_vector.reserve(this->mData.size());
   //   for (const auto& data : this->mData)
   //   {
   //      g_vector.emplace_back(ComputeG(data));
   //   }

   //   // Compute all hessian element for double derivatives wrt. mu
   //   for (Uin r = 0; r < this->mParams.size(); r += 2)
   //   {
   //      for (Uin k = 0; k < this->mParams.size(); k += 2)
   //      {
   //         value_type mean_k = this->mParams(k);
   //         value_type sigma_k = this->mParams(k + 1);
   //         value_type mean_r = this->mParams(r);
   //         value_type sigma_r = this->mParams(r + 1);

   //         value_type tmp(0);

   //         for (Uin i = 0; i < this->mData.size(); i++)
   //         {
   //            value_type x_i = this->mData[i];

   //            value_type exp_val_k = ComputeExponential(x_i, mean_k, sigma_k);
   //            value_type exp_val_r = ComputeExponential(x_i, mean_r, sigma_r);

   //            value_type frac_k = (x_i - mean_k) / sigma_k;

   //            value_type first_term = r == k ? std::pow(frac_k, 2) - 1 : 0;
   //            value_type second_term = -(x_i - mean_k) * (x_i - mean_r) * exp_val_r / g_vector[i];
   //            second_term /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(sigma_r, 3);

   //            tmp += exp_val_k * (first_term + second_term) / g_vector[i];
   //         }
   //         tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(sigma_k, 3);

   //         arHessian(r, k) = tmp;
   //         arHessian(k, r) = tmp;
   //      }
   //   }

   //   // Compute all hessian element for double derivatives wrt. sigma
   //   for (Uin r = 0; r < this->mParams.size(); r += 2)
   //   {
   //      for (Uin k = 0; k < this->mParams.size(); k += 2)
   //      {
   //         value_type tmp(0);

   //         for (Uin i = 0; i < this->mData.size(); i++)
   //         {
   //            value_type exp_val_k = ComputeExponential(this->mData[i], this->mParams(k), this->mParams(k + 1));
   //            value_type exp_val_r = ComputeExponential(this->mData[i], this->mParams(r), this->mParams(r + 1));

   //            value_type frac_k = (this->mData[i] - this->mParams(k)) / this->mParams(k + 1);
   //            value_type frac_r = (this->mData[i] - this->mParams(r)) / this->mParams(r + 1);
   //            value_type first_term = r == k 
   //                                  ? (std::pow(frac_k, 4) - 5*std::pow(frac_k, 2) + 2) / this->mParams(k+1)
   //                                  : 0;
   //            value_type second_term = -exp_val_r * (std::pow(frac_r, 2) - 1) * (std::pow(frac_k, 2) - 1) / g_vector[i];
   //            second_term /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(r + 1), 2);

   //            tmp += exp_val_k * (first_term + second_term) / g_vector[i];
   //         }

   //         tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(k + 1), 2);

   //         // Insertion into hessian is +1 for sigma values.
   //         arHessian(r + 1, k + 1) = tmp;
   //         arHessian(k + 1, r + 1) = tmp;
   //      }
   //   }

   //   // Compute all hessian element for derivatives wrt. sigma and mu
   //   for (Uin r = 0; r < this->mParams.size(); r += 2)
   //   {
   //      for (Uin k = 0; k < this->mParams.size(); k += 2)
   //      {
   //         value_type tmp(0);

   //         value_type mean_k = this->mParams(k);
   //         value_type mean_r = this->mParams(r);

   //         for (Uin i = 0; i < this->mData.size(); i++)
   //         {
   //            value_type x_i = this->mData[i];

   //            value_type exp_val_k = ComputeExponential(x_i, mean_k, this->mParams(k + 1));
   //            value_type exp_val_r = ComputeExponential(x_i, mean_r, this->mParams(r + 1));

   //            value_type frac_k = (x_i - mean_k) / this->mParams(k + 1);
   //            value_type frac_r = (x_i - mean_r) / this->mParams(r + 1);
   //            value_type first_term = r == k 
   //                                  ? (std::pow(frac_k, 2) - 3) * frac_k
   //                                  : 0;
   //            value_type second_term = -(x_i - mean_k) * exp_val_r * (std::pow(frac_r, 2) - 1) / g_vector[i];
   //            second_term /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(r + 1), 2);

   //            tmp += exp_val_k * (first_term + second_term) / g_vector[i];
   //         }

   //         tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(k + 1), 3);

   //         // Insertion into hessian is +1 for sigma values.
   //         arHessian(r + 1, k) = tmp;
   //         arHessian(k, r + 1) = tmp;
   //      }
   //   }
   //}
}

#endif /* CAUCHYDISTRIBUTION_IMPL_HPP */