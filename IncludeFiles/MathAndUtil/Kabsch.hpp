/********************************************************************
 * This file contains namespace for all Kabsch related stuff
 * including a RMSD data class.
 ********************************************************************/

#ifndef KABSCH_HPP
#define KABSCH_HPP

#include<iostream>
#include<map>
#include<filesystem>
#include "Eigen_wrapper.hpp"

#include "Structure/StructureIdentification.hpp"
#include "Concepts.hpp"

// Forward declarations
namespace Structure
{
   class Molecule;
   class Cluster;
   enum class AtomName;
   enum class ConformerEnum;
   enum class InputOptions;
   class StructureIdentification;
}
namespace Kabsch
{
   class RmsdData;
}

namespace Kabsch
{
   //! Computes the Rotation matrix between arMat1 and arMat2 and returns it.
   Eigen::MatrixXd Kabsch(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Computes the centroid for a given matrix
   Eigen::RowVectorXd Centroid(const Eigen::MatrixXd& arMat);

   //! Translate structure based on centroid.
   Eigen::MatrixXd MoveStructureToCenter(const Eigen::MatrixXd& arStructure);

   //! Compute Center-of-Mass
   Eigen::RowVector3d CenterOfMass(const std::vector<Structure::AtomName>& arAtomNames, const Eigen::MatrixXd& arAtomPositions);

   //! Retrieves the rotated structure based on rotation matrix.
   Eigen::MatrixXd GetRotatedStructure
      (  const Eigen::MatrixXd& arStructureMat
      ,  const Eigen::MatrixXd& arRotMat
      );

   //! Function for make matrices for use in Kabsch algorithm
   std::pair<Eigen::MatrixXd, Eigen::MatrixXd> SetupMoleculeMatsForKabsch
      (  const Structure::Molecule& arMolecule1
      ,  const Structure::Molecule& arMolecule2
      ,  const Structure::InputOptions& arInputOption
      );

   //! Computes RMSD between two molecules.
   double MoleculeRMSD
      (  const Structure::Molecule& arMolecule1
      ,  const Structure::Molecule& arMolecule2
      ,  const Structure::InputOptions& arInputOption
      );

   template
      <  MoleculeIter ITER_1
      ,  MoleculeIter ITER_2
      >
   std::vector<double> RMSD
      (  const ITER_1 aIt1Begin
      ,  const ITER_1 aIt1End
      ,  const ITER_2 aIt2Begin
      ,  const ITER_2 aIt2End
      ,  const Structure::InputOptions& arInputOption
      );
               
   //! Computes The RMSD of clusters in a simple manner
   RmsdData SimpleClusterRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const std::vector<Structure::Cluster>& arClusterCompare
      );
   
   //! Computes RMSD of structures for Cluster's
   RmsdData ClusterRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const std::vector<Structure::Cluster>& arClusterCompare
      ,  const Structure::InputOptions& arKabschHydrogen
      ,  const std::vector<Eigen::Matrix3d>& arRotMats = std::vector<Eigen::Matrix3d>{}
      );

   //! Computes DipoleMoment Rmsd for all the comparing clusters.
   RmsdData ClusterDipoleMomentRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const std::vector<Structure::Cluster>& arClusterCompare
      ,  const std::vector<Eigen::Matrix3d>& arRotMats = std::vector<Eigen::Matrix3d>{}
      );

   //! Computes rotation matrices between monomers
   std::vector<std::pair<std::string, Eigen::MatrixXd>> ComputeAllRotMats
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  const Structure::InputOptions& arInputOption
      );
   
   //! Computes the RMSD values for all rotation matrices
   std::vector<double> ComputeAllRotMatRmsd
      (  const std::vector<std::pair<std::string, Eigen::MatrixXd>>& arRotMats
      );

   //! Computes the RMSD values for all atoms
   std::vector<double> ComputeRmsdForEachAtom
      (  const Structure::Molecule& arMolecule1
      ,  const Structure::Molecule& arMolecule2
      );

   //! Gets the indices from the input molecules which corresponds to the same atoms in the cluster.
   std::vector<std::pair<Uin, Uin>> GetOverlappingMoleculeIdx
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      );

   //! Constructs pair of matrices based on the overlapping indices between two molecules
   std::pair<Eigen::MatrixX3d, Eigen::MatrixX3d> GetOverlappingMoleculePositions
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      );

   //! Computes RMSD based on the input function f. Wrapper for the other methods which compute a RMSD for "monomers across"
   template<typename FUNC>
   std::vector<double> ComputeRmsdAcrossMonomers
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  FUNC aFuncForRMSD
      ,  const std::vector<Eigen::Matrix3d>& arRotMats = std::vector<Eigen::Matrix3d>{}
      );

   //! Computes the RMSD values for rot mats across monomers.
   std::vector<double> ComputeRotMatRmsdAcross
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  const std::vector<Eigen::Matrix3d>& arRotMats = std::vector<Eigen::Matrix3d>{}
      );
   
   //! Compute the RMSD of all the atoms which are present present in both molecules.
   std::vector<double> ComputeAtomicPositionRMSD
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      );

   //! Compute RMSD for all monomers across based on dipole-moment
   std::vector<double> ComputeDipoleMomentRMSD
      (  const Structure::Cluster& arClusterRef
      ,  const Structure::Cluster& arClusterCompare
      ,  const std::vector<Eigen::Matrix3d>& arRotMats = std::vector<Eigen::Matrix3d>{}
      );

   //! Compute RMSD for two specific molecules based on their dipolemoment.
   std::vector<double> ComputeDipoleMomentRMSD
      (  const Structure::Molecule& arMol1
      ,  const Structure::Molecule& arMol2
      );

   void ConvertSimpleRmsdDataToTableAndPrint
      (  const std::map<std::string, RmsdData>& arRmsdDataMap
      ,  const std::filesystem::path& arPath
      ,  const std::string& arFileSuffix
      );

   /********************************************************************
    * Small POD class containing information about RMSD analysis.
    * 
    * The idea is that this should be returned from all RMSD functions
    * with all the appropriate members/values set.
    * 
    * Object exists on a Id basis, i.e. sone RmsdData object for each
    * Reference StructureId, and then the object contains a map pointing
    * to all StructureId's we compare with this reference.
    ********************************************************************/
   class RmsdData
   {
      public:
         //! C-tor
         RmsdData() = default;
         RmsdData
            (  const Structure::StructureIdentification& arRefId  
            ,  const Structure::Molecule& arRefMolecule
            );
      
         //!@{ Setter functions
         //!@}

         //!@{ Getter functions
         const Structure::StructureIdentification& GetRefId() const { return mReferenceId; }

         //! Used when computing RMSD for isolated monomers.
         const Structure::Molecule& GetReferenceMolecule() const { return mReferenceMolecule; }
         //!@}

         void AddComparedMethodAndRMSD
            (  const Structure::StructureIdentification& arId
            ,  const Structure::ConformerEnum arEnum
            ,  std::vector<double> arRMSD
            );

         //!@{ Inquiries
         //!@}

         //!@{ STL interface.
         auto begin() const { return mComparedMethods.cbegin(); }
         auto end()   const { return mComparedMethods.cend(); }
         auto front() const { return mComparedMethods.front(); }
         auto back()  const { return mComparedMethods.back(); }
         auto& front()      { return mComparedMethods.front(); }
         auto& back()       { return mComparedMethods.back(); }
         //!@}

         //!@{ Analysis
         std::map<Structure::StructureIdentification, Structure::ConformerEnum> CheckConformersByAtomicPositions
            (  const std::size_t aNumMonomers
            ,  const std::size_t aSizeSmallestMonomer
            ,  const double aSmallestBondLength
            )  const;

         std::map<Structure::StructureIdentification, Structure::ConformerEnum> CheckConformersByDipoleMoment([[maybe_unused]] const std::size_t aNumMonomers) const;

         //std::map<Structure::StructureIdentification, Structure::ConformerEnum> CheckConformersByCoulombMatrix([[maybe_unused]] const std::size_t aNumMonomers, const double aCoulombMatrixNorm) const;
         //!@}

         //!@{ Data extraction
         void PrintHistogramData(const std::filesystem::path& arDataDir) const;
         //!@}
      
      private:
         //!@{ Reference info. Const ref's to ensure they are initialized at construction.
         const Structure::StructureIdentification& mReferenceId;
         const Structure::Molecule& mReferenceMolecule;
         //!@}

         /********************************************************************
          * When constructing mCompared method it should be checked that
          * Id::Name is same as ReferenceId::Name.
          * 
          * Key -> Value, is Id -> RMSD. 
          * 
          * Value is saved as vector such that it also works if supplied with
          * RMSD between each monomer with the same StructureIdentification.
          ********************************************************************/
         std::vector
            <  std::tuple
                  <  Structure::StructureIdentification
                  ,  Structure::ConformerEnum
                  ,  std::vector<double>
                  >
            > mComparedMethods;
   };

   //! Overload of operator<<
   std::ostream& operator<<(std::ostream& arOut, const RmsdData& arRmsdData);
}

/********************************************************************
 * Overload RMSD to take vector of molecules and returns vector of
 * RMSD values (doubles).
 ********************************************************************/
template
   <  MoleculeIter ITER_1
   ,  MoleculeIter ITER_2
   >
std::vector<double> Kabsch::RMSD
   (  const ITER_1 aIt1Begin
   ,  const ITER_1 aIt1End
   ,  const ITER_2 aIt2Begin
   ,  const ITER_2 aIt2End
   ,  const Structure::InputOptions& arInputOption
   )
{
   if (std::distance(aIt1Begin, aIt1End) != std::distance(aIt2Begin, aIt2End))
   {
      std::cerr << "ERROR in RMSD for vectors of molecules; unequal number of molecule !\n"
         << "std::distance(aIt1Begin, aIt1End) = " << std::distance(aIt1Begin, aIt1End) << "\n"
         << "std::distance(aIt2Begin, aIt2End) = " << std::distance(aIt2Begin, aIt2End) << "\n"
         << std::endl;
      exit(1);
   }

   // Result vector
   std::vector<double> res_vec;

   ITER_1 it_1 = aIt1Begin;
   ITER_2 it_2 = aIt2Begin;
   for(
      ;  (it_1 != aIt1End) && (it_2 != aIt2End)
      ;  it_1++, it_2++
      )
   {
      res_vec.emplace_back(MoleculeRMSD(*it_1, *it_2, arInputOption));
   }

   return res_vec;
}

#endif /* KABSCH_HPP */