/********************************************************************
 *
 ********************************************************************/

#ifndef MAXIMUMLOGLIKELIHOODOPTIMISER_IMPL_HPP
#define MAXIMUMLOGLIKELIHOODOPTIMISER_IMPL_HPP

#include<stdexcept>
#include<numbers>
#include<numeric>
#include<LBFGSB.h>

#include "MathAndUtil/MaximumLogLikelihood.hpp"

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   MaximumLogLikelihoodOptimiser<CONT, DIST>::MaximumLogLikelihoodOptimiser
      (  const CONT arData
      ,  const int aNumDistributions
      ,  const value_type aThr
      )
      :  mMaxLogLikelihood(arData, aNumDistributions)
   {
      // Setup some LBFGSB++ parameters
      LBFGSpp::LBFGSBParam<value_type> lbfgsb_params;
      lbfgsb_params.epsilon = aThr;  // Threshold for norm of gradient
      lbfgsb_params.past = 1;        // Used in delta threshold
      lbfgsb_params.delta = 1.0E-08;  // Threshold for |f_{k-past} (x) - f_{k} (k)|
      lbfgsb_params.max_iterations = 200;

      LBFGSpp::LBFGSBSolver<value_type> solver(lbfgsb_params);

      // Generate initial guess.
      vec_type params = mMaxLogLikelihood.GetParams(); // This should get default constructed to the above call

      const auto [lower_bounds, upper_bounds] = mMaxLogLikelihood.GetOptimisationBounds();

      // Define value to be overwritten
      value_type func_val(0.0);

      // Run the actual optimisation
      try
      {
         mNumIter = solver.minimize(mMaxLogLikelihood, params, func_val, lower_bounds, upper_bounds);

         // Save the results to the mMaxLogLikelihood member
         mMaxLogLikelihood.SetOptimisedLogLikelihoodValue(func_val);
         mMaxLogLikelihood.SetParams(params);
         mConverged = true;
      }
      catch (const std::range_error& err)
      {
         //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
         //std::cout << err.what() << std::endl;
         //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
         mNumIter = 0;
         mMaxLogLikelihood.SetOptimisedLogLikelihoodValue(0.0);
         mMaxLogLikelihood.SetParams(vec_type::Zero(params.size()));
         mConverged = false;
      }
   }
}

#endif /* MAXIMUMLOGLIKELIHOODOPTIMISER_IMPL_HPP */