/********************************************************************
 *
 ********************************************************************/

#ifndef MONTECARLOINTEGRATION_HPP
#define MONTECARLOINTEGRATION_HPP

#include "MonteCarloIntegration_Decl.hpp"

#include "MonteCarloIntegration_Impl.hpp"

#endif /* MONTECARLOINTEGRATION_HPP */