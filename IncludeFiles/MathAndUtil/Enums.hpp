#ifndef MATHENUMS_HPP
#define MATHENUMS_HPP

namespace Math
{
   namespace detail
   {
      enum class IntegralLimitTransformation
      {  UpperInf
      ,  LowerInf
      ,  DoubleInf
      ,  NoInf
      };
   }
}

#endif /* MATHENUMS_HPP */