/********************************************************************
 *
 ********************************************************************/

#ifndef MAXIMUMLOGLIKELIHOOD_DECL_HPP
#define MAXIMUMLOGLIKELIHOOD_DECL_HPP

#include<tuple>
#include<optional>
#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Class for performing maximum loglikelihood analysis.
    * 
    * Templated over data container and the type of distribution.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   class MaximumLogLikelihood
   {
      public:
         using value_type = CONT::value_type;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;
         using mat_type = Eigen::Matrix<value_type, Eigen::Dynamic, Eigen::Dynamic>;

      public:
         //!@{ Constructors
         MaximumLogLikelihood() = delete;
         MaximumLogLikelihood(const CONT arData, const int aNumDistributions);
         //!@}

         //!@{ LBFGS++ interface
         value_type operator()(const vec_type& arParams, vec_type& arGradient);
         vec_type GenerateStartingGuess(const int aNumDistributions) const { return mDistribution.GenerateStartingGuess(aNumDistributions); }
         std::pair<vec_type, vec_type> GetOptimisationBounds() const { return mDistribution.GetOptimisationBounds(); }
         //!@}

         //!@{ GETTERS
         const vec_type& GetParams() const { return mDistribution.GetParams(); }
         value_type GetOptimisedLogLikelihoodValue() const { return mOptimisedLogLikelihoodValue.value(); }
         template<int PARAM> const value_type GetOptimisedParam(const int aParamIdx) const { return std::get<PARAM>(mOptimisedDistributions[aParamIdx]); }
         //!@}

         //!@{ QUERIES
         bool CheckMeansSmallerThanThreshold(const value_type aThr) const;
         bool CheckMeansLargerThanThreshold(const value_type aThr) const;
         //!@}

         //!@{ SETTERS
         void SetOptimisedLogLikelihoodValue(const value_type& arVal) { mOptimisedLogLikelihoodValue = std::make_optional(arVal); }
         void SetParams(const vec_type& arParams);
         //!@}

         //!@{ STATISTICAL FUNCTIONS
         value_type EmpiricalCDF(const value_type aVal) const;
         std::vector<value_type> ComputeKolmogorovSmirnov() const;
         value_type ComputeAndersonDarlingByIntegral() const;
         value_type ComputeAndersonDarlingBySummation() const;
         //!@}

      private:
         //! Data we are trying to optimise our function to.
         CONT mData;

         //! The given distribution begin optimised
         DIST mDistribution;

         //! Container for optimised parameters.
         std::vector<std::tuple<value_type, value_type>> mOptimisedDistributions;

         //! Value of the function at the optimised parameters.
         std::optional<value_type> mOptimisedLogLikelihoodValue;

      private:
         //! Will wrap the optimised parameters from the optimiser class in a nicer vector in this class.
         void SetOptimisedDistributions();
         
   };
}

#endif /* MAXIMUMLOGLIKELIHOOD_DECL_HPP */