/********************************************************************
 *
 ********************************************************************/

#ifndef NUMERICALINTEGRATION_HPP
#define NUMERICALINTEGRATION_HPP

#include "NumericalIntegration_Decl.hpp"

#include "NumericalIntegration_Impl.hpp"

#endif /* NUMERICALINTEGRATION_HPP */