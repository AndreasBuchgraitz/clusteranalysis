/********************************************************************
 * This file contains different math functionalites.
 ********************************************************************/

#ifndef RANDOM_DECL_HPP
#define RANDOM_DECL_HPP

//#include "Eigen_wrapper.hpp"
#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Templated functions
    ********************************************************************/
   template<typename T>
   T GetRandomNumber(const T aMinVal, const T aMaxVal);

   template<FloatingPointContainer CONT, int SIZE>
   CONT GetRandomVector(const typename CONT::value_type aMinVal, const typename CONT::value_type aMaxVal);
}

#endif /* RANDOM_DECL_HPP */