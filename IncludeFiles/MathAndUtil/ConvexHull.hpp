/********************************************************************
 *
 ********************************************************************/

#ifndef CONVEXHULL_HPP
#define CONVEXHULL_HPP

#include "MathAndUtil/ConvexHull_Decl.hpp"

#include "MathAndUtil/ConvexHull_Impl.hpp"

#endif /* CONVEXHULL_HPP */