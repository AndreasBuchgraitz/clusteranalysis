#ifndef STATISTICS_IMPL_HPP
#define STATISTICS_IMPL_HPP

#include<random>
#include<type_traits>

#include "Concepts.hpp"
#include "MathAndUtil/MaximumLogLikelihood.hpp"
#include "MathAndUtil/MonteCarloIntegration.hpp"

namespace Math
{
   /********************************************************************
    * Computes the Kolmogorov-Smirnov statistic for all the given
    * MaxLogLikelihood objects.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename CONT::value_type> ComputeKolmogorovSmirnov
      (  const std::vector<MaximumLogLikelihood<CONT, DIST>>& aMaxLikelihoods
      )
   {
      using value_type = CONT::value_type;

      std::vector<value_type> max_vals;

      for (const auto& max_likelihood : aMaxLikelihoods)
      {
         auto kol_smi_vec = max_likelihood.ComputeKolmogorovSmirnov();

         max_vals.emplace_back(kol_smi_vec.back());
      }

      return max_vals;
   }

   /********************************************************************
    * Computes the Anderson-Darling statistic for all the given
    * MaxLogLikelihood objects.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename CONT::value_type> ComputeAndersonDarlingByIntegral
      (  const std::vector<MaximumLogLikelihood<CONT, DIST>>& aMaxLikelihoods
      )
   {
      using value_type = CONT::value_type;

      std::vector<value_type> anderson_darling_distances;

      for (const auto& max_likelihood : aMaxLikelihoods)
      {
         anderson_darling_distances.emplace_back(max_likelihood.ComputeAndersonDarlingByIntegral());
      }

      return anderson_darling_distances;
   }

   /********************************************************************
    * Computes the Anderson-Darling statistic for all the given
    * MaxLogLikelihood objects.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename CONT::value_type> ComputeAndersonDarlingBySummation
      (  const std::vector<MaximumLogLikelihood<CONT, DIST>>& aMaxLikelihoods
      )
   {
      using value_type = CONT::value_type;

      std::vector<value_type> anderson_darling_distances;

      for (const auto& max_likelihood : aMaxLikelihoods)
      {
         anderson_darling_distances.emplace_back(max_likelihood.ComputeAndersonDarlingBySummation());
      }

      return anderson_darling_distances;
   }

   /********************************************************************
    * Compares Anderson-Darling statistics. Returns vector of bools
    * being true if the value corresponding to the same index is within
    * the significance level (i.e. we cannot reject it) and false if the
    * value is outside of the expected significance region.
    * 
    * From table on wikipedia: 
    ********************************************************************/
   template<FloatingPoint T>
   std::vector<bool> CompareAndersonDarlingStatistics
      (  const std::vector<T>& aVec
      ,  const double aSignificanceLevel
      )
   {
      std::vector<bool> result;

      for (const T elem : aVec)
      {
         // If value within significance region
         if (  elem > GetAndersonDarlingLowerTailCriticalValue(aSignificanceLevel)
            && elem < GetAndersonDarlingUpperTailCriticalValue(aSignificanceLevel)
            )
         {
            result.emplace_back(true);
         }
         else
         {
            result.emplace_back(false);
         }
      }

      return result;
   }
}

#endif /* STATISTICS_IMPL_HPP */