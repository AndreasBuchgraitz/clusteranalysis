/********************************************************************
 *
 ********************************************************************/

#ifndef POLYTOPE_HPP
#define POLYTOPE_HPP

#include "MathAndUtil/Polytope_Decl.hpp"

#include "MathAndUtil/Polytope_Impl.hpp"

#endif /* POLYTOPE_HPP */