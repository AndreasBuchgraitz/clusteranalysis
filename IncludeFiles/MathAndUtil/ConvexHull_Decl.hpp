/********************************************************************
 *
 ********************************************************************/

#ifndef CONVEXHULL_DECL_HPP
#define CONVEXHULL_DECL_HPP

#include<array>
#include<vector>
#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"
#include "MathAndUtil/Polytope.hpp"

namespace Math
{
   /********************************************************************
    * Class to compute the convex hull for a container of floating
    * point data.
    * 
    * https://en.wikipedia.org/wiki/Quickhull#cite_note-3
    * http://algolist.ru/maths/geom/convhull/qhull3d.php
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   class ConvexHull
   {
      public:
         //!@{ ALIASES
         static constexpr int mDim = DIM;
         using value_t = T;
         using vector_t = Eigen::Matrix<T, 1, DIM>;
         using polytope_t = Polytope<DIM, T>;
         //!@}
         
         static_assert(mDim == 3, "ConvexHull only implemented for 3-dimensional cases so far !");

      public:
         //!@{ Constructors
         ConvexHull() = delete;
         ConvexHull(const std::vector<vector_t>& arData);
         //!@}

         //!@{ Getters
         const polytope_t& GetPolytope() const { return mPolytope; }
         const auto& GetHullVertices() const { return mPolytope.GetVertices(); }
         //!@}

         std::vector<int> GenerateStartingGuess
            (  const int aNumPoints
            )  const;
      
         std::vector<int> FindNewPointsForPolytope
            (  const polytope_t& arPolytope
            ,  const std::vector<vector_t>& arData
            )  const;

      private:
         bool CheckPointsInSamePlane(const std::vector<int>& arIdxVec, const std::vector<vector_t>& arData) const;

      private:
         //! Data to find the convex hull for
         std::vector<vector_t> mData;

         //! The actual convex hull
         polytope_t mPolytope;
   };
}

#endif /* CONVEXHULL_DECL_HPP */