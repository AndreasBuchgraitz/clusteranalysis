#ifndef STATISTICS_DECL_HPP
#define STATISTICS_DECL_HPP

#include<vector>

#include "Concepts.hpp"

// Forward declarations
namespace Math
{
   template<FloatingPointContainer CONT, Distribution<CONT> DIST> class MaximumLogLikelihood;
}

namespace Math
{
   //!@{ Getters for Anderson-Darling critical values
   double GetAndersonDarlingUpperTailCriticalValue(const double aSignificanceLevel);
   double GetAndersonDarlingLowerTailCriticalValue(const double aSignificanceLevel);
   //!@}

   // Function to compute the Kolmogorov-Smirnov statistic for a given vector of MaximumLogLikelihoods
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename CONT::value_type> ComputeKolmogorovSmirnov
      (  const std::vector<MaximumLogLikelihood<CONT, DIST>>& aMaxLikelihoods
      );

   // Function to compute the Anderson-Darling statistic for a given vector of MaximumLogLikelihoods
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename CONT::value_type> ComputeAndersonDarlingByIntegral
      (  const std::vector<MaximumLogLikelihood<CONT, DIST>>& aMaxLikelihoods
      );

   // Function to compute the Anderson-Darling statistic for a given vector of MaximumLogLikelihoods
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   std::vector<typename CONT::value_type> ComputeAndersonDarlingBySummation
      (  const std::vector<MaximumLogLikelihood<CONT, DIST>>& aMaxLikelihoods
      );
   
   template<FloatingPoint T>
   std::vector<bool> CompareAndersonDarlingStatistics
      (  const std::vector<T>& aVec
      );
}

#endif /* STATISTICS_DECL_HPP */