/********************************************************************
 *
 ********************************************************************/

#ifndef MAXIMUMLOGLIKELIHOODOPTIMISER_DECL_HPP
#define MAXIMUMLOGLIKELIHOODOPTIMISER_DECL_HPP

#include "Concepts.hpp"
#include "LBFGSB.h"

// Forward declarations
namespace Math
{
   template<FloatingPointContainer CONT, Distribution<CONT> DIST> class MaximumLogLikelihood;
}

namespace Math
{
   /********************************************************************
    * Class for performing optimising maximum loglikelihood using
    * LBFGSB++.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      ,  Distribution<CONT> DIST
      >
   class MaximumLogLikelihoodOptimiser
   {
      public:
         using value_type = CONT::value_type;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;
         using mat_type = Eigen::Matrix<value_type, Eigen::Dynamic, Eigen::Dynamic>;

      public:
         //!@{ Constructors
         MaximumLogLikelihoodOptimiser() = delete;
         MaximumLogLikelihoodOptimiser
            (  const CONT arData
            ,  const int aNumDistributions = 1
            ,  const value_type aThr = 1.0E-5 // This is LBFGS++ default
            );
         //!@}

         //!@{ GETTERS
         int GetNumIter() const { return mNumIter; }
         const MaximumLogLikelihood<CONT, DIST>& GetMaxLogLikelihood() const { return mMaxLogLikelihood; }
         //!@}

         //!@{
         bool Converged() const { return mConverged; }
         //!@}

      private:
         MaximumLogLikelihood<CONT, DIST> mMaxLogLikelihood;

         //! Used number of iterations
         int mNumIter = 0;

         //!
         bool mConverged = false;
   };
}

#endif /* MAXIMUMLOGLIKELIHOODOPTIMISER_DECL_HPP */