/********************************************************************
 *
 ********************************************************************/

#ifndef PLANE3D_HPP
#define PLANE3D_HPP

#include "MathAndUtil/Plane3D_Decl.hpp"

#include "MathAndUtil/Plane3D_Impl.hpp"

#endif /* PLANE3D_HPP */