/********************************************************************
 *
 ********************************************************************/

#ifndef QUADRATUREINTEGRATION_DECL_HPP
#define QUADRATUREINTEGRATION_DECL_HPP

#include "Eigen_wrapper.hpp"

#include "MathAndUtil/Enums.hpp"
#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Performs numerical integration based on Dmitri Fedorovs lecture
    * notes.
    * 
    * LOOK AT TABLE 7.3 IN NOTES !!!
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   class QuadratureIntegration
   {
      public:
         using value_type = VALUE_TYPE;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;
         using mat_type = Eigen::Matrix<value_type, Eigen::Dynamic, Eigen::Dynamic>;

      public:
         //!@{ Constructors
         QuadratureIntegration() = delete;
         QuadratureIntegration
            (  FUNC aFunc
            ,  const value_type aThr
            ,  const value_type aLowerLimit
            ,  const value_type aUpperLimit
            );
         //!@}

         //!@{ GETTERS
         value_type GetIntegral() const { return mIntegral; }
         //!@}

      private:
         //! Lambda for the function we want to integrate
         FUNC mFunc;

         //! Threshold for converged integration
         value_type mRelThr;

         //!
         value_type mLowerLimit{0};
         value_type mUpperLimit{0};

         //!
         detail::IntegralLimitTransformation mTransformedLimits = detail::IntegralLimitTransformation::NoInf;

         //!
         value_type mIntegral{0};

         // Set the weights used for the high-order quadrature
         constexpr static value_type w1 =  4738.0 / 19845.0;
         constexpr static value_type w2 =   -59.0 /   567.0;
         constexpr static value_type w3 =  5869.0 / 13230.0;
         constexpr static value_type w4 =   -74.0 /   945.0;
         constexpr static value_type w5 = w4;
         constexpr static value_type w6 = w3;
         constexpr static value_type w7 = w2;
         constexpr static value_type w8 = w1;

         // Set the weights used for the low-order quadrature
         constexpr static value_type v1 = 208.0 / 945.0;
         constexpr static value_type v2 =  -7.0 / 135.0;
         constexpr static value_type v3 = 209.0 / 630.0;
         constexpr static value_type v4 =           0.0;
         constexpr static value_type v5 = v4;
         constexpr static value_type v6 = v3;
         constexpr static value_type v7 = v2;
         constexpr static value_type v8 = v1;

      private:
         //!@{ Different integration quadratures.
         //! Wrapper for the integration and limit transformations.
         value_type Integrate() const;

         //!
         value_type ComputeFunctionValue(const value_type aVal) const;

         //!
         value_type Open8PointQuad
            (  const value_type aLowerLimit
            ,  const value_type aUpperLimit
            ,  const value_type aRelThr
            ,  value_type f2 = std::numeric_limits<value_type>::quiet_NaN()
            ,  value_type f3 = std::numeric_limits<value_type>::quiet_NaN()
            ,  value_type f6 = std::numeric_limits<value_type>::quiet_NaN()
            ,  value_type f7 = std::numeric_limits<value_type>::quiet_NaN()
            )  const;
         //!@}
   };
}

#endif /* QUADRATUREINTEGRATION_DECL_HPP */