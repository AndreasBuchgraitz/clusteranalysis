/********************************************************************
 * This file contains different math functionalites.
 ********************************************************************/

#ifndef MATH_IMPL_HPP
#define MATH_IMPL_HPP

#include<iostream>
#include<cmath>
#include "Eigen_wrapper.hpp"

namespace Math
{
   /********************************************************************
    * Computes the sign of a number
    * 
    * @return
    *    -1 if negative, 0 if val is 0, and 1 is positive.
    ********************************************************************/
   template <typename T> int GetSign(T val)
   {
      return (T(0) < val) - (val < T(0));
   } 

   /********************************************************************
    * Computes the std::acos of the input, but can take care of small
    * numerical noise which would otherwise result in NaN.
    ********************************************************************/
   template<typename T>
   T ArcCos(T aVal)
   {
      if (  aVal > 1
         && (aVal - 1) < 1.0E-12
         )
      {
         return std::acos(T(1));
      }
      else if  (  aVal < -1
               && (aVal + 1) < 1.0E-12
               )
      {
         return std::acos(T(-1));
      }

      // If arg is out of range and not caught above it is an error
      if (aVal > 1 || aVal < -1)
      {
         std::cerr << "Argument for std::acos("<<aVal<<") is out of bounds and will lead to NaN. I stop here !" << std::endl;
         exit(1);
      }
      else
      {
         return std::acos(aVal);
      }
   }

   /********************************************************************
    * Computes the Inverse of the error function. Only well defined
    * for input in the open interval (-1, 1).
    * 
    * Based on series expansion from wiki:
    * https://en.wikipedia.org/wiki/Error_function#Inverse_functions
    ********************************************************************/
   template<FloatingPoint T> T InvErr(const T aVal)
   {
      const T contrib_1 = aVal;
      const T contrib_2 = std::numbers::pi_v<T> * std::pow(aVal, 3) / 12.0;
      const T contrib_3 = 7 * std::pow(std::numbers::pi_v<T>, 2) * std::pow(aVal, 5) / 480.0;
      const T contrib_4 = 127 * std::pow(std::numbers::pi_v<T>, 3) * std::pow(aVal, 7) / 40320.0;
      const T contrib_5 = 4369 * std::pow(std::numbers::pi_v<T>, 4) * std::pow(aVal, 9) / 5806080.0;
      const T contrib_6 = 34807 * std::pow(std::numbers::pi_v<T>, 5) * std::pow(aVal, 11) / 182476800.0;

      return std::sqrt(std::numbers::pi_v<T>) / 2.0 * (contrib_1 + contrib_2 + contrib_3 + contrib_4 + contrib_5 + contrib_6);
   }

   /********************************************************************
    * Computes Centroid of
    ********************************************************************/
   template<RowVectorIter ITER>
   Eigen::RowVectorXd Centroid(ITER aItBegin, ITER aItEnd)
   {
      Eigen::RowVectorXd centroid = Eigen::RowVectorXd::Zero(aItBegin->size());

      // We add all coordinates
      for (auto it = aItBegin; it != aItEnd; it++)
      {
         centroid += *it;
      }

      // Scale by the number of points
      centroid /= std::distance(aItBegin, aItEnd);

      return centroid;
   }

   /********************************************************************
    * Computes binomial coefs.
    * https://www.geeksforgeeks.org/binomial-coefficient-dp-9/
    * 
    * MIGHT BE INEFFICIENT; SHOULD MAYBE TIME IT... OTHER SOLUTIONS IN SAME LINK SHOULD BE MORE EFFICIENT...
    ********************************************************************/
   template<typename T>
   T BinomialCoef(T n, T k)
   {
      // Base case 1
      if (k > n)
      {
         return 0;
      }
      else if (k == 0 || k == n) // base case 2
      {
         return 1;
      }
      else // recursion
      {
         return BinomailCoef(n - 1, k - 1) + BinomailCoef(n - 1, k);
      }
   }

   /********************************************************************
    * Computes Cross product of two eigen vectors
    ********************************************************************/
   template<typename EIG_VEC_TYPE>
   EIG_VEC_TYPE CrossProduct(const EIG_VEC_TYPE& arVec1, const EIG_VEC_TYPE& arVec2)
   {
      if (  arVec1.size() != arVec2.size()
         || arVec1.size() != 3
         )
      {
         std::cerr << "Mismatch in input vector sizes in CrossProduct !" << std::endl;
         std::cerr << "arVec1.size() = " << arVec1.size() << std::endl;
         std::cerr << "arVec2.size() = " << arVec2.size() << std::endl;
         exit(1);
      }

      EIG_VEC_TYPE res_vec;
      res_vec <<  arVec1(1)*arVec2(2) - arVec1(2)*arVec2(1)
               ,  arVec1(2)*arVec2(0) - arVec1(0)*arVec2(2)
               ,  arVec1(0)*arVec2(1) - arVec1(1)*arVec2(0)
               ;

      return res_vec;
   }

   /********************************************************************
    * Generate all unique n-tuples of the input data.
    ********************************************************************/
   template<int SIZE, typename DATA_CONT>
   void GenerateNTuples
      (  const DATA_CONT& arInpData
      ,  std::vector<DATA_CONT>& arOutData
      ,  DATA_CONT& arTempCont
      ,  int aInpDataIdx
      )
   {
      // Base case
      if (arTempCont.size() == SIZE)
      {
         arOutData.emplace_back(arTempCont);
         arTempCont.pop_back();
         return;
      }
      else
      {
         for (Uin i = aInpDataIdx; i < arInpData.size(); i++)
         {
            arTempCont.emplace_back(arInpData[i]);
            GenerateNTuples<SIZE>(arInpData, arOutData, arTempCont, i + 1);
         }
         arTempCont.pop_back();
      }
   }
}

#endif /* MATH_IMPL_HPP */