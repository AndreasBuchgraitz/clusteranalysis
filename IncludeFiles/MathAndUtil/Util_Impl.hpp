/********************************************************************
 * Implementations of templated util functions.
 ********************************************************************/

#ifndef UTIL_IMPL_HPP
#define UTIL_IMPL_HPP

#include<iostream>
#include<algorithm>

#include "CommonAliases.hpp"

namespace Util
{
   /**
    * Get a Vector from a given string
    **/
   template<typename T>
   std::vector<T> VectorFromString(const std::string& arString, const std::string& arSeperator)
   {
      std::vector<T> res_vec;
      std::string temp_str = "";
      std::string::const_iterator it;

      // Loop over all characters in aString.
      for (it = arString.begin(); it != arString.end(); ++it)
      {
         // This is unnecessary if aString has been 'trimmed'
         if (*it == '\n')
         {
            std::cout << "Hit the end of the line in VectorFromString" << std::endl;
            break;
         }

         // Create T value as string, if "*it" is not in the arSeperator string.
         if (arSeperator.find(*it) == std::string::npos)
         {
            temp_str += *it;
         }

         // If we hit space between T's or the end of string save number
         if (  ( arSeperator.find(*it) != std::string::npos || (it + 1) == arString.end() )
            && temp_str != ""
            )
         {
            if constexpr (std::is_same_v<T, std::string>)
            {
               res_vec.emplace_back(temp_str);
            }
            else
            {
               res_vec.emplace_back(NumFromString<T>(temp_str));
            }

            // Reset string
            temp_str = "";
         }
      }
      return res_vec;
   }

   /**
    * Creates an array from a string.
    **/
   template<typename T, std::size_t SIZE>
   std::array<T, SIZE> ArrayFromString
      (  const std::string& arString
      )
   {
      std::array<T, SIZE> res_array;
      std::string temp_str = "";
      std::string::const_iterator it;
      int array_index = 0;

      // Loop over all characters in aString.
      for (it = arString.begin(); it != arString.end(); ++it)
      {
         // This is unnecessary if aString has been 'trimmed'
         if (*it == '\n')
         {
            std::cout << "Hit the end of the line in VectorFromString" << std::endl;
            break;
         }
         // Create number as string
         if (*it != ' ')
         {
            temp_str += *it;
         }
         // If we hit space between numbers or the end of string save number
         if (  ( *it == ' ' || (it + 1) == arString.end() )
            && temp_str != ""
            )
         {
            res_array.at(array_index++) = NumFromString<T>(temp_str);
            temp_str = "";
         }
      }
      return res_array;
   }

   /********************************************************************
    * Converts string into Eigen::RowVector of type T and size SIZE.
    ********************************************************************/
   template<typename T, int SIZE>
   Eigen::Matrix<T, 1, SIZE> RowVectorFromString
      (  const std::string& arString
      ,  const std::string& arSeperator
      )
   {
      Eigen::Matrix<T, 1, SIZE> res_vec;

      std::string temp_str = "";
      std::string::const_iterator it;
      int array_index = 0;

      // Loop over all characters in aString.
      for (it = arString.begin(); it != arString.end(); ++it)
      {
         // This is unnecessary if aString has been 'trimmed'
         if (*it == '\n')
         {
            std::cout << "Hit the end of the line in VectorFromString" << std::endl;
            break;
         }

         // Create number as string
         if (arSeperator.find(*it) == std::string::npos)
         {
            temp_str += *it;
         }

         // If we hit space between numbers or the end of string save number
         if (  ( arSeperator.find(*it) != std::string::npos || (it + 1) == arString.end() )
            && temp_str != ""
            )
         {
            res_vec(array_index++) = NumFromString<T>(temp_str);
            temp_str = "";
         }
      }

      if (array_index != SIZE)
      {
         std::cerr << "Unexpected unmatched sizes in RowVectorFromString" << std::endl;
         exit(1);
      }

      return res_vec;
   }


   /********************************************************************
    * @param[in] arHeader
    *    The header line for the printed table
    * @param[in] arTableData
    *    The data to be printed for the table. operator[][] is [row][col]
    * @param[in] arPath
    *    Path to where the table should end up. This should include the
    *    filename and extension.
    ********************************************************************/
   template<typename T>
   void PrintTable
      (  const std::vector<std::string>& arHeader
      ,  const std::vector<std::vector<T>>& arTableData
      ,  const std::filesystem::path& arPath
      )
   {
      // Check if directories of path exists, if not create them
      if (!std::filesystem::is_directory(arPath.parent_path()))
      {
         std::filesystem::create_directories(arPath.parent_path());
      }

      std::fstream fs;
      fs.open(arPath, std::ios_base::out);
      fs << std::scientific;
      fs << std::left;

      // Print header
      for (const auto& s : arHeader)
      {
         fs << std::setw(30) << s;
      }
      fs << std::endl;

      // Print data
      for (const std::vector<T>& vec : arTableData)
      {
         for (const T& val : vec)
         {
            fs << std::setw(30) << val;
         }
         fs << std::endl;
      }

      fs.close();
   }

      /********************************************************************
    * @param[in] arHeader
    *    The header line for the printed vectors
    * @param[in] arTableData
    *    The data to be printed for the table.
    * @param[in] arPath
    *    Path to where the table should end up. This should include the
    *    filename and extension.
    ********************************************************************/
   template<typename T>
   void PrintColumnVectors
      (  const std::vector<std::string>& arHeader
      ,  const std::vector<std::vector<T>>& arTableData
      ,  const std::filesystem::path& arPath
      )
   {
      // Check if directories of path exists, if not create them
      if (!std::filesystem::is_directory(arPath.parent_path()))
      {
         std::filesystem::create_directories(arPath.parent_path());
      }

      std::size_t min_vec_size = std::numeric_limits<std::size_t>::max();
      for (const auto& vec : arTableData)
      {
         if (vec.size() < min_vec_size)
         {
            min_vec_size = vec.size();
         }
      }

      std::fstream fs;
      fs.open(arPath, std::ios_base::out);
      fs << std::scientific;
      fs << std::left;
      fs << std::setprecision(17);

      // Print header
      for (const auto& s : arHeader)
      {
         fs << std::setw(30) << s;
      }
      fs << std::endl;

      // Print data
      for (Uin i = 0; i < min_vec_size; i++)
      {
         for (Uin j = 0; j < arTableData.size(); j++)
         {
            fs << std::setw(30) << arTableData[j][i];
         }
         fs << std::endl;
      }

      fs.close();
   }

   /********************************************************************
    * 
    ********************************************************************/
   template<>
   inline int NumFromString(const std::string& arString)
   {
      return std::stoi(arString);
   }

   /********************************************************************
    * 
    ********************************************************************/
   template<>
   inline unsigned int NumFromString(const std::string& arString)
   {
      try
      {
         return std::stoul(arString);
      }
      catch(const std::invalid_argument& e)
      {
         std::cerr << "Caught std::invalid_argument in std::stoul (NumFromString) with input: " << arString << std::endl;
         exit(1);
      }
   }
   
   /********************************************************************
    * 
    ********************************************************************/
   template<>
   inline double NumFromString(const std::string& arString)
   {
      return std::stod(arString);
   }
}

#endif /* UTIL_IMPL_HPP */