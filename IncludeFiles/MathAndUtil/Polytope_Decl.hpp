/********************************************************************
 *
 ********************************************************************/

#ifndef POLYTOPE_DECL_HPP
#define POLYTOPE_DECL_HPP

#include<vector>
#include<set>
#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"
#include "MathAndUtil/Plane3D.hpp"

namespace Math
{
   /********************************************************************
    * Class defining a polytope from some input points and the faces
    * defined.
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   class Polytope
   {
      public:
         //!@{ ALIASES
         static constexpr int mDim = DIM;
         using value_t = T;
         using vector_t = Eigen::Matrix<T, 1, DIM>;
         //!@}

      public:
         //!@{ Constructors
         Polytope() = default;
         Polytope(const std::vector<vector_t>& arData);
         //!@}

         //! Find the points in arData which are outside of _this_
         std::pair<std::vector<int>, std::vector<int>> FindPointsOutsidePolytope
            (  const std::vector<vector_t>& arData
            )  const;


         //!@{ GETTERS
         const auto& GetVertices() const { return mVertices; }
         const auto& GetFaces() const { return mFaces; }
         const auto& GetCentroid() const { return mCentroid; }
         const auto GetNumFaces() const { return mFaces.size(); }
         //!@}

         //!@{ Comparison
         bool operator==(const Polytope& arOther) const { return this->GetVertices() == arOther.GetVertices(); }
         bool operator!=(const Polytope& arOther) const { return !(this->operator==(arOther)); }
         //!@}

         //! Find a set of points for each face
         std::vector<std::set<int>> DividePointsIntoSetsForFaces
            (  const std::vector<vector_t>& arData
            )  const;

      private:
         void CleanFaceVerticesFromNTuples();

      private:
         //! Data points defining the vertices of the polytope
         std::vector<vector_t> mVertices;

         //! Faces defined as 3-dimensional planes
         std::vector<Plane3D<value_t>> mFaces;

         //! Centroid of the polytope
         Eigen::RowVectorXd mCentroid;
   };
}

#endif /* POLYTOPE_DECL_HPP */