/********************************************************************
 *
 ********************************************************************/

#ifndef MONTECARLOINTEGRATION_DECL_HPP
#define MONTECARLOINTEGRATION_DECL_HPP

#include<random>
#include<tuple>
#include "Eigen_wrapper.hpp"

#include "MathAndUtil/Enums.hpp"
#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Performs monte carlo numerical integration based on Dmitri
    * Fedorovs lecture notes.
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   class MonteCarloIntegration
   {
      public:
         using value_type = VALUE_TYPE;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;
         using mat_type = Eigen::Matrix<value_type, Eigen::Dynamic, Eigen::Dynamic>;

      public:
         //!@{ Constructors
         MonteCarloIntegration() = delete;
         MonteCarloIntegration
            (  FUNC aFunc
            ,  value_type aThr
            ,  int aNumPoints
            ,  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            );
         //!@}

         //!@{ GETTERS
         value_type GetIntegral() const { return mResult.first; }
         value_type GetIntegralError() const { return mResult.second; }
         int GetNumRecursiveCalls() const { return mNumStratifiedIntegrationCalls; }
         //!@}

      private:
         //! Threshold for converged integration
         value_type mRelThr;

         //! Lambda for the function we want to integrate
         FUNC mFunc;

         //! Number of points used in ComputeMeanAndError
         value_type mNumPoints;

         //! Vectors of limits
         vec_type mLowerLimits;
         vec_type mUpperLimits;

         //! Vector keeping track of which limits have been transformed
         std::vector<detail::IntegralLimitTransformation> mTransformedLimits;

         // Container for the final result
         std::pair<value_type, value_type> mResult;

         // Count the number of (recursive) calls to StratifiedIntegration
         mutable int mNumStratifiedIntegrationCalls{0};

      private:
         //! Wrapper for the mFunc (to handle limit transformations)
         value_type ComputeFunctionValue(const vec_type& aVec) const;

         //! Call the recursive stratified routine. Returns pair: [integral, error estimate]
         std::pair<value_type, value_type> StratifiedIntegration
            (  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            ,  const value_type aAbsThr
            ,  const int aNumPoints
            )  const;

         //!
         std::pair<value_type, value_type> PlainIntegration
            (  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            ,  const int aNumPoints
            )  const;

         //!
         value_type ComputeVolume
            (  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            )  const;

         //!
         std::pair<value_type, value_type> ComputeMeanAndError
            (  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            ,  const int aNumPoints
            )  const;

         //!
         vec_type GenerateRandomPoint
            (  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            )  const;

         //!
         std::tuple<vec_type, vec_type, int, int> SubdivideLimits
            (  const vec_type& aLowerLimits
            ,  const vec_type& aUpperLimits
            ,  const int aNumPoints
            )  const;
   };
}

#endif /* MONTECARLOINTEGRATION_DECL_HPP */