/********************************************************************
 *
 ********************************************************************/

#ifndef NORMALDISTRIBUTION_DECL_HPP
#define NORMALDISTRIBUTION_DECL_HPP

#include "Concepts.hpp"
#include "MathAndUtil/DistributionBase.hpp"

namespace Math
{
   /********************************************************************
    * Class holding all parameters for a the normal distribution as
    * well as certain member functions unique for this distribution.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   class NormalDistribution : public DistributionBase<CONT>
   {
      public:
         using value_type = CONT::value_type;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;

      public:
         //!@{ Constructors
         NormalDistribution() = delete;
         NormalDistribution(const CONT& arData, const int aNumDistributions);
         //!@}

         //!@{ Analyze the data
         bool HasOutliers() const;
         //!@}

         //!@{ Queries
         value_type ComputeMean(const CONT& arData) const;
         value_type ComputeStandardDeviation(const CONT& arData) const;
         //!@}

         //!@{ Statistics
         virtual value_type TheoreticalCDF(const value_type aVal) const override;
         virtual value_type EvaluateProbDensFunc(const value_type aVal, const vec_type& arParams) const override;
         //!@}

         //!@{ Optimisation
         virtual value_type ComputeLogLikelihood(const vec_type& arParams) const override;
         virtual void ComputeLogLikelihoodGradient(const vec_type& arParams, vec_type& arGradient) const override;
         virtual std::pair<vec_type, vec_type> GetOptimisationBounds() const override;
         //!@}

      protected:
         //!@{ Optimisation
         virtual vec_type GenerateStartingGuess(const int aNumDistributions) const override;
         //!@}


      private:
         //!@{ Local helper functions
         value_type ComputeExponential
            (  const value_type arDataPoint
            ,  const value_type arMean
            ,  const value_type arStdDevi
            )  const;
         //!@}
   };
}

#endif /* NORMALDISTRIBUTION_DECL_HPP */