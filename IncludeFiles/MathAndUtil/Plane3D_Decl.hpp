/********************************************************************
 *
 ********************************************************************/

#ifndef PLANE3D_DECL_HPP
#define PLANE3D_DECL_HPP

#include<array>
#include<vector>
#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Class defining a 3 dimensional plane by 3 points in space.
    * Computes the plane equation, and contains methods for evaluating
    * an arbitrary point in this plane as well as checking if point lies
    * in the plane.
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   class Plane3D
   {
      public:
         //!@{ ALIASES
         using value_t = T;
         using vector_t = Eigen::Matrix<T, 1, 3>;
         //!@}

      public:
         //!@{ Constructors
         Plane3D() = default;
         Plane3D(const std::array<vector_t, 3>& arData);
         Plane3D(const std::array<vector_t, 3>&& arData);
         //!@}

         //!@{ GETTERS
         const auto& GetDataPoints() const { return mDataPoints; }
         const auto& GetEquation() const { return mPlaneEquation; }
         //!@}

         //!@{ Comparison
         bool operator==(const Plane3D& arOther) const;
         bool operator!=(const Plane3D& arOther) const { return !(this->operator==(arOther)); }
         //!@}

         //!
         int ComputeWhichSideOfPlanePointIs
            (  const vector_t& arPoint
            )  const;

         //!
         value_t DistanceFromPlaneToPoint
            (  const vector_t& arPoint
            )  const;

         //!
         inline value_t EvalPlaneForPoint
            (  const vector_t& arPoint
            )  const;
         
      private:
         void Initialize();
      
      private:
         //! The 3 data points defining the plane
         std::array<vector_t, 3> mDataPoints;

         //! Plane equation (a, b, c, d) in a*x + b*y + c*z + d = 0
         std::array<value_t, 4> mPlaneEquation;
   };

}
//!
template
   <  FloatingPoint T
   >
std::ostream& operator<<(std::ostream& arOut, const Math::Plane3D<T>& arPlane3D);

#endif /* PLANE3D_DECL_HPP */