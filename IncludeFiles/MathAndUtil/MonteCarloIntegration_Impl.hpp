/********************************************************************
 *
 ********************************************************************/

#ifndef MONTECARLOINTEGRATION_IMPL_HPP
#define MONTECARLOINTEGRATION_IMPL_HPP

#include<utility>
#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"
#include "MathAndUtil/Statistics.hpp"
#include "MathAndUtil/Random.hpp"

namespace Math
{
   /********************************************************************
    * Constructor
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::MonteCarloIntegration
      (  FUNC aFunc
      ,  value_type aThr
      ,  int aNumPoints
      ,  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      )
      :  mRelThr(aThr)
      ,  mFunc(aFunc)
      ,  mNumPoints(aNumPoints)
      ,  mLowerLimits(aLowerLimits)
      ,  mUpperLimits(aUpperLimits)
      ,  mTransformedLimits(aLowerLimits.size(), detail::IntegralLimitTransformation::NoInf)
   {
      // Sanity check
      if (mLowerLimits.size() != mUpperLimits.size())
      {
         std::cerr << "Mismatch in dimensions in limit vectors" << std::endl;
         exit(1);
      }

      // Make sure we have properly transformed possible infinite integration limits.
      for (Uin i = 0; i < mLowerLimits.size(); i++)
      {
         // Transformations are taken from: https://en.wikipedia.org/wiki/Numerical_integration#Integrals_over_infinite_intervals
         if (  mLowerLimits(i) == -std::numeric_limits<value_type>::infinity()
            && mUpperLimits(i) ==  std::numeric_limits<value_type>::infinity()
            )
         {
            // If both limits are (+/-)infinity: [-inf, inf] -> [-1, +1]
            // And f(x)dx -> f(t / (1-t^2)) * (1 + t^2) / (1 - t^2)^2 dt
            mLowerLimits(i) = -1;
            mUpperLimits(i) = +1;

            mTransformedLimits[i] = detail::IntegralLimitTransformation::DoubleInf;
         }
         else if  (  mLowerLimits(i) == -std::numeric_limits<value_type>::infinity()
                  && mUpperLimits(i) !=  std::numeric_limits<value_type>::infinity()
                  )
         {
            // If only lower limit is -inf: [-inf, a] -> [0, 1]
            // And f(x)dx -> f(a - (1-t)/t) 1/t^2 dt
            mLowerLimits(i) = 0;
            mUpperLimits(i) = 1;

            mTransformedLimits[i] = detail::IntegralLimitTransformation::LowerInf;
         }
         else if  (  mLowerLimits(i) != -std::numeric_limits<value_type>::infinity()
                  && mUpperLimits(i) ==  std::numeric_limits<value_type>::infinity()
                  )
         {
            // If only upper limit is +inf: [a, inf] -> [0, 1]
            // And f(x)dx -> f(a + t/(1-t)) 1/(1 - t)^2 dt
            mLowerLimits(i) = 0;
            mUpperLimits(i) = 1;

            mTransformedLimits[i] = detail::IntegralLimitTransformation::UpperInf;
         }
      }

      // Perform actual integration
      mResult = StratifiedIntegration(mLowerLimits, mUpperLimits, mRelThr, mNumPoints);
   }

   /********************************************************************
    * Computes the function value, handles the cases where limits have
    * been transformed to handle infinites.
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type MonteCarloIntegration<FUNC, VALUE_TYPE>::ComputeFunctionValue
      (  const vec_type& aVec
      )  const
   {
      vec_type transformed_input = aVec;

      value_type factor(1.0);

      // Surprisingly this loop is ~25 times faster than using eigen slicing... Must be because small vectors somehow...
      for (Uin i = 0; i < mTransformedLimits.size(); i++)
      {
         // Save input variable
         value_type t = transformed_input(i);

         // Apply transformations
         if (mTransformedLimits[i] == detail::IntegralLimitTransformation::DoubleInf)
         {
            transformed_input(i) = t / (1 - t*t);

            factor *= (1 + t*t) / std::pow(1 - t*t, 2);
         }
         else if (mTransformedLimits[i] == detail::IntegralLimitTransformation::LowerInf)
         {
            transformed_input(i) = mUpperLimits(i) - (1 - t) / t;

            factor /= t*t;
         }
         else if (mTransformedLimits[i] == detail::IntegralLimitTransformation::UpperInf)
         {
            transformed_input(i) = mLowerLimits(i) + t / (1 - t);

            factor /= std::pow(1 - t, 2);
         }
      }

      return mFunc(transformed_input) * factor;
   }

   /********************************************************************
    * Do the actual integration based on the settings setup in the c-tor
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   std::pair
      <  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type
      ,  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::StratifiedIntegration
      (  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      ,  const value_type aAbsThr
      ,  const int aNumPoints
      )  const
   {
      // Increment counter for recursive calls.
      mNumStratifiedIntegrationCalls++;

      const auto [integral, integral_error] = PlainIntegration(aLowerLimits, aUpperLimits, aNumPoints);

      if (  integral_error < aAbsThr + mRelThr * std::abs(integral)
         || aNumPoints < 20
         )
      {
         return std::make_pair(integral, integral_error);
      }
      else
      {
         const auto [new_lower_limits, new_upper_limits, points_for_new_upper_limits, points_for_new_lower_limits] = SubdivideLimits(aLowerLimits, aUpperLimits, aNumPoints);

         value_type abs_thr = aAbsThr / std::sqrt(2.0);
         const auto [integral_1, integral_error_1] = StratifiedIntegration(aLowerLimits, new_upper_limits, abs_thr, points_for_new_upper_limits);
         const auto [integral_2, integral_error_2] = StratifiedIntegration(new_lower_limits, aUpperLimits, abs_thr, points_for_new_lower_limits);

         value_type grand_average = integral_1 + integral_2;
         value_type grand_error = std::sqrt(std::pow(integral_error_1, 2) + std::pow(integral_error_2, 2));

         return std::make_pair(grand_average, grand_error);
      }
   }

   /********************************************************************
    * Perfrom MC integration in plain simple way...
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   std::pair
      <  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type
      ,  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::PlainIntegration
      (  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      ,  const int aNumPoints
      )  const
   {
      value_type volume = ComputeVolume(aLowerLimits, aUpperLimits);

      const auto [mean, err] = ComputeMeanAndError(aLowerLimits, aUpperLimits, aNumPoints);

      return std::make_pair(volume * mean, volume * err);
   }

   /********************************************************************
    * Computes the volume of the integration box
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type MonteCarloIntegration<FUNC, VALUE_TYPE>::ComputeVolume
      (  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      )  const
   {
      return std::transform_reduce
               (  aLowerLimits.begin(), aLowerLimits.end()
               ,  aUpperLimits.begin()
               ,  value_type(1)
               ,  std::multiplies<>()
               ,  [](const value_type aLower, const value_type aUpper)
                  {
                     return aUpper - aLower;
                  }
               );
   }

   /********************************************************************
    * Computes the volume of the integration box
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   std::pair
      <  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type
      ,  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::value_type
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::ComputeMeanAndError
      (  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      ,  const int aNumPoints
      )  const
   {
      value_type sum(0);
      value_type sum_squared(0);

      for (int i = 0; i < aNumPoints; i++)
      {
         value_type approximate_integral = ComputeFunctionValue(GenerateRandomPoint(aLowerLimits, aUpperLimits));
         sum += approximate_integral;
         sum_squared += approximate_integral * approximate_integral;
      }

      value_type mean_integral = sum / value_type(aNumPoints);

      value_type integral_error = std::sqrt(sum_squared / aNumPoints - mean_integral * mean_integral) / std::sqrt(aNumPoints);
      
      if (std::isnan(integral_error))
      {
         // sum_squared is most likely inf so we just set integral_error to inf instead of nan
         std::cerr << "I get nan in ComputeMeanAndError !!!" << std::endl;
         integral_error = std::numeric_limits<value_type>::infinity();
      }

      return std::make_pair(mean_integral, integral_error);
   }

   /********************************************************************
    * Generates a random vector within the given integration limits.
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::vec_type MonteCarloIntegration<FUNC, VALUE_TYPE>::GenerateRandomPoint
      (  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      )  const
   {
      vec_type rand_vec = vec_type::Zero(aLowerLimits.size());

      for (int i = 0; i < rand_vec.size(); i++)
      {
         rand_vec(i) = aLowerLimits(i) + Math::GetRandomNumber<value_type>(0, 1) * (aUpperLimits(i) - aLowerLimits(i));
      }

      return rand_vec;
   }

   /********************************************************************
    * Subdivides the most suitable dimension.
    * 
    * When estimating which dimension to subdivide along we reduce the
    * number of points used for faster evaluation. We never use below
    * 20 points.
    ********************************************************************/
   template
      <  typename FUNC
      ,  FloatingPoint VALUE_TYPE
      >
   std::tuple
      <  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::vec_type
      ,  typename MonteCarloIntegration<FUNC, VALUE_TYPE>::vec_type
      ,  int
      ,  int
      >
   MonteCarloIntegration<FUNC, VALUE_TYPE>::SubdivideLimits
      (  const vec_type& aLowerLimits
      ,  const vec_type& aUpperLimits
      ,  const int aNumPoints
      )  const
   {
      value_type largest_sub_variance(0);

      int num_points_1(0), num_points_2(0);
      
      // Compute small and larger number of points to a max of 10000
      int large_num_points = std::max(int(aNumPoints * 0.9), 20);
      int small_num_points = std::max(int(aNumPoints * 0.4), 20);

      std::pair<vec_type, vec_type> result_vec;
      
      // Make a copy of the limit vectors so we can change these one dimension at a time.
      vec_type lower_limit_copy(aLowerLimits), upper_limit_copy(aUpperLimits);

      for (Uin i = 0; i < aLowerLimits.size(); i++)
      {
         // Sub-divide dimension i
         lower_limit_copy(i) = (aLowerLimits(i) + aUpperLimits(i)) / 2;
         upper_limit_copy(i) = (aLowerLimits(i) + aUpperLimits(i)) / 2;

         // Estimate mean and variance using the small number of points
         const auto [mean1, var1] = ComputeMeanAndError(aLowerLimits, upper_limit_copy, small_num_points);
         const auto [mean2, var2] = ComputeMeanAndError(lower_limit_copy, aUpperLimits, small_num_points);

         // If this division leads to larger variance we choose to sub-divide this specific dimension.
         if (var1 > largest_sub_variance || var2 > largest_sub_variance)
         {
            largest_sub_variance = var1 > var2 ? var1 : var2;
            result_vec = std::make_pair(lower_limit_copy, upper_limit_copy);

            auto check_val = std::abs(var1 - var2)/std::max(var1, var2);
            if (std::isnan(check_val))
            {
               std::cerr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
               std::cerr << "I found nan !" << std::endl;
               std::cerr << "var1 = " << var1 << std::endl;
               std::cerr << "var2 = " << var2 << std::endl;
               std::cerr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
            }
            std::cout << "std::abs(var1 - var2)/std::max(var1, var2) = " << check_val << std::endl;
            if (  check_val < 1.0E-2
               || std::isnan(check_val)
               )
            {
               // If the two variance are close to each other use many points in both subdivision
               num_points_1 = large_num_points;
               num_points_2 = large_num_points;
            }
            else
            {
               // Use many points in the division leading to largest variance.
               num_points_1 = var1 > var2 ? large_num_points : small_num_points;
               num_points_2 = var2 > var1 ? large_num_points : small_num_points;
            }
         }

         // Change the copy back to original values before next iteration
         lower_limit_copy(i) = aLowerLimits(i);
         upper_limit_copy(i) = aUpperLimits(i);
      }

      // If for some reason nothing happens choose a random direction.
      if (largest_sub_variance == 0.0)
      {
         int i = Math::GetRandomNumber<int>(0, aLowerLimits.size() - 1);
         // Sub-divide dimension i
         lower_limit_copy(i) = (aLowerLimits(i) + aUpperLimits(i)) / 2;
         upper_limit_copy(i) = (aLowerLimits(i) + aUpperLimits(i)) / 2;
         num_points_1 = small_num_points;
         num_points_2 = small_num_points;

         result_vec = std::make_pair(lower_limit_copy, upper_limit_copy);
      }

      return std::make_tuple(result_vec.first, result_vec.second, num_points_1, num_points_2);
   }
}

#endif /* MONTECARLOINTEGRATION_IMPL_HPP */