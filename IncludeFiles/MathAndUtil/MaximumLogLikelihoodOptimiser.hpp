/********************************************************************
 *
 ********************************************************************/

#ifndef MAXIMUMLOGLIKELIHOODOPTIMISER_HPP
#define MAXIMUMLOGLIKELIHOODOPTIMISER_HPP

#include "MaximumLogLikelihoodOptimiser_Decl.hpp"

#include "MaximumLogLikelihoodOptimiser_Impl.hpp"

#endif /* MAXIMUMLOGLIKELIHOODOPTIMISER_HPP */