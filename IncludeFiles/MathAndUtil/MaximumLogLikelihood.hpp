/********************************************************************
 *
 ********************************************************************/

#ifndef MAXIMUMLOGLIKELIHOOD_HPP
#define MAXIMUMLOGLIKELIHOOD_HPP

#include "MaximumLogLikelihood_Decl.hpp"

#include "MaximumLogLikelihood_Impl.hpp"

#endif /* MAXIMUMLOGLIKELIHOOD_HPP */