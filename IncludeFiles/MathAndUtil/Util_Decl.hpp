/********************************************************************
 *
 ********************************************************************/

#ifndef UTIL_DECL_HPP
#define UTIL_DECL_HPP

#include<vector>
#include<array>
#include<string>
#include "Eigen_wrapper.hpp"
#include<fstream>
#include<filesystem>

namespace Util
{
   //! Converts string to std::vector
   template<typename T>
   std::vector<T> VectorFromString(const std::string& arString, const std::string& arSeperator = " ");

   //! Converts string to std::array
   template<typename T, std::size_t SIZE>
   std::array<T, SIZE> ArrayFromString(const std::string& arString);

   //! Converts string to Eigen::RowVector of const size
   template<typename T, int SIZE>
   Eigen::Matrix<T, 1, SIZE> RowVectorFromString
      (  const std::string& arString
      ,  const std::string& arSeperator = " "
      );

   //! Prints table
   template<typename T>
   void PrintTable
      (  const std::vector<std::string>& arHeader
      ,  const std::vector<std::vector<T>>& arTableData
      ,  const std::filesystem::path& arPath
      );

   //! Prints table
   template<typename T>
   void PrintColumnVectors
      (  const std::vector<std::string>& arHeader
      ,  const std::vector<std::vector<T>>& arTableData
      ,  const std::filesystem::path& arPath
      );

   //!
   std::string Trim(const std::string& str, const std::string& whitespace = " \t");

   //! Removes anything which is not an alphabetical letter from the string
   std::string TrimAtomName(const std::string& arStr);

   //!
   std::string RemoveWhiteSpace(const std::string& arStr);

   //!
   std::string ConvertToUpperCase(const std::string& arStr);

   //!
   std::istream& GetLine(std::fstream& is, std::string& str, const bool aNoRecursive = false);

   //! Convert string into some kind of number
   template<typename T>
   T NumFromString(const std::string& arString);

   //! Check if a string is a number
   bool IsNumber(const std::string& arString);

   //! Remove all non-alphabetical characters from a string
   std::string RemoveNonAlpha(const std::string& arString);
}

#endif /* UTIL_HPP */