/********************************************************************
 *
 ********************************************************************/

#ifndef POLYTOPE_IMPL_HPP
#define POLYTOPE_IMPL_HPP

#include "MathAndUtil/Polytope.hpp"
#include "MathAndUtil/Math.hpp"

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   Polytope<DIM, T>::Polytope
      (  const std::vector<vector_t>& arData
      )
      :  mVertices(arData)
      ,  mCentroid(Centroid(mVertices.begin(), mVertices.end()))
   {
      if (mVertices.empty())
      {
         return;
      }

      // Create the mFaceVertices container
      Uin num_faces = mVertices.size() * (mVertices.size() - 1) * (mVertices.size() - 2) / 6;
      std::vector<std::vector<vector_t>> face_vertices;
      face_vertices.reserve(num_faces);

      // Generate all possible (3-tuple) combinations of vertices as a guess for the faces
      std::vector<vector_t> temp_vec;
      Math::GenerateNTuples<3>(mVertices, face_vertices, temp_vec, 0);
      
      // Create the mFacePlanes container
      for (const auto& face : face_vertices)
      {
         mFaces.emplace_back(std::array<vector_t, 3>{face[0], face[1], face[2]});
      }

      // Remove all N-tuples not corresponding to an actual face.
      this->CleanFaceVerticesFromNTuples();
   }

   /********************************************************************
    * https://en.wikipedia.org/wiki/Euclidean_planes_in_three-dimensional_space
    * 
    * Loop over all points in ReducedData and check if it is outside one of the faces
    * defined by the polytope. If it is on the other side of a face than the centroid, then
    * it is outside the polytope all together and should be kept for later iterations.
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   std::pair<std::vector<int>, std::vector<int>> Polytope<DIM, T>::FindPointsOutsidePolytope
      (  const std::vector<vector_t>& arReducedData
      )  const
   {
      // Initialize some result containers.
      std::vector<int> points_outside_polytope;
      std::vector<int> points_inside_polytope;

      // Loop over data
      for (Uin i_dat = 0; i_dat < arReducedData.size(); i_dat++)
      {
         // Loop over faces to find points outside of the polytope
         for (const auto& face : this->GetFaces())
         {
            int centroid_sign = face.ComputeWhichSideOfPlanePointIs(this->GetCentroid());
            int point_sign = face.ComputeWhichSideOfPlanePointIs(arReducedData[i_dat]);

            // If different sign, then the points are on different sides of plane.
            if (  centroid_sign != point_sign
               && point_sign != 0
               )
            {
               points_outside_polytope.emplace_back(i_dat);
               break;
            }
         }

         // If i_dat is not outside of any faces, then it must be inside the polytope
         if (  points_outside_polytope.empty()
            || points_outside_polytope.back() != int(i_dat)
            )
         {
            points_inside_polytope.emplace_back(i_dat);
         }
      }

      // Remove possible duplicate idx (1 idx might be considered outside/inside of multiple faces) from result vectors.
      std::sort(points_outside_polytope.begin(), points_outside_polytope.end());
      auto last_outside = std::unique(points_outside_polytope.begin(), points_outside_polytope.end());
      points_outside_polytope.erase(last_outside, points_outside_polytope.end());

      std::sort(points_inside_polytope.begin(), points_inside_polytope.end());
      auto last_inside = std::unique(points_inside_polytope.begin(), points_inside_polytope.end());
      points_inside_polytope.erase(last_inside, points_inside_polytope.end());

      return std::make_pair(points_outside_polytope, points_inside_polytope);
   }

   /********************************************************************
    * Assumes arData are a collection of points strictly outside of
    * _this_ polytope.
    * 
    * Loop over face(plane)s:
    *    Loop over data:
    *       Assign to set if data point is on other side of centroid.
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   std::vector<std::set<int>> Polytope<DIM, T>::DividePointsIntoSetsForFaces
      (  const std::vector<vector_t>& arData
      )  const
   {
      // Result and temporary containers.
      std::vector<std::set<int>> face_sets;
      face_sets.reserve(this->GetNumFaces());
      std::set<int> used_idx;

      // Loop over faces
      for (const auto& face : this->GetFaces())
      {
         // Get sign for centroid of the face
         int centroid_sign = face.ComputeWhichSideOfPlanePointIs(this->GetCentroid());

         // Compute the index set outside the given face.
         std::set<int> idx_set;
         for (Uin i_dat = 0; i_dat < arData.size(); i_dat++)
         {
            int point_sign = face.ComputeWhichSideOfPlanePointIs(arData[i_dat]);

            if (  point_sign != centroid_sign
               && point_sign != 0
               && !used_idx.contains(i_dat)
               )
            {
               idx_set.emplace(i_dat);
               used_idx.emplace(i_dat);
            }
         }
         face_sets.emplace_back(idx_set);
      }

      // Sanity check
      Uin acc(0);
      for (const auto& s : face_sets)
      {
         acc += s.size();
      }
      if (acc != arData.size())
      {
         std::cerr << "I did not assign all points to a face set !!" << std::endl;
         exit(1);
      }

      return face_sets;
   }

   /********************************************************************
    * Loop over all mFacePlanes
    *    If there are points in mVertices on both sides of the face
    *       Then this is NOT a proper face and it is removed.
    ********************************************************************/
   template
      <  int DIM
      ,  FloatingPoint T
      >
   void Polytope<DIM, T>::CleanFaceVerticesFromNTuples()
   {
      std::vector<Plane3D<value_t>> new_faces;

      // Start out by removing non-unique elements
      for (const auto& face : mFaces)
      {
         auto it_find = std::find(new_faces.cbegin(), new_faces.cend(), face);
         if (it_find == new_faces.cend())
         {
            new_faces.emplace_back(face);
         }
      }

      // Clear out the undefined elements after move operation.
      mFaces = std::move(new_faces);
      new_faces.clear();

      // Now filter out any planes which is not a face.
      std::vector<int> signs;
      for (const auto& face : mFaces)
      {
         signs.clear();
         for (const auto& vertex : mVertices)
         {
            signs.emplace_back(face.ComputeWhichSideOfPlanePointIs(vertex));
         }
         // If all signs are either +1 (or 0) or -1 (or 0), but not all 0, then all vertices lies on the same side of the face and it is a proper face
         if (  (  std::all_of
                     (  signs.begin(), signs.end()
                     ,  [](const auto aInt)
                        { return aInt == +1 || aInt == 0; }
                     )
               || std::all_of
                     (  signs.begin(), signs.end()
                     ,  [](const auto aInt)
                        { return aInt == -1 || aInt == 0; }
                     )
               )
               && !std::all_of
                     (  signs.begin(), signs.end()
                     ,  [](const auto aInt)
                        { return aInt == 0; }
                     )
            )
         {
            new_faces.emplace_back(face);
         }
      }

      mFaces = std::move(new_faces);
      new_faces.clear();

      // Now at the end filter out any equivalent plane equations
      // Example: 4*x + 4 = 0 it equivalent to -4*x - 4 = 0.

      std::array<double, 4> temp_array{0, 0, 0, 0};
      for (auto it_face = mFaces.cbegin(); it_face != mFaces.cend(); it_face++)
      {
         // Create negated plane equation
         temp_array[0] = it_face->GetEquation()[0] * -1;
         temp_array[1] = it_face->GetEquation()[1] * -1;
         temp_array[2] = it_face->GetEquation()[2] * -1;
         temp_array[3] = it_face->GetEquation()[3] * -1;

         auto it_find = std::find_if
            (  std::next(it_face)
            ,  mFaces.cend()
            ,  [&temp_array](const auto& arPlane3D)
               {
                  return temp_array == arPlane3D.GetEquation();
               }
            );
         if (it_find == mFaces.cend())
         {
            new_faces.emplace_back(*it_face);
         }

      }

      mFaces = std::move(new_faces);
   }
}

#endif /* POLYTOPE_IMPL_HPP */