/********************************************************************
 *
 ********************************************************************/

#ifndef NORMALDISTRIBUTION_IMPL_HPP
#define NORMALDISTRIBUTION_IMPL_HPP

#include<cmath>
#include<numeric>

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::NormalDistribution
      (  const CONT& arData
      ,  const int aNumDistributions
      )
      :  DistributionBase<CONT>
            (  arData
            )
   {
      this->mParams = GenerateStartingGuess(aNumDistributions);
   }

   /********************************************************************
    * Compute the Mean of given input data.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::value_type NormalDistribution<CONT>::ComputeMean(const CONT& arData) const
   {
      if (arData.empty())
      {
         std::cerr << "arData is empty, this is not allowed as we divide by the size of arData here in ComputeMean()" << std::endl;
         exit(1);
      }
      else
      {
         return std::accumulate(arData.begin(), arData.end(), value_type(0)) / arData.size();
      }
   }

   /********************************************************************
    * Compute the standard deviation of the data
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::value_type NormalDistribution<CONT>::ComputeStandardDeviation(const CONT& arData) const
   {
      value_type mean = ComputeMean(arData);
      if (arData.empty())
      {
         std::cerr << "arData is empty this does not make any sense. To have a distribution over nothing !!" << std::endl;
         exit(1);
      }
      else
      {
         return std::sqrt
                  (  std::accumulate
                        (  arData.begin()
                        ,  arData.end()
                        ,  value_type(0)
                        ,  [&mean](value_type acc, const value_type& data)
                              {  
                                 return acc + std::pow(data - mean, 2);
                              }
                        ) / arData.size()
                  );
      }
   }

   /********************************************************************
    * Computes the exponential function part of the probability density
    * function.
    *
    * returns
    *    exp(-0.5 ((x - mean_j) / std_devi_j))
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::value_type NormalDistribution<CONT>::ComputeExponential
      (  const value_type arDataPoint
      ,  const value_type arMean
      ,  const value_type arStdDevi
      )  const
   {
      return std::exp(-0.5* std::pow((arDataPoint - arMean)/arStdDevi, 2));
   }

   /********************************************************************
    * Computes the probability density function at a given point.
    *
    * G(x) = 1 / (sqrt(2*pi)*M) \sum[j=1:M] exp(-0.5 ((x - mean_j) / std_devi_j)) / std_devi_j
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::value_type NormalDistribution<CONT>::EvaluateProbDensFunc
      (  const value_type aVal
      ,  const vec_type& arParams
      )  const
   {
      value_type tmp(0);
      for (Uin i = 0; i < arParams.size(); i += 2)
      {
         value_type mean = arParams(i);
         value_type std_devi = arParams(i + 1);
         tmp += ComputeExponential(aVal, mean, std_devi) / std_devi;
      }
      tmp /= std::sqrt(2*std::numbers::pi_v<value_type>);
      
      // Normalise by dividing with the number of gaussians.
      tmp /= (arParams.size() / 2);
      return tmp;
   }
   /********************************************************************
    * Computes the theoretical Cumultative Distribution Function.
    *
    * G(x) = 1 / (2*M) \sum[j=1:M] (1 + erf((x-mean_j) / (sqrt(2)*std_devi_j))
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::value_type NormalDistribution<CONT>::TheoreticalCDF(const value_type aVal) const
   {
      value_type res(0);

      for (Uin i = 0; i < this->mParams.size(); i += 2)
      {
         value_type mean = this->mParams(i);
         value_type std_devi = this->mParams(i + 1);
         res += (1 + std::erf((aVal - mean) / (std_devi * std::sqrt(2))));
      }
      // 
      res *= 0.5;
      // Normalise the CDF by the number of gaussians used.
      res /= (this->mParams.size() / 2.0);

      return res;
   }

   /********************************************************************
    * Compute value of function begin optimised.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::value_type NormalDistribution<CONT>::ComputeLogLikelihood
      (  const vec_type& arParams
      )  const
   {
      return std::transform_reduce
               (  this->mData.begin()
               ,  this->mData.end()
               ,  value_type(0)
               ,  std::plus<>()
               ,  [this, &arParams](const value_type aDataPoint)
                  {
                     return std::log(EvaluateProbDensFunc(aDataPoint, arParams));
                  }
               );
   }

   /********************************************************************
    * Compute gradient vector inplace on input vector
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   void NormalDistribution<CONT>::ComputeLogLikelihoodGradient
      (  const vec_type& arParams
      ,  vec_type& arGradient
      )  const
   {
      // Compute g for all data points (intermediate)
      std::vector<value_type> g_vector;
      g_vector.reserve(this->mData.size());
      for (const auto& data : this->mData)
      {
         g_vector.emplace_back(EvaluateProbDensFunc(data, arParams));
      }

      // Loop over all even k idx
      for (Uin k = 0; k < arParams.size(); k += 2)
      {
         value_type mean_k = arParams(k);
         value_type std_devi_k = arParams(k + 1);

         // Compute derivatives
         value_type mean_deriv_tmp(0);
         value_type std_devi_deriv_tmp(0);
         for (Uin i = 0; i < this->mData.size(); i++)
         {
            value_type x_i = this->mData[i];

            // Compute exponential value for given data point and parameters
            value_type exp_val = ComputeExponential(x_i, mean_k, std_devi_k);

            // Contribution to the derivative wrt. mean
            mean_deriv_tmp += (x_i - mean_k) * exp_val / g_vector[i];

            // Contribution to the derivative wrt. sigma
            value_type factor = std::pow((x_i - mean_k) / std_devi_k, 2) - 1;
            std_devi_deriv_tmp += factor * exp_val / g_vector[i];
         }

         mean_deriv_tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(std_devi_k, 3);
         arGradient(k) = mean_deriv_tmp;

         std_devi_deriv_tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(std_devi_k, 2);
         arGradient(k + 1) = std_devi_deriv_tmp;
      }
   }

   /********************************************************************
    * Compute gradient vector inplace on input vector
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   NormalDistribution<CONT>::vec_type NormalDistribution<CONT>::GenerateStartingGuess
      (  const int aNumDistributions
      )  const
   {
      // Create initial guess by simply dividing the this->mData into i normal distributions without any overlap.
      std::vector<CONT> starting_guess_chunks;

      int chunks = this->mData.size() / aNumDistributions;

      auto it_chunk_start = this->mData.begin();
      auto it_chunk_end = this->mData.begin();
      std::advance(it_chunk_end, chunks);
      for (int i = 0; i < aNumDistributions; i++)
      {
         starting_guess_chunks.emplace_back(CONT(it_chunk_start, it_chunk_end));
         std::advance(it_chunk_start, chunks);

         // For the last chunk simply take the rest.
         if (i == aNumDistributions - 1)
         {
            it_chunk_end = this->mData.end();
         }
         else
         {
            std::advance(it_chunk_end, chunks);
         }
      }

      // Two parameters per distribution.
      vec_type params(2 * aNumDistributions);

      // Insert starting guess in params CONT
      for (int i = 0; i < params.size(); i += 2)
      {
         int distance = i / 2;
         params(i) = ComputeMean(starting_guess_chunks.at(distance));
         params(i + 1) = ComputeStandardDeviation(starting_guess_chunks.at(distance));
      }

      return params;
   }

   /********************************************************************
    * Construct the vector of bounds for the optimisation.
    * 
    * Lower bound for mean is -inf and for std-devi it is 0.0.
    * Upper bound for mean is +inf and for std-devi it is +inf.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   std::pair
      <  typename NormalDistribution<CONT>::vec_type
      ,  typename NormalDistribution<CONT>::vec_type
      >
   NormalDistribution<CONT>::GetOptimisationBounds
      (
      )  const
   {
      vec_type lower_bounds = vec_type::Constant(this->mParams.size(), 0.0);
      for (int i = 0; i < this->mParams.size(); i += 2)
      {
         // Mean lower bound
         lower_bounds(i) = -std::numeric_limits<value_type>::infinity();
         // Standard deviation lower bound
         lower_bounds(i + 1) = 0.0; // std deviation can never be lower than 0.0
      }
      vec_type upper_bounds = vec_type::Constant(this->mParams.size(), +std::numeric_limits<value_type>::infinity());

      return std::make_pair(lower_bounds, upper_bounds);
   }

   /********************************************************************
    * Compute hessian matrix inplace on input matrix
    * 
    * Saved just in case i need it again, but maybe just delete LBFGS++
    * works very nicely.
    ********************************************************************/
   //template
   //   <  FloatingPointContainer CONT
   //   >
   //void MaximumLogLikelihood<CONT>::ComputeHessianMatrix
   //   (  mat_type& arHessian
   //   )  const
   //{
   //   // Compute g for all data points (intermediate)
   //   std::vector<value_type> g_vector;
   //   g_vector.reserve(this->mData.size());
   //   for (const auto& data : this->mData)
   //   {
   //      g_vector.emplace_back(ComputeG(data));
   //   }

   //   // Compute all hessian element for double derivatives wrt. mu
   //   for (Uin r = 0; r < this->mParams.size(); r += 2)
   //   {
   //      for (Uin k = 0; k < this->mParams.size(); k += 2)
   //      {
   //         value_type mean_k = this->mParams(k);
   //         value_type sigma_k = this->mParams(k + 1);
   //         value_type mean_r = this->mParams(r);
   //         value_type sigma_r = this->mParams(r + 1);

   //         value_type tmp(0);

   //         for (Uin i = 0; i < this->mData.size(); i++)
   //         {
   //            value_type x_i = this->mData[i];

   //            value_type exp_val_k = ComputeExponential(x_i, mean_k, sigma_k);
   //            value_type exp_val_r = ComputeExponential(x_i, mean_r, sigma_r);

   //            value_type frac_k = (x_i - mean_k) / sigma_k;

   //            value_type first_term = r == k ? std::pow(frac_k, 2) - 1 : 0;
   //            value_type second_term = -(x_i - mean_k) * (x_i - mean_r) * exp_val_r / g_vector[i];
   //            second_term /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(sigma_r, 3);

   //            tmp += exp_val_k * (first_term + second_term) / g_vector[i];
   //         }
   //         tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(sigma_k, 3);

   //         arHessian(r, k) = tmp;
   //         arHessian(k, r) = tmp;
   //      }
   //   }

   //   // Compute all hessian element for double derivatives wrt. sigma
   //   for (Uin r = 0; r < this->mParams.size(); r += 2)
   //   {
   //      for (Uin k = 0; k < this->mParams.size(); k += 2)
   //      {
   //         value_type tmp(0);

   //         for (Uin i = 0; i < this->mData.size(); i++)
   //         {
   //            value_type exp_val_k = ComputeExponential(this->mData[i], this->mParams(k), this->mParams(k + 1));
   //            value_type exp_val_r = ComputeExponential(this->mData[i], this->mParams(r), this->mParams(r + 1));

   //            value_type frac_k = (this->mData[i] - this->mParams(k)) / this->mParams(k + 1);
   //            value_type frac_r = (this->mData[i] - this->mParams(r)) / this->mParams(r + 1);
   //            value_type first_term = r == k 
   //                                  ? (std::pow(frac_k, 4) - 5*std::pow(frac_k, 2) + 2) / this->mParams(k+1)
   //                                  : 0;
   //            value_type second_term = -exp_val_r * (std::pow(frac_r, 2) - 1) * (std::pow(frac_k, 2) - 1) / g_vector[i];
   //            second_term /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(r + 1), 2);

   //            tmp += exp_val_k * (first_term + second_term) / g_vector[i];
   //         }

   //         tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(k + 1), 2);

   //         // Insertion into hessian is +1 for sigma values.
   //         arHessian(r + 1, k + 1) = tmp;
   //         arHessian(k + 1, r + 1) = tmp;
   //      }
   //   }

   //   // Compute all hessian element for derivatives wrt. sigma and mu
   //   for (Uin r = 0; r < this->mParams.size(); r += 2)
   //   {
   //      for (Uin k = 0; k < this->mParams.size(); k += 2)
   //      {
   //         value_type tmp(0);

   //         value_type mean_k = this->mParams(k);
   //         value_type mean_r = this->mParams(r);

   //         for (Uin i = 0; i < this->mData.size(); i++)
   //         {
   //            value_type x_i = this->mData[i];

   //            value_type exp_val_k = ComputeExponential(x_i, mean_k, this->mParams(k + 1));
   //            value_type exp_val_r = ComputeExponential(x_i, mean_r, this->mParams(r + 1));

   //            value_type frac_k = (x_i - mean_k) / this->mParams(k + 1);
   //            value_type frac_r = (x_i - mean_r) / this->mParams(r + 1);
   //            value_type first_term = r == k 
   //                                  ? (std::pow(frac_k, 2) - 3) * frac_k
   //                                  : 0;
   //            value_type second_term = -(x_i - mean_k) * exp_val_r * (std::pow(frac_r, 2) - 1) / g_vector[i];
   //            second_term /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(r + 1), 2);

   //            tmp += exp_val_k * (first_term + second_term) / g_vector[i];
   //         }

   //         tmp /= std::sqrt(2*std::numbers::pi_v<value_type>) * std::pow(this->mParams(k + 1), 3);

   //         // Insertion into hessian is +1 for sigma values.
   //         arHessian(r + 1, k) = tmp;
   //         arHessian(k, r + 1) = tmp;
   //      }
   //   }
   //}
}

#endif /* NORMALDISTRIBUTION_IMPL_HPP */