/********************************************************************
 *
 ********************************************************************/

#ifndef CAUCHYDISTRIBUTION_DECL_HPP
#define CAUCHYDISTRIBUTION_DECL_HPP

#include "Concepts.hpp"
#include "MathAndUtil/DistributionBase.hpp"

namespace Math
{
   /********************************************************************
    * Class holding all parameters for a the cauchy distribution as
    * well as certain member functions unique for this distribution.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   class CauchyDistribution : public DistributionBase<CONT>
   {
      public:
         using value_type = CONT::value_type;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;

      public:
         //!@{ Constructors
         CauchyDistribution() = delete;
         CauchyDistribution(const CONT& arData, const int aNumDistributions);
         //!@}

         //!@{ Analyze the data
         bool HasOutliers() const;
         //!@}

         //!@{ Queries
         value_type ComputeMedian(const CONT& arData) const;
         value_type EstimateGamma(const CONT& arData) const;
         //!@}

         //!@{ Statistics
         virtual value_type TheoreticalCDF(const value_type aVal) const override;
         virtual value_type EvaluateProbDensFunc(const value_type aVal, const vec_type& arParams) const override;
         //!@}

         //!@{ Optimisation
         virtual value_type ComputeLogLikelihood(const vec_type& arParams) const override;
         virtual void ComputeLogLikelihoodGradient(const vec_type& arParams, vec_type& arGradient) const override;
         virtual std::pair<vec_type, vec_type> GetOptimisationBounds() const override;
         //!@}

      protected:
         //!@{ Optimisation
         virtual vec_type GenerateStartingGuess(const int aNumDistributions) const override;
         //!@}
   };
}

#endif /* CAUCHYDISTRIBUTION_DECL_HPP */