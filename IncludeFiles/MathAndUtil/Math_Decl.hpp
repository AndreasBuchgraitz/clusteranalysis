/********************************************************************
 * This file contains different math functionalites.
 ********************************************************************/

#ifndef MATH_DECL_HPP
#define MATH_DECL_HPP

#include "Eigen_wrapper.hpp"
#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Templated functions
    ********************************************************************/
   // Evaluates the sign of some sort of number
   template <typename T> int GetSign(T val);

   // Evaluates std::acos, but with more reasonable error output and checks on numerical noise.
   template<typename T> T ArcCos(T aVal);

   // Evaluate the inverse of the error function
   template<FloatingPoint T> T InvErr(T aVal);

   //! Centroid of points
   template<RowVectorIter ITER>
   Eigen::RowVectorXd Centroid(ITER aItBegin, ITER aItEnd);

   //! Compute the binomial coefficient
   template<typename T>
   T BinomialCoef(T n, T k);

   //! Compute cross product of two eigen vectors
   template<typename EIG_VEC_TYPE>
   EIG_VEC_TYPE CrossProduct(const EIG_VEC_TYPE& arVec1, const EIG_VEC_TYPE& arVec2);

   //! Generate all unique n-tuples of input data
   template<int SIZE, typename DATA_CONT>
   void GenerateNTuples
      (  const DATA_CONT& arData
      ,  std::vector<DATA_CONT>& arOutData
      ,  DATA_CONT& arTempCont
      ,  int aInpDataIdx
      );

   /********************************************************************
    * Non-templated functions
    ********************************************************************/
   //! Root mean squared deviation
   double MatrixRMSD(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMart2);

   //! Differences in matrix elements
   std::vector<double> MatrixElementDiff(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Distance between matrix as if it was a euclidian vector
   std::vector<double> MatrixDistanceRowByRow(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMart2);

   //! Absolute distance of each element
   double AbsoluteDistance(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Root squared distance
   double RootSquaredDistance(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Frobenius distance
   double FrobeniusDistance(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Angle between rotation matrices computed through trace of product
   double AngleOfRotationMatrices(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Angle between to vectorized matrices.
   double VectorAngle(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Dot product of matrices as vectors
   double Dot(const Eigen::MatrixXd& arMat1, const Eigen::MatrixXd& arMat2);

   //! Checks if vectors are parallel
   std::pair<bool, int> IsVectorsParallel(const Eigen::VectorXd& arVec1, const Eigen::VectorXd& arVec2, const double aThreshold);

   //! Normalize a vector
   Eigen::VectorXd NormalizeVector(const Eigen::VectorXd& arVec);
}

#endif /* MATH_DECL_HPP */