/********************************************************************
 *
 ********************************************************************/

#ifndef PLANE3D_IMPL_HPP
#define PLANE3D_IMPL_HPP

#include<vector>
#include "Eigen_wrapper.hpp"

#include "Concepts.hpp"
#include "MathAndUtil/Math.hpp"

namespace Math
{
   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   Plane3D<T>::Plane3D
      (  const std::array<vector_t, 3>& arData
      )
      :  mDataPoints(arData)
   {
      this->Initialize();
   }

   /********************************************************************
    * C-tor
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   Plane3D<T>::Plane3D
      (  const std::array<vector_t, 3>&& arData
      )
      :  mDataPoints(std::move(arData))
   {
      this->Initialize();
   }

   /********************************************************************
    * Initialize the mPlaneEquation member
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   void Plane3D<T>::Initialize()
   {
      // Compute the plane equation
      const auto& p1 = mDataPoints[0];
      const auto& p2 = mDataPoints[1];
      const auto& p3 = mDataPoints[2];
      vector_t n = CrossProduct(vector_t(p1 - p2), vector_t(p1 - p3));
      double d = - Dot(n, p1); // p1 could be any point in the plane...
      mPlaneEquation = std::array<value_t, 4>{n(0), n(1), n(2), d};
   }

   /********************************************************************
    * Evaluate the smallest distance from the plane to the given point.
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   Plane3D<T>::value_t Plane3D<T>::DistanceFromPlaneToPoint
      (  const vector_t& arPoint
      )  const
   {
      double numerator = std::abs
         (  mPlaneEquation[0]*arPoint(0)
         +  mPlaneEquation[1]*arPoint(1)
         +  mPlaneEquation[2]*arPoint(2)
         +  mPlaneEquation[3]
         );

      double denominator = std::sqrt
         (  mPlaneEquation[0]*mPlaneEquation[0]
         +  mPlaneEquation[1]*mPlaneEquation[1]
         +  mPlaneEquation[2]*mPlaneEquation[2]
         );

      return numerator / denominator;
   }

   /********************************************************************
    * Evaluate the plane equation for the given point.
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   Plane3D<T>::value_t Plane3D<T>::EvalPlaneForPoint
      (  const vector_t& arPoint
      )  const
   {
      return mPlaneEquation[0]*arPoint(0) + mPlaneEquation[1]*arPoint(1) + mPlaneEquation[2]*arPoint(2) + mPlaneEquation[3];
   }

   /********************************************************************
    * Compute which side of the plane the given point is on.
    * +1 or -1:
    *    The point is on either side of the plane
    * 0:
    *    The point is "inside" the plane.
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   int Plane3D<T>::ComputeWhichSideOfPlanePointIs
      (  const vector_t& arPoint
      )  const
   {
      auto point_val = this->EvalPlaneForPoint(arPoint);

      if (std::abs(point_val) < arPoint.norm() * 1.0E-12)
      {
         return 0;
      }
      else
      {
         return GetSign<T>(point_val);
      }
   }

   /********************************************************************
    * Overload of operator==
    * 
    * Checks the plane equation and NOT the data points as many points
    * might lie in the same plane.
    * 
    * Checks if the equation is directly equal or if the negated
    * equation is equal.
    ********************************************************************/
   template
      <  FloatingPoint T
      >
   bool Plane3D<T>::operator==
      (  const Plane3D& arOther
      )  const
   {
      std::array<value_t, 4> other_negated = std::array<value_t, 4>
         {  arOther.GetEquation()[0] * -1
         ,  arOther.GetEquation()[1] * -1
         ,  arOther.GetEquation()[2] * -1
         ,  arOther.GetEquation()[3] * -1
         };
      
      return this->GetEquation() == arOther.GetEquation() || this->GetEquation() == other_negated;
   }
}

/********************************************************************
 * 
 ********************************************************************/
template
   <  FloatingPoint T
   >
std::ostream& operator<<(std::ostream& arOut, const Math::Plane3D<T>& arPlane3D)
{
   //arOut << "Plane3D Points:" << std::endl;
   //for (const auto& point : arPlane3D.GetDataPoints())
   //{
   //   arOut << point << std::endl;
   //}
   arOut << "Plane3D Equation:" << std::endl;
   arOut << arPlane3D.GetEquation()[0]<<", "
         << arPlane3D.GetEquation()[1]<<", "
         << arPlane3D.GetEquation()[2]<<", "
         << arPlane3D.GetEquation()[3]<<", "
         << std::endl;
   return arOut;
}

#endif /* PLANE3D_IMPL_HPP */