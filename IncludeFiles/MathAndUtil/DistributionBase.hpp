/********************************************************************
 *
 ********************************************************************/

#ifndef DISTRIBUTIONBASE_HPP
#define DISTRIBUTIONBASE_HPP

#include "Concepts.hpp"

namespace Math
{
   /********************************************************************
    * Pure virtual base class for all distribution classes defining
    * their interface.
    * 
    * Quantities like mean, variance are defined in derived classes, as
    * this is not well-defined for all types of distributions.
    * 
    * All parameters (in need of optimisation) are stored in mParams,
    * with the layout of this vector being defined in derived class.
    ********************************************************************/
   template
      <  FloatingPointContainer CONT
      >
   class DistributionBase
   {
      public:
         using value_type = CONT::value_type;
         using vec_type = Eigen::Matrix<value_type, Eigen::Dynamic, 1>;

      public:
         //!@{ Constructors
         DistributionBase() = delete;
         DistributionBase
            (  const CONT& arData
            )
            :  mData(arData)
         {
         }
         //!@}

         //!@{ Queries
         auto NumParams() const { return mParams.size(); }
         //!@}

         //!@{
         void SetParams(const vec_type& arParams) { mParams = arParams; }
         const vec_type& GetParams() const { return mParams; }
         //!@}

         //!@{ Statistics
         virtual value_type TheoreticalCDF(const value_type aVal) const = 0;
         virtual value_type EvaluateProbDensFunc(const value_type aVal, const vec_type& arParams) const = 0;
         //!@}

         //!@{ Optimisation
         virtual value_type ComputeLogLikelihood(const vec_type& arParams) const = 0;
         virtual void ComputeLogLikelihoodGradient(const vec_type& arParams, vec_type& arGradient) const = 0;
         virtual std::pair<vec_type, vec_type> GetOptimisationBounds() const = 0;
         //!@}

      protected:
         //! Data (const ref as it might be many data points)
         const CONT& mData;
   
         //! Parameters (This vector _should_ always be small so no const ref and the ref might not be well defined all the time...)
         vec_type mParams;

      protected:
         //!@{ Optimisation
         virtual vec_type GenerateStartingGuess(const int aNumDistributions) const = 0;
         //!@}
   };
}

#endif /* DISTRIBUTIONBASE_HPP */