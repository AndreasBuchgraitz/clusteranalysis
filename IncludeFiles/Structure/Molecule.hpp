/********************************************************************
 * This file contains the Atom and Molecule class.
 ********************************************************************/

#ifndef MOLECULE_HPP
#define MOLECULE_HPP

#include<set>
#include<vector>
#include<array>
#include<map>
#include<memory>
#include<iostream>
#include<fstream>
#include<iterator>
#include<filesystem>
#include "Eigen_wrapper.hpp"

#include "CommonAliases.hpp"
#include "StructureIdentification.hpp"
#include "Concepts.hpp"
#include "Structure/DipoleMoment.hpp"
#include "Structure/CoulombMatrix.hpp"

// Forward declarations
namespace Structure
{
   enum class AtomName;
}

namespace Structure
{
   /********************************************************************
    * Simple dataclass to contain a molecule. 
    * 
    * Molecule name and atoms.
    ********************************************************************/
   class Molecule
   {
      public:
         //c-tors
         Molecule() = default;
         Molecule
            (  const std::string& arName
            ,  const std::pair<std::vector<AtomName>, std::vector<std::string>>& arAtomNames
            ,  const Eigen::MatrixX3d& arAtomPositions
            ,  const std::vector<Uin>& arClusterIdx
            );

         Molecule
            (  const std::string& arName
            ,  const std::pair<std::vector<AtomName>, std::vector<std::string>>& arAtomNames
            ,  const Eigen::MatrixX3d& arAtomPositions
            ,  const std::map<Uin, std::vector<Uin>>& arAtomConnectivityMap
            ,  const std::vector<Uin>& arClusterIdx
            );

         //!@{ GETTERS
         const std::string& GetName() const { return mName; }
         const std::pair<std::vector<AtomName>, std::vector<std::string>>& GetAtomNames() const { return mAtomNames; }
         template<UinIter ITER> std::pair<std::vector<AtomName>, std::vector<std::string>> GetAtomNames(const ITER aBegin, const ITER aEnd) const;
         const std::vector<AtomName>& GetAtomNameEnums() const { return mAtomNames.first; }
         const std::vector<std::string>& GetAtomNameStrings() const { return mAtomNames.second; }
         AtomName GetAtomName(const int aAtomIdx) const { return mAtomNames.first[aAtomIdx]; }
         int GetMass(const int aAtomIdx) const;
         const std::string& GetAtomStringName(const int aAtomIdx) const { return mAtomNames.second[aAtomIdx]; }
         const Eigen::MatrixX3d& GetAtomPositions() const { return mAtomPositions; }
         Eigen::RowVector3d GetCoordinate(const int aCoordIdx) const { return mAtomPositions.row(aCoordIdx); } // .row() returns temporary object.
         const std::vector<Uin>& GetNonHydrogenIndex() const;
         std::vector<Uin> GetHydrogenIndex() const;
         const std::set<Uin>& GetAtomNameIndices(const AtomName arAtomName) const;

         double GetSmallestBondLength() const { return mSmallestBondLength; }

         const std::vector<std::pair<Uin, Uin>>& GetClusterIdxVec() const { return mClusterIdxVec; }

         const Eigen::MatrixX3d& GetEigenMat() const;
         Eigen::MatrixX3d GetEigenMat(const std::vector<Uin>& arIndexVec) const;
         Eigen::MatrixX3d GetEigenMat(const std::set<Uin>& arIndexSet) const;

         const Eigen::Matrix3d& GetMoleculeRotationMatrix() const { return mRotationMatrix; }

         const Eigen::RowVector3d& GetDipoleMomentVector() const { return mDipoleMoment.GetDipoleMoment(); }
         bool HaveDipole() const { return mDipoleMoment.HaveDipole(); }

         const Eigen::MatrixXd& GetCoulombMatrix() const { return mCoulombMatrix.GetCoulombMatrix(); }

         Uin GetNumAtoms() const { return mAtomNames.first.size(); }
         int GetNumAtoms(const AtomName aAtomName) const;

         bool Empty() const { return mName.empty(); }

         const std::map<Uin, std::vector<Uin>>& GetAtomConnectivityMap() const { return mAtomConnectivityMap; }
         //!@}

         //!@{ SETTERS
         void SetMoleculeRotationMatrix(const Eigen::Matrix3d& arMat) { mRotationMatrix = arMat; }
         //!@}

         //!@{ INQUIRIES
         Eigen::RowVector3d CalcCenterOfMass() const;
         Eigen::RowVector3d CalcMolecularCentroid() const;
         double CalcMolecularRadius() const;

         //!
         std::pair<Uin, double> GetClosestAtomOfType
            (  const Uin aAtomIdx
            ,  const AtomName aAtomsToCheck
            )  const;

         double CoulombMatrixNorm() const { return mCoulombMatrix.Norm(); }
         Eigen::MatrixXd ComputeCoulombMatrix(const std::vector<Uin>& arAtomIdx) const { return mCoulombMatrix.ComputeCoulombMatrix(arAtomIdx); }
         //!@}

         //! Add hydrogens to a given connectivity map
         std::map<Uin, std::vector<Uin>> AddHydrogensToConnectivityMap
            (  std::map<Uin, std::vector<Uin>> arMap
            )  const;

         double CalcDistanceOfAtoms(const int atom_idx_1, const int atom_idx_2) const;
         double CalcDistanceOfAtoms(const Eigen::RowVector3d& arVec1, const Eigen::RowVector3d& arVec2) const;

         //! Operator less than overload.
         bool operator<(const Molecule& arMoleculeRhs) const;

         //! Operator== overload
         bool operator==(const Molecule& arMoleculeRhs) const {return !(this->operator<(arMoleculeRhs) && !(arMoleculeRhs.operator<(*this))); }
         //! Operator!= overload
         bool operator!=(const Molecule& arMoleculeRhs) const {return !(this->operator==(arMoleculeRhs)); }

      private:
         //! Name of Molecule
         std::string mName;

         //! Container for all atom data
         std::pair<std::vector<AtomName>, std::vector<std::string>> mAtomNames;

         //! The matrix containing the positions of all the atoms
         Eigen::MatrixX3d mAtomPositions;

         //! Rotation matrix, if applied to mAtomPositions they are rotated into optimal overlap with some reference Molecule
         Eigen::Matrix3d mRotationMatrix;

         //! Should this Molecule be divided into ChemicalGroups in c-tor
         bool mDivideIntoChemicalGroups;
         
         //! Container for all index of non-hydrogen atoms only getter func for this, set during Initialize
         std::vector<Uin> mNonHydrogenIndex;

         //! map with set of indices for the given AtomName
         std::map<AtomName, std::set<Uin>> mAtomIndexMap;

         //! Atom connectivity map (excluding hydrogens)
         std::map<Uin, std::vector<Uin>> mAtomConnectivityMap;

         //! Object for containing and computing the dipole moment of this molecule
         DipoleMoment mDipoleMoment;

         //! Object for containing the coulomb matrix of this given molecule
         CoulombMatrix mCoulombMatrix;
         
         //! Vector to map between cluster atom idx and molecule atom idx. Will only be used inside the cluster object which owns this molecule (if any such cluster exists), otherwise should just be empty.
         std::vector<std::pair<Uin, Uin>> mClusterIdxVec;

         //! Number to contain the value of the smallest bond length in the this Molecule
         double mSmallestBondLength;

      private:
         //! Get atom indices as std map with AtomName -> set of indices.
         std::map<AtomName, std::set<Uin>> ConstructAtomIndicesMap() const;

         //!
         std::vector<Uin> ConstructNonHydrogenIndexVector() const;

         //!
         std::map<Uin, std::vector<Uin>> ConstructNonHydrogenConnectivityMap
            (  const std::map<AtomName, std::set<Uin>>& arAtomIndexMap
            )  const;

         //!
         double ComputeSmallestBondLength() const;
   };

   //!
   std::ostream& operator<<(std::ostream& arOut, const Molecule& arMolecule);

   //! Print molecules in container (CONT_T needs .begin() and .end())
   template<MoleculeIter ITER>
   void PrintMolecules
      (  const ITER& arMoleculesStart
      ,  const ITER& arMoleculesEnd
      ,  const std::filesystem::path& arPath
      )
   {
      Uin tot_num_atoms = 0;
      for (auto it = arMoleculesStart; it != arMoleculesEnd; it++)
      {
         tot_num_atoms += it->GetNumAtoms();
      }

      std::string comment = "Comment";

      // If dimer we write the distance between the COMs as the comment
      if (std::distance(arMoleculesStart, arMoleculesEnd) == 2)
      {
         auto it = arMoleculesStart;
         auto it_next = arMoleculesStart;
         std::advance(it_next, 1);
         double dis = Eigen::RowVector3d(it->CalcCenterOfMass() - it_next->CalcCenterOfMass()).norm();
         comment = "Center-of-Mass distance: " + std::to_string(dis);
      }

      // Setup coordinate file
      std::fstream xyz_file;
      xyz_file.open(arPath.string(), std::ios_base::out);
      xyz_file << std::left;
      xyz_file << std::setprecision(16);
      xyz_file << std::scientific;

      // Actually print stuff to file
      xyz_file << tot_num_atoms << "\n";
      xyz_file << comment << "\n";
      for (auto it = arMoleculesStart; it != arMoleculesEnd; it++)
      {
         for (Uin i_atom = 0; i_atom < it->GetNumAtoms(); i_atom++)
         {
            xyz_file << std::setw(6) << it->GetAtomStringName(i_atom) << it->GetCoordinate(i_atom) << "\n";
         }
      }

      xyz_file.close();
   }

   /********************************************************************
    * Returns set containing atom indices for AtomName
    ********************************************************************/
   template<UinIter ITER>
   std::pair<std::vector<AtomName>, std::vector<std::string>> Molecule::GetAtomNames
      (  const ITER aBegin
      ,  const ITER aEnd
      )  const
   {
      std::pair<std::vector<AtomName>, std::vector<std::string>> res;
      res.first.reserve(std::distance(aBegin, aEnd));
      res.second.reserve(std::distance(aBegin, aEnd));

      for (ITER it = aBegin; it != aEnd; it++)
      {
         res.first.emplace_back(this->GetAtomName(*it));
         res.second.emplace_back(this->GetAtomStringName(*it));
      }

      return res;
   }
}

#endif /* MOLECULE_HPP */