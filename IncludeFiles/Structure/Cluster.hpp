#ifndef CLUSTER_HPP
#define CLUSTER_HPP

#include<string>
#include<deque>
#include<map>
#include "Eigen_wrapper.hpp"
#include<filesystem>
#include<limits>

#include "Structure/StructureEnums.hpp"
#include "Structure/StructureIdentification.hpp"
#include "Structure/Molecule.hpp"
#include "Structure/SolvationShells.hpp"

namespace Structure
{
   class Molecule;
   enum class InputOptions;
}

namespace Structure
{
   /********************************************************************
    * Cluster class defines the cluster by the StructureIdentification
    * object. Contains the molecular structure as both 1 Molecule and 
    * a set of Molecule's.
    ********************************************************************/
   class Cluster
   {
      public:
         Cluster() = delete;

         Cluster
            (  const std::filesystem::path& arPathName
            ,  const StructureIdentification& arId
            );

         //!@{ GETTERS
         const StructureIdentification& GetId() const { return mId; }
         const std::string& GetMethod() const { return mId.GetCompMethod(); }
         const std::string& GetBasisSet() const { return mId.GetBasisSet(); }
         const std::string& GetName() const { return mId.GetName(); }
         bool GetReferenceMethod() const { return mId.GetReferenceMethod(); }
         const std::filesystem::path& GetPathToXYZ() const { return mPathToXYZ; }
         auto GetMonomersIterConstBegin() const { return mMonomers.cbegin(); }
         auto GetMonomersIterConstEnd()   const { return mMonomers.cend(); }
         auto GetMonomersIterBegin()       { return mMonomers.begin(); }
         auto GetMonomersIterEnd()         { return mMonomers.end(); }
         const Molecule& GetClusterMolecule() const { return mCluster; }
         const Eigen::MatrixX3d& GetClusterMoleculeMatrix() const { return mCluster.GetEigenMat(); }
         const Eigen::Matrix3d& GetClusterMoleculeRotationMatrix() const { return mCluster.GetMoleculeRotationMatrix(); }
         auto GetNumMonomers() const { return mMonomers.size(); }
         double GetSmallestBondLength() const { return mCluster.GetSmallestBondLength(); }
         ConformerEnum GetConformerEnum() const { return mConformerEnum; }
         const auto& GetSolvationShells() const { return mSolvationShells; }
         auto GetNumSolvationShells() const { return mSolvationShells.GetNumSolvationShells(); }
         //!@}

         //! Queries
         //!@{
         bool Empty() const { return mCluster.Empty(); }
         std::size_t GetSizeSmallestMonomer() const { return *mMonomerSizes.begin(); }
         double SmallestMonomerCoulombMatrixNorm() const;
         double LargestMonomerCoulombMatrixNorm() const;
         //!@}

         //! Setters
         //!@{
         void SetConformerEnum(const ConformerEnum aConformerEnum) { mConformerEnum = aConformerEnum; }
         void SetClusterMoleculeRotationMatrix(const Eigen::Matrix3d& arMat) { mCluster.SetMoleculeRotationMatrix(arMat); }
         //!@}

         //! Computes the Coordinate for seperation of the dimer
         Eigen::RowVector3d ComputeDisplacementCoordinate1D() const;
         Eigen::RowVector3d ComputeDisplacementCoordinate1DCenterOfMass() const;
         Eigen::RowVector3d ComputeDisplacementCoordinate1DActualHydrogenBond() const;
         std::vector<std::vector<Molecule>> ApplyDisplacement(const Eigen::RowVector3d& arCoord) const;

         //! Compute the solvation shells in-place updating _this_
         void ComputeSolvationShells();
         void PrintSolvationShellToFile(const std::filesystem::path& arWorkingDir) const { mSolvationShells.PrintSolvationToFile(arWorkingDir, this->GetName()); }

         //! Compute the Equilibrium distance between all the monomers.
         std::vector<double> ComputeEqDistances() const;

         //! Operator less than overload.
         bool operator<(const Cluster& arClusterRhs) const;

      private:
         //!
         Eigen::MatrixX3d ComputeCenterOfMassesForMonomers() const;

         //!
         std::vector<Molecule> ConstructMonomersNewImpl
            (  const Molecule& arClusterMolecule
            ,  const bool aDebug
            )  const;
         
         //!
         std::vector<std::set<Uin>> MergeAtomsToMonomers
            (  const std::map<Uin, std::vector<Uin>>& arAtomConnectivityMap
            ,  const bool aDebug
            )  const;
         
         //!
         void RecursiveConnectAtoms
            (  std::vector<std::set<Uin>>& arMonomerSets
            ,  const Uin aAtomIdx
            ,  const std::map<Uin, std::vector<Uin>>& arAtomConnectivityMap
            )  const;

         //!
         void AddHydrogensToMonomerSets
            (  std::vector<std::set<Uin>>& arMonomerSets
            ,  const std::set<Uin>& arHydrogenIdxSet
            ,  const bool aDebug
            )  const;

         //!
         std::vector<Molecule> ConstructMoleculesFromIndexSet
            (  const std::vector<std::set<Uin>>& arIndexSets
            ,  const bool aDebug
            )  const;

         //!
         std::string CreateMonomernameFromIndices(const std::set<Uin>& arSet) const;

         //! Full path to optimized XYZ file
         const std::filesystem::path mPathToXYZ;

         //! How to identify the cluster.
         const StructureIdentification mId;

         /********************************************************************
          * The total cluster as a Molecule is simply included with the
          * entire cluster name representing it (mId->GetName()).
          ********************************************************************/
         Molecule mCluster;

         //! Monomers are now stored in a set such that the Molecule::operator< ensures the same order across Clusters
         std::vector<Molecule> mMonomers; // If updates to names of molecules remember to sort the vector

         //! Monomer sizes. As mMonomers is a multiset, be careful assuming anything about the order here !
         std::multiset<std::size_t> mMonomerSizes;

         //! Enum to indicate whether this given cluster is the same conformer as the reference cluster
         ConformerEnum mConformerEnum = ConformerEnum::UNINITIALIZED;

         //!
         SolvationShells mSolvationShells;
   };

   //! Overload of operator<<
   std::ostream& operator<<(std::ostream& arOut, const Cluster& arCluster);
}

#endif /* CLUSTER_HPP */