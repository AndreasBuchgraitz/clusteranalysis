/********************************************************************
 * This file contains the definition of a ClusterSet.
 * 
 * This should contain a set of all clusters aswell as "global
 * information" which might be needed in construction of clusters or
 * other stuff. This might fx. be a list of all clusters and 
 * monomers. 
 * If I ever implement a "CalcDef" class this ClusterSet, should
 * propably just be constructed from it.
 * 
 * ALL members in this class should be set in the ReadInputFile
 * function and nowhere else !
 ********************************************************************/

#ifndef CLUSTER_SET_HPP
#define CLUSTER_SET_HPP

#include<fstream>
#include<map>
#include<set>
#include<tuple>
#include<vector>
#include<filesystem>

#include "Structure/StructureEnums.hpp"
#include "MathAndUtil/Kabsch.hpp"
#include "Structure/Cluster.hpp"

namespace Structure
{
   class StructureIdentification;
   class Cluster;
   class Molecule;
}
namespace Kabsch
{
   class RmsdData;
}

namespace Structure
{
   class ClusterSet
   {
      public:
         ClusterSet() = default;

         // Functions for reading input which initializes the mClusterNames and mMonomerNames members.
         //!@{
         bool ReadKabschOptions(std::fstream& arInp, std::string& s);

         bool ReadClusterCompare(std::fstream& arInp, std::string& s);
         bool ReadFilterUniqueClusters(std::fstream& arInp, std::string& s);
         bool ReadCheckSolvation(std::fstream& arInp, std::string& s);
         std::pair<bool, std::tuple<std::filesystem::path, std::string, std::string, bool>>         ReadComputationalData(std::fstream& arInp, std::string& s);
         bool ReadComputationalMethod
            (  std::fstream& arInp
            ,  std::string& s
            ,  std::tuple<std::filesystem::path, std::string, std::string, bool>& arTup
            );
         //!@}

         //!@{ SETTERS
         void SetKabschInputOptions(const Structure::KabschInputOptions aOption) { mKabschInputOptions = aOption; }
         //!@}

         //! Gives path to where output files should be written
         std::filesystem::path GetOutputDataDir() const { return mOutputDataDir; }
         void SetOutputDataDir(const std::filesystem::path& arPath) { mOutputDataDir = arPath; }

         //! Initialize mClusters member.
         void InitializeClusterCompareStructures(const bool aUseClusterIdx);

         //! Getter for InputOptions
         const std::map<std::string, InputOptions>& GetInputoptions() const { return mInputOptions; }

         //! Checks whether conformations from different comp methods are the same.
         void CheckConformersAcrossMethods(const std::filesystem::path& arWorkingDir);
         
         //! Filter unique clusters from within a single method
         void FilterUniqueClusters(const std::filesystem::path& arWorkingDir);

         //!
         void CheckSolvation(const std::filesystem::path& arWorkingDir);

         //! Calculates RMSD of all clusters
         std::map<std::string, Kabsch::RmsdData> CalculateConformationRMSD
            (
            )  const;

         std::map<std::string, Kabsch::RmsdData> CalculateConformationAngleDeviation
            (
            )  const;

         std::map<std::string, Kabsch::RmsdData> CalculateConformationCoordinateDeviation
            (
            )  const;

         //! Creates and saves all the cluters in mFilePaths.GetDataDir() to the mClusters map.
         void InitializeCoordinateScan();
         //! 
         void RunCoordinateScan1D() const;

         //!
         void RunEqDistances() const;

         //! Sanity check functions
         void SanityCheckCompareClustersAcrossMethods() const;

         //! Iterators
         auto CompareClustersBegin() const { return mCompareClusters.cbegin(); }
         auto CompareClustersEnd()   const { return mCompareClusters.cend(); }

      private:
         //! InitializeMonomers()
         void InitializeMonomers();
         void InitializeClusters();

         //! Save a given cluster
         void SaveCluster
            (  const std::filesystem::path& arPath
            ,  const std::string& arCompMethod
            ,  const std::string& arBasisSet
            ,  const bool aIsReference  
            ,  const bool aUseClusterIdx
            );

         //!
         std::string ReadClusterName
            (  const std::filesystem::path& arFilePath
            ,  const bool aUseClusterIdx = true
            )  const;
         
         //!
         std::string InterpretStringAsClusterName
            (  std::string arString
            ,  const bool aUseClusterIdx = true
            )  const;

         //! Will update the ConformerEnum object of Cluster objects in mClusters
         void ScreenForProtonTransfer
            (
            );

         //! Will update the ConformerEnum object of Cluster objects in mClusters
         void ScreenForDifferingMonomers
            (
            );
         
         //! Untility
         void WriteUniqueClustersToFile
            (  const std::string& arClusterType 
            ,  const std::vector<std::pair<const Cluster&, const Cluster&>>& arClusterPairs
            ,  const std::vector<ConformerEnum>& arConformerEnums
            ,  const std::filesystem::path& arWorkingDir
            )  const;

         //! 
         void PrintClusterSolvationShellsForPlot
            (  const std::map<std::string, std::vector<Cluster>>& arClusters
            ,  const std::filesystem::path& arWorkingDir
            )  const;

      private:
         //! Set of all Cluster's we have in our data set.
         std::map<std::string, std::vector<Cluster>> mCompareClusters;
         std::map<std::string, Cluster> mReferenceClusters;

         //!
         std::vector<std::tuple<std::filesystem::path, std::string, std::string, bool>> mCompareClusterInput;

         //!
         std::map<std::string, Structure::InputOptions> mInputOptions = 
         {  {"KabschHydrogen", Structure::InputOptions::ALL_ALL} // Calc from all and apply to all by default.
         };

         //!
         Structure::KabschInputOptions mKabschInputOptions = Structure::KabschInputOptions::COMPARE_INDIVIDUAL_ATOMS;

         //! Bools to signify where to read the ClusterName.
         bool mClusterNameInFilename = false;
         bool mClusterNameInComment = false;
         
         // A string which should be ignored if it is found the the clustername
         std::string mStringToIgnoreInFilename;

         //! Path to where output data should be written
         std::filesystem::path mOutputDataDir;
   };
}

#endif /* CLUSTER_SET_HPP */