/********************************************************************
 * Containing all relevant Enums and getters for these.
 ********************************************************************/

#ifndef STRUCTUREENUMS_HPP
#define STRUCTUREENUMS_HPP

#include<string>
#include<iostream>
#include<map>

#include "MathAndUtil/Util.hpp"

namespace Structure
{
   // Define InputOptions enum class
   enum class InputOptions
   {  ERROR
   ,  ALL_ALL                    // Calc rot mat from all atoms and apply to all
   ,  NON_HYDROGEN_ALL           // Calc rot mat only from non-hydrogen atoms and apply to all
   ,  NON_HYDROGEN_NON_HYDROGEN  // Calc rot mat only from non-hydrogen atoms and apply only to non-hydrogen atoms.
   };

   enum class KabschInputOptions
   {  COMPARE_MONOMER_STRUCTURES // COMPARES rotated monomer structures directly
   ,  COMPARE_CLUSTER_STRUCTURES // Compares (RMSD) rotated cluster structures directly.
   ,  COMPARE_MONOMER_ROT_MATS_DIRECTLY  // Compares (RMSD) rotation matrices instead of rotated structures.
   ,  COMPARE_MONOMER_ROT_MATS_ACROSS    // Compares rotation matrices by rotating the other monomers and computing rmsd of these monomer structures, for all monomers. (Hopefully to generate a lot of data for normal distribution stuff.)
   ,  COMPARE_INDIVIDUAL_ATOMS // Compute a deviation value for each atom based on rot mat of entire cluster.
   };

   /********************************************************************
    * Define AtomName enum class. As the underlying type is int, order
    * is important so operator< behaves as expected.
    * 
    * Defines masses for each type of atom. Rounded to nearest int.
    ********************************************************************/
   enum class AtomName : int
   {  H  = 1
   ,  HE = 2
   ,  LI = 3
   ,  BE = 4
   ,  B  = 5
   ,  C  = 6
   ,  N  = 7
   ,  O  = 8
   ,  F  = 9
   ,  NE = 10
   ,  NA = 11
   ,  MG = 12
   ,  AL = 13
   ,  SI = 14
   ,  P  = 15
   ,  S  = 16
   ,  CL = 17
   ,  AR = 18
   ,  A // Dummy atom A used for tests and similar !
   };

   //! Helper function for computing approximate mass of atoms
   inline int GetMass(const AtomName aName)
   {
      if (aName == AtomName::H)
      {
         return std::underlying_type_t<AtomName>(aName);
      }
      else
      {
         return 2 * std::underlying_type_t<AtomName>(aName);
      }
   }

   //! Helper function to get the underlying integer
   inline int GetNuclearCharge(const AtomName aName)
   {
      return std::underlying_type_t<AtomName>(aName);
   }

   /********************************************************************
    *
    ********************************************************************/
   enum class ConformerEnum
   {  UNINITIALIZED
   ,  DefinitelyTheSameAsReference
   ,  SameAsReference
   ,  DifferentFromReference
   ,  ProtonTransfer
   ,  REFERENCE
   };

   /********************************************************************
    * Namespace here to contain maps between string and enums.
    ********************************************************************/
   namespace EnumMaps
   {
      const static inline std::map<std::string, InputOptions> 
      InputOptionsMap = 
         {  {"ALL_ALL", InputOptions::ALL_ALL}
         ,  {"NON_HYDROGEN_ALL", InputOptions::NON_HYDROGEN_ALL}
         ,  {"NON_HYDROGEN_NON_HYDROGEN", InputOptions::NON_HYDROGEN_NON_HYDROGEN}
         };

      const static inline std::map<std::string, KabschInputOptions>
      KabschInputOptionsMap = 
         {  {"COMPARE_CLUSTER_STRUCTURES", KabschInputOptions::COMPARE_CLUSTER_STRUCTURES}
         ,  {"COMPARE_MONOMER_STRUCTURES", KabschInputOptions::COMPARE_MONOMER_STRUCTURES}
         ,  {"COMPARE_MONOMER_ROT_MATS_DIRECTLY", KabschInputOptions::COMPARE_MONOMER_ROT_MATS_DIRECTLY}
         ,  {"COMPARE_MONOMER_ROT_MATS_ACROSS", KabschInputOptions::COMPARE_MONOMER_ROT_MATS_ACROSS}
         ,  {"COMPARE_INDIVIDUAL_ATOMS", KabschInputOptions::COMPARE_INDIVIDUAL_ATOMS}
         };
      
      const static inline std::map<std::string, AtomName>
      AtomNameMap =
         {  {"H", AtomName::H}
         ,  {"He", AtomName::HE}
         ,  {"Li", AtomName::LI}
         ,  {"Be", AtomName::BE}
         ,  {"B", AtomName::B}
         ,  {"C", AtomName::C}
         ,  {"N", AtomName::N}
         ,  {"O", AtomName::O}
         ,  {"F", AtomName::F}
         ,  {"Ne", AtomName::NE}
         ,  {"Na", AtomName::NA}
         ,  {"Mg", AtomName::MG}
         ,  {"Al", AtomName::AL}
         ,  {"Si", AtomName::SI}
         ,  {"P", AtomName::P}
         ,  {"S", AtomName::S}
         ,  {"Cl", AtomName::CL}
         ,  {"Ar", AtomName::AR}
         ,  {"A", AtomName::A} // Dummy atom A used for tests and similar !
         };

      const static inline std::map<std::string, ConformerEnum>
      ConformerEnumMap = 
         {  {"UNINITIALIZED", ConformerEnum::UNINITIALIZED}
         ,  {"DefinitelyTheSameAsReference", ConformerEnum::DefinitelyTheSameAsReference}
         ,  {"SameAsReference", ConformerEnum::SameAsReference}
         ,  {"DifferentFromReference", ConformerEnum::DifferentFromReference}
         ,  {"ProtonTransfer", ConformerEnum::ProtonTransfer}
         ,  {"REFERENCE", ConformerEnum::REFERENCE}
         };
   }

   /********************************************************************
    * Section to contain "map getters"
    ********************************************************************/
   //!@{
   template<class ENUM>
   const std::map<std::string, ENUM>& GetEnumMap() {std::cerr << "Should not call this !" << std::endl; exit(1);}

   template<> inline const std::map<std::string, InputOptions>& GetEnumMap() { return EnumMaps::InputOptionsMap;}
   template<> inline const std::map<std::string, KabschInputOptions>& GetEnumMap() { return EnumMaps::KabschInputOptionsMap;}
   template<> inline const std::map<std::string, AtomName>& GetEnumMap() { return EnumMaps::AtomNameMap;}
   template<> inline const std::map<std::string, ConformerEnum>& GetEnumMap() { return EnumMaps::ConformerEnumMap;}
   //!@}


   /********************************************************************
    * Return enum from enum class based on input string.
    ********************************************************************/
   template<class ENUM>
   ENUM GetEnumFromString(const std::string& arInputString)
   {
      try
      {
         if constexpr (std::is_same_v<ENUM, AtomName>)
         {
            return GetEnumMap<ENUM>().at(::Util::TrimAtomName(arInputString));
         }
         else
         {
            return GetEnumMap<ENUM>().at(arInputString);
         }
      }
      catch(const std::out_of_range& e)
      {
         std::cerr << "Could not find " << arInputString << " in enum map !\n" << e.what() << '\n';
      }

      // If string not found we stop !
      exit(1);
   }

   /********************************************************************
    * Return enum from enum class based on input string.
    * 
    * Should not be used extensively scaled linearly with number
    * of elements in ENUM.
    ********************************************************************/
   template<class ENUM>
   std::string GetStringFromEnum(const ENUM& arInputEnum)
   {
      for (const auto& [string_name, enum_name] : GetEnumMap<ENUM>())
      {
         if (enum_name == arInputEnum)
         {
            return string_name;
         }
      }

      std::cerr << "I did not find any string matching the input enum !" << std::endl;

      // If string not found we stop !
      exit(1);
   }
}

#endif /* STRUCTUREENUMS_HPP */