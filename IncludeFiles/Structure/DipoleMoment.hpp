/********************************************************************
 * 
 ********************************************************************/

#ifndef DIPOLEMOMENT_HPP
#define DIPOLEMOMENT_HPP

#include<vector>
#include "Eigen_wrapper.hpp"

// Forward declares
namespace Structure
{
   enum class AtomName;
}

namespace Structure
{
   /********************************************************************
    * Class for computing and storing the dipolemoment vector
    ********************************************************************/
   class DipoleMoment
   {
      public:
         //!@{ Constructors
         DipoleMoment() = default;
         DipoleMoment
            (  const std::vector<AtomName>& arAtomNames
            ,  const Eigen::MatrixX3d& arAtomPositions
            ,  const std::map<Uin, std::vector<Uin>>& arConnectivityMap
            );
         //!@}

         //!@{ Getters
         const Eigen::RowVector3d& GetDipoleMoment() const { return mDipoleMoment; }
         bool HaveDipole() const { return mHaveDipole; }
         //!@}

      private:
         //! The actual computed dipole moment
         Eigen::RowVector3d mDipoleMoment;

         //! Is the dipole large enough to be significant or does it belong to point group where it is zero.
         bool mHaveDipole = true;

         //! Method to compute the dipole moment.
         Eigen::RowVector3d ComputeDipoleMoment
            (  const std::vector<AtomName>& arAtomNames
            ,  const Eigen::MatrixX3d& arAtomPositions
            ,  const std::map<Uin, std::vector<Uin>>& arConnectivityMap
            );
   };
}

#endif /* DIPOLEMOMENT_HPP */