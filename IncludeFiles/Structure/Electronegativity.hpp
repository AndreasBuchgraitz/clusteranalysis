/********************************************************************
 * File to contain the Electronegativity map
 ********************************************************************/

#ifndef ELECTRONEGATIVITY_HPP
#define ELECTRONEGATIVITY_HPP

#include<cstddef>

namespace Structure
{
   enum class AtomName;
}

/********************************************************************
 * Define namespace instead of class as everything would be static.
 ********************************************************************/
namespace Structure::Electronegativity
{
   //
   double GetElectronegativity(const int aAtomIdx);
   //
   double GetElectronegativity(const Structure::AtomName aAtomName);

   //
   std::size_t GetNumElectronsInOuterShell(const int aAtomIdx);
   //
   std::size_t GetNumElectronsInOuterShell(const Structure::AtomName aAtomName);
}

#endif /* ELECTRONEGATIVITY_HPP */