#ifndef SOLVATIONSHELLS_HPP
#define SOLVATIONSHELLS_HPP

#include<map>
#include<vector>
#include<string>
#include<filesystem>
#include "Eigen_wrapper.hpp"

// Forward declarations
namespace Structure
{
   class Molecule;
}

namespace Structure
{
   /********************************************************************
    * Class to describe and compute the solvation shells for a cluster
    * using ConvecHulls
    ********************************************************************/
   class SolvationShells
   {
      public:
         //! Default c-tor
         SolvationShells() = default;
         //!
         SolvationShells(const std::vector<Molecule>& arMonomers);

         //!@{
         const auto GetNumSolvationShells() const { return mShells.size(); }
         const auto& GetShells() const { return mShells; }
         //!@}

         void PrintSolvationToFile
            (  const std::filesystem::path& arWorkingDir
            ,  const std::string& arStructureName
            )  const;

      private:
         std::vector<std::string> ComputeCurrentShell
            (  const std::map<std::string, Eigen::RowVector3d>& arComs
            ,  const std::map<std::string, double>& arMolecularRadii
            )  const;

      private:
         //! Key: shell number (counting from outward -> inside). Value: Cluster monomer names
         std::map<int, std::vector<std::string>> mShells;

         //!
         std::map<std::string, Eigen::RowVector3d> mCenterOfMasses;
   };

   //!
   std::ostream& operator<<(std::ostream& arOut, const SolvationShells& arSolvationShells);
}



#endif /* SOLVATIONSHELLS_HPP */