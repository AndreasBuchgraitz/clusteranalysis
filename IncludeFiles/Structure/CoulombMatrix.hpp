/********************************************************************
 * 
 ********************************************************************/

#ifndef COULOMBMATRIX_HPP
#define COULOMBMATRIX_HPP

#include<vector>
#include "Eigen_wrapper.hpp"

// Forward declarations
namespace Structure
{
   enum class AtomName;
}

namespace Structure
{
   class CoulombMatrix
   {
      public:
         //!@{ Constructors
         CoulombMatrix() = default;
         CoulombMatrix(const std::vector<AtomName>& arAtomNames, const Eigen::MatrixX3d& arAtomPositions);
         //!@}

         //!@{ GETTERS
         const Eigen::MatrixXd& GetCoulombMatrix() const { return mMatrix; }
         //!@}

         //!@{ Inquiries
         double Norm() const { return mMatrix.norm(); }
         //!@}

         //!@{ Computations
         Eigen::MatrixXd ComputeCoulombMatrix(const std::vector<Uin>& arAtomIdx) const;
         //!@}

      private:
         std::vector<AtomName> mAtomNames;
         Eigen::MatrixX3d mAtomPositions;
         Eigen::MatrixXd mMatrix;
   };
}

#endif /* COULOMBMATRIX_HPP */