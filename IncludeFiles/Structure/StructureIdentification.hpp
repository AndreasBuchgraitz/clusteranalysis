#ifndef STRUCTUREIDENTIFICATION_HPP
#define STRUCTUREIDENTIFICATION_HPP

#include<vector>
#include<tuple>
#include<string>
#include<map>

#include "CommonAliases.hpp"

namespace Structure
{
   /********************************************************************
    * Small POD class containing information which makes a structure 
    * unique.
    * 
    * Operator< is defined to allow insertion in std::map<>'s as Key's.
    ********************************************************************/
   class StructureIdentification
   {
      public:
         StructureIdentification
            (
            )
            :  mStructureName("THIS IS GARBAGE !")
            ,  mCompMethod("THIS IS GARBAGE !")
            ,  mBasisSet("THIS IS GARBAGE !")
            ,  mReferenceMethod(false)
         {
         }

         StructureIdentification
            (  const std::string& arStructureName
            ,  const std::string& arCompMethod
            ,  const std::string& arBasisSet
            ,  const bool arIsReference
            )
            :  mStructureName(arStructureName)
            ,  mCompMethod(arCompMethod)
            ,  mBasisSet(arBasisSet)
            ,  mReferenceMethod(arIsReference)
         {
         }
      
      
         const std::string& GetName() const { return mStructureName; }
         const std::string& GetCompMethod() const { return mCompMethod; }
         const std::string& GetBasisSet() const { return mBasisSet; }
         const bool& GetReferenceMethod() const { return mReferenceMethod; }

         bool operator<(const StructureIdentification& arRhsId) const;
         bool operator==(const StructureIdentification& arRhsId) const;
      
      private:
         std::string mStructureName;
         std::string mCompMethod;
         std::string mBasisSet;
         
         //! Is this id refering to a structure computed with a reference method.
         bool mReferenceMethod;
   };
   std::ostream& operator<<(std::ostream& arOut, const StructureIdentification& arId);
}

#endif /* STRUCTUREIDENTIFICATION_HPP */