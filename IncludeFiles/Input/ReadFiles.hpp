/********************************************************************
 * This file contains functionalities to read files.
 ********************************************************************/

#ifndef READFILES_HPP
#define READFILES_HPP

#include<filesystem>
#include<string>
#include<set>
#include "Eigen_wrapper.hpp"

namespace Input
{
   class InputInfo;
}

namespace Structure
{
   class Molecule;
   class Atom;
   class ClusterSet;
   enum class AtomName;
}

namespace Input
{
   //! Read entire input file and create InputInfo object, which contains all info.
   InputInfo ReadInputFile(const std::filesystem::path& arPathToInputFile);

   //! Remove whitespace and convert to uppercase.
   std::string ParseInputKeyword(const std::string& arString);

   //! Check if line is somesort of keyword
   bool CheckIfKeyword(const std::string& arString);
   
   //! Check if line is lowerlevel keyword
   bool IsLowerLevelKeyword(const std::string& arString, const int aCurrentInputLevel);

   //! Read xyz file and return Molecule
   Structure::Molecule ReadXYZAsMolecule
      (  const std::filesystem::path& arInputxyz
      ,  const std::string& arStructureName
      );
   
   //! Convert line from xyz file into Atom object.
   void AtomFromString
      (  const std::string& arString
      ,  std::vector<Structure::AtomName>& arAtomNames
      ,  std::vector<std::string>& arAtomStringNames
      ,  Eigen::MatrixX3d& arCoordinateMatrix
      );
}

#endif /* READFILES_HPP */