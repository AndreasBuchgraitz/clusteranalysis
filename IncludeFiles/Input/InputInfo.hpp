/********************************************************************
 * Class definition for object containing information from inputfile.
 * Members are simply "CalcDef's" for the different modules.
 ********************************************************************/

#ifndef INPUTINFO_HPP
#define INPUTINFO_HPP

#include<filesystem>
#include<fstream>

#include "Structure/ClusterSet.hpp"

namespace Input
{
   class InputInfo
   {
      public:
         InputInfo() = delete;

         InputInfo(const std::filesystem::path& arPathToInputFile);
         
         //!@{ Cluster conformation/ClusterSet members
         Structure::ClusterSet& GetClusterSetClusterConformation() { return mClusterCompareClusterSet; }
         bool GetDoCheckConformersAcrossMethods() const { return mDoCheckConformersAcrossMethods; }
         //!@}

         //!@{ FilterUniqueClusters
         Structure::ClusterSet& GetClusterSetFilterUniqueClusters() { return mFilterUniqueClusters; }
         bool GetDoFilterUniqueClusters() const { return mDoFilterUniqueClusters; }
         //!@}

         //!@{ CheckSolvation
         Structure::ClusterSet& GetClusterSetCheckSolvation() { return mCheckSolvationClusters; }
         bool GetDoCheckSolvation() const { return mDoCheckSolvation; }
         //!@}

         //!@{ CoordinateScan1D
         const Structure::ClusterSet& GetClusterSetCoordinateScan1D() const { return mCoordinateScan1DClusterSet; }
         bool GetDoCoordinateScan1D() const { return mDoCoordinateScan1D; }
         //!@}

         //!@{ EqDistances
         const Structure::ClusterSet& GetClusterSetEqDistances() const { return mEqDistancesClusterSet; }
         bool GetDoEqDistances() const { return mDoEqDistances; }
         //!@}

      private:
         //! Initialize the different ClusterSets defined below by reading the input file.
         void InitializeClusterSets
            (  std::fstream& arInputFileStream
            ,  std::string& s
            );

         //!@{ ClusterCompareAcrossMethods member
         Structure::ClusterSet mClusterCompareClusterSet;
         bool mDoCheckConformersAcrossMethods = false;
         //!@}

         //!@{ FilterUniqueClusters member
         Structure::ClusterSet mFilterUniqueClusters;
         bool mDoFilterUniqueClusters = false;
         //!@}

         //!@{ CheckSolvation member
         Structure::ClusterSet mCheckSolvationClusters;
         bool mDoCheckSolvation = false;
         //!@}

         //!@{ CoordinateScan1D
         Structure::ClusterSet mCoordinateScan1DClusterSet;
         bool mDoCoordinateScan1D = false;
         //!@}

         //!@{ EqDistances
         Structure::ClusterSet mEqDistancesClusterSet;
         bool mDoEqDistances = false;
         //!@}
   };
}

#endif /* INPUTINFO_HPP */