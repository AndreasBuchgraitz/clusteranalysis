/********************************************************************
 * Contains definitions of several concepts
 ********************************************************************/

#ifndef CONCEPTS_HPP
#define CONCEPTS_HPP

#include<concepts>
#include<iterator>
#include<ranges>
#include<complex>

#include "CommonAliases.hpp"
#include "Eigen_wrapper.hpp"

// Forward declare
namespace Structure
{
   class Molecule;
}

//!@{ Define concepts for iterators
template<typename ITER, typename VALUE_TYPE>
concept IterConcept = std::same_as<typename std::iterator_traits<ITER>::value_type, VALUE_TYPE>;

template<typename ITER>
concept MoleculeIter = IterConcept<ITER, Structure::Molecule>;

template<typename ITER>
concept UinIter = IterConcept<ITER, Uin>;
//!@}

//!@{ Define concepts for (complex) numbers
template<typename NUMBER>
struct is_complex_number : std::false_type {};

template<std::floating_point NUMBER>
struct is_complex_number<std::complex<NUMBER>> : std::true_type {};

template<typename NUMBER>
concept FloatingPoint = std::is_floating_point_v<NUMBER> || is_complex_number<NUMBER>::value;
//!@}

//!@{ Container for FloatingPoint types.
template<typename CONT>
concept FloatingPointContainer = requires(CONT a)
{
   requires std::ranges::common_range<CONT>;
   typename CONT::value_type;
   requires FloatingPoint<typename CONT::value_type>;
   { a.size() };
};
//!@}

//!@{ Concept for classes describing a probability distribution
template<typename DIST, typename CONT>
concept Distribution
   =  FloatingPointContainer<CONT> // CONT needs to be be FloatingPointContainer
   && requires(DIST a)
{
   //! Required member functions
   { a.TheoreticalCDF(typename CONT::value_type()) } -> std::same_as<typename CONT::value_type>;
};
//!@}

//! Concept for iterators over Eigen::RowVectorXd
template<typename ITER>
concept RowVectorIter = requires(ITER a)
{
   typename ITER::value_type;
   std::is_same_v<typename ITER::value_type, Eigen::RowVectorXd>;
};

#endif /* CONCEPTS_HPP */
