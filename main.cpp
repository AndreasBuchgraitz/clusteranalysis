#include<iostream>
#include<iomanip>
#include<numeric>
#include<algorithm>
#include<execution>

#include "MathAndUtil/Kabsch.hpp"
#include "MathAndUtil/Util.hpp"
#include "Structure/Molecule.hpp"
#include "Structure/Cluster.hpp"
#include "Structure/ClusterSet.hpp"
#include "Input/ReadFiles.hpp"
#include "Tests.hpp"
#include "Input/InputInfo.hpp"


int main(int argc, char** argv)
{
   // Outstream setup
   std::cout << std::setprecision(16);
   std::cerr << std::setprecision(16);

   // Should we run the tests.
   bool DoTestRun = false;

   // Path to input file
   std::filesystem::path path_to_input;

   // Get abs path to input file and check if we should run tests.
   for (int i = 1; i < argc; i++)
   {
      std::cout << argv[i] << std::endl;

      if (Util::ConvertToUpperCase(std::string(argv[i])) == "RUNTESTS")
      {
         DoTestRun = true;
      }
      else
      {
         std::filesystem::path input_path(argv[i]);
         
         // If relative assume input file is in current directory
         if (input_path.is_relative())
         {
            path_to_input = std::filesystem::current_path() / input_path.filename();
         }
         else
         {
            path_to_input = input_path;
         }
      }
   }

   if (DoTestRun)
   {
      std::cout << "Will run all tests now." << std::endl;
      Test::RunTests();
   }
   else if  (  path_to_input.string().empty()
            || !path_to_input.has_root_path()
            || !path_to_input.has_filename()
            || !path_to_input.has_stem()
            || !path_to_input.has_extension()
            )
   {
      std::cout << "No path to input file given." << std::endl;
      exit(1);
   }
   else
   {
      if (path_to_input.extension() != ".inp")
      {
         std::cout << "I expect input file to have extension \".inp\"" << std::endl;
         exit(1);
      }

      Input::InputInfo input_info(path_to_input);

      if (input_info.GetDoCheckConformersAcrossMethods())
      {
         input_info.GetClusterSetClusterConformation().CheckConformersAcrossMethods(std::filesystem::current_path());
      }
      if (input_info.GetDoFilterUniqueClusters())
      {
         input_info.GetClusterSetFilterUniqueClusters().FilterUniqueClusters(std::filesystem::current_path());
      }
      if (input_info.GetDoCheckSolvation())
      {
         input_info.GetClusterSetCheckSolvation().CheckSolvation(std::filesystem::current_path());
      }
      if (input_info.GetDoCoordinateScan1D())
      {
         input_info.GetClusterSetCoordinateScan1D().RunCoordinateScan1D();
      }
      if (input_info.GetDoEqDistances())
      {
         input_info.GetClusterSetEqDistances().RunEqDistances();
      }
   }

   return 0;
}